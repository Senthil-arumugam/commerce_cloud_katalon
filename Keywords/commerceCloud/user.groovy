package commerceCloud
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.util.KeywordUtil

import internal.GlobalVariable

//getLoginName(String name)
//login()
//logout()

class user {


	/**
	 * GET LOGIN NAME
	 * @param teamType - is the name of the team to target... send null if using system created Date or specify prefix in global variables
	 * @return username
	 */
	@Keyword
	def String getLoginName(String name) {

		String strUserName = ""
		if(!(name.equalsIgnoreCase("agent"))){

			strUserName = GlobalVariable.GlobalUserName
			KeywordUtil.logInfo("UserName = " + strUserName)
		}else{

			KeywordUtil.logInfo("UserName = " + strUserName)
			strUserName = GlobalVariable.GlobalUserName
		}

		return strUserName.toLowerCase()
	}



	/**
	 * LOGIN USER
	 * @return true if login success, otherwise false
	 */
	@Keyword
	def login() {

		String userName = ""
		userName = (new commerceCloud.Cart()).getUserName("").trim()
		userName = userName.toLowerCase()

		KeywordUtil.logInfo("Log in process trigger under commerce Cloud application")

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Login/Sign In or Register"),60)) {
			KeywordUtil.logInfo("Cannot see Sign In or Register")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Login/Sign In or Register"))) {
			KeywordUtil.logInfo("Cannot click Sign In or Register")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Login/User Name"),60)) {
			KeywordUtil.logInfo("Cannot see User Name")
			return false
		}

		//Enter the user name or password do login
		KeywordUtil.logInfo("Logging in user name : "  + userName)
		if (!(new commerceCloud.utils()).sendKeys(findTestObject("Login/User Name"),userName)) {
			KeywordUtil.logInfo("Cannot enter User Name")
			return false
		}

		KeywordUtil.logInfo("Password : " + GlobalVariable.GlobalPassword)
		if (!(new commerceCloud.utils()).sendKeys(findTestObject("Login/Password"),GlobalVariable.GlobalPassword)) {
			KeywordUtil.logInfo("Cannot enter Password")
			return false
		}
		if (!(new commerceCloud.utils()).click(findTestObject("Login/Log In"))) {
			KeywordUtil.logInfo("Cannot click Log In")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Login/Signed In As"),60)) {
			KeywordUtil.logInfo("Cannot see Signed In As")
			return false
		}
		KeywordUtil.logInfo("User successfully log in the commerce Cloud application")
		return true

	}


	/**
	 * loginWithUserName(String strUserName) 
	 * @return true if login success, otherwise false
	 */
	@Keyword
	def loginWithUserName(String strUserName, String strRolePassword) {

		String userName = ""
		userName = strUserName.toLowerCase().trim()

		KeywordUtil.logInfo("Log in process trigger under commerce Cloud application")

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Login/Sign In or Register"),60)) {
			KeywordUtil.logInfo("Cannot see Sign In or Register")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Login/Sign In or Register"))) {
			KeywordUtil.logInfo("Cannot click Sign In or Register")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Login/User Name"),60)) {
			KeywordUtil.logInfo("Cannot see User Name")
			return false
		}

		//Enter the user name or password do login
		KeywordUtil.logInfo("Logging in user name : "  + userName)
		if (!(new commerceCloud.utils()).sendKeys(findTestObject("Login/User Name"),userName)) {
			KeywordUtil.logInfo("Cannot enter User Name")
			return false
		}

		KeywordUtil.logInfo("Password : " + strRolePassword)
		if (!(new commerceCloud.utils()).sendKeys(findTestObject("Login/Password"),strRolePassword)) {
			KeywordUtil.logInfo("Cannot enter Password")
			return false
		}
		if (!(new commerceCloud.utils()).click(findTestObject("Login/Log In"))) {
			KeywordUtil.logInfo("Cannot click Log In")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Login/Signed In As"),60)) {
			KeywordUtil.logInfo("Cannot see Signed In As")
			return false
		}
		KeywordUtil.logInfo("User successfully log in the commerce Cloud application")
		return true

	}


	/**
	 * logout
	 * @return true if logout completed, otherwise false
	 */

	@Keyword
	def logout() {

		KeywordUtil.logInfo("Logout operation is triggered")

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Login/Signed In As"),60)) {
			KeywordUtil.logInfo("Cannot see Signed In As")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Login/Signed In As"))) {
			KeywordUtil.logInfo("Cannot click Signed In As")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Login/Log Out"),60)) {
			KeywordUtil.logInfo("Cannot see Log Out")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Login/Log Out"))) {
			KeywordUtil.logInfo("Cannot click Log Out")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Login/Log In"),60)) {
			KeywordUtil.logInfo("Cannot see Log In")
			return false
		}

		if (!(new commerceCloud.utils()).closeSite()) {
			KeywordUtil.logInfo("Cannot close the application")
			return false
		}

		KeywordUtil.logInfo("Successfully logout the commerce cloud application")
		return true
	}

}
