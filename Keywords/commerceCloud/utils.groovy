package commerceCloud
import java.text.NumberFormat as NumberFormat

import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.ui.Select

import com.itextpdf.text.pdf.PdfReader
import com.itextpdf.text.pdf.parser.PdfTextExtractor
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.SelectorMethod
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.testobject.TestObjectProperty
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.exception.WebElementNotFoundException
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable as GlobalVariable

//acceptAlert()
//verifyElementIsDisable(TestObject to, int iWaitTime = null)
//verifyElementIsEnabled(TestObject to, int iWaitTime = null)
//writeToCSVFile(def csvPath = null, def partsName = null, def qualitySize = null)
//ReadCSVFile(String strFilePath, int iRow)
//ReadPDFFile(String strFilePath, String strExpectedValue)
//ReadExcelFile(String strFilePath, String strExpText)
//compareListSize(TestObject to, def expValues)
//WriteExcelFile(String strFilePath, String strSheetName, def strSheetOneData, def strSheetTwoData)
//takeScreenShot()
//sendKeys(TestObject to, String keys)
//getURL()
//sendKeysOutWait(TestObject to, String keys)
//waitForPageLoad(int time)
//clearsendKeys(TestObject to, String keys)
//delay(int time)
//getCSSValue(TestObject to, String strValues, String strExpectedValues)
//click(TestObject to)
//CompareListWebElement(TestObject to, def expValues)
//getText(TestObject to,String strName)
//sReturnText(TestObject to)
//GetAttributeText(TestObject to,String strAttribute)
//selectDropDownValue(TestObject to, String labelName)
//scrollToElement(TestObject to)
//verifyPresent(TestObject to, int timeout)
//verifyVisibleOfElement(TestObject to, int timeout)
//verifyElementDidNotDisplayed(String strXPath = null)
//verifyElementIsDisplayed(TestObject to, int iWaitTime = null)
//refresh()
//verifyElementNotPresent(TestObject to, int timeout)
//verifyElementChecked(TestObject to)
//String getName()
//newSession()
//openSite()
//closeSite()
//advancePagination(int PageNum)
//ListOfWebElement(TestObject to, def expValues)
//sorting(String sortByNameOrDate, String sortType)
//getFirstSelectedOption(TestObject to, String strSelectValues = null)


class utils {


	/**
	 * @author Senthil.Arumugam
	 * writeToCSVFile(String filePath, String ProjectName, String DeliveryDate, String Summary,String Value)
	 * https://www.logicbig.com/tutorials/misc/groovy/data-type-and-variables.html
	 * @param to CSV filePath, ProjectName, DeliveryDate, Summary and Value
	 * @return True if CSV file is write successfully otherwise will return false  
	 */
	@Keyword
	def writeToCSVFile(def csvPath = null, def partsName = null, def qualitySize = null) {

		try {
			def list = Arrays.asList(Arrays.asList(partsName, qualitySize))

			FileWriter csvWriter = new FileWriter(csvPath)

			csvWriter.append('Sku')

			csvWriter.append(',')

			csvWriter.append('Quantity')

			csvWriter.append('\n')

			for (def listData : list) {
				csvWriter.append(String.join(',', listData))

				csvWriter.append('\n')
			}

			csvWriter.flush()

			csvWriter.close()
			return true
		} catch (Exception e) {
			println e
			KeywordUtil.logInfo("Failed to update CSV file path : " + csvPath)
			return false
		}
		return false
	}

	/**
	 * @author Senthil.Arumugam
	 * ReadCSVFile(String strFilePath, int iRow)
	 * @param strFilePath and iRow
	 * @return True if CSV file is read specific line successfully otherwise will return false
	 */
	@Keyword
	def ReadCSVFile(String strFilePath, int iRow) {
		try {
			ArrayList<List> records = new ArrayList()
			BufferedReader br = new BufferedReader(new FileReader(strFilePath))
			String line

			while ((line = br.readLine()) != null) {

				String[] values = line.split(',')

				records.add(Arrays.asList(values))
			}
			br.close()

			return records.get(iRow)
		} catch (Exception e) {

			KeywordUtil.logInfo("Can not read the csv file : " + strFilePath)
			return 0
		}
		return 0
	}


	/**
	 * @author Senthil.Arumugam
	 * ReadPDFFile(String strFilePath, String strExpectedValue)
	 * @param strFilePath and strExpectedValue
	 * @return True if PDF file is read and compared to specific values otherwise will return false
	 */
	@Keyword
	def ReadPDFFile(String strFilePath, String strExpectedValue) {

		Boolean blnValues = false

		try {
			//Create PdfReader instance.
			PdfReader pdfReader = new PdfReader(strFilePath)

			//Get the number of pages in pdf.
			int pages = pdfReader.getNumberOfPages()

			//Iterate the pdf through pages.
			for (int i = 1; i <= pages; i++) {
				//Extract the page content using PdfTextExtractor.
				String pageContent = PdfTextExtractor.getTextFromPage(pdfReader, i)

				if (pageContent.contains(strExpectedValue)) {
					blnValues = true
				}
				//Print the page content on console.
				KeywordUtil.logInfo("Content on Page " + i + ": " + pageContent)
			}

			//Close the PdfReader.
			pdfReader.close()

		} catch (Exception e) {

			KeywordUtil.logInfo("Can not read the PDF file : " + strFilePath)
			return blnValues
		}
		return blnValues
	}

	/**
	 * @author Senthil.Arumugam
	 * ReadExcelFile(String strFilePath, String strExpText)
	 * @param strFilePath and strExpText
	 * @return True if Excel file is read and compared to specific values otherwise will return false
	 */
	@Keyword
	def ReadExcelFile(String strFilePath, String strExpText) {

		Boolean blnValues = false

		try {

			FileInputStream fis = new FileInputStream(strFilePath)

			XSSFWorkbook workbook = new XSSFWorkbook(fis)

			XSSFSheet sheet = workbook.getSheetAt(0)

			int rowsCount = sheet.getLastRowNum()

			KeywordUtil.logInfo("Total Number of Rows : " +  rowsCount + 1 )

			for (int i = 0; i <= rowsCount; i++) {
				Row row = sheet.getRow(i)

				int colCounts = row.getLastCellNum()

				KeywordUtil.logInfo("Total Number of Cols : " + colCounts)

				for (int j = 0; j < colCounts; j++) {
					Cell cell = row.getCell(j)

					if(cell.getStringCellValue().contains(strExpText)){
						blnValues = true
					}
				}
			}

			return blnValues
		} catch (Exception e) {
			KeywordUtil.logInfo("Can not read the csv file : " + strFilePath)

			return blnValues
		}

		return blnValues
	}


	/**
	 * @author Senthil.Arumugam
	 * WriteExcelFile(String strFilePath, String strSheetName, def strSheetOneData, def strSheetTwoData) 
	 * @param strFilePath and strSheetName and strSheetOneData and strSheetTwoData
	 * @return True if Excel file is write two sheets otherwise will return false
	 */
	@Keyword
	def WriteExcelFile(String strFilePath, String strSheetName, def strSheetOneData, def strSheetTwoData) {

		Boolean blnValues = false

		def i = 0

		XSSFWorkbook workbook = new XSSFWorkbook()

		String[] str = strSheetName.split('##')

		for (String values : str) {
			i = (i + 1)

			def datatypes

			XSSFSheet sheet = workbook.createSheet(values)

			if (i == 1) {
				datatypes = strSheetOneData
			} else {
				datatypes = strSheetTwoData
			}

			int rowNum = 0

			KeywordUtil.logInfo("Creating excel")

			for (Object[] datatype : datatypes) {
				Row row = sheet.createRow(rowNum++)

				int colNum = 0

				for (Object field : datatype) {
					Cell cell = row.createCell(colNum++)

					if (field instanceof String) {
						cell.setCellValue(((field) as String))
					} else if (field instanceof Integer) {
						cell.setCellValue(((field) as Integer))
					}
					blnValues = true
				}
			}
		}

		try {
			FileOutputStream outputStream = new FileOutputStream(strFilePath)

			workbook.write(outputStream)

			workbook.close()
		}
		catch (FileNotFoundException e) {
			KeywordUtil.logInfo(e.printStackTrace())
		}
		catch (IOException e) {
			KeywordUtil.logInfo(e.printStackTrace())
		}

		KeywordUtil.logInfo("Excel sheet is created")

		return blnValues
	}


	/**
	 * @author Senthil.Arumugam
	 * takeScreenShot()
	 * @return true if script take a screenshot otherwise false
	 */


	@Keyword
	def takeScreenShot() {
		String	path=""
		try {
			def pool = [('a'..'z'), ('A'..'Z'), (0..9), '-'].flatten()

			Random random = new Random(System.currentTimeMillis())

			def randomChars = (0..30 - 1).collect({
				pool[random.nextInt(pool.size())]
			})
			println(randomChars)
			def randomString = randomChars.join()
			path = System.getProperty("user.dir")+'\\Reports\\ScreenShot\\' + randomString+'.png'
			WebUI.takeScreenshot(path)
			return true
		} catch (Exception e) {
			KeywordUtil.logInfo("cannot take screenshot path is : " + path)
			return false
		}
		return false
	}



	/**
	 * @author Senthil.Arumugam
	 * sendKeys(TestObject to, String keys)
	 * @param TestObject and keys
	 * @return True if values are entering into specific object otherwise will return false
	 */	
	@Keyword
	def sendKeys(TestObject to, String keys) {
		try {
			if(verifyVisibleOfElement(to,1)){
				WebUI.sendKeys(to, keys)
				return true
			}
		} catch (Exception e) {
			KeywordUtil.logInfo("Failed to sendkeys without clear to object: " + to.getObjectId())
			return false
		}
		return false
	}

	/**
	 * @author Senthil.Arumugam
	 * setText(TestObject to, String keys)
	 * @param TestObject and keys
	 * @return True if values are entering into specific object otherwise will return false
	 */
	@Keyword
	def setText(TestObject to, String keys) {
		try {
			if(verifyVisibleOfElement(to,1)){
				WebUI.clearText(to)
				WebUI.setText(to, keys)
				return true
			}
		} catch (Exception e) {
			KeywordUtil.logInfo("Failed to sendkeys without clear to object: " + to.getObjectId())
			return false
		}
		return false
	}


	/**
	 * @author Senthil.Arumugam
	 * acceptAlert()
	 * @return True if alert is accept otherwise will return false
	 */
	@Keyword
	def acceptAlert() {
		try {
			WebUI.acceptAlert()
			return true
		} catch (Exception e) {
			KeywordUtil.logInfo("Failed to Accept Alert")
			return false
		}
		return false
	}



	/**
	 * @author Senthil.Arumugam
	 * getURL() 
	 * @return True if URL is available on application otherwise will return false
	 */
	@Keyword
	def getURL() {
		try {

			return  WebUI.getUrl()

		} catch (Exception e) {
			KeywordUtil.logInfo("Failed to get current URL on page")
			return ""
		}
		return ""
	}


	/**
	 * @author Senthil.Arumugam
	 * sendKeysOutWait(TestObject to, String keys)
	 * @param TestObject and keys
	 * @return True if values are entering into specific object otherwise will return false
	 */
	@Keyword
	def sendKeysOutWait(TestObject to, String keys) {
		try {
			WebUI.sendKeys(to, keys)
			return true
		} catch (Exception e) {
			KeywordUtil.logInfo("Failed to sendkeys waithout wait : " + to.getObjectId())
			return false
		}
		return false
	}



	/**
	 * @author Senthil.Arumugam
	 * waitForPageLoad(int time)
	 * @param time
	 * @return True if page loaded within expected time otherwise will return false
	 */
	@Keyword
	def waitForPageLoad(int time) {
		try {
			WebUI.waitForPageLoad(time)
			return true
		} catch (Exception e) {
			KeywordUtil.logInfo("Failed to load a page")
			return false
		}
		return false
	}

	/**
	 * @author Senthil.Arumugam
	 * clearsendKeys(TestObject to, String keys)
	 * @param TestObject and keys
	 * @return True if values are entering into specific object after clearing old values otherwise will return false
	 */
	@Keyword
	def clearsendKeys(TestObject to, String keys) {
		try {
			if(verifyVisibleOfElement(to,1)){
				WebElement element = WebUI.findWebElement(to)
				element.clear()
				element.sendKeys(keys)
				return true
			}
		} catch (Exception e) {
			KeywordUtil.logInfo("Failed to sendkeys to object: " + to.getObjectId())
			return false
		}
		return false
	}

	/**
	 * @author Senthil.Arumugam
	 * delay(int time)
	 * @param time
	 * @return True if script is waiting particular time without any expectation otherwise will return false
	 */
	@Keyword
	def delay(int time) {
		try {
			WebUI.delay(time)
			return true
		} catch (Exception e) {
			KeywordUtil.logInfo("Failed to wait a time  " + time)
			return false
		}
		return false
	}

	/**
	 * @author Senthil.Arumugam
	 * getCSSValue(TestObject to, String strValues, String strExpectedValues) 
	 * @param TestObject and strValues and strExpectedValues
	 * @return True if user able to get specific object CSS values otherwise will return false
	 */
	@Keyword
	def getCSSValue(TestObject to, String strValues, String strExpectedValues) {
		try {
			if(verifyVisibleOfElement(to,5)){
				if(WebUI.getCSSValue(to, strValues).trim().equalsIgnoreCase(strExpectedValues)){
					return true
				}
			}
		} catch (Exception e) {
			KeywordUtil.logInfo("Failed to get CSS values object: " + to.getObjectId())
			return false
		}
		return false
	}


	/**
	 * @author Senthil.Arumugam
	 * click(TestObject to)
	 * @param TestObject
	 * @return True if user able to click specific object otherwise will return false
	 */
	@Keyword
	def click(TestObject to) {
		try {
			if(verifyVisibleOfElement(to,10)){
				WebUI.click(to)
				//KeywordUtil.logInfo("click the object and Object name is : " + to.getObjectId())
				return true
			}
		} catch (Exception e) {
			KeywordUtil.logInfo("Failed to click object: " + to.getObjectId())
			return false
		}
		return false
	}

	/**
	 * @author Senthil.Arumugam
	 * mouseOver(TestObject to)
	 * @param TestObject
	 * @return True if user able to mouse Over specific object otherwise will return false
	 */
	@Keyword
	def mouseOver(TestObject to) {
		try {
			if(verifyVisibleOfElement(to,10)){
				WebUI.mouseOver(to)

				return true
			}
		} catch (Exception e) {
			KeywordUtil.logInfo("Failed to mouse Over object: " + to.getObjectId())
			return false
		}
		return false
	}


	/**
	 * @author Senthil.Arumugam
	 * switchToWindowIndex(TestObject to)
	 * @param TestObject
	 * @return True if user able to click specific object otherwise will return false
	 */
	@Keyword
	def switchToWindow(int index) {
		try {
			WebUI.switchToWindowIndex(index)
			return true
		} catch (Exception e) {
			KeywordUtil.logInfo("Failed to switch To Window")
			return false
		}
		return false
	}

	/**
	 * @author Senthil.Arumugam
	 * switchToFrame(TestObject to)
	 * @param TestObject
	 * @return True if Switch to iframe otherwise will return false
	 */
	@Keyword
	def switchToFrame(TestObject to) {
		try {
			WebUI.switchToFrame(to, 10)
			return true
		} catch (Exception e) {
			KeywordUtil.logInfo("Failed to switch To Frame")
			return false
		}
		return false
	}

	/**
	 * @author Senthil.Arumugam
	 * switchToDefaultContent()
	 * @param TestObject
	 * @return True Switch back to current window otherwise will return false
	 */
	@Keyword
	def switchToDefaultContent() {
		try {
			WebUI.switchToDefaultContent()
			return true
		} catch (Exception e) {
			KeywordUtil.logInfo("Failed to switch To Default Content")
			return false
		}
		return false
	}


	/**
	 * @author Senthil.Arumugam
	 * closeWindowIndex(TestObject to)
	 * @param TestObject
	 * @return True if close Window based on passing Index otherwise will return false
	 */
	@Keyword
	def closeWindowIndex(int index) {
		try {
			WebUI.closeWindowIndex(index)
			return true
		} catch (Exception e) {
			KeywordUtil.logInfo("Failed to close Window Index")
			return false
		}
		return false
	}

	/**
	 * @author Senthil.Arumugam
	 * closeWindowIndex(TestObject to)
	 * @param TestObject
	 * @return True if close Window based on passing Index otherwise will return false
	 */
	@Keyword
	int getWindowIndex() {
		try {

			return WebUI.getWindowIndex()

		} catch (Exception e) {
			KeywordUtil.logInfo("Failed to close Window Index")
			return 0
		}
		return 0
	}


	/**
	 * @author Senthil.Arumugam
	 * closeWindowUrl(TestObject to)
	 * @param TestObject
	 * @return True if close Window based on  Url otherwise will return false
	 */
	@Keyword
	def closeWindowUrl(String strURL) {
		try {
			WebUI.closeWindowUrl(strURL)
			return true
		} catch (Exception e) {
			KeywordUtil.logInfo("Failed to close Window URL : " + strURL)
			return false
		}
		return false
	}

	/**
	 * @author Senthil.Arumugam
	 * compareListSize(TestObject to, def expValues)
	 * @param TestObject
	 * @return True if object return a size of values otherwise will return false
	 */
	@Keyword
	def compareListSize(TestObject to, def expValues) {

		boolean blnFlag = false
		ArrayList<WebElement> lst = null

		String strXpathLocator = ""
		Map allSelectors =  to.getSelectorCollection()
		strXpathLocator =  allSelectors.get(SelectorMethod.XPATH)

		try {
			println(strXpathLocator)
			WebDriver driver = DriverFactory.getWebDriver()
			lst = driver.findElements(By.xpath(strXpathLocator))
			def actValues = lst.size()
			KeywordUtil.logInfo("actValues :" +actValues)
			if(actValues == expValues){
				blnFlag = true
			}

			return blnFlag

		} catch (Exception e) {

			KeywordUtil.logInfo("Failed to return list of values object: " + to)
			return blnFlag

		}
		return blnFlag
	}


	/**
	 * @author Senthil.Arumugam
	 * ListOfWebElement(TestObject to, def expValues)
	 * @param TestObject
	 * @return True if object return a size of values otherwise will return false
	 */
	@Keyword
	def ListOfWebElement(TestObject to) {

		boolean blnFlag = false
		ArrayList<WebElement> lst = null

		String strXpathLocator = ""
		Map allSelectors =  to.getSelectorCollection()
		strXpathLocator =  allSelectors.get(SelectorMethod.XPATH)

		try {

			WebDriver driver = DriverFactory.getWebDriver()
			lst = driver.findElements(By.xpath(strXpathLocator))
			return lst

		} catch (Exception e) {

			KeywordUtil.logInfo("Failed to return list of values object: " + to)
			return null

		}
		return null
	}


	/**
	 * @author Senthil.Arumugam
	 * getText(TestObject to,String strName)
	 * @param TestObject and strName
	 * @return True if able to get text for specific test object otherwise will return false
	 */
	@Keyword
	def  getText(TestObject to,String strName) {
		try {
			if(verifyVisibleOfElement(to,10)){
				KeywordUtil.logInfo(WebUI.getText(to))
				if (WebUI.getText(to).trim().equalsIgnoreCase(strName)) {
					return true
				}
			}
		} catch (Exception e) {
			KeywordUtil.logInfo("Failed to get text object: " + to.getObjectId())
			return false
		}
		return false
	}

	/**
	 * @author Senthil.Arumugam
	 * sReturnText(TestObject to)
	 * @param TestObject
	 * @return True if able to get text for specific test object otherwise will return false
	 */
	@Keyword
	def sReturnText(TestObject to) {
		try {
			if(verifyVisibleOfElement(to,10)){
				KeywordUtil.logInfo(WebUI.getText(to))
				return WebUI.getText(to).trim()
			}
		} catch (Exception e) {
			KeywordUtil.logInfo("Failed to get text object: " + to.getObjectId())
			return ""
		}
		return ""
	}

	/**
	 * @author Senthil.Arumugam
	 * GetAttributeText(TestObject to,String strAttribute)
	 * @param TestObject and strAttribute
	 * @return True if able to get attribute for specific test object otherwise will return empty values
	 */
	@Keyword
	String  GetAttributeText(TestObject to,String strAttribute) {
		try {
			if(verifyVisibleOfElement(to,10)){
				return WebUI.getAttribute(to, strAttribute).trim()
			}
		} catch (Exception e) {
			KeywordUtil.logInfo("Failed to get Attribute object: " + to.getObjectId())
			return ""
		}
		return ""
	}

	/**
	 * @author Senthil.Arumugam
	 * selectDropDownValue(TestObject to, String labelName)
	 * @param TestObject and labelName
	 * @return True if able to select dropdown values for specific test object otherwise will return false
	 */
	@Keyword
	def selectDropDownValue(TestObject to, String labelName) {

		try {
			if(verifyVisibleOfElement(to,5)){
				WebUI.selectOptionByLabel(to, labelName, false)
				WebUI.delay(1)
				return true
			}
		} catch (Exception e) {
			KeywordUtil.logInfo("Failed to select drop down values object: " + to.getObjectId())
			return false
		}
		return false
	}


	/**
	 * @author Senthil.Arumugam
	 * scrollToElement(TestObject to)
	 * @param TestObject
	 * @return True if able to scroll for specific test object otherwise will return false
	 */
	@Keyword
	def scrollToElement(TestObject to) {
		try {
			WebUI.scrollToElement(to,10)
			return true
		} catch (Exception e) {
			KeywordUtil.logInfo("Failed to scroll to object: " + to.getObjectId())
			return false
		}
		return false
	}


	/**
	 * @author Senthil.Arumugam
	 * verifyPresent(TestObject to, int timeout)
	 * @param TestObject and timeout 
	 * @return True if able to display for specific test object otherwise will return false
	 */
	@Keyword
	def verifyPresent(TestObject to, int timeout){
		try {
			//WebUI.waitForPageLoad(timeout)
			WebElement element = WebUI.findWebElement(to, timeout)
			if(element.displayed){
				//WebUI.delay(2)
				return true
			}/*else {
			 WebUI.waitForAngularLoad(timeout)
			 }*/
		} catch (WebElementNotFoundException e) {
			KeywordUtil.logInfo(to.getObjectId() + " not displayed")
			return false
		}
		return false
	}


	/**
	 * @author Senthil.Arumugam
	 * verifyVisibleOfElement(TestObject to, int timeout)
	 * @param TestObject and timeout
	 * @return True if element to be visible for specific test object otherwise will return false
	 */
	@Keyword
	def verifyVisibleOfElement(TestObject to, int timeout){
		try {
			//WebUI.waitForPageLoad(timeout)
			if(WebUI.waitForElementVisible(to, timeout)){
				//WebUI.delay(2)
				return true
			} /*else {
			 WebUI.waitForAngularLoad(timeout)
			 }*/
		} catch (WebElementNotFoundException e) {
			KeywordUtil.logInfo(to.getObjectId() + " Element not Visible")
			return false
		}
		return false
	}

	/**
	 * @author Senthil.Arumugam
	 * verifyElementDidNotDisplayed(String strXPath = null)
	 * @param strXPath - user should pass the xpath 
	 * @return True if element to be Displayed for specific test object otherwise will return false
	 */
	@Keyword
	def verifyElementDidNotDisplayed(TestObject to, int iWaitTime){
		try {
			boolean blnFlag = false
			String strXpathLocator = ""
			Map allSelectors =  to.getSelectorCollection()
			strXpathLocator =  allSelectors.get(SelectorMethod.XPATH)
			println(strXpathLocator)

			for (int i = 1; i <= iWaitTime; i++) {

				WebDriver driver = DriverFactory.getWebDriver()
				int iSize = driver.findElements(By.xpath(strXpathLocator)).size()
				println(iSize)
				WebUI.delay(1)
				WebUI.waitForPageLoad(iWaitTime)
				if(iSize == 0 || iSize == 1){
					if(iSize == 1){
						WebElement element = driver.findElement(By.xpath(strXpathLocator))

						if (element.isDisplayed()) {
						}else{
							//WebUI.delay(2)
							blnFlag = true
							break
						}
					}else{
						blnFlag = true
						break
					}
				}
			}

			return blnFlag

		} catch (WebElementNotFoundException e) {
			KeywordUtil.logInfo(to.getSelectorCollection() + " Element is Visible")
			return false
		}
		return false
	}


	/**
	 * @author Senthil.Arumugam
	 * verifyElementIsDisplayed(String strXPath = null)
	 * @param strXPath - user should pass the xpath
	 * @return True if element to be Displayed for specific test object otherwise will return false
	 */
	@Keyword
	def verifyElementIsDisplayed(TestObject to, int iWaitTime = null){
		try {

			boolean blnFlag = false
			String strXpathLocator = ""
			Map allSelectors =  to.getSelectorCollection()
			strXpathLocator =  allSelectors.get(SelectorMethod.XPATH)


			for (int i = 1; i <= iWaitTime; i++) {

				WebDriver driver = DriverFactory.getWebDriver()
				int iSize = driver.findElements(By.xpath(strXpathLocator)).size()
				WebUI.delay(1)
				//WebUI.waitForPageLoad(iWaitTime)

				if(!(iSize == 0)){
					WebElement element = driver.findElement(By.xpath(strXpathLocator))
					if (element.isDisplayed()) {
						//WebUI.delay(2)
						blnFlag = true
						break
					}
				}
			}

			return blnFlag

		} catch (WebElementNotFoundException e) {
			KeywordUtil.logInfo(to.getSelectorCollection() + " Element not Visible")
			return false
		}
		return false
	}

	/**
	 * @author Senthil.Arumugam
	 * verifyElementIsDisplayed(String strXPath = null)
	 * @param strXPath - user should pass the xpath
	 * @return True if element to be Displayed for specific test object otherwise will return false
	 */
	@Keyword
	def verifyElementIsEnabled(TestObject to, int iWaitTime = null){
		try {

			boolean blnFlag = false
			String strXpathLocator = ""
			Map allSelectors =  to.getSelectorCollection()
			strXpathLocator =  allSelectors.get(SelectorMethod.XPATH)

			for (int i = 1; i <= iWaitTime; i++) {

				WebDriver driver = DriverFactory.getWebDriver()
				int iSize = driver.findElements(By.xpath(strXpathLocator)).size()
				WebUI.delay(1)
				//WebUI.waitForPageLoad(iWaitTime)

				if(!(iSize == 0)){
					WebElement element = driver.findElement(By.xpath(strXpathLocator))
					if (element.isEnabled()) {
						//WebUI.delay(2)
						blnFlag = true
						break
					}
				}
			}

			return blnFlag

		} catch (WebElementNotFoundException e) {
			KeywordUtil.logInfo(to.getSelectorCollection() + " Element not Visible")
			return false
		}
		return false
	}

	/**
	 * @author Senthil.Arumugam
	 * verifyElementIsDisplayed(String strXPath = null)
	 * @param strXPath - user should pass the xpath
	 * @return True if element to be Displayed for specific test object otherwise will return false
	 */
	@Keyword
	def verifyElementIsDisable(TestObject to, int iWaitTime = null){
		try {

			boolean blnFlag = false
			String strXpathLocator = ""
			Map allSelectors =  to.getSelectorCollection()
			strXpathLocator =  allSelectors.get(SelectorMethod.XPATH)

			for (int i = 1; i <= iWaitTime; i++) {

				WebDriver driver = DriverFactory.getWebDriver()
				int iSize = driver.findElements(By.xpath(strXpathLocator)).size()
				WebUI.delay(1)
				//WebUI.waitForPageLoad(iWaitTime)

				if(!(iSize == 0)){
					WebElement element = driver.findElement(By.xpath(strXpathLocator))
					if (element.isEnabled()) {
						//WebUI.delay(2)
						blnFlag = true
						break
					}
				}
			}

			return blnFlag

		} catch (WebElementNotFoundException e) {
			KeywordUtil.logInfo(to.getSelectorCollection() + " Element not Visible")
			return false
		}
		return false
	}

	/**
	 * @author Senthil.Arumugam
	 * getFirstSelectedOption(String strXPath = null)
	 * @param to -should pass the object 
	 * @param strSelectValues - should pass the expected values
	 * @return True if user able to get dropdown list of valus otherwise will return false
	 */
	@Keyword
	def getFirstSelectedOption(TestObject to, String strSelectValues){

		boolean blnFlag = false

		try {

			String strXpathLocator = ""
			Map allSelectors =  to.getSelectorCollection()
			strXpathLocator =  allSelectors.get(SelectorMethod.XPATH)

			WebDriver driver = DriverFactory.getWebDriver()
			Select select = new Select(driver.findElement(By.xpath(strXpathLocator)))
			List<WebElement> dropdown=select.getOptions();

			for(int i=0;i<dropdown.size();i++){

				String drop_down_values=dropdown.get(i).getText();

				if(!(drop_down_values.trim().equalsIgnoreCase(strSelectValues.trim()))){
					return true
					break
				}

			}
			//			String optionLabel = select.getFirstSelectedOption().getText()

			return false

		} catch (WebElementNotFoundException e) {
			KeywordUtil.logInfo(to.getSelectorCollection() + " Element not Visible")
			return false
		}
		return false
	}



	/**
	 * @author Senthil.Arumugam
	 * refresh
	 * @param 
	 * @return True if page is refreshed otherwise will return false
	 */
	@Keyword
	def refresh(){
		try {

			WebUI.refresh()
			return true

		} catch (WebElementNotFoundException e) {
			KeywordUtil.logInfo("unable to reload the Element")
			return false
		}
		return false
	}

	/**
	 * @author Senthil.Arumugam
	 * verifyElementNotPresent(TestObject to, int timeout)
	 * @param TestObject and timeout 
	 * @return True if element not visible for specific test object otherwise will return false
	 */
	@Keyword
	def verifyElementNotPresent(TestObject to, int timeout){
		try {
			WebUI.delay(2)
			if(WebUI.waitForElementNotPresent(to, timeout)){
				return true
			}
		} catch (WebElementNotFoundException e) {
			KeywordUtil.logInfo(to.getObjectId() + " Element Present")
			return false
		}
		return false
	}


	/**
	 * @author Senthil.Arumugam
	 * verifyElementChecked(TestObject to)
	 * @param TestObject
	 * @return True if Element to checked for specific test object otherwise will return false
	 */
	@Keyword
	def verifyElementChecked(TestObject to) {
		try {
			if(verifyVisibleOfElement(to,1)){
				boolean result = WebUI.verifyElementChecked(to, 1)
				if (!result){
					WebUI.findWebElement(to).click()
				}
				return true
			}
		} catch (Exception e) {
			KeywordUtil.logInfo("Failed to verify Element Checked object: " + to.getObjectId())
			return false
		}
		return false
	}


	/**
	 * @author Senthil.Arumugam
	 * getName()
	 * @return True if name is created otherwise will return empty values
	 */
	@Keyword
	def String getName() {

		Date date = new Date()
		String name = ""

		if (GlobalVariable.GlobalUserName != null) {

			name = name + GlobalVariable.GlobalUserName
		}
		if (name == ""){
			name = name + date.format('yyyy-MM-dd')
		}
		return name
	}


	/**
	 * GET PROJECT DELIVERY DATE
	 * @return DeliveryDate
	 */
	@Keyword
	def String getDueDate() {
		Date date = new Date()
		Date deliveryDate = date
		deliveryDate.format('MM/dd/yyyy')
		KeywordUtil.logInfo("Due Date is generated")
		return deliveryDate.format('MM/dd/yyyy')
	}



	/**
	 * @author Senthil.Arumugam
	 * newSession()
	 * @return True if specific site is open and login is success otherwise will return false
	 */
	@Keyword
	def newSession() {
		if (!openSite()) {
			return false
		}
		if (!(new commerceCloud.user()).login()) {
			return false
		}
		return true
	}

	/**
	 * @author Senthil.Arumugam
	 * newSessionWithUserName(String strUserName)
	 * @return True if specific site is open and login is success otherwise will return false
	 */
	@Keyword
	def newSessionWithUserName(String strUserName, String strRolePassword) {
		if (!openSite()) {
			return false
		}
		if (!(new commerceCloud.user()).loginWithUserName(strUserName, strRolePassword)) {
			return false
		}
		return true
	}

	/**
	 * @author Senthil.Arumugam
	 * openSite()
	 * @return True if site is opened and maximized windows otherwise will return false
	 */
	@Keyword
	def openSite() {
		KeywordUtil.logInfo("Opening Site  " + GlobalVariable.TestSite)
		try {
			WebUI.openBrowser(GlobalVariable.TestSite)
			WebUI.maximizeWindow()
			return true
		} catch (Exception e) {
			return false
		}
	}


	/**
	 * @author Senthil.Arumugam
	 * openWithSiteURL()
	 * @return True if site is opened and maximized windows otherwise will return false
	 */
	@Keyword
	def openWithSiteURL(String strURL) {
		KeywordUtil.logInfo("Opening Site  " + strURL)
		try {
			WebUI.openBrowser(strURL)
			WebUI.maximizeWindow()
			return true
		} catch (Exception e) {
			return false
		}
	}


	/**
	 * @author Senthil.Arumugam
	 * openWithSiteURL()
	 * @return True if site is opened and maximized windows otherwise will return false
	 */
	@Keyword
	def navigateToUrl(String strURL) {
		KeywordUtil.logInfo("Navigate Site  " + strURL)
		try {
			WebUI.navigateToUrl(strURL)

			return true
		} catch (Exception e) {
			return false
		}
	}


	/**
	 * @author Senthil.Arumugam
	 * openSite()
	 * @return True if site is closed otherwise will return false
	 */
	@Keyword
	def closeSite() {
		KeywordUtil.logInfo("Close Site  " + GlobalVariable.TestSite)
		try {
			WebUI.closeBrowser()
			return true
		} catch (Exception e) {
			return false
		}
	}


	/**
	 * @author Senthil.Arumugam
	 * back()
	 * @return True if page is return to previous page return false
	 */
	@Keyword
	def back() {
		KeywordUtil.logInfo("Return to previous page")
		try {
			WebUI.back()
			return true
		} catch (Exception e) {
			return false
		}
	}

	/**
	 * @author Senthil.Arumugam
	 * writeExcelFile(String strFilePath, String strSFDCId, String strAccountNo,String strUserName) 
	 * @return True if user can write excel sheet, otherwise will return false
	 */
	@Keyword
	def writeExcelFile(String strFilePath, String strSFDCId, String strAccountNo,String strUserName) {

		KeywordUtil.logInfo("Writing test data into excel file and stored file Path is " + strFilePath)
		try {

			FileInputStream fis = new FileInputStream(strFilePath);
			XSSFWorkbook workbook = new XSSFWorkbook(fis);
			XSSFSheet sheet = workbook.getSheet("Sheet1");
			int rowCount = sheet.getLastRowNum()+1;

			Row row = sheet.createRow(rowCount);
			Cell cell1 = row.createCell(0);
			cell1.setCellType(cell1.CELL_TYPE_STRING);
			cell1.setCellValue(strSFDCId);

			Cell cell2 = row.createCell(1);
			cell2.setCellType(cell2.CELL_TYPE_STRING);
			cell2.setCellValue(strAccountNo);

			Cell cell3 = row.createCell(2);
			cell3.setCellType(cell3.CELL_TYPE_STRING);
			cell3.setCellValue(strUserName);

			FileOutputStream fos = new FileOutputStream(strFilePath);
			workbook.write(fos);
			fos.close();
			return true

		} catch (Exception e) {
			return false
		}
		return true

	}



	/**
	 * @author Senthil.Arumugam
	 * advancePagination(int PageNum)
	 * @param PageNum
	 * @return true if element present, otherwise false
	 */
	@Keyword
	def advancePagination(int PageNum) {

		KeywordUtil.logInfo("Util advancePagination triggered")

		TestObject to = new TestObject("Pagination")
		String xpath = "//*[@class='pagination']//a[contains(text(),'" + PageNum + "']"
		TestObjectProperty prop = new TestObjectProperty("xpath", ConditionType.EQUALS, xpath)
		to.addProperty(prop)

		try {
			WebElement element = WebUI.findWebElement(to,3)
			element.click()
			WebUI.delay(1)
			KeywordUtil.logInfo("Pagination Advanced")
			return true
		}
		catch (WebElementNotFoundException e) {
			KeywordUtil.logInfo("Pagination can not be advanced")
			return false
		}
		catch (Exception e) {
			KeywordUtil.logInfo("Pagination advancing error")
			return false
		}
	}



	/**
	 * @author Senthil.Arumugam
	 * sorting(String sortByNameOrDate, String sortType)
	 * @param sortByNameOrDate - User should pass Accesnding Or Decending
	 * @param sortType - User should pass Name Or Date
	 * @return true if element is sorted, otherwise false
	 */
	@Keyword
	def sorting(def sortingObject, String sortByNameOrDate, String sortType) {

		if(sortByNameOrDate.equalsIgnoreCase("Name")){
			//Assending or Descending Order Project Name

			if(sortType.equalsIgnoreCase("asc")){
				KeywordUtil.logInfo("Accesnding order Name")
				List<WebElement> lst = (new commerceCloud.utils()).ListOfWebElement(sortingObject)

				def lstGroovy = []

				def newLstGroovy = []

				for (int i = 0; i < lst.size(); i++) {
					String list = lst.get(i).getText().trim()
					lstGroovy.add(i, list)

					newLstGroovy.add(i, list)
				}

				newLstGroovy = newLstGroovy.sort()

				def newLstSize = newLstGroovy.size()

				for (int i = 0; i < lstGroovy.size(); i++) {
					if (!(lstGroovy.get(i).toString().equalsIgnoreCase(newLstGroovy.get(i)))) {
						KeywordUtil.logInfo("unable to do Decending order in Name =  " +newLstGroovy)
						KeywordUtil.logInfo("unable to do Decending order in Name =  " +lstGroovy)
						return false
						break
					}
				}
				return true

			} else {

				KeywordUtil.logInfo("Decending order Name")
				List<WebElement> lstDesc = (new commerceCloud.utils()).ListOfWebElement(sortingObject)

				def arrDescBefSort = []

				def arrDescAftSort = []

				def lstDesSize = 0

				for (int i = 0; i < lstDesc.size(); i++) {
					arrDescBefSort.add(i, lstDesc.get(i).getText().trim())

					arrDescAftSort.add(i, lstDesc.get(i).getText().trim())
				}

				arrDescAftSort = arrDescAftSort.sort()

				lstDesSize = (arrDescAftSort.size() - 1)
				println(arrDescBefSort)
				println(arrDescAftSort)
				for (int i = 0; i < arrDescBefSort.size(); i++) {
					if (!(arrDescBefSort.get(lstDesSize).toString().equalsIgnoreCase(arrDescAftSort.get(i)))) {
						KeywordUtil.logInfo("unable to do Decending order in arrDescBefSort =  " +arrDescBefSort.get(lstDesSize))
						KeywordUtil.logInfo("unable to do Decending order in arrDescAftSort =  " +arrDescAftSort.get(i))
						KeywordUtil.logInfo("unable to do Decending order in list =  " +arrDescBefSort)
						return false
						break
					}

					lstDesSize = (lstDesSize - 1)
				}
				return true
			}

		}else if(sortByNameOrDate.equalsIgnoreCase("Number")){

			//Assending or Descending Order Project Name

			if(sortType.equalsIgnoreCase("asc")){
				KeywordUtil.logInfo("Accesnding order Number")
				List<WebElement> lst = (new commerceCloud.utils()).ListOfWebElement(sortingObject)

				def lstGroovy = []

				def newLstGroovy = []

				for (int i = 0; i < lst.size(); i++) {
					NumberFormat myFormat = NumberFormat.getInstance()

					myFormat.setGroupingUsed(true)

					String list = lst.get(i).getText().trim()

					String text = list.replaceAll('[^a-zA-Z0-9]', '')
					text = text.replaceAll('SALE', '')
					int iInt = Integer.parseInt(text)
					//text = text.replaceAll('.00', '')
					//Double numbers = Double.valueOf(text)

					//DecimalFormat decimalFormat = new DecimalFormat('###')

					//decimalFormat.setGroupingUsed(true)

					lstGroovy.add(i, iInt) //decimalFormat.format(numbers))

					newLstGroovy.add(i, iInt) //decimalFormat.format(numbers))
				}


				newLstGroovy.sort()

				def newLstSize = newLstGroovy.size()

				for (int i = 0; i < lstGroovy.size(); i++) {
					if (!((lstGroovy.get(i)) == (newLstGroovy.get(i)))) {
						KeywordUtil.logInfo("unable to do Accesnding order in Name =  " +newLstGroovy)
						return false
						break
					}
				}
				return true

			} else {

				KeywordUtil.logInfo("Decending order Number")
				List<WebElement> lstDesc = (new commerceCloud.utils()).ListOfWebElement(sortingObject)

				def arrDescBefSort = []

				def arrDescAftSort = []

				def lstDesSize = 0

				for (int i = 0; i < lstDesc.size(); i++) {
					NumberFormat myFormat = NumberFormat.getInstance()

					myFormat.setGroupingUsed(true)

					String list = lstDesc.get(i).getText().trim()
					String text = list.replaceAll('[^a-zA-Z0-9]', '')
					text = text.replaceAll('SALE', '')
					int iInt = Integer.parseInt(text)
					//Double numbers = Double.valueOf(text)

					//DecimalFormat decimalFormat = new DecimalFormat('###')

					//decimalFormat.setGroupingUsed(true)

					arrDescBefSort.add(i, iInt)//decimalFormat.format(numbers))

					arrDescAftSort.add(i, iInt)//decimalFormat.format(numbers))
				}
				arrDescAftSort.sort()


				lstDesSize = (arrDescAftSort.size() - 1)

				for (int i = 0; i < arrDescBefSort.size()-1; i++) {
					if (!((arrDescBefSort.get(i)) == (arrDescAftSort.get(lstDesSize)))) {
						KeywordUtil.logInfo("unable to do Decending order in arrDescBefSort =  " +arrDescBefSort.get(lstDesSize))
						KeywordUtil.logInfo("unable to do Decending order in arrDescAftSort =  " +arrDescAftSort.get(i))
						KeywordUtil.logInfo("unable to do Decending order in list =  " +arrDescBefSort)
						return false
						break
					}

					lstDesSize = (lstDesSize - 1)
				}
				return true
			}



		}else{

			if(sortType.equalsIgnoreCase("asc")){
				//Assending or Descending Order Project Name
				KeywordUtil.logInfo("Accesnding order Date")

				WebDriver driver = DriverFactory.getWebDriver()
				List<WebElement> lst = (new commerceCloud.utils()).ListOfWebElement(sortingObject)

				def lstGroovy = []

				def newLstGroovy = []

				for (int i = 0; i < lst.size(); i++) {
					def val = lst.get(i).getText().trim()

					def replceVal = val.replaceAll('Delivery', '').trim()

					Date date = Date.parse('MMM d, yyyy', replceVal)

					lstGroovy.add(i, date)

					newLstGroovy.add(i, date)
				}

				newLstGroovy = newLstGroovy.sort()

				for (int i = 0; i < lstGroovy.size(); i++) {
					if (!(lstGroovy.get(i).toString().equalsIgnoreCase(newLstGroovy.get(i).toString()))) {
						KeywordUtil.logInfo("unable to do Accesnding order in Date =  " +lstGroovy)
						return false
						break
					}
				}
				return true

			} else {
				//Decending order
				KeywordUtil.logInfo("Decending order Date")

				def arrDescBefSort = []

				def arrDescAftSort = []

				def lstDesSize = 0

				List<WebElement> lstDesc = (new commerceCloud.utils()).ListOfWebElement(sortingObject)

				for (int i = 0; i < lstDesc.size(); i++) {
					def val = lstDesc.get(i).getText().trim()

					def replceVal = val.replaceAll('Delivery', '').trim()

					if (!(replceVal.trim().equalsIgnoreCase(''))) {
						Date date = Date.parse('MMM d, yyyy', replceVal)
						arrDescBefSort.add(i, date)

						arrDescAftSort.add(i, date)
					}
				}

				arrDescAftSort = arrDescAftSort.sort()

				lstDesSize = (arrDescAftSort.size() - 1)

				for (int i = 0; i < arrDescBefSort.size(); i++) {
					if (!(arrDescBefSort.get(lstDesSize).toString().equalsIgnoreCase(arrDescAftSort.get(i).toString()))) {
						KeywordUtil.logInfo("unable to do Decending order in Date =  " +arrDescBefSort)
						return false
						break
					}
					lstDesSize = (lstDesSize - 1)
				}
				return true
			}
		}
	}
}