package commerceCloud
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject



import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.testobject.TestObjectProperty
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.driver.DriverFactory



class ProductSearch {


	/**
	 * searchTesscoProduct(TestObject ProductionList, TestObject ProductionSubList, def Search)
	 * @param Search - User should send production search parts details
	 * @return true if parts is searched, otherwise false
	 */

	@Keyword
	def searchTesscoProduct(def Search) {

		if(Search == null){
			Search = "Cable"
		}

		KeywordUtil.logInfo("Validate the ability to search products using product textbox")

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("ProductSearch/search tessco com textbox"),60)) {
			KeywordUtil.logInfo("Cannot see the search tessco com textbox")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("ProductSearch/search tessco com textbox"), Search)) {
			KeywordUtil.logInfo("Cannot enter search tessco com textbox")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("ProductSearch/search tessco com button"))) {
			KeywordUtil.logInfo("Cannot click search tessco com button")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("ProductSearch/search result center block margin"),60)) {
			KeywordUtil.logInfo("Cannot see the search result center block margin")
			return false
		}

		KeywordUtil.logInfo("Validated user successfully search products using product textbox")
		return true
	}



	/**
	 * SelectTesscoProduct(TestObject ProductionList, TestObject ProductionSubList, def Search)
	 * @param ProductionList - User should click the production list object
	 * @param ProductionSubList - User should click the production sub list object
	 * @param Search - User should send production search parts details
	 * @return true if parts is searched, otherwise false
	 */

	@Keyword
	def selectTesscoProduct(TestObject ProductionList, TestObject ProductionSubList) {
		KeywordUtil.logInfo("Validate the ability to search product under product list")

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("AntennasAndFilterProducts/Products"),60)) {
			KeywordUtil.logInfo("Cannot see the search tessco com textbox")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("AntennasAndFilterProducts/Products"))) {
			KeywordUtil.logInfo("Cannot click search tessco com button")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(ProductionList,60)) {
			KeywordUtil.logInfo("Cannot see the Production List")
			return false
		}

		if (!(new commerceCloud.utils()).click(ProductionList)) {
			KeywordUtil.logInfo("Cannot click Production List")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(ProductionSubList,60)) {
			KeywordUtil.logInfo("Cannot see the Production Sub List")
			return false
		}

		if (!(new commerceCloud.utils()).click(ProductionSubList)) {
			KeywordUtil.logInfo("Cannot click Production Sub List")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("ProductSearch/VIEW"),60)) {
			KeywordUtil.logInfo("Cannot see the VIEW")
			return false
		}


		KeywordUtil.logInfo("Validated user successfully search product under product list")
		return true
	}

	/**
	 * viewMoreProductsInSearch()
	 * @param Search - user should send the tessco product details
	 * @return true if Validate the The ability to view more products in search results page, otherwise false
	 */

	@Keyword
	def viewMoreProductsInSearch(def Search) {

		KeywordUtil.logInfo("Validate the The ability to view more products in search results page")

		if (!(new commerceCloud.ProductSearch()).selectTesscoProduct(findTestObject("AntennasAndFilterProducts/Antennas and Filter Products"),findTestObject("AntennasAndFilterProducts/Antennas Filter Products View All"))) {
			KeywordUtil.logInfo("Cannot select the Antennas and Filter Products AND Antennas Filter Products View All")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ProductSearch/Select product listing price"),60)) {
			KeywordUtil.logInfo("Cannot see the Select product listing price")
			return false
		}

		if (!(new commerceCloud.utils()).compareListSize(findTestObject("ProductSearch/size of product price"),24)) {
			KeywordUtil.logInfo("Cannot compare size of product 24 page")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("ProductSearch/48"))) {
			KeywordUtil.logInfo("Cannot click 48 page size")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ProductSearch/Select product listing price"),60)) {
			KeywordUtil.logInfo("Cannot see the Select product listing price")
			return false
		}

		if (!(new commerceCloud.utils()).compareListSize(findTestObject("ProductSearch/size of product price"),48)) {
			KeywordUtil.logInfo("Cannot compare size of product 48 page")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("ProductSearch/Home"))) {
			KeywordUtil.logInfo("Cannot click Home")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ProductSearch/Learn More"),60)) {
			KeywordUtil.logInfo("Cannot see the Learn More")
			return false
		}

		if (!(new commerceCloud.ProductSearch()).searchTesscoProduct(Search)) {
			KeywordUtil.logInfo("Cannot search the Tessco Product")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ProductSearch/product listing price"),60)) {
			KeywordUtil.logInfo("Cannot see the product listing price")
			return false
		}

		if (!(new commerceCloud.utils()).compareListSize(findTestObject("ProductSearch/size of product price"),48)) {
			KeywordUtil.logInfo("Cannot compare size of product 48 page")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("ProductSearch/24"))) {
			KeywordUtil.logInfo("Cannot click 24 page size")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ProductSearch/product listing price"),60)) {
			KeywordUtil.logInfo("Cannot see the product listing price")
			return false
		}

		if (!(new commerceCloud.utils()).compareListSize(findTestObject("ProductSearch/size of product price"),24)) {
			KeywordUtil.logInfo("Cannot compare size of product 24 page")
			return false
		}


		KeywordUtil.logInfo("Validated The ability to view more products in search results page")
		return true
	}

	/**
	 * searchProductsUsingAttributes()
	 * @param Search - user should send the tessco product details
	 * @return true if Validate the The ability to search products using product attributes, otherwise false
	 */

	@Keyword
	def searchProductsUsingAttributes(def Search) {

		KeywordUtil.logInfo("Validate the The ability to search products using product attributes")

		if (!(new commerceCloud.ProductSearch()).searchTesscoProduct(Search)) {
			KeywordUtil.logInfo("Cannot search the Tessco Product")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ProductSearch/product listing price"),60)) {
			KeywordUtil.logInfo("Cannot see the product listing price")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ProductSearch/Product List Manufacture Name"),1)) {
			KeywordUtil.logInfo("Cannot see the Product List Manufacture Name")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ProductSearch/product list name"),1)) {
			KeywordUtil.logInfo("Cannot see the product list name")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ProductSearch/product list quantity panel"),1)) {
			KeywordUtil.logInfo("Cannot see the product list quantity panel")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ProductSearch/product list thumb"),1)) {
			KeywordUtil.logInfo("Cannot see the product list thumb")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ProductSearch/search result center block margin"),1)) {
			KeywordUtil.logInfo("Cannot see the product listing price")
			return false
		}

		KeywordUtil.logInfo("Validated The ability to search products using product attributes")
		return true
	}

	/**
	 * ProductTechnicalSpecs(def Search)
	 * @param Search - user should send the tessco product details
	 * @return true if Validate the The ability to search products using product attributes, otherwise false
	 */

	@Keyword
	def ProductTechnicalSpecs(def Search) {

		KeywordUtil.logInfo("Validate the ability to search products and see Technical Specs")

		if (!(new commerceCloud.ProductSearch()).searchTesscoProduct(Search)) {
			KeywordUtil.logInfo("Cannot search the Tessco Product")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ProductSearch/product listing price"),60)) {
			KeywordUtil.logInfo("Cannot see the product listing price")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("ProductSearch/product list name"))) {
			KeywordUtil.logInfo("Cannot click product list first name")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ProductSearch/TechnicalSpecs"),60)) {
			KeywordUtil.logInfo("Cannot see the Technical Specs")
			return false
		}


		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ProductSearch/ProductClassificationsPdpTechSpecs"),1)) {
			KeywordUtil.logInfo("Cannot see the Product Classifications Pdp Tech Specs")
			return false
		}

		KeywordUtil.logInfo("Validated the ability to search products and see Technical Specs")
		return true
	}


	/**
	 * BrandProtection(String Search)
	 * @param Search - user should send the tessco product details
	 * @return true if Validate the The ability to search products using product attributes, otherwise false
	 */

	@Keyword
	def BrandProtection(String Search) {

		KeywordUtil.logInfo("Validate the ability to search Brand Protection Store front products")

		if (!(new commerceCloud.ProductSearch()).searchTesscoProduct(Search)) {
			KeywordUtil.logInfo("Cannot search the Tessco Product")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ProductSearch/product listing price"),60)) {
			KeywordUtil.logInfo("Cannot see the product listing price")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("ProductSearch/48"))) {
			KeywordUtil.logInfo("Cannot click 48")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ProductSearch/product listing price"),60)) {
			KeywordUtil.logInfo("Cannot see the product listing price")
			return false
		}


		if (!(new commerceCloud.utils()).scrollToElement(findTestObject("ProductSearch/BrandProtectedLearnMore"))) {
			KeywordUtil.logInfo("Cannot scroll Brand Protected Learn More")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ProductSearch/BrandProtectedLearnMore"),60)) {
			KeywordUtil.logInfo("Cannot see the Brand Protected Learn More")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ProductSearch/BrandProtected"),1)) {
			KeywordUtil.logInfo("Cannot see the Brand Protected")
			return false
		}

		KeywordUtil.logInfo("Validated the ability to search Brand Protection Store front products")
		return true
	}

	/**
	 * LatestNews() 
	 * @return true if Validate the 3 News components are displayed below featured products, otherwise false
	 */

	@Keyword
	def LatestNews() {

		KeywordUtil.logInfo("Validate the 3 News components are displayed below featured products")

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("LatestNews/LatestNews"),60)) {
			KeywordUtil.logInfo("Cannot see the Latest News")
			return false
		}

		if (!(new commerceCloud.utils()).scrollToElement(findTestObject("LatestNews/LatestNews"))) {
			KeywordUtil.logInfo("Cannot scroll Latest News")
			return false
		}

		List<WebElement> lst = (new commerceCloud.utils()).ListOfWebElement(findTestObject("LatestNews/LatestNewsSize"))
		if (lst.size() == 0) {
			KeywordUtil.logInfo("Cannot see the Latest News 3 components")
			return false
		}


		for (int i=0;i<3;i++) {

			lst = (new commerceCloud.utils()).ListOfWebElement(findTestObject("LatestNews/LatestNewsSize"))
			String beforeURL = ((new commerceCloud.utils()).getURL())
			lst.get(i).click()

			if (!(new commerceCloud.utils()).verifyElementDidNotDisplayed(findTestObject("LatestNews/LatestNews"),60)) {
				KeywordUtil.logInfo("Can see Latest News")
				return false
			}

			String afterURL = ((new commerceCloud.utils()).getURL())

			if(beforeURL.equalsIgnoreCase(afterURL)){
				KeywordUtil.logInfo("Cannot navigate to next page")
				return false
			}

			if (!(new commerceCloud.utils()).back()) {
				KeywordUtil.logInfo("Can Go Back")
				return false
			}

			if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("LatestNews/LatestNews"),60)) {
				KeywordUtil.logInfo("Cannot see the Latest News")
				return false
			}
			lst = (new commerceCloud.utils()).ListOfWebElement(findTestObject("LatestNews/LatestNewsSize"))
		}

		KeywordUtil.logInfo("Validated the 3 News components are displayed below featured products")
		return true
	}


	/**
	 * Footer()
	 * @return true if Validate the social website like Twitter, Youtube and Linked In, otherwise false
	 */

	@Keyword
	def Footer() {

		KeywordUtil.logInfo("Validate the presents of social website like Twitter, Youtube and Linked In")

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("HomePage/LinkedIn"),60)) {
			KeywordUtil.logInfo("Cannot see the Linked In")
			return false
		}

		if (!(new commerceCloud.utils()).scrollToElement(findTestObject("HomePage/LinkedIn"))) {
			KeywordUtil.logInfo("Cannot scroll Linked In")
			return false
		}

		for (int i=0;i<3;i++) {

			String beforeURL = ((new commerceCloud.utils()).getURL())

			if(i == 0){
				if (!(new commerceCloud.utils()).click(findTestObject("HomePage/LinkedIn"))) {
					KeywordUtil.logInfo("Cannot click Linked In")
					return false
				}
			}else if(i == 1){
				if (!(new commerceCloud.utils()).click(findTestObject("HomePage/Twitter"))) {
					KeywordUtil.logInfo("Cannot click Twitter")
					return false
				}
			}else if(i == 2){
				if (!(new commerceCloud.utils()).click(findTestObject("HomePage/Youtube"))) {
					KeywordUtil.logInfo("Cannot click Youtube")
					return false
				}
			}else

			if (!(new commerceCloud.utils()).delay(15)) {
				KeywordUtil.logInfo("Cannot wait 15 second")
				return false
			}

			if (!(new commerceCloud.utils()).switchToWindow(1)) {
				KeywordUtil.logInfo("Cannot switch To Window")
				return false
			}

			if (!(new commerceCloud.utils()).verifyElementDidNotDisplayed(findTestObject("LatestNews/LatestNews"),60)) {
				KeywordUtil.logInfo("Can see Latest News")
				return false
			}

			String afterURL = ((new commerceCloud.utils()).getURL())

			if(i==0){
				if(!(afterURL.contains("linkedin"))){
					KeywordUtil.logInfo("Cannot navigate to linkedin page")
					return false
				}
			}

			if(i==1){
				if(!(afterURL.contains("twitter"))){
					KeywordUtil.logInfo("Cannot navigate to twitter page")
					return false
				}
			}

			if(i==2){
				if(!(afterURL.contains("youtube"))){
					KeywordUtil.logInfo("Cannot navigate to youtube page")
					return false
				}
			}

			if (!(new commerceCloud.utils()).closeWindowIndex(1)) {
				KeywordUtil.logInfo("Cannot close Window Index")
				return false
			}

			if (!(new commerceCloud.utils()).switchToWindow(0)) {
				KeywordUtil.logInfo("Cannot switch To Window")
				return false
			}

			if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("LatestNews/LatestNews"),60)) {
				KeywordUtil.logInfo("Cannot see the Latest News")
				return false
			}
		}

		KeywordUtil.logInfo("Validated the presents of social website like Twitter, Youtube and Linked In")
		return true
	}


	/**
	 * NavigationBar()
	 * @return true if the navigation bar shows the following links, otherwise false
	 */

	@Keyword
	def NavigationBar() {

		KeywordUtil.logInfo("Validate the navigation bar shows the following links")

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("NavigationBar/MarketsWeServe"),60)) {
			KeywordUtil.logInfo("Cannot see the Markets We Serve")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("NavigationBar/VerizonMMDDs"),1)) {
			KeywordUtil.logInfo("Cannot see the Verizon MMDDs")
			return false
		}
		
		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("NavigationBar/Products"),1)) {
			KeywordUtil.logInfo("Cannot see the Products")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("NavigationBar/Resources"),1)) {
			KeywordUtil.logInfo("Cannot see the Resources")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("NavigationBar/Services"),1)) {
			KeywordUtil.logInfo("Cannot see the Services")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("NavigationBar/SignedInAs"),1)) {
			KeywordUtil.logInfo("Cannot see the SignedInAs")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("NavigationBar/Solutions"),1)) {
			KeywordUtil.logInfo("Cannot see the Solutions")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("NavigationBar/Suppliers"),1)) {
			KeywordUtil.logInfo("Cannot see the Suppliers")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("NavigationBar/Tools"),1)) {
			KeywordUtil.logInfo("Cannot see the Tools")
			return false
		}


		KeywordUtil.logInfo("Validated the navigation bar shows the following links")
		return true
	}
	/**
	 * sortSearchResults()
	 * @param Search - user should send the tessco product details
	 * @return true if The ability to sort search results page, otherwise false
	 */

	@Keyword
	def sortSearchResults(def Search, String strSorting, String NameOrDate, String ascOrDsc) {

		KeywordUtil.logInfo("Validate the The ability to sort search results page")

		if (!(new commerceCloud.ProductSearch()).searchTesscoProduct(Search)) {
			KeywordUtil.logInfo("Cannot search the Tessco Product")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ProductSearch/product listing price"),60)) {
			KeywordUtil.logInfo("Cannot see the product listing price")
			return false
		}

		/*	if (!(new commerceCloud.utils()).getFirstSelectedOption(findTestObject("ProductSearch/SortOptions"), "Relevance")) {
		 KeywordUtil.logInfo("Cannot get the Sort Options dropdown list ")
		 return false
		 }*/

		if (!(new commerceCloud.utils()).click(findTestObject("ProductSearch/SortOptions"))) {
			KeywordUtil.logInfo("Cannot click the Sort Options under Product Search page")
			return false
		}

		TestObject toVisible = new TestObject("Sorting")
		String xpathVisible = "//a[contains(text(),'" + strSorting + "')]"
		TestObjectProperty propVisible = new TestObjectProperty("xpath", ConditionType.EQUALS, xpathVisible)
		toVisible.addProperty(propVisible)

		if (!(new commerceCloud.utils()).click(toVisible)) {
			KeywordUtil.logInfo("Cannot click the sorting : " + strSorting)
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ProductSearch/product listing price"),60)) {
			KeywordUtil.logInfo("Cannot see the product listing price")
			return false
		}

		if (!(new commerceCloud.utils()).delay(5)) {
			KeywordUtil.logInfo("Cannot wait a sec")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ProductSearch/product listing price"),60)) {
			KeywordUtil.logInfo("Cannot see the product listing price")
			return false
		}
		
		if (!(new commerceCloud.utils()).sorting(findTestObject("ProductSearch/size of product price"), NameOrDate, ascOrDsc)) {
			KeywordUtil.logInfo("Cannot select the Sort Options under Product Search page")
			return false
		}

		KeywordUtil.logInfo("Validated The ability to sort search results page")
		return true
	}

	/**
	 * facetsAndFilters()
	 * @return true if Ability to filter search results by category, otherwise false
	 */

	@Keyword
	def facetsAndFilters() {

		KeywordUtil.logInfo("Validate the Facets and filters [cable product and filter all Categories]")

		if (!(new commerceCloud.ProductSearch()).selectTesscoProduct(findTestObject("CableProducts/CableProducts"),findTestObject("CableProducts/View All"))) {
			KeywordUtil.logInfo("Cannot select the Cable Products AND View All")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ProductSearch/Select product listing price"),60)) {
			KeywordUtil.logInfo("Cannot see the Select product listing price")
			return false
		}

		if (!(new commerceCloud.utils()).scrollToElement(findTestObject("FILTERS/All Categories"))) {
			KeywordUtil.logInfo("Cannot see the All Categories")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("FILTERS/All Categories Plus"),60)) {
			KeywordUtil.logInfo("Cannot see the All Categories Plus")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("FILTERS/All Categories Plus"))) {
			KeywordUtil.logInfo("Cannot click All Categories Plus")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("FILTERS/All Categories Minus"),60)) {
			KeywordUtil.logInfo("Cannot see the All Categories Minus")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("FILTERS/CheckCategoryCheckboxs"))) {
			KeywordUtil.logInfo("Cannot click Check one of the category checkboxs")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("FILTERS/clearFilters"))) {
			KeywordUtil.logInfo("Cannot Click the 'Clear Filter' option")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("FILTERS/CableProductsHompage"),60)) {
			KeywordUtil.logInfo("Cannot see Verify clearing filter does not remove category selection from categories displayed above filters")
			return false
		}

		KeywordUtil.logInfo("Validated the Facets and filters [cable product and filter all Categories]")


		KeywordUtil.logInfo("Verify the the category hierarchy displayed under categories section is as expected")

		if (!(new commerceCloud.ProductSearch()).selectTesscoProduct(findTestObject("AntennasAndFilterProducts/Antennas and Filter Products"),findTestObject("AntennasAndFilterProducts/Antennas Filter Products View All"))) {
			KeywordUtil.logInfo("Cannot select the Antennas and Filter Products AND Antennas Filter Products View All")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ProductSearch/Select product listing price"),60)) {
			KeywordUtil.logInfo("Cannot see the Select product listing price")
			return false
		}

		if (!(new commerceCloud.utils()).scrollToElement(findTestObject("FILTERS/All Categories"))) {
			KeywordUtil.logInfo("Cannot see the All Categories")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("FILTERS/All Categories Plus"),60)) {
			KeywordUtil.logInfo("Cannot see the All Categories Plus")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("FILTERS/All Categories Plus"))) {
			KeywordUtil.logInfo("Cannot click All Categories Plus")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("FILTERS/All Categories Minus"),60)) {
			KeywordUtil.logInfo("Cannot see the All Categories Minus")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("FILTERS/CheckCategoryCheckboxs"))) {
			KeywordUtil.logInfo("Cannot click Check one of the category checkboxs")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("FILTERS/clearFilters"))) {
			KeywordUtil.logInfo("Cannot Click the 'Clear Filter' option")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("FILTERS/All Categories"),60)) {
			KeywordUtil.logInfo("Cannot see All Categories")
			return false
		}

		KeywordUtil.logInfo("Verified the the category hierarchy displayed under categories section is as expected")

		return true
	}


	/**
	 * abilityToClearAllFilters()
	 * @return true if Ability to clear all filters with one button click, otherwise false
	 */

	@Keyword
	def abilityToClearAllFilters() {

		KeywordUtil.logInfo("Validate the Ability to clear all filters with one button click")

		if (!(new commerceCloud.ProductSearch()).selectTesscoProduct(findTestObject("AntennasAndFilterProducts/Antennas and Filter Products"),findTestObject("AntennasAndFilterProducts/Antennas Filter Products View All"))) {
			KeywordUtil.logInfo("Cannot select the Antennas and Filter Products AND Antennas Filter Products View All")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ProductSearch/Select product listing price"),60)) {
			KeywordUtil.logInfo("Cannot see the Select product listing price")
			return false
		}

		if (!(new commerceCloud.utils()).scrollToElement(findTestObject("FILTERS/Filters"))) {
			KeywordUtil.logInfo("Cannot see the Filters")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("FILTERS/Price Plus"),60)) {
			KeywordUtil.logInfo("Cannot see the Price Plus")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("FILTERS/Price Plus"))) {
			KeywordUtil.logInfo("Cannot click Price Plus")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("FILTERS/Price Minus"),60)) {
			KeywordUtil.logInfo("Cannot see the Price Minus")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("FILTERS/Price 20"))) {
			KeywordUtil.logInfo("Cannot click Price 20 checkboxs")
			return false
		}

		boolean bFlag = true
		List<WebElement> lst = (new commerceCloud.utils()).ListOfWebElement(findTestObject("ProductSearch/size of product price"))

		for (int i = 0; i < lst.size(); i++) {
			String list = lst.get(i).getText().trim()
			String text = list.replaceAll('[^a-zA-Z0-9]', '')
			text = text.replaceAll('SALE', '')
			KeywordUtil.logInfo("text  : "+text)
			if (text.equalsIgnoreCase("2000")){
				bFlag = true
			}else{
				bFlag = false
				break
			}
		}

		if (!(bFlag)) {
			KeywordUtil.logInfo("Cannot Verfify only products with a price of 20 displays")
			return false
		}


		if (!(new commerceCloud.utils()).click(findTestObject("FILTERS/clearFilters"))) {
			KeywordUtil.logInfo("Cannot Click the 'Clear Filter' option")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementDidNotDisplayed(findTestObject("FILTERS/clearFilters"),60)) {
			KeywordUtil.logInfo("Can see Clear Filter ")
			return false
		}

		KeywordUtil.logInfo("Validated the user able to clear all filters with one button click")
		return true
	}


	/**
	 * productFilterOnSale()
	 * @return true if Product Filter OnSale, otherwise false
	 */

	@Keyword
	def productFilterOnSale() {

		KeywordUtil.logInfo("Validate the Product Filter OnSale")

		if (!(new commerceCloud.ProductSearch()).selectTesscoProduct(findTestObject("AntennasAndFilterProducts/Antennas and Filter Products"),findTestObject("AntennasAndFilterProducts/Antennas Filter Products View All"))) {
			KeywordUtil.logInfo("Cannot select the Antennas and Filter Products AND Antennas Filter Products View All")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ProductSearch/Select product listing price"),60)) {
			KeywordUtil.logInfo("Cannot see the Select product listing price")
			return false
		}

		if (!(new commerceCloud.utils()).scrollToElement(findTestObject("FILTERS/Filters"))) {
			KeywordUtil.logInfo("Cannot see the Filters")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("FILTERS/Promotions Plus"),60)) {
			KeywordUtil.logInfo("Cannot see the Promotions Plus")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("FILTERS/Promotions Plus"))) {
			KeywordUtil.logInfo("Cannot click Promotions Plus")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("FILTERS/Promotions Minus"),60)) {
			KeywordUtil.logInfo("Cannot see the Price Minus")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("FILTERS/Promotion Sale"))) {
			KeywordUtil.logInfo("Cannot click Promotion Sale checkboxs")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ProductSearch/Select product listing price"),60)) {
			KeywordUtil.logInfo("Cannot see the Select product listing price")
			return false
		}

		boolean bFlag = false
		List<WebElement> lst = (new commerceCloud.utils()).ListOfWebElement(findTestObject("ProductSearch/size of product price"))

		for (int i = 0; i < lst.size(); i++) {
			String list = lst.get(i).getText().trim()
			String text = list.replaceAll('[^a-zA-Z0-9]', '')
			KeywordUtil.logInfo("text  : "+text)
			if (text.contains("SALE")){
				bFlag = true
			}else{
				bFlag = false
				break
			}
		}

		if (!(bFlag)) {
			KeywordUtil.logInfo("Cannot Verfify only products with a price of SALE displays")
			return false
		}

		KeywordUtil.logInfo("Validated the Product Filter OnSale")
		return true
	}

	/**
	 * switchAccount() 
	 * @para strAccountType -> user should pass the account type [ Rustic Retail Or 2898693]
	 * @return true if User Switch Account, otherwise false
	 */

	@Keyword
	def switchAccount(String strAccountType) {

		KeywordUtil.logInfo("Validate the User Switch Account")

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("SwitchAccount/SignedInAs"),60)) {
			KeywordUtil.logInfo("Cannot see the Signed In As Button")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("SwitchAccount/SignedInAs"))) {
			KeywordUtil.logInfo("Cannot click Signed In As Button")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("SwitchAccount/SwitchAccount"),60)) {
			KeywordUtil.logInfo("Cannot see the Switch Account")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("SwitchAccount/SwitchAccount"))) {
			KeywordUtil.logInfo("Cannot click Switch Account")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("SwitchAccount/SelectAccount"),60)) {
			KeywordUtil.logInfo("Cannot see the Select Account")
			return false
		}
		 
		
		/*
		if (!(new commerceCloud.utils()).click(findTestObject("Cart/SaveCart"))) {
			KeywordUtil.logInfo("Cannot click Save Cart")
			return false
		}
*/
		
		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("SwitchAccount/Account"),1)) {
			KeywordUtil.logInfo("Cannot see the Account")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("SwitchAccount/AccountSelect"))) {
			KeywordUtil.logInfo("Cannot click Account Select")
			return false
		}


		TestObject to = new TestObject("Account Type")
		String xpath = "//a[contains(text(),'" + strAccountType + "')]"
		TestObjectProperty prop = new TestObjectProperty("xpath", ConditionType.EQUALS, xpath)
		to.addProperty(prop)

		if(!(new commerceCloud.utils()).click(to)) {
			KeywordUtil.logInfo("Cannot click Account Values : " + strAccountType)
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("SwitchAccount/Continue"))) {
			KeywordUtil.logInfo("Cannot click Continue")
			return false
		}

		
		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("SwitchAccount/SignedInAs"),60)) {
			KeywordUtil.logInfo("Cannot see the Signed In As Button")
			return false
		}
 		
		if (!(new commerceCloud.utils()).click(findTestObject("SwitchAccount/SignedInAs"))) {
			KeywordUtil.logInfo("Cannot click Signed In As Button")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("SwitchAccount/UserAccountNumber"),60)) {
			KeywordUtil.logInfo("Cannot see the User Account Number")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("SwitchAccount/SignedInAs"))) {
			KeywordUtil.logInfo("Cannot click Signed In As Button")
			return false
		}

		KeywordUtil.logInfo("Validated the User Switch Account")
		return true
	}


	/**
	 * viewBillTrustInvoices() 
	 * @return true if Verify users have permission to view bill trust invoices, otherwise false
	 */

	@Keyword
	def viewBillTrustInvoices() {

		KeywordUtil.logInfo("Validate the users have permission to view bill trust invoices")

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("SwitchAccount/SignedInAs"),60)) {
			KeywordUtil.logInfo("Cannot see the Signed In As Button")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("SwitchAccount/SignedInAs"))) {
			KeywordUtil.logInfo("Cannot click Signed In As Button")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("SwitchAccount/SwitchAccount"),60)) {
			KeywordUtil.logInfo("Cannot see the Switch Account")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("SwitchAccount/Invoices"))) {
			KeywordUtil.logInfo("Cannot click Invoices")
			return false
		}

		if (!(new commerceCloud.utils()).delay(5)) {
			KeywordUtil.logInfo("Cannot wait 5 sec")
			return false
		}

		if (!(new commerceCloud.utils()).switchToWindow(1)) {
			KeywordUtil.logInfo("Cannot switch To bill trust Window")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("SwitchAccount/BillTrustOpen"),60)) {
			KeywordUtil.logInfo("Cannot see the Open button under Bill Trust page")
			return false
		}

		if (!(new commerceCloud.utils()).switchToWindow(0)) {
			KeywordUtil.logInfo("Cannot switch To default Window")
			return false
		}

		KeywordUtil.logInfo("Validated the users have permission to view bill trust invoices")
		return true
	}


	/**
	 * PaginationForSearchResults()
	 * @return true if Pagination for search results, when number of products in result exceed page size, otherwise false
	 */

	@Keyword
	def PaginationForSearchResults() {

		KeywordUtil.logInfo("Validate the Pagination for search results, when number of products in result exceed page size")

		if (!(new commerceCloud.ProductSearch()).selectTesscoProduct(findTestObject("AntennasAndFilterProducts/Antennas and Filter Products"),findTestObject("AntennasAndFilterProducts/Antennas Filter Products View All"))) {
			KeywordUtil.logInfo("Cannot select the Antennas and Filter Products AND Antennas Filter Products View All")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ProductSearch/Select product listing price"),60)) {
			KeywordUtil.logInfo("Cannot see the Select product listing price")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("FILTERS/paginationLabel"),1)) {
			KeywordUtil.logInfo("Cannot see the pagination Label")
			return false
		}

		if (!(new commerceCloud.utils()).scrollToElement(findTestObject("FILTERS/PaginationRight"))) {
			KeywordUtil.logInfo("Cannot see the Pagination Right")
			return false
		}

		for (int i = 2; i <=6; i++) {

			TestObject to = new TestObject("pagination")
			String xpath = "//ul[@class='pagination']//a[contains(text(),'"+i+"')]"
			TestObjectProperty prop = new TestObjectProperty("xpath", ConditionType.EQUALS, xpath)
			to.addProperty(prop)

			if(!(new commerceCloud.utils()).click(to)) {
				KeywordUtil.logInfo("Cannot click the pagination element")
				return false
			}

			TestObject toVisible = new TestObject("paginationNext")
			String xpathVisible = "//li[@class='active']//span[contains(text(),'"+i+"')]"
			TestObjectProperty propVisible = new TestObjectProperty("xpath", ConditionType.EQUALS, xpathVisible)
			toVisible.addProperty(propVisible)

			if(!(i == 6)){
				if (!(new commerceCloud.utils()).scrollToElement(findTestObject("FILTERS/PaginationLeft"))) {
					KeywordUtil.logInfo("Cannot scroll untill Pagination Left presents on element")
					return false
				}


				if(!(new commerceCloud.utils()).verifyPresent(toVisible,60)){
					KeywordUtil.logInfo("Cannot see the pagination selected element")
					return false
				}
			}
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("FILTERS/paginationLabel"),30)) {
			KeywordUtil.logInfo("Cannot see the pagination Label")
			return false
		}
		
		KeywordUtil.logInfo("Validated the Pagination for search results, when number of products in result exceed page size")
		return true
	}

	/**
	 * BreadcrumbsInCategory()
	 * @return true if Breadcrumbs in category results page for easy navigation, otherwise false
	 */

	@Keyword
	def BreadcrumbsInCategory() {

		KeywordUtil.logInfo("Validate the Breadcrumbs in category results page for easy navigation")

		if (!(new commerceCloud.ProductSearch()).selectTesscoProduct(findTestObject("CableProducts/CableProducts"),findTestObject("CableProducts/View All"))) {
			KeywordUtil.logInfo("Cannot select the Cable Products AND View All")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("CATEGORIES/CoaxialCableConnectorsJumpers"),60)) {
			KeywordUtil.logInfo("Cannot see the Coaxial Cable Connectors Jumpers")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("CATEGORIES/CoaxialCableConnectorsJumpers"))) {
			KeywordUtil.logInfo("Cannot click Coaxial Cable Connectors Jumpers")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("CATEGORIES/50OhmBraidedCoaxCable"),60)) {
			KeywordUtil.logInfo("Cannot see the 50 Ohm Braided Coax Cable")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("CATEGORIES/50OhmBraidedCoaxCable"))) {
			KeywordUtil.logInfo("Cannot click 50 Ohm Braided Coax Cable")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("CATEGORIES/50OhmBraidedCoaxCableSub"),60)) {
			KeywordUtil.logInfo("Cannot see the 50 Ohm Braided Coax Cable sub")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("CATEGORIES/50OhmBraidedCoaxCableSub"))) {
			KeywordUtil.logInfo("Cannot click 50 Ohm Braided Coax Cable sub")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("CATEGORIES/100SeriesCable"),60)) {
			KeywordUtil.logInfo("Cannot see the 100 Series Cable")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("CATEGORIES/100SeriesCable"))) {
			KeywordUtil.logInfo("Cannot click 100 Series Cable")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ProductSearch/Select product listing price"),60)) {
			KeywordUtil.logInfo("Cannot see Select product listing price")
			return false
		}

		if (!(new commerceCloud.utils()).compareListSize(findTestObject("ProductSearch/size of product price"),1)) {
			KeywordUtil.logInfo("Cannot compare size of 100 Series Cable product")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("CATEGORIES/CableProductsHeader"))) {
			KeywordUtil.logInfo("Cannot click Cable Products Header")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("CATEGORIES/NetworkingCableConnectorsJumpers"),60)) {
			KeywordUtil.logInfo("Cannot see the Networking Cable Connectors Jumpers")
			return false
		}

		KeywordUtil.logInfo("Validated the Breadcrumbs in category results page for easy navigation")

		return true
	}


	/**
	 * searchTypeAheadSuggestion()
	 * @return true if Search Type Ahead Suggestion, otherwise false
	 */

	@Keyword
	def searchTypeAheadSuggestion(def strSearchText) {

		KeywordUtil.logInfo("Validate the Search Type Ahead Suggestion")

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("ProductSearch/search tessco com textbox"), strSearchText)) {
			KeywordUtil.logInfo("Cannot see the search tessco com textbox")
			return false
		}

		if (!(new commerceCloud.utils()).delay(5)) {
			KeywordUtil.logInfo("Cannot click Suggestion First Product")
			return false
		}

		List<WebElement> lst = (new commerceCloud.utils()).ListOfWebElement(findTestObject("ProductSearch/Suggestion List"))
		if (!(lst.size()<=10)) {
			KeywordUtil.logInfo("Cannot Verfify Suggestion List size is less than 10")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("ProductSearch/Suggestion First Product"))) {
			KeywordUtil.logInfo("Cannot click Suggestion First Product")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ProductSearch/product listing price"),60)) {
			KeywordUtil.logInfo("Cannot see the product listing price")
			return false
		}

		KeywordUtil.logInfo("Validated the Search Type Ahead Suggestio")
		return true
	}

	/**
	 * AddingCartToProductDetailsList(String Search, String strWithdraw)
	 * @param Search - user should send the tessco product details
	 * @return true if Validate the The ability to search products using product attributes, otherwise false
	 */

	@Keyword
	def AddingCartToProductDetailsList(String Search, String strWithdraw) {

		KeywordUtil.logInfo("Validate the Adding a product to the cart from search confirmation")

		if (!(new commerceCloud.ProductSearch()).searchTesscoProduct(Search)) {
			KeywordUtil.logInfo("Cannot search the Tessco Product")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ProductSearch/product listing price"),60)) {
			KeywordUtil.logInfo("Cannot see the product listing price")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("ProductSearch/Product List Add to Cart"))) {
			KeywordUtil.logInfo("Cannot click Product List Add to Cart")
			return false
		}


		if (!(new commerceCloud.Cart()).CableAddToCart()) {
			KeywordUtil.logInfo("Cannot add Cable product to Add To Cart")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("AddToCart/GoToCart"))) {
			KeywordUtil.logInfo("Cannot click Product List Add to Cart")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Cart/ClearCart"),60)) {
			KeywordUtil.logInfo("Cannot see the Clear Cart")
			return false
		}

		if(strWithdraw.equalsIgnoreCase("Yes")){

			TestObject toVisible = new TestObject("Cart Name")
			String xpathVisible = "//div[contains(text(),'" + Search + "')][1]"
			TestObjectProperty propVisible = new TestObjectProperty("xpath", ConditionType.EQUALS, xpathVisible)
			toVisible.addProperty(propVisible)
			println (xpathVisible)
			
			WebDriver driver = DriverFactory.getWebDriver()
			List<WebElement> lst = driver.findElements(By.xpath(xpathVisible))
			int iSize = lst.size()
			println(iSize)
			
			if ((iSize == 0)) {
				KeywordUtil.logInfo("Can see the Saved Carts & Quotes")
				return false
			}
		}

		KeywordUtil.logInfo("Validated The Adding a product to the cart from search confirmation")
		return true
	}

	/**
	 * AddingCartToProductList() 
	 * @return true if Adding a product to the cart from the Product List Page, otherwise false
	 */

	@Keyword
	def AddingCartToProductList() {

		KeywordUtil.logInfo("Validate the Adding a product to the cart from the Product List Page")

		if (!(new commerceCloud.ProductSearch()).selectTesscoProduct(findTestObject("AntennasAndFilterProducts/Antennas and Filter Products"),findTestObject("AntennasAndFilterProducts/Antennas Filter Products View All"))) {
			KeywordUtil.logInfo("Cannot select the Antennas and Filter Products AND Antennas Filter Products View All")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ProductSearch/Select product listing price"),60)) {
			KeywordUtil.logInfo("Cannot see the Select product listing price")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("ProductSearch/Select Product List Add to Cart"))) {
			KeywordUtil.logInfo("Cannot click Select Product List Add to Cart")
			return false
		}

		if (!(new commerceCloud.Cart()).CableAddToCart()) {
			KeywordUtil.logInfo("Cannot add Cable product to Add To Cart")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("AddToCart/GoToCart"))) {
			KeywordUtil.logInfo("Cannot click Go To Cart")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Cart/ClearCart"),60)) {
			KeywordUtil.logInfo("Cannot see the Clear Cart")
			return false
		}

		KeywordUtil.logInfo("Validated The Adding a product to the cart from the Product List Page")
		return true
	}

	/**
	 * AddingCartToProductDetailsList()
	 * @return true if Adding a product to the cart from the Product Details List Page, otherwise false
	 */

	@Keyword
	def AddingCartToProductDetailsList() {

		KeywordUtil.logInfo("Validate the Adding a product to the cart from the Product Details List Page")

		if (!(new commerceCloud.ProductSearch()).selectTesscoProduct(findTestObject("AntennasAndFilterProducts/Antennas and Filter Products"),findTestObject("AntennasAndFilterProducts/Antennas Filter Products View All"))) {
			KeywordUtil.logInfo("Cannot select the Antennas and Filter Products AND Antennas Filter Products View All")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ProductSearch/Select product listing price"),60)) {
			KeywordUtil.logInfo("Cannot see the Select product listing price")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("ProductSearch/Select product list name"))) {
			KeywordUtil.logInfo("Cannot click Select product list name")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ProductSearchDetails/Add To Cart Button"),60)) {
			KeywordUtil.logInfo("Cannot see the Add To Cart Button")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("ProductSearchDetails/Add To Cart Button"))) {
			KeywordUtil.logInfo("Cannot click Add To Cart Button")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("AddToCart/GoToCart"))) {
			KeywordUtil.logInfo("Cannot click Go To Cart")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Cart/ClearCart"),60)) {
			KeywordUtil.logInfo("Cannot see the Clear Cart")
			return false
		}

		KeywordUtil.logInfo("Validated The Adding a product to the cart from the Product Details List Page")
		return true
	}

	/**
	 * ContnueShopping()
	 * @return true if Adding a product to the cart from the Product Details List Page, otherwise false
	 */

	@Keyword
	def ContnueShopping() {

		KeywordUtil.logInfo("Validate the confirmation modal showing the product image Manufacturer name, product description, quantity added and price")

		if (!(new commerceCloud.ProductSearch()).selectTesscoProduct(findTestObject("AntennasAndFilterProducts/Antennas and Filter Products"),findTestObject("AntennasAndFilterProducts/Antennas Filter Products View All"))) {
			KeywordUtil.logInfo("Cannot select the Antennas and Filter Products AND Antennas Filter Products View All")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ProductSearch/Select product listing price"),60)) {
			KeywordUtil.logInfo("Cannot see the Select product listing price")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("ProductSearch/Select Product List Add to Cart"))) {
			KeywordUtil.logInfo("Cannot click Select Product List Add to Cart")
			return false
		}


		if (!(new commerceCloud.Cart()).CableAddToCart()) {
			KeywordUtil.logInfo("Cannot add Cable product to Add To Cart")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("AddToCart/manufacturer"),60)) {
			KeywordUtil.logInfo("Cannot see the manufacturer")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("AddToCart/name"),1)) {
			KeywordUtil.logInfo("Cannot see the name")
			return false
		}

		/*if (!(new commerceCloud.utils()).verifyPresent(findTestObject("AddToCart/Price"),1)) {
		 KeywordUtil.logInfo("Cannot see the Price")
		 return false
		 }*/

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("AddToCart/ProductName"),1)) {
			KeywordUtil.logInfo("Cannot see the Product Name")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("AddToCart/qty"),1)) {
			KeywordUtil.logInfo("Cannot see the qty")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("AddToCart/thumb"),1)) {
			KeywordUtil.logInfo("Cannot see the thumb")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("AddToCart/ContinueShopping"))) {
			KeywordUtil.logInfo("Cannot click Continue Shopping")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("AddToCart/Antennas Filter Products"),60)) {
			KeywordUtil.logInfo("Cannot see the Antennas Filter Products")
			return false
		}

		KeywordUtil.logInfo("Validated confirmation modal showing the product image Manufacturer name, product description, quantity added and price")
		return true
	}


	/**
	 * ClearCart()
	 * @return true if user removed card, otherwise false
	 */

	@Keyword
	def ClearCart() {

		KeywordUtil.logInfo("Validate the user removed card")

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Cart/Cart"),60)) {
			KeywordUtil.logInfo("Cannot see the Cart button")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/Cart"))) {
			KeywordUtil.logInfo("Cannot click Cart button")
			return false
		}

		if (!(new commerceCloud.utils().delay(5))) {
			KeywordUtil.logInfo("Cannot wait a sec")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Cart/ImportCSV"),60)) {
			KeywordUtil.logInfo("Cannot see the Import CSV")
			return false
		} 
		String strBeforeUpdate =  (new commerceCloud.utils()).sReturnText(findTestObject("Cart/ClearCartAndEmpty"))
		println(strBeforeUpdate)
		if(!(strBeforeUpdate.contains("Your Cart is empty"))){

			if (!(new commerceCloud.utils().scrollToElement(findTestObject("Cart/ClearCart")))) {
				KeywordUtil.logInfo("Cannot scroll the Clear Cart")
				return false
			}
			
			if (!(new commerceCloud.utils()).click(findTestObject("Cart/ClearCart"))) {
				KeywordUtil.logInfo("Cannot click Clear Cart")
				return false
			}
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Cart/CartIsEmpty"),60)) {
			KeywordUtil.logInfo("Cannot see the Your Cart is empty")
			return false
		}
		
		KeywordUtil.logInfo("Validated the user can see the empty card")
		return true
	}


	/**
	 * ClearCartWithValidation()
	 * @return true if user removed card, otherwise false
	 */

	@Keyword
	def ClearCartWithValidation() {

		KeywordUtil.logInfo("Validate the user removed card")

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Cart/Cart"),60)) {
			KeywordUtil.logInfo("Cannot see the Cart button")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/Cart"))) {
			KeywordUtil.logInfo("Cannot click Cart button")
			return false
		}

		if (!(new commerceCloud.utils().delay(5))) {
			KeywordUtil.logInfo("Cannot wait a sec")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Cart/YourCart"),60)) {
			KeywordUtil.logInfo("Cannot see the Your Cart")
			return false
		}

		if (!(new commerceCloud.utils().scrollToElement(findTestObject("Cart/ClearCart")))) {
			KeywordUtil.logInfo("Cannot scroll the Clear Cart")
			return false
		}

		String strBeforeUpdate =  (new commerceCloud.utils()).sReturnText(findTestObject("OrderSummary/OrderTotal"))

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/UpdateEntryQuantityPlusFirst"))) {
			KeywordUtil.logInfo("Cannot click Update Entry Quantity Plus First button")
			return false
		}

		if (!(new commerceCloud.utils()).delay(5)) {
			KeywordUtil.logInfo("Cannot wait a sec")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Cart/YourCart"),60)) {
			KeywordUtil.logInfo("Cannot see the Your Cart")
			return false
		}

		if (!(new commerceCloud.utils().scrollToElement(findTestObject("Cart/ClearCart")))) {
			KeywordUtil.logInfo("Cannot scroll the Clear Cart")
			return false
		}


		String strAfterUpdate =  (new commerceCloud.utils()).sReturnText(findTestObject("OrderSummary/OrderTotal"))

		if ((strBeforeUpdate.equalsIgnoreCase(strAfterUpdate))) {
			KeywordUtil.logInfo("Cannot increase the product qty")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/UpdateEntryQuantityMinusFirst"))) {
			KeywordUtil.logInfo("Cannot click Update Entry Quantity Minus First button")
			return false
		}

		if (!(new commerceCloud.utils()).delay(5)) {
			KeywordUtil.logInfo("Cannot wait a sec")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Cart/YourCart"),60)) {
			KeywordUtil.logInfo("Cannot see the Your Cart")
			return false
		}

		if (!(new commerceCloud.utils().scrollToElement(findTestObject("Cart/ClearCart")))) {
			KeywordUtil.logInfo("Cannot scroll the Clear Cart")
			return false
		}

		String strAct =  (new commerceCloud.utils()).sReturnText(findTestObject("OrderSummary/OrderTotal"))

		if (!(strBeforeUpdate.equalsIgnoreCase(strAct))) {
			KeywordUtil.logInfo("Cannot Decrease the product qty")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Cart/YourCart"),60)) {
			KeywordUtil.logInfo("Cannot see the Your Cart")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/RemoveFirstProductInCart"))) {
			KeywordUtil.logInfo("Cannot Remove the First Product In Cart")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Cart/ClearCart"),60)) {
			KeywordUtil.logInfo("Cannot see the Checkout button")
			return false
		}

		if (!(new commerceCloud.utils().scrollToElement(findTestObject("Cart/ClearCart")))) {
			KeywordUtil.logInfo("Cannot scroll the Clear Cart")
			return false
		}


		String strExp =  (new commerceCloud.utils()).sReturnText(findTestObject("OrderSummary/OrderTotal"))

		if (strAct.equalsIgnoreCase(strExp)) {
			KeywordUtil.logInfo("Cannot verify the number of products and total price reflects the removal")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Cart/ClearCart"),30)) {
			KeywordUtil.logInfo("Cannot see the Clear Cart")
			if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Cart/CartIsEmpty"),60)) {
				KeywordUtil.logInfo("Cannot see the Cart Is Empty")
				return false
			}

			if (!(new commerceCloud.utils()).verifyElementDidNotDisplayed(findTestObject("OrderSummary/Checkout"),60)) {
				KeywordUtil.logInfo("Can see the Order Summary")
				return false
			}
		}else {
			if (!(new commerceCloud.utils()).scrollToElement(findTestObject("Cart/ClearCart"))) {
				KeywordUtil.logInfo("Cannot scroll Clear Cart")
				return false
			}

			if (!(new commerceCloud.utils()).click(findTestObject("Cart/ClearCart"))) {
				KeywordUtil.logInfo("Cannot click Clear Cart")
				return false
			}

			if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Cart/CartIsEmpty"),60)) {
				KeywordUtil.logInfo("Cannot see the Cart Is Empty")
				return false
			}

			if (!(new commerceCloud.utils()).verifyElementDidNotDisplayed(findTestObject("OrderSummary/Checkout"),60)) {
				KeywordUtil.logInfo("Can see the Order Summary")
				return false
			}
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/ContinueShopping"))) {
			KeywordUtil.logInfo("Cannot click Continue Shopping")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Cart/AntennasFilterProducts"),60)) {
			KeywordUtil.logInfo("Cannot see the Antennas Filter Products")
			return false
		}

		if (!(new commerceCloud.utils()).compareListSize(findTestObject("Cart/CartItemsTotal"),0)) {
			KeywordUtil.logInfo("Cannot Verify minicart amount is removed from header")
			return false
		}

		KeywordUtil.logInfo("Validated the user removed card")
		return true
	}


	/**
	 * AddingProductToCart(String Search, String strYesOrNo)
	 * @param Search - user should send the tessco product details
	 * @param strYesOrNo - Yes Or No to validate related product
	 * @return true if Validate the The ability to search products using product attributes, otherwise false
	 */

	@Keyword
	def AddingProductToCart(String Search, String strYesOrNo) {

		KeywordUtil.logInfo("Validate the no Related Products and Related Products ")

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Product/FeaturedProduct"),60)) {
			KeywordUtil.logInfo("Cannot see the Featured Product")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Product/FeaturedProductFirst"),60)) {
			KeywordUtil.logInfo("Cannot see the Featured Product First")
			return false
		}

		if (!(new commerceCloud.ProductSearch()).searchTesscoProduct(Search)) {
			KeywordUtil.logInfo("Cannot search the Tessco Product")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ProductSearch/product list name"),60)) {
			KeywordUtil.logInfo("Cannot see the product list name")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("ProductSearch/product list name"))) {
			KeywordUtil.logInfo("Cannot click product list name")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ProductSearchDetails/Add To Cart Button"),60)) {
			KeywordUtil.logInfo("Cannot see the Add To Cart Button")
			return false
		}

		if(strYesOrNo.equalsIgnoreCase("Yes")){

			if (!(new commerceCloud.utils()).scrollToElement(findTestObject("Product/RelatedProducts"))) {
				KeywordUtil.logInfo("Cannot scroll To Related Products")
				return false
			}

			if (!(new commerceCloud.utils()).click(findTestObject("Product/RelatedProductsFirst"))) {
				KeywordUtil.logInfo("Cannot click Related Products First Product")
				return false
			}

			if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ProductSearchDetails/Add To Cart Button"),60)) {
				KeywordUtil.logInfo("Cannot see the Add To Cart Button")
				return false
			}

			if (!(new commerceCloud.utils()).back()) {
				KeywordUtil.logInfo("Cannot come back")
				return false
			}
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ProductSearchDetails/Add To Cart Button"),60)) {
			KeywordUtil.logInfo("Cannot see the Add To Cart Button")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("ProductSearchDetails/Add To Cart Button"))) {
			KeywordUtil.logInfo("Cannot click Add To Cart Button")
			return false
		}


		if (!(new commerceCloud.Cart()).CableAddToCart()) {
			KeywordUtil.logInfo("Cannot add Cable product to Add To Cart")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("AddToCart/GoToCart"))) {
			KeywordUtil.logInfo("Cannot click Go To Cart")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Cart/ClearCart"),60)) {
			KeywordUtil.logInfo("Cannot see the Clear Cart")
			return false
		}

		TestObject toProduct = new TestObject("SKU No")
		String xpathProduct = "//div[contains(text(),'" + Search + "')]"
		TestObjectProperty propProduct = new TestObjectProperty("xpath", ConditionType.EQUALS, xpathProduct)
		toProduct.addProperty(propProduct)

		if(!(new commerceCloud.utils()).verifyPresent(toProduct,60)){
			KeywordUtil.logInfo("Cannot see the SKU No")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/ContinueShopping"))) {
			KeywordUtil.logInfo("Cannot click the Continue Shopping")
			return false
		}

		KeywordUtil.logInfo("Validated The no Related Products and Related Products ")
		return true
	}

	/**
	 * MobileAccessoryFinder(String strBrandName, String strMobileModel)
	 * @return true if Mobile Accessory Finder - Brand model page, otherwise false
	 */

	@Keyword
	def MobileAccessoryFinder(String strBrandName, String strMobileModel) {

		KeywordUtil.logInfo("Validate the Mobile Accessory Finder - Brand model page")

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Tools/Tools"),60)) {
			KeywordUtil.logInfo("Cannot see the Tools")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Tools/Tools"))) {
			KeywordUtil.logInfo("Cannot click Tools")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Tools/MobileAccessoryFinder"),60)) {
			KeywordUtil.logInfo("Cannot see the Mobile Accessory Finder")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Tools/MobileAccessoryFinder"))) {
			KeywordUtil.logInfo("Cannot click Mobile Accessory Finder")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Tools/MobileAccessoryFinderText"),60)) {
			KeywordUtil.logInfo("Cannot see the Mobile Accessory Finder Text")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Tools/MobileAccessoryFinderDescription"),60)) {
			KeywordUtil.logInfo("Cannot see the Mobile Accessory Finder Descriptiont")
			return false
		}

		if(strBrandName.equalsIgnoreCase("Apple")){
			if (!(new commerceCloud.utils()).click(findTestObject("Tools/Apple"))) {
				KeywordUtil.logInfo("Cannot click Apple Brand")
				return false
			}
		}else if(strBrandName.equalsIgnoreCase("Blackberry")){
			if (!(new commerceCloud.utils()).click(findTestObject("Tools/Blackberry"))) {
				KeywordUtil.logInfo("Cannot click Blackberry Brand")
				return false
			}
		}else if(strBrandName.equalsIgnoreCase("Samsung")){
			if (!(new commerceCloud.utils()).click(findTestObject("Tools/Samsung"))) {
				KeywordUtil.logInfo("Cannot click Samsung Brand")
				return false
			}
		}else if(strBrandName.equalsIgnoreCase("Asus")){
			if (!(new commerceCloud.utils()).click(findTestObject("Tools/Asus"))) {
				KeywordUtil.logInfo("Cannot click Asus Brand")
				return false
			}
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Tools/ChangeBrand"),60)) {
			KeywordUtil.logInfo("Cannot see the Change Brand")
			return false
		}

		List<WebElement> lst = (new commerceCloud.utils()).ListOfWebElement(findTestObject("Tools/YourAppleDeviceList"))
		for (var in lst) {
			String strVar = var.getText().trim()
			if(strVar.equalsIgnoreCase(strMobileModel)){
				var.click()
				KeywordUtil.logInfo("Can click the : " + strMobileModel)
				break
			}else if(strVar.equalsIgnoreCase(strMobileModel)){
				var.click()
				KeywordUtil.logInfo("Can click the : " + strMobileModel)
				break
			}else if(strVar.equalsIgnoreCase(strMobileModel)){
				var.click()
				KeywordUtil.logInfo("Can click the : " + strMobileModel)
				break
			}else if(strVar.equalsIgnoreCase(strMobileModel)){
				var.click()
				KeywordUtil.logInfo("Can click the : " + strMobileModel)
				break
			}else if(strVar.equalsIgnoreCase(strMobileModel)){
				var.click()
				KeywordUtil.logInfo("Can click the : " + strMobileModel)
				break
			}else if(strVar.equalsIgnoreCase(strMobileModel)){
				var.click()
				KeywordUtil.logInfo("Can click the : " + strMobileModel)
				break
			}else if(strVar.equalsIgnoreCase(strMobileModel)){
				var.click()
				KeywordUtil.logInfo("Can click the : " + strMobileModel)
				break
			}else if(strVar.equalsIgnoreCase(strMobileModel)){
				var.click()
				KeywordUtil.logInfo("Can click the : " + strMobileModel)
				break
			}else if(strVar.equalsIgnoreCase(strMobileModel)){
				var.click()
				KeywordUtil.logInfo("Can click the : " + strMobileModel)
				break
			}else if(strVar.equalsIgnoreCase(strMobileModel)){
				var.click()
				KeywordUtil.logInfo("Can click the : " + strMobileModel)
				break
			}else if(strVar.equalsIgnoreCase(strMobileModel)){
				var.click()
				KeywordUtil.logInfo("Can click the : " + strMobileModel)
				break
			}else if(strVar.equalsIgnoreCase(strMobileModel)){
				var.click()
				KeywordUtil.logInfo("Can click the : " + strMobileModel)
				break
			}else if(strVar.equalsIgnoreCase(strMobileModel)){
				var.click()
				KeywordUtil.logInfo("Can click the : " + strMobileModel)
				break
			}
		}
		KeywordUtil.logInfo("Can click the : " + strMobileModel)

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ProductSearch/product list name"),60)) {
			KeywordUtil.logInfo("Cannot see the product list name")
			return false
		}

		KeywordUtil.logInfo("Validated the Mobile Accessory Finder - Brand model page")
		return true
	}


	/**
	 * MobileAccessorySearch(String strSearchBrandName, String strBrandName)
	 * @return true if Mobile Accessory search - Brand model page, otherwise false
	 */

	@Keyword
	def MobileAccessorySearch(String strSearchBrandName, String strBrandName) {

		KeywordUtil.logInfo("Validate the Mobile Accessory search - Brand model page")

		if (!(new commerceCloud.ProductSearch()).searchTesscoProduct(strSearchBrandName)) {
			KeywordUtil.logInfo("Cannot search the Tessco Product")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ProductSearch/product listing price"),60)) {
			KeywordUtil.logInfo("Cannot see the product listing price")
			return false
		}

		if (!(new commerceCloud.utils()).scrollToElement(findTestObject("Tools/CompatibleWith"))) {
			KeywordUtil.logInfo("Cannot scroll Compatible With")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Tools/CompatibleWith"),60)) {
			KeywordUtil.logInfo("Cannot see the Compatible With")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Tools/Compatible_with_plus"))) {
			KeywordUtil.logInfo("Cannot click Compatible with plus")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Tools/Compatible_Apple_down"),60)) {
			KeywordUtil.logInfo("Cannot see the Compatible Apple down")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Tools/Compatible_Apple_down"))) {
			KeywordUtil.logInfo("Cannot click Compatible Apple down")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Tools/Compatible_iphone11"),60)) {
			KeywordUtil.logInfo("Cannot see the Compatible iphone 11")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Tools/Compatible_iphone11"))) {
			KeywordUtil.logInfo("Cannot click Compatible iphone 11")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Tools/CompatibleWith"),60)) {
			KeywordUtil.logInfo("Cannot see the Compatible With")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Tools/Compatible_iphone7pro"))) {
			KeywordUtil.logInfo("Cannot click Compatible iphone 7 pro")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Tools/CompatibleWith"),60)) {
			KeywordUtil.logInfo("Cannot see the Compatible With")
			return false
		}

		boolean blnBrand = false
		List<WebElement> lst = (new commerceCloud.utils()).ListOfWebElement(findTestObject("Tools/ProductNameList"))
		for (var in lst) {
			String strVar = var.getText().trim()
			if(strVar.contains(strBrandName)){
				blnBrand = true
				break
			}
		}

		if (!(blnBrand)) {
			KeywordUtil.logInfo("Cannot Verify the products for the correct brand populate the results panel to the right : " + strBrandName)
			return false
		}

		KeywordUtil.logInfo("Validated the Mobile Accessory search - Brand model page")
		return true
	}
}
