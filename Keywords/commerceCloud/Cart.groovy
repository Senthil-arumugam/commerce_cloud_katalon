package commerceCloud

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.text.SimpleDateFormat

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.stringtemplate.v4.compiler.STParser.ifstat_return

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.testobject.TestObjectProperty
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.driver.DriverFactory

import internal.GlobalVariable

class Cart {


	/**
	 * getCartName(String name)
	 * @param teamType - is the name of the team to target... send null if using system created Date or specify prefix in global variables
	 * @return username
	 */
	@Keyword
	def String getCartName(String name) {

		String strCartName = ""
		if((name.equalsIgnoreCase(""))){

			def date = new Date()
			def sdf = new SimpleDateFormat("MMddyyyy") // HH:mm:ss

			strCartName = GlobalVariable.GlobalSavedCartName + ""+sdf.format(date)
			KeywordUtil.logInfo("CartName = " + strCartName)

		} else {

			def date = new Date()
			def sdf = new SimpleDateFormat("MMddyyyy") // HH:mm:ss

			strCartName = name + ""+sdf.format(date)
			KeywordUtil.logInfo("CartName = " + strCartName)

		}

		return strCartName.toLowerCase()
	}

	/**
	 * getUserName(String name)
	 * @param name -> user name type
	 * @return username
	 */
	@Keyword
	def String getUserName(String name) {

		String strUserName = ""
		if((name.equalsIgnoreCase(""))){

			def date = new Date()
			def sdf = new SimpleDateFormat("MMddyyyy") // HH:mm:ss

			strUserName = GlobalVariable.GlobalUserName //+ "@tessco.com"
			KeywordUtil.logInfo("UserName = " + strUserName)

		} else {

			def date = new Date()
			def sdf = new SimpleDateFormat("MMddyyyy") // HH:mm:ss

			strUserName = name + "@tessco.com"
			KeywordUtil.logInfo("UserName = " + strUserName)

		}

		return strUserName.toLowerCase()
	}

	/**
	 * getRegistrationUserName(String name)
	 * @param name -> user name type
	 * @return username
	 */
	@Keyword
	def String getRegistrationUserName(String name) {

		String strUserName = ""
		if((name.equalsIgnoreCase(""))){

			def date = new Date()
			def sdf = new SimpleDateFormat("MMddyyyy") // HH:mm:ss

			strUserName = GlobalVariable.GlobalRegistrationUserName + "@tessco.com"
			KeywordUtil.logInfo("UserName = " + strUserName)

		} else {

			def date = new Date()
			def sdf = new SimpleDateFormat("MMddyyyy") // HH:mm:ss

			strUserName = name + "@tessco.com"
			KeywordUtil.logInfo("UserName = " + strUserName)

		}

		return strUserName.toLowerCase()
	}


	/**
	 * addOneProductToCart(def Search)
	 * @param Search - user should send the tessco product details
	 * @return true if add one product to cart, otherwise false
	 */

	@Keyword
	def addOneProductToCart(def Search) {

		KeywordUtil.logInfo("Validate the add one product to cart")

		if (!(new commerceCloud.ProductSearch()).searchTesscoProduct(Search)) {
			KeywordUtil.logInfo("Cannot search the Tessco Product")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("ProductSearch/product listing price"),60)) {
			KeywordUtil.logInfo("Cannot see the product listing price")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("ProductSearch/Product List Add to Cart"))) {
			KeywordUtil.logInfo("Cannot click search tessco com button")
			return false
		}

		if (!(new commerceCloud.Cart()).CableAddToCart()) {
			KeywordUtil.logInfo("Cannot add Cable product to Add To Cart")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("AddToCart/GoToCart"))) {
			KeywordUtil.logInfo("Cannot click Go To Cart")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Cart/ClearCart"),60)) {
			KeywordUtil.logInfo("Cannot see the Clear Cart")
			return false
		}

		KeywordUtil.logInfo("Validate the added one product to cart")
		return true
	}



	/**
	 * addProductToCart(def Search)
	 * @param Search - user should send the tessco product details
	 * @return true if add a product to the cart if there is not a product(s) already in the cart, otherwise false
	 */

	@Keyword
	def addProductToCart(def Search) {

		KeywordUtil.logInfo("Validate the user able to add search product and added product to cart")

		if (!(new commerceCloud.ProductSearch()).searchTesscoProduct(Search)) {
			KeywordUtil.logInfo("Cannot search the Tessco Product")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("ProductSearch/product listing price"),60)) {
			KeywordUtil.logInfo("Cannot see the product listing price")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("ProductSearch/Product List Add to Cart"))) {
			KeywordUtil.logInfo("Cannot click search tessco com button")
			return false
		}


		if (!(new commerceCloud.Cart()).CableAddToCart()) {
			KeywordUtil.logInfo("Cannot add Cable product to Add To Cart")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("AddToCart/ContinueShopping"))) {
			KeywordUtil.logInfo("Cannot click Continue Shopping")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("ProductSearch/product listing price"),60)) {
			KeywordUtil.logInfo("Cannot see the product listing price")
			return false
		}

		if (!(new commerceCloud.utils()).scrollToElement(findTestObject("ProductSearch/Product List Add to Cart Two"))) {
			KeywordUtil.logInfo("Cannot scroll search tessco com button")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("ProductSearch/Product List Add to Cart Two"))) {
			KeywordUtil.logInfo("Cannot click search tessco com button")
			return false
		}


		if (!(new commerceCloud.Cart()).CableAddToCart()) {
			KeywordUtil.logInfo("Cannot add Cable product to Add To Cart")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("AddToCart/RemoveAddToCart"))) {
			KeywordUtil.logInfo("Cannot click Remove Add To Cart")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("ProductSearch/product listing price"),60)) {
			KeywordUtil.logInfo("Cannot see the product listing price")
			return false
		}

		if (!(new commerceCloud.utils()).scrollToElement(findTestObject("ProductSearch/Product List Add to Cart Three"))) {
			KeywordUtil.logInfo("Cannot scroll the Product List Add to Cart Three")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("ProductSearch/Product List Add to Cart Three"))) {
			KeywordUtil.logInfo("Cannot click search tessco com button")
			return false
		}


		if (!(new commerceCloud.Cart()).CableAddToCart()) {
			KeywordUtil.logInfo("Cannot add Cable product to Add To Cart")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("AddToCart/GoToCart"))) {
			KeywordUtil.logInfo("Cannot click Go To Cart")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Cart/ClearCart"),60)) {
			KeywordUtil.logInfo("Cannot see the Clear Cart")
			return false
		}

		String strAct =  (new commerceCloud.utils()).sReturnText(findTestObject("Cart/CartItemsTotal"))

		int iAct = Integer.parseInt(strAct)
		if (!(new commerceCloud.utils()).compareListSize(findTestObject("Cart/CartProductSize"),iAct)) {
			KeywordUtil.logInfo("Cannot compare the cart size and displayed product list")
			return false
		}


		KeywordUtil.logInfo("Validate the user able to add search product and added product to cart")
		return true
	}


	/**
	 * addCableProductToCart(def Search)
	 * @param Search - user should send the tessco product details
	 * @return true if add a cable product to the cart if there is not a product(s) already in the cart, otherwise false
	 */

	@Keyword
	def addCableProductToCart(def Search) {

		KeywordUtil.logInfo("Validate the user able to add search product and added product to cart")

		if (!(new commerceCloud.ProductSearch()).searchTesscoProduct(Search)) {
			KeywordUtil.logInfo("Cannot search the Tessco Product")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("ProductSearch/Cable Product List Add to Cart"),60)) {
			KeywordUtil.logInfo("Cannot see the Cable Product List Add to Cart")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("ProductSearch/Cable Product List Add to Cart"))) {
			KeywordUtil.logInfo("Cannot click Cable Product List Add to Cart button")
			return false
		}


		if (!(new commerceCloud.Cart()).OnlyCableAddToCart()) {
			KeywordUtil.logInfo("Cannot add Cable product to Add To Cart")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("AddToCart/GoToCart"))) {
			KeywordUtil.logInfo("Cannot click Go To Cart")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Cart/ClearCart"),60)) {
			KeywordUtil.logInfo("Cannot see the Clear Cart")
			return false
		}

		String strAct =  (new commerceCloud.utils()).sReturnText(findTestObject("Cart/CartItemsTotal"))

		int iAct = Integer.parseInt(strAct)
		if (!(new commerceCloud.utils()).compareListSize(findTestObject("Cart/CartProductSize"),iAct)) {
			KeywordUtil.logInfo("Cannot compare the cart size and displayed product list")
			return false
		}


		KeywordUtil.logInfo("Validate the user able to add search product and added product to cart")
		return true
	}

	/**
	 * saveCart(def Search)
	 * @param Search - user should send the tessco product details
	 * @return true if Saved Carts & Quotes, otherwise false
	 */

	@Keyword
	def saveCart(def strCartName) {

		KeywordUtil.logInfo("Validate Saved Carts & Quotes")

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Cart/SaveCart"),60)) {
			KeywordUtil.logInfo("Cannot see the Save Cart")
			return false
		}


		if (!(new commerceCloud.utils()).click(findTestObject("Cart/SaveCart"))) {
			KeywordUtil.logInfo("Cannot click Save Cart")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Cart/saveedCancelSaveCartButton"),60)) {
			KeywordUtil.logInfo("Cannot see the saveed Cancel Save Cart Button")
			return false
		}


		if (!(new commerceCloud.utils()).click(findTestObject("Cart/saveedCancelSaveCartButton"))) {
			KeywordUtil.logInfo("Cannot click saveed Cancel Save Cart Button")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Cart/SaveCart"),60)) {
			KeywordUtil.logInfo("Cannot see the Save Cart")
			return false
		}


		if (!(new commerceCloud.utils()).click(findTestObject("Cart/SaveCart"))) {
			KeywordUtil.logInfo("Cannot click Save Cart")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Cart/saveCartName"),60)) {
			KeywordUtil.logInfo("Cannot see the save  Cart Name")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("Cart/saveCartName"), strCartName)) {
			KeywordUtil.logInfo("Cannot enter save  Cart Name")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("Cart/saveCartDescription"), "Automation Testing Purpose Only")) {
			KeywordUtil.logInfo("Cannot enter save Cart Description")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/saveCartButton"))) {
			KeywordUtil.logInfo("Cannot click save Cart Button")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Cart/savedCartDescription"),60)) {
			KeywordUtil.logInfo("Cannot see the saved Cart Description Text")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Cart/savedCartId"),1)) {
			KeywordUtil.logInfo("Cannot see the saved Cart Id")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Cart/savedCartName"),1)) {
			KeywordUtil.logInfo("Cannot see the saved Cart Name")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Cart/savedReturnCartButton"),1)) {
			KeywordUtil.logInfo("Cannot see the saved Return Cart Button")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/savedCartQuotesButton"))) {
			KeywordUtil.logInfo("Cannot click saved Cart Quotes Button")
			return false
		}

		TestObject toVisible = new TestObject("paginationNext")
		String xpathVisible = "//span[contains(text(),'" + strCartName + "')]/following::td/a[contains(text(),'Remove')][1]"
		TestObjectProperty propVisible = new TestObjectProperty("xpath", ConditionType.EQUALS, xpathVisible)
		toVisible.addProperty(propVisible)

		if (!(new commerceCloud.utils()).verifyPresent(toVisible,60)) {
			KeywordUtil.logInfo("Cannot see the Saved Carts & Quotes")
			return false
		}

		/*if (!(new commerceCloud.utils()).verifyPresent(findTestObject("SavedCartsQuotes/AddUserAccountList"),1)) {
		 KeywordUtil.logInfo("Cannot see the Add User Account List Button")
		 return false
		 }
		 if (!(new commerceCloud.utils()).verifyPresent(findTestObject("SavedCartsQuotes/ApplyFinancingList"),1)) {
		 KeywordUtil.logInfo("Cannot see the Apply Financing List Button")
		 return false
		 }
		 if (!(new commerceCloud.utils()).verifyPresent(findTestObject("SavedCartsQuotes/ChangePasswordList"),1)) {
		 KeywordUtil.logInfo("Cannot see the Change Password List Button")
		 return false
		 }
		 if (!(new commerceCloud.utils()).verifyPresent(findTestObject("SavedCartsQuotes/EditAccountList"),1)) {
		 KeywordUtil.logInfo("Cannot see the Edit Account List Button")
		 return false
		 }
		 if (!(new commerceCloud.utils()).verifyPresent(findTestObject("SavedCartsQuotes/InvoicesList"),1)) {
		 KeywordUtil.logInfo("Cannot see the Invoices List Button")
		 return false
		 }
		 if (!(new commerceCloud.utils()).verifyPresent(findTestObject("SavedCartsQuotes/MyAccountList"),1)) {
		 KeywordUtil.logInfo("Cannot see the My Account List Button")
		 return false
		 }
		 if (!(new commerceCloud.utils()).verifyPresent(findTestObject("SavedCartsQuotes/OrderHistoryList"),1)) {
		 KeywordUtil.logInfo("Cannot see the Order History List Button")
		 return false
		 }
		 if (!(new commerceCloud.utils()).verifyPresent(findTestObject("SavedCartsQuotes/ReturnsList"),1)) {
		 KeywordUtil.logInfo("Cannot see the Returns List Button")
		 return false
		 }
		 if (!(new commerceCloud.utils()).verifyPresent(findTestObject("SavedCartsQuotes/SavedCartsQuotesList"),1)) {
		 KeywordUtil.logInfo("Cannot see the Saved Carts QuotesList Button")
		 return false
		 }*/
		KeywordUtil.logInfo("Validate the Saved Carts & Quotes")
		return true
	}


	/**
	 * CableProductTOCart(String strCartName)
	 * @param Search - user should send the tessco product details
	 * @return true if Saved Carts & Quotes, otherwise false
	 */

	@Keyword
	def CableProductTOCart(String strCartName) {

		KeywordUtil.logInfo("Validate Saved Carts & Quotes")

		String strQuoteNoValues = ""

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Cart/CableItemConfigurations"),60)) {
			KeywordUtil.logInfo("Cannot see the Item Configurations for Cable Product")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/CableItemConfigurations"))) {
			KeywordUtil.logInfo("Cannot click the Item Configurations for Cable Product")
			return false
		}

		if (!(new commerceCloud.Cart()).OnlyCableUpdateToCart()) {
			KeywordUtil.logInfo("Cannot Modify Cable product to Add To Cart")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsEnabled(findTestObject("Cart/CheckoutTop"),60)) {
			KeywordUtil.logInfo("Cannot see the Checkout Top button")
			return false
		}

		ArrayList<WebElement> strBeforeOrderTotalList =  (new commerceCloud.utils()).ListOfWebElement(findTestObject("Cart/CableOrderTotal"))
		String strBeforeOrderTotal = strBeforeOrderTotalList.get(0).getText()

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/CableUpdateQuantityPlus"))) {
			KeywordUtil.logInfo("Cannot click the Cable Update Quantity Plus")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsEnabled(findTestObject("Cart/CheckoutTop"),60)) {
			KeywordUtil.logInfo("Cannot see the Checkout Top button")
			return false
		}

		ArrayList<WebElement> strAfterOrderTotalList =  (new commerceCloud.utils()).ListOfWebElement(findTestObject("Cart/CableOrderTotal"))
		String strAfterOrderTotal = strAfterOrderTotalList.get(0).getText()

		if  (strBeforeOrderTotal.equalsIgnoreCase(strAfterOrderTotal)) {
			KeywordUtil.logInfo("Cannot change the Order Total under cart page")
			return false
		}

		ArrayList<WebElement> strBeforeOrderTotalMinusList =  (new commerceCloud.utils()).ListOfWebElement(findTestObject("Cart/CableOrderTotal"))
		String strBeforeOrderMinusTotal = strBeforeOrderTotalMinusList.get(0).getText()

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/CableUpdateQuantityMinus"))) {
			KeywordUtil.logInfo("Cannot click the Cable Update Quantity Minus")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsEnabled(findTestObject("Cart/CheckoutTop"),60)) {
			KeywordUtil.logInfo("Cannot see the Checkout Top button")
			return false
		}

		ArrayList<WebElement> strAfterOrderTotalMinusList =  (new commerceCloud.utils()).ListOfWebElement(findTestObject("Cart/CableOrderTotal"))
		String strAfterOrderMinusTotal = strAfterOrderTotalMinusList.get(0).getText()

		if  (strBeforeOrderMinusTotal.equalsIgnoreCase(strAfterOrderMinusTotal)) {
			KeywordUtil.logInfo("Cannot change the Order Total under cart page")
			return false
		}


		KeywordUtil.logInfo("Validate the Saved Carts & Quotes")
		return true
	}

	/**
	 * saveCartAndReturnCart(def Search)
	 * @param strCartName - user should pass cart name
	 * @return true if The ability to save my current cart for future use so I can create a new blank cart, otherwise false
	 */

	@Keyword
	def saveCartAndReturnCart(def strCartName) {

		KeywordUtil.logInfo("Validate user should add product to cart, save cart and I can create a new blank cart")

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Cart/SaveCart"),60)) {
			KeywordUtil.logInfo("Cannot see the Save Cart")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/SaveCart"))) {
			KeywordUtil.logInfo("Cannot click Save Cart")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Cart/saveCartName"),60)) {
			KeywordUtil.logInfo("Cannot see the save  Cart Name")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("Cart/saveCartName"), strCartName)) {
			KeywordUtil.logInfo("Cannot enter save  Cart Name")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("Cart/saveCartDescription"), "Automation Testing Purpose Only")) {
			KeywordUtil.logInfo("Cannot enter save Cart Description")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/saveCartButton"))) {
			KeywordUtil.logInfo("Cannot click save Cart Button")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/savedReturnCartButton"))) {
			KeywordUtil.logInfo("Cannot click saved Return Cart Button")
			return false
		}

		TestObject toVisible = new TestObject("Cart Name")
		String xpathVisible = "//div[contains(@class,'Component')]//div[contains(text(),'" + strCartName + "')]"
		TestObjectProperty propVisible = new TestObjectProperty("xpath", ConditionType.EQUALS, xpathVisible)
		toVisible.addProperty(propVisible)

		if (!(new commerceCloud.utils()).verifyPresent(toVisible,60)) {
			KeywordUtil.logInfo("Cannot see the Saved Carts name")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Cart/NewCart"),1)) {
			KeywordUtil.logInfo("Cannot see the New Cart Button")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/DeleteSavedCart"))) {
			KeywordUtil.logInfo("Cannot click saved Delete Saved Cart Button")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Cart/CartIsEmpty"),60)) {
			KeywordUtil.logInfo("Cannot see the Cart Is Empty")
			return false
		}

		KeywordUtil.logInfo("Validate user should add product to cart, save cart and I can create a new blank cart")
		return true
	}


	/**
	 * navigateSavedCartsQuotes(def Search)
	 * @param strCartName - user should pass the save cart name 
	 * @return true if navigate to Saved Carts Quotes page, otherwise false
	 */

	@Keyword
	def navigateSavedCartsQuotes(def strCartName) {

		KeywordUtil.logInfo("Validate the Saved Carts & Quotes")

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("SavedCartsQuotes/Signed In As"),60)) {
			KeywordUtil.logInfo("Cannot see the Signed In As Button")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("SavedCartsQuotes/Signed In As"))) {
			KeywordUtil.logInfo("Cannot click Signed In As Button")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("SavedCartsQuotes/SavedCartsQuotes"),60)) {
			KeywordUtil.logInfo("Cannot see the Saved Carts Quotes button")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("SavedCartsQuotes/SavedCartsQuotes"))) {
			KeywordUtil.logInfo("Cannot click Saved Carts Quotes button")
			return false
		}

		TestObject toVisible = new TestObject("Cart Name")
		String xpathVisible = "//span[contains(text(),'" + strCartName + "')]/following::td/a[contains(text(),'Remove')][1]"
		TestObjectProperty propVisible = new TestObjectProperty("xpath", ConditionType.EQUALS, xpathVisible)
		toVisible.addProperty(propVisible)

		if (!(new commerceCloud.utils()).verifyPresent(toVisible,60)) {
			KeywordUtil.logInfo("Cannot see the Saved Carts & Quotes")
			return false
		}


		KeywordUtil.logInfo("Validated the Saved Carts & Quotes")
		return true
	}


	/**
	 * navigateQuotes()
	 * @param strCartName - user should pass the save cart name
	 * @return true if navigate to Saved Carts Quotes page, otherwise false
	 */

	@Keyword
	def navigateQuotes() {

		KeywordUtil.logInfo("Validate the navigate of Quotes")

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("SavedCartsQuotes/Signed In As"),60)) {
			KeywordUtil.logInfo("Cannot see the Signed In As Button")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("SavedCartsQuotes/Signed In As"))) {
			KeywordUtil.logInfo("Cannot click Signed In As Button")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("SavedCartsQuotes/SavedCartsQuotes"),60)) {
			KeywordUtil.logInfo("Cannot see the Saved Carts Quotes button")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("SavedCartsQuotes/SavedCartsQuotes"))) {
			KeywordUtil.logInfo("Cannot click Saved Carts Quotes button")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Quote/QuoteIDHeading"),60)) {
			KeywordUtil.logInfo("Cannot see the Quote ID Heading")
			return false
		}

		KeywordUtil.logInfo("Validated the navigate of Quotes")
		return true
	}

	
	/**
	 * navigateBillingNameChange()
	 * @return true if navigate to Saved Carts Quotes page, otherwise false
	 */

	@Keyword
	def navigateBillingNameChange() {

		KeywordUtil.logInfo("Validate the navigate of Billing Name Change")

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("SavedCartsQuotes/Signed In As"),60)) {
			KeywordUtil.logInfo("Cannot see the Signed In As Button")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("SavedCartsQuotes/Signed In As"))) {
			KeywordUtil.logInfo("Cannot click Signed In As Button")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("ChangeBillingName/ChangeBillingName"),60)) {
			KeywordUtil.logInfo("Cannot see the Billing Name Change button")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("ChangeBillingName/ChangeBillingName"))) {
			KeywordUtil.logInfo("Cannot click Billing Name Change button")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("ChangeBillingName/ChangeBillingNameTitle"),60)) {
			KeywordUtil.logInfo("Cannot see the Billing Name Change Title")
			return false
		}

		KeywordUtil.logInfo("Validated the navigate of Billing Name Change")
		return true
	}

	

	/**
	 * sessionCartWithQuoteOpen() 
	 * @return true if current user has a session cart with products and quote is in open status, otherwise false
	 */

	@Keyword
	def sessionCartWithQuoteOpen() {

		KeywordUtil.logInfo("Validate the current user has a session cart with products and quote is in open status")

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Quote/QuoteIDHeading"),60)) {
			KeywordUtil.logInfo("Cannot see the Quote ID Heading")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("QuickOrder/QuickOrder"),60)) {
			KeywordUtil.logInfo("Cannot see the Quick Order")
			return false
		}

		if (!(new commerceCloud.utils().delay(10))) {
			KeywordUtil.logInfo("Cannot wait 10 sec")
			return false
		}


		List<WebElement> lstAct = (new commerceCloud.utils()).ListOfWebElement(findTestObject("Cart/CartItemsTotal"))
		String strAct =""
		if(!(lstAct.size() == 0)){
			strAct = lstAct.get(0).getText()
		}

		String strQuoteStatus = ""
		String strQuoteId =""
		List<WebElement> lstQuoteStatus = (new commerceCloud.utils()).ListOfWebElement(findTestObject("Quote/QuoteStatusList"))
		List<WebElement> lstQuoteId = (new commerceCloud.utils()).ListOfWebElement(findTestObject("Quote/QuoteQUOTEIDList"))
		println(lstQuoteStatus.size())
		for (int i=0; i<lstQuoteStatus.size(); i++) {
			strQuoteStatus = lstQuoteStatus.get(i).getText().trim()
			println(strQuoteStatus)
			if(strQuoteStatus.equalsIgnoreCase("Open")){
				strQuoteId = lstQuoteId.get(i).getText()
				println(strQuoteId)
				break
			}
		}

		if ((strQuoteId.equalsIgnoreCase(""))) {
			KeywordUtil.logInfo("Cannot see the Open staus task under Saved Carts & Quotes page")
			return false
		}

		TestObject toQuoteId = new TestObject("Quote Id")
		String xpathQuoteId = "//a/span[contains(text(),'" + strQuoteId + "')]"
		TestObjectProperty propQuoteId = new TestObjectProperty("xpath", ConditionType.EQUALS, xpathQuoteId)
		toQuoteId.addProperty(propQuoteId)

		if (!(new commerceCloud.utils()).click(toQuoteId)) {
			KeywordUtil.logInfo("Cannot click Quote Id link : "+ strQuoteId)
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Quote/Checkout"),60)) {
			KeywordUtil.logInfo("Cannot see the Checkout button")
			return false
		}

		/*if (!(new commerceCloud.utils()).click(findTestObject("Quote/Checkout"))) {
		 KeywordUtil.logInfo("Cannot click Checkout button")
		 return false
		 }
		 if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Quote/ContinueToPayment"),60)) {
		 KeywordUtil.logInfo("Cannot see the Continue To Payment button")
		 return false
		 }
		 if (!(new commerceCloud.utils()).click(findTestObject("Quote/ReturnToQuote"))) {
		 KeywordUtil.logInfo("Cannot click Return To Quote button")
		 return false
		 }
		 if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Quote/Checkout"),60)) {
		 KeywordUtil.logInfo("Cannot see the Checkout button")
		 return false
		 }*/

		if (!(new commerceCloud.utils()).delay(5)) {
			KeywordUtil.logInfo("Cannot wait 5 sec")
			return false
		}

		String strExp=""

		List<WebElement> lstExp = (new commerceCloud.utils()).ListOfWebElement(findTestObject("Cart/CartItemsTotal"))
		if(!(lstExp.size() == 0)){
			strExp= lstExp.get(0).getText()
		}

		println(strAct)
		println(strExp)
		if (!(strAct.equalsIgnoreCase(strExp))) {
			KeywordUtil.logInfo("Cannot quotation cart of the user is removed when user discards the quotation checkout and users has different session cart")
			return false
		}

		KeywordUtil.logInfo("Validated the current user has a session cart with products and quote is in open status")
		return true
	}

	/**
	 * NoLongerSavedCartsQuotes(def Search)
	 * @param Search - user should pass the save cart name
	 * @return true if navigate to Saved Carts Quotes page, otherwise false
	 */

	@Keyword
	def NoLongerSavedCartsQuotes(def strCartName) {

		KeywordUtil.logInfo("Validate the Saved Carts & Quotes")

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("SavedCartsQuotes/Signed In As"),60)) {
			KeywordUtil.logInfo("Cannot see the Signed In As Button")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("SavedCartsQuotes/Signed In As"))) {
			KeywordUtil.logInfo("Cannot click Signed In As Button")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("SavedCartsQuotes/SavedCartsQuotes"),60)) {
			KeywordUtil.logInfo("Cannot see the Saved Carts Quotes button")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("SavedCartsQuotes/SavedCartsQuotes"))) {
			KeywordUtil.logInfo("Cannot click Saved Carts Quotes button")
			return false
		}

		if (!(new commerceCloud.utils()).delay(5)) {
			KeywordUtil.logInfo("Cannot wait 5 sec")
			return false
		}

		TestObject toVisible = new TestObject("Cart Name")
		String xpathVisible = "//span[contains(text(),'" + strCartName + "')]"
		TestObjectProperty propVisible = new TestObjectProperty("xpath", ConditionType.EQUALS, xpathVisible)
		toVisible.addProperty(propVisible)

		WebDriver driver = DriverFactory.getWebDriver()
		List<WebElement> lst = driver.findElements(By.xpath(xpathVisible))
		int iSize = lst.size()

		if (!(iSize == 0)) {
			KeywordUtil.logInfo("Can see the Saved Carts & Quotes")
			return false
		}

		KeywordUtil.logInfo("Validated the Saved Carts & Quotes")
		return true
	}

	/**
	 * Restore(def Search)
	 * @param Search - user should send the tessco product details
	 * @return true if Restore cart under cart page, otherwise false
	 */

	@Keyword
	def Restore(def strCartName) {

		KeywordUtil.logInfo("Validate the Restore cart")

		TestObject toVisible = new TestObject("paginationNext")
		String xpathVisible = "//span[contains(text(),'" + strCartName + "')]"
		TestObjectProperty propVisible = new TestObjectProperty("xpath", ConditionType.EQUALS, xpathVisible)
		toVisible.addProperty(propVisible)

		if (!(new commerceCloud.utils()).verifyPresent(toVisible,60)) {
			KeywordUtil.logInfo("Cannot see the Saved Carts & Quotes")
			return false
		}

		if (!(new commerceCloud.utils()).click(toVisible)) {
			KeywordUtil.logInfo("Cannot click Saved Cart Name")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Cart/Restore"),60)) {
			KeywordUtil.logInfo("Cannot see the Restore Button")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Logo/TesscoHomePageLogo"),1)) {
			KeywordUtil.logInfo("Cannot see the Tessco Home Page Logo")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Cart/RestoreProduct"),60)) {
			KeywordUtil.logInfo("Cannot see the product name Button")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/RestoreCancel"))) {
			KeywordUtil.logInfo("Cannot click Restore Cancel Button")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(toVisible,60)) {
			KeywordUtil.logInfo("Cannot see the Saved Carts & Quotes")
			return false
		}

		if (!(new commerceCloud.utils()).click(toVisible)) {
			KeywordUtil.logInfo("Cannot click Saved Cart Name")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Cart/Restore"),60)) {
			KeywordUtil.logInfo("Cannot see the Restore Button")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/Restore"))) {
			KeywordUtil.logInfo("Cannot click Restore Button")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Cart/CheckoutTop"),60)) {
			KeywordUtil.logInfo("Cannot see the Checkout Top button")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/NewCart"))) {
			KeywordUtil.logInfo("Cannot click New Cart Button")
			return false
		}


		KeywordUtil.logInfo("Validate the Restore cart")
		return true
	}

	/**
	 * RestoreOnly(def Search)
	 * @param Search - user should send the tessco product details
	 * @return true if Restore cart under cart page, otherwise false
	 */

	@Keyword
	def RestoreOnly(def strCartName) {

		KeywordUtil.logInfo("Validate the Restore cart")

		TestObject toVisible = new TestObject("paginationNext")
		String xpathVisible = "//span[contains(text(),'" + strCartName + "')]"
		TestObjectProperty propVisible = new TestObjectProperty("xpath", ConditionType.EQUALS, xpathVisible)
		toVisible.addProperty(propVisible)

		if (!(new commerceCloud.utils()).verifyPresent(toVisible,60)) {
			KeywordUtil.logInfo("Cannot see the Saved Carts & Quotes")
			return false
		}

		if (!(new commerceCloud.utils()).click(toVisible)) {
			KeywordUtil.logInfo("Cannot click Saved Cart Name")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Cart/Restore"),60)) {
			KeywordUtil.logInfo("Cannot see the Restore Button")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Cart/RestoreProduct"),60)) {
			KeywordUtil.logInfo("Cannot see the product name Button")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/RestoreCancel"))) {
			KeywordUtil.logInfo("Cannot click Restore Cancel Button")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(toVisible,60)) {
			KeywordUtil.logInfo("Cannot see the Saved Carts & Quotes")
			return false
		}

		if (!(new commerceCloud.utils()).click(toVisible)) {
			KeywordUtil.logInfo("Cannot click Saved Cart Name")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Cart/Restore"),60)) {
			KeywordUtil.logInfo("Cannot see the Restore Button")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/Restore"))) {
			KeywordUtil.logInfo("Cannot click Restore Button")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Cart/CheckoutTop"),60)) {
			KeywordUtil.logInfo("Cannot see the Checkout Top button")
			return false
		}

		KeywordUtil.logInfo("Validate the Restore cart")
		return true
	}


	/**
	 * RestoreAndChangeQty(def Search)
	 * @param Search - user should send the tessco product details
	 * @return true if Restore cart under cart and change the qty, otherwise false
	 */

	@Keyword
	def RestoreAndChangeQty(def strCartName) {

		KeywordUtil.logInfo("Validate the Restore cart and change the Qty")

		TestObject toVisible = new TestObject("strCartName")
		String xpathVisible = "//span[contains(text(),'" + strCartName + "')]"
		TestObjectProperty propVisible = new TestObjectProperty("xpath", ConditionType.EQUALS, xpathVisible)
		toVisible.addProperty(propVisible)

		if (!(new commerceCloud.utils()).verifyPresent(toVisible,60)) {
			KeywordUtil.logInfo("Cannot see the Saved Carts & Quotes")
			return false
		}

		if (!(new commerceCloud.utils()).click(toVisible)) {
			KeywordUtil.logInfo("Cannot click Saved Cart Name")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Cart/Restore"),60)) {
			KeywordUtil.logInfo("Cannot see the Restore Button")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/Restore"))) {
			KeywordUtil.logInfo("Cannot click Restore Button")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Cart/CheckoutTop"),60)) {
			KeywordUtil.logInfo("Cannot see the Checkout Top button")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/UpdateEntryQuantityPlusFirst"))) {
			KeywordUtil.logInfo("Cannot click Update Entry Quantity Plus First button")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("OrderSummary/Checkout"),60)) {
			KeywordUtil.logInfo("Cannot see the Checkout button")
			return false
		}
		String strAfterUpdate =  (new commerceCloud.utils()).GetAttributeText(findTestObject("Cart/UpdateEntryQuantityFirst"),"value")

		if (!(new commerceCloud.Cart()).navigateSavedCartsQuotes(strCartName)) {
			KeywordUtil.logInfo("Cannot navigate to Saved Carts Quotes Page")
			return false
		}

		TestObject to = new TestObject("strCartName")
		String xpath = "//span[contains(text(),'" + strCartName + "')]"
		TestObjectProperty prop = new TestObjectProperty("xpath", ConditionType.EQUALS, xpath)
		to.addProperty(prop)

		if (!(new commerceCloud.utils()).verifyPresent(to,60)) {
			KeywordUtil.logInfo("Cannot see the Saved Carts & Quotes")
			return false
		}

		if (!(new commerceCloud.utils()).click(to)) {
			KeywordUtil.logInfo("Cannot click Saved Cart Name")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Cart/RestoreCancel"),60)) {
			KeywordUtil.logInfo("Cannot see the Restore Cancel Button")
			return false
		}


		boolean blnValues = false
		List<WebElement> lst = (new commerceCloud.utils()).ListOfWebElement(findTestObject("SavedCartsQuotes/RestoreCartQuantity"))

		for (svar in lst) {
			String strExp = svar.getText()
			strExp = strExp.replaceAll('Qty', '')
			println(strExp)
			if(strAfterUpdate.trim().equalsIgnoreCase(strExp.trim())){
				blnValues = true
				break
			}
		}

		if (!(blnValues)) {
			KeywordUtil.logInfo("Cannot Verify the Restore Cart popup modal displays and the change in quantity can be observed")
			return false
		}


		if (!(new commerceCloud.utils()).click(findTestObject("Cart/RestoreCancel"))) {
			KeywordUtil.logInfo("Cannot click Restore Cancel Button")
			return false
		}
		KeywordUtil.logInfo("Validate the Restore cart and change the Qty")
		return true
	}

	/**
	 * RemoveSaveItems(def strCartName)
	 * @param Search - user should cart name
	 * @return true if Restore cart and Remove Save Items under cart page, otherwise false
	 */

	@Keyword
	def RemoveSaveItems(def strCartName) {

		KeywordUtil.logInfo("Validate the Remove Save Items cart")

		TestObject toVisible = new TestObject("paginationNext")
		String xpathVisible = "//span[contains(text(),'" + strCartName + "')]"
		TestObjectProperty propVisible = new TestObjectProperty("xpath", ConditionType.EQUALS, xpathVisible)
		toVisible.addProperty(propVisible)

		if (!(new commerceCloud.utils()).verifyPresent(toVisible,60)) {
			KeywordUtil.logInfo("Cannot see the Saved Carts & Quotes")
			return false
		}

		if (!(new commerceCloud.utils()).click(toVisible)) {
			KeywordUtil.logInfo("Cannot click Saved Cart Name")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Cart/RemoveRestoreActiveCart"),60)) {
			KeywordUtil.logInfo("Cannot see the Restore Button")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Logo/TesscoHomePageLogo"),1)) {
			KeywordUtil.logInfo("Cannot see the Tessco Home Page Logo")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/RemoveRestoreActiveCart"))) {
			KeywordUtil.logInfo("Cannot click Restore Button")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Cart/RemoveDeleteItems"),60)) {
			KeywordUtil.logInfo("Cannot see the Remove Delete Items button")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/RemoveDeleteItems"))) {
			KeywordUtil.logInfo("Cannot click Remove Delete Items Button")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Cart/CheckoutTop"),60)) {
			KeywordUtil.logInfo("Cannot see the Checkout Top button")
			return false
		}

		KeywordUtil.logInfo("Validate the Remove Save Items cart")
		return true
	}


	/**
	 * SaveItems(def strCartName)
	 * @param Search - user should cart name
	 * @return true if Restore cart and Remove Save Items under cart page, otherwise false
	 */

	@Keyword
	def SaveItems(def strCartName) {

		KeywordUtil.logInfo("Validate the Remove Save Items cart")

		TestObject toVisible = new TestObject("paginationNext")
		String xpathVisible = "//span[contains(text(),'" + strCartName + "')]"
		TestObjectProperty propVisible = new TestObjectProperty("xpath", ConditionType.EQUALS, xpathVisible)
		toVisible.addProperty(propVisible)
		println(xpathVisible)

		if (!(new commerceCloud.utils()).verifyPresent(toVisible,60)) {
			KeywordUtil.logInfo("Cannot see the Saved Carts & Quotes")
			return false
		}

		if (!(new commerceCloud.utils()).click(toVisible)) {
			KeywordUtil.logInfo("Cannot click Saved Cart Name")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Cart/RemoveRestoreActiveCart"),60)) {
			KeywordUtil.logInfo("Cannot see the Restore Button")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/RemoveRestoreActiveCart"))) {
			KeywordUtil.logInfo("Cannot click Restore Button")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Cart/RemoveSaveItems"),60)) {
			KeywordUtil.logInfo("Cannot see the Remove Save Items button")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/RemoveSaveItems"))) {
			KeywordUtil.logInfo("Cannot click Remove Save Items Button")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Cart/saveCartName"),60)) {
			KeywordUtil.logInfo("Cannot see the save  Cart Name")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("Cart/saveCartName"), strCartName)) {
			KeywordUtil.logInfo("Cannot enter save  Cart Name")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("Cart/saveCartDescription"), "Automation Testing Purpose Only")) {
			KeywordUtil.logInfo("Cannot enter save Cart Description")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/saveCartButton"))) {
			KeywordUtil.logInfo("Cannot click save Cart Button")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Cart/ContinueRestore"),60)) {
			KeywordUtil.logInfo("Cannot see the Continue Restore button")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/ContinueRestore"))) {
			KeywordUtil.logInfo("Cannot click save Cart Button")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Cart/CheckoutTop"),60)) {
			KeywordUtil.logInfo("Cannot see the Checkout Top button")
			return false
		}

		KeywordUtil.logInfo("Validate the Remove Save Items cart")
		return true
	}

	/**
	 * removeSavedCartsQuotes(def Search)
	 * @param Search - user should send cart name
	 * @return true if Restore cart under cart page, otherwise false
	 */

	@Keyword
	def removeSavedCartsQuotes(def strCartName) {

		KeywordUtil.logInfo("Validate the Remove cart")

		TestObject toVisible = new TestObject("paginationNext")
		String xpathVisible = "//span[contains(text(),'" + strCartName + "')]/following::td/a[contains(text(),'Remove')][1]"
		TestObjectProperty propVisible = new TestObjectProperty("xpath", ConditionType.EQUALS, xpathVisible)
		toVisible.addProperty(propVisible)

		if (!(new commerceCloud.utils()).verifyPresent(toVisible,60)) {
			KeywordUtil.logInfo("Cannot see the remove the saved Carts & Quotes")
			return false
		}

		String strExp =  (new commerceCloud.utils()).sReturnText(findTestObject("SavedCartsQuotes/SavedCartSize"))
		String[] arrOfStrExp = strExp.split(' ')

		if (!(new commerceCloud.utils()).click(toVisible)) {
			KeywordUtil.logInfo("Cannot click remove the saved Cart")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/NoDeleteIt"))) {
			KeywordUtil.logInfo("Cannot click No Delete It")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(toVisible,60)) {
			KeywordUtil.logInfo("Cannot see the remove the saved Carts & Quotes")
			return false
		}

		if (!(new commerceCloud.utils()).click(toVisible)) {
			KeywordUtil.logInfo("Cannot click remove the saved Cart")
			return false
		}

		/*if (!(new commerceCloud.utils()).click(findTestObject("Cart/YesDeleteItCloseX"))) {
		 KeywordUtil.logInfo("Cannot click It Close using X")
		 return false
		 }
		 if (!(new commerceCloud.utils()).verifyPresent(toVisible,60)) {
		 KeywordUtil.logInfo("Cannot see the remove the saved Carts & Quotes")
		 return false
		 }
		 if (!(new commerceCloud.utils()).click(toVisible)) {
		 KeywordUtil.logInfo("Cannot click remove the saved Cart")
		 return false
		 }
		 */
		if (!(new commerceCloud.utils()).click(findTestObject("Cart/YesDeleteIt"))) {
			KeywordUtil.logInfo("Cannot click Yes Delete It")
			return false
		}


		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Cart/Cart"),60)) {
			KeywordUtil.logInfo("Cannot see the Cart button")
			return false
		}

		String strAct =  (new commerceCloud.utils()).sReturnText(findTestObject("SavedCartsQuotes/SavedCartSize"))
		String[] arrOfStrAct = strAct.split(' ')

		if ((arrOfStrAct[0].equalsIgnoreCase(arrOfStrExp[0]))) {
			KeywordUtil.logInfo("Cannot see the saved cart is removed and saved cart count in header will be updated, upon the removal of the cart")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/Cart"))) {
			KeywordUtil.logInfo("Cannot click Cart")
			return false
		}

		KeywordUtil.logInfo("Validate the Remove cart")
		return true
	}

	/**
	 * removeAllSavedCartsQuotes(def Search)
	 * @param Search - user should send cart name
	 * @return true if Restore cart under cart page, otherwise false
	 */

	@Keyword
	def removeAllSavedCartsQuotes() {

		KeywordUtil.logInfo("Validate the Remove cart")

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("SavedCartsQuotes/Signed In As"),60)) {
			KeywordUtil.logInfo("Cannot see the Signed In As Button")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("SavedCartsQuotes/Signed In As"))) {
			KeywordUtil.logInfo("Cannot click Signed In As Button")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("SavedCartsQuotes/SavedCartsQuotes"),60)) {
			KeywordUtil.logInfo("Cannot see the Saved Carts Quotes button")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("SavedCartsQuotes/SavedCartsQuotes"))) {
			KeywordUtil.logInfo("Cannot click Saved Carts Quotes button")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("SavedCartsQuotes/SavedCartsQuotesHeading"),60)) {
			KeywordUtil.logInfo("Cannot see the Saved Carts Quotes Heading")
			return false
		}

		List<WebElement> lst = (new commerceCloud.utils().ListOfWebElement(findTestObject("Cart/RemoveCartSize")))

		for(int i=0;i<lst.size();i++){


			if (!(new commerceCloud.utils()).click(findTestObject("Cart/RemoveFirstCart"))) {
				KeywordUtil.logInfo("Cannot click Remove First Cart")
				return false
			}

			if (!(new commerceCloud.utils()).click(findTestObject("Cart/YesDeleteIt"))) {
				KeywordUtil.logInfo("Cannot click Yes Delete It")
				return false
			}

			if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Cart/Cart"),60)) {
				KeywordUtil.logInfo("Cannot see the Cart button")
				return false
			}
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("SavedCartsQuotes/NoSavedCartsFound"),60)) {
			KeywordUtil.logInfo("Cannot see the No Saved Carts Found")
			return false
		}


		KeywordUtil.logInfo("Validate the Remove cart")
		return true
	}


	/**
	 * SortSavedCartsQuotes() 
	 * @return true if Verify Saved carts heading shows, sorting and removing , number of saved carts created by current user, otherwise false
	 */

	@Keyword
	def SortSavedCartsQuotes() {

		KeywordUtil.logInfo("Validate the user able to Sort Saved Carts and Quotes page")
		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("SavedCartsQuotes/CartHeadingTable"),60)) {
			KeywordUtil.logInfo("Cannot see the remove the saved Carts & Quotes")
			return false
		}

		//Cart Name sorting
		if (!(new commerceCloud.utils()).click(findTestObject("SavedCartsQuotes/CartHeadingTable"))) {
			KeywordUtil.logInfo("Cannot click Name the saved Cart")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("SavedCartsQuotes/CartHeadingTable"),60)) {
			KeywordUtil.logInfo("Cannot see the Cart button")
			return false
		}

		if (!(new commerceCloud.utils()).sorting(findTestObject("SavedCartsQuotes/CartTable"), 'Name', 'desc')) {
			KeywordUtil.logInfo("Cannot sort the Cart name under table")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("SavedCartsQuotes/CartHeadingTable"))) {
			KeywordUtil.logInfo("Cannot click Name the saved Cart")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("SavedCartsQuotes/CartHeadingTable"),60)) {
			KeywordUtil.logInfo("Cannot see the Cart button")
			return false
		}

		if (!(new commerceCloud.utils()).sorting(findTestObject("SavedCartsQuotes/CartTable"), 'Name', 'asc')) {
			KeywordUtil.logInfo("Cannot sort the Cart name under table")
			return false
		}

		//Cart DESCRIPTION sorting
		if (!(new commerceCloud.utils()).click(findTestObject("SavedCartsQuotes/DescriptionHeadingTable"))) {
			KeywordUtil.logInfo("Cannot click DESCRIPTION the saved Cart")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("SavedCartsQuotes/DescriptionHeadingTable"),60)) {
			KeywordUtil.logInfo("Cannot see the DESCRIPTION button")
			return false
		}

		if (!(new commerceCloud.utils()).sorting(findTestObject("SavedCartsQuotes/DescriptionTable"), 'Name', 'desc')) {
			KeywordUtil.logInfo("Cannot sort the DESCRIPTION under table")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("SavedCartsQuotes/DescriptionHeadingTable"))) {
			KeywordUtil.logInfo("Cannot click DESCRIPTION the saved Cart")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("SavedCartsQuotes/DescriptionHeadingTable"),60)) {
			KeywordUtil.logInfo("Cannot see the DESCRIPTION button")
			return false
		}

		if (!(new commerceCloud.utils()).sorting(findTestObject("SavedCartsQuotes/DescriptionTable"), 'Name', 'asc')) {
			KeywordUtil.logInfo("Cannot sort the DESCRIPTION under table")
			return false
		}


		//Cart ITEMS sorting
		if (!(new commerceCloud.utils()).click(findTestObject("SavedCartsQuotes/ItemsHeadingTable"))) {
			KeywordUtil.logInfo("Cannot click ITEMS the saved Cart")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("SavedCartsQuotes/ItemsHeadingTable"),60)) {
			KeywordUtil.logInfo("Cannot see the Cart button")
			return false
		}

		if (!(new commerceCloud.utils()).sorting(findTestObject("SavedCartsQuotes/ItemsTable"), 'Name', 'desc')) {
			KeywordUtil.logInfo("Cannot sort the Cart name under table")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("SavedCartsQuotes/ItemsHeadingTable"))) {
			KeywordUtil.logInfo("Cannot click remove the saved Cart")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("SavedCartsQuotes/ItemsHeadingTable"),60)) {
			KeywordUtil.logInfo("Cannot see the Cart button")
			return false
		}

		if (!(new commerceCloud.utils()).sorting(findTestObject("SavedCartsQuotes/ItemsTable"), 'Name', 'asc')) {
			KeywordUtil.logInfo("Cannot sort the Cart name under table")
			return false
		}

		//Cart TOTAL sorting
		if (!(new commerceCloud.utils()).click(findTestObject("SavedCartsQuotes/TotalHeadingTable"))) {
			KeywordUtil.logInfo("Cannot click TOTAL the saved Cart")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("SavedCartsQuotes/TotalHeadingTable"),60)) {
			KeywordUtil.logInfo("Cannot see the TOTAL button")
			return false
		}

		if (!(new commerceCloud.utils()).sorting(findTestObject("SavedCartsQuotes/TotalTable"), 'Name', 'desc')) {
			KeywordUtil.logInfo("Cannot sort the TOTAL under table")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("SavedCartsQuotes/TotalHeadingTable"))) {
			KeywordUtil.logInfo("Cannot click TOTAL under saved Cart")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("SavedCartsQuotes/TotalHeadingTable"),60)) {
			KeywordUtil.logInfo("Cannot see the TOTAL button")
			return false
		}

		if (!(new commerceCloud.utils()).sorting(findTestObject("SavedCartsQuotes/TotalTable"), 'Name', 'asc')) {
			KeywordUtil.logInfo("Cannot sort the TOTAL under table")
			return false
		}

		KeywordUtil.logInfo("Validated the user able to Sort Saved Carts and Quotes page")
		return true
	}


	/**
	 * orderSummary(def Search)
	 * @param Search - user should send the tessco product details
	 * @return true if UI: Cart details page - Order Summary, otherwise false
	 */

	@Keyword
	def orderSummary(def Search) {

		KeywordUtil.logInfo("Validate the Cart details page - Order Summary")

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("ProductSearch/HomePageBanners"),60)) {
			KeywordUtil.logInfo("Cannot see Home Page Banners")
			return false
		}

		if (!(new commerceCloud.ProductSearch()).searchTesscoProduct(Search)) {
			KeywordUtil.logInfo("Cannot search the Tessco Product")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("ProductSearch/product listing price"),60)) {
			KeywordUtil.logInfo("Cannot see the product listing price")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("ProductSearch/Product List Add to Cart"))) {
			KeywordUtil.logInfo("Cannot click search tessco com button")
			return false
		}

		if (!(new commerceCloud.Cart()).CableAddToCart()) {
			KeywordUtil.logInfo("Cannot add Cable product to Add To Cart")
			return false
		}

		/*if (!(new commerceCloud.utils()).click(findTestObject("ProductSearch/search tessco com button"))) {
		 KeywordUtil.logInfo("Cannot click search tessco com button")
		 return false
		 }*/

		if (!(new commerceCloud.utils()).click(findTestObject("AddToCart/GoToCart"))) {
			KeywordUtil.logInfo("Cannot click Go To Cart")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Cart/ClearCart"),60)) {
			KeywordUtil.logInfo("Cannot see the Clear Cart")
			return false
		}


		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderSummary/Checkout"),1)) {
			KeywordUtil.logInfo("Cannot see the Checkout")
			return false
		}


		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderSummary/OrderSummary"),1)) {
			KeywordUtil.logInfo("Cannot see the Order Summary")
			return false
		}


		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderSummary/OrderTotal"),1)) {
			KeywordUtil.logInfo("Cannot see the Order Total")
			return false
		}


		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderSummary/Shipping"),1)) {
			KeywordUtil.logInfo("Cannot see the Shipping")
			return false
		}


		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderSummary/Subtotal"),1)) {
			KeywordUtil.logInfo("Cannot see the Subtotal")
			return false
		}


		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderSummary/Tax"),1)) {
			KeywordUtil.logInfo("Cannot see the Tax")
			return false
		}
		KeywordUtil.logInfo("Validated The Cart details page - Order Summary")
		return true
	}

	/**
	 * emptyCart() 
	 * Verify only import CSV button is active and other buttons Create Quote, Save Cart, Export CSV are disabled
	 * 
	 * @return true if UI: Cart details page - Order Summary, otherwise false
	 */

	@Keyword
	def emptyCart() {

		KeywordUtil.logInfo("Validate the user removed card")


		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Cart/Cart"),60)) {
			KeywordUtil.logInfo("Cannot see the Cart button")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/Cart"))) {
			KeywordUtil.logInfo("Cannot click Cart button")
			return false
		}


		if (!(new commerceCloud.utils()).verifyElementIsEnabled(findTestObject("Cart/ImportCSV"),60)) {
			KeywordUtil.logInfo("Cannot Enabled the Import CSV")
			return false
		}

		if ((new commerceCloud.utils()).verifyPresent(findTestObject("Cart/CartIsEmpty"),1)) {
			KeywordUtil.logInfo("Can see the Cart Is Empty")


			if (!(new commerceCloud.utils()).verifyElementIsDisable(findTestObject("Cart/CreateQuote"),30)) {
				KeywordUtil.logInfo("Cannot Disable the Create Quote")
				return false
			}

			if (!(new commerceCloud.utils()).verifyElementIsDisable(findTestObject("Cart/SaveCart"),1)) {
				KeywordUtil.logInfo("Cannot Disable the Save Cart")
				return false
			}

			if (!(new commerceCloud.utils()).verifyElementIsDisable(findTestObject("Cart/ExportCSV"),1)) {
				KeywordUtil.logInfo("Cannot Disable the Export CSV")
				return false
			}

			if (!(new commerceCloud.utils()).verifyElementIsEnabled(findTestObject("Cart/ImportCSV"),1)) {
				KeywordUtil.logInfo("Cannot Enabled the Import CSV")
				return false
			}
		}else {
			KeywordUtil.logInfo("User clicked Clear Cart button and now able to see the Cart Is Empty")

			if (!(new commerceCloud.utils()).click(findTestObject("Cart/ClearCart"))) {
				KeywordUtil.logInfo("Cannot click Clear Cart button")
				return false
			}

			if (!(new commerceCloud.utils()).verifyElementIsDisable(findTestObject("Cart/CreateQuote"),30)) {
				KeywordUtil.logInfo("Cannot Disable the Create Quote")
				return false
			}

			if (!(new commerceCloud.utils()).verifyElementIsDisable(findTestObject("Cart/SaveCart"),1)) {
				KeywordUtil.logInfo("Cannot Disable the Save Cart")
				return false
			}

			if (!(new commerceCloud.utils()).verifyElementIsDisable(findTestObject("Cart/ExportCSV"),1)) {
				KeywordUtil.logInfo("Cannot Disable the Export CSV")
				return false
			}

			if (!(new commerceCloud.utils()).verifyElementIsEnabled(findTestObject("Cart/ImportCSV"),1)) {
				KeywordUtil.logInfo("Cannot Enabled the Import CSV")
				return false
			}
		}

		KeywordUtil.logInfo("Validated the user removed card")

		return true
	}

	/**
	 * RemoveSavedCart(def Search = null, def cartName = null)
	 * @param Search - user should send the tessco product details
	 * @return true if UI: Cart details page - Order Summary, otherwise false
	 */

	@Keyword
	def RemoveSavedCart(def Search, def cartName) {

		KeywordUtil.logInfo("Validate the Cart details page - Order Summary")

		if (!(new commerceCloud.Cart()).addProductToCart(Search)) {
			KeywordUtil.logInfo("Cannot search the Tessco Product")
			return false
		}

		String strCartName = (new commerceCloud.Cart()).getCartName(cartName)
		if (!(new commerceCloud.Cart()).saveCart(strCartName)) {
			KeywordUtil.logInfo("Cannot Saved Carts & Quotes")
			return false
		}

		KeywordUtil.logInfo("Validated The Cart details page - Order Summary")
		return true
	}


	/**
	 * addingCableProducttoCart(String Search, String cartName)
	 * @param Search - user should search the tessco product details
	 * @param cartName - user should add the cart name under added product into cart
	 * @return true if Cable Product is added to Cart, otherwise false
	 */

	@Keyword
	def addingCableProducttoCart(String Search, String cartName) {

		KeywordUtil.logInfo("Validate the adding Cable Product to Cart")

		if (!(new commerceCloud.Cart()).addProductToCart(Search)) {
			KeywordUtil.logInfo("Cannot search the Tessco Product")
			return false
		}

		String strCartName = (new commerceCloud.Cart()).getCartName(cartName)
		if (!(new commerceCloud.Cart()).CableProductTOCart(strCartName)) {
			KeywordUtil.logInfo("Cannot Saved Carts & Quotes")
			return false
		}

		if (!(new commerceCloud.Checkout()).CompleteOrderingProcess()) {
			KeywordUtil.logInfo("Cannot the user can complete the Order")
			return false
		}

		KeywordUtil.logInfo("Validated the adding Cable Product to Cart")
		return true
	}

	/**
	 * addingCableProducttoCart(String Search, String cartName)
	 * @param Search - user should search the tessco product details 
	 * @return true if validate the Delivery Option Lift Gate and Inside Delivery price, otherwise false
	 */

	@Keyword
	def DeliveryOptionLiftGateAndInsideDelivery(String Search) {

		KeywordUtil.logInfo("Validate the adding Cable Product to Cart")

		if (!(new commerceCloud.Cart()).addProductToCart(Search)) {
			KeywordUtil.logInfo("Cannot search the Tessco Product")
			return false
		}

		if (!(new commerceCloud.Checkout()).DeliveryOptionValidation()) {
			KeywordUtil.logInfo("Cannot the validate the Delivery Option Lift Gate and Inside Delivery")
			return false
		}

		if (!(new commerceCloud.Checkout()).CompleteOrderingProcess()) {
			KeywordUtil.logInfo("Cannot the user can complete the Order")
			return false
		}

		KeywordUtil.logInfo("Validated the adding Cable Product to Cart")
		return true
	}

	/**
	 * SaveCartPopupVisible
	 * @return true if Verify upon successful login & account selection the save cart popup will be displayed, otherwise false
	 */
	@Keyword
	def SaveCartPopupVisible() {

		String userName = ""
		userName = (new commerceCloud.Cart()).getUserName("").trim()
		userName = userName.toLowerCase()

		KeywordUtil.logInfo("Verify upon successful login & account selection the save cart popup will be displayed")

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/SaveCart"))) {
			KeywordUtil.logInfo("Cannot click Save Cart")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Login/User Name"),60)) {
			KeywordUtil.logInfo("Cannot see User Name")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("Login/User Name"),userName)) {
			KeywordUtil.logInfo("Cannot enter User Name")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("Login/Password"),GlobalVariable.GlobalPassword)) {
			KeywordUtil.logInfo("Cannot enter Password")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Login/Log In"))) {
			KeywordUtil.logInfo("Cannot click Log In")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Login/Signed In As"),60)) {
			KeywordUtil.logInfo("Cannot see Signed In As")
			return false
		}
		KeywordUtil.logInfo("Verified upon successful login & account selection the save cart popup will be displayed")
		return true
	}

	/**
	 * RemoveSavedCart(def Search = null, def cartName = null)
	 * @param Search - user should send the tessco product details
	 * @return true if UI: Cart details page - Order Summary, otherwise false
	 */

	@Keyword
	def SaveCurrentCartFutureBlank(def Search, def cartName) {

		KeywordUtil.logInfo("Validate the Cart details page - Order Summary")

		if (!(new commerceCloud.Cart()).addOneProductToCart(Search)) {
			KeywordUtil.logInfo("Cannot search the Tessco Product")
			return false
		}

		String strCartName = (new commerceCloud.Cart()).getCartName(cartName)
		if (!(new commerceCloud.Cart()).saveCartAndReturnCart(strCartName)) {
			KeywordUtil.logInfo("Cannot user save cart and I can create a new blank cart")
			return false
		}

		String strCartNameSec = (new commerceCloud.Cart()).getCartName(cartName)
		if (!(new commerceCloud.Cart()).NoLongerSavedCartsQuotes(strCartNameSec)) {
			KeywordUtil.logInfo("Cannot navigate to Saved Carts Quotes Page")
			return false
		}

		if (!((new commerceCloud.Cart()).logout())) {
			KeywordUtil.markFailedAndStop('Can not logout the commerceCloud')
			return false
		}

		if (!(new commerceCloud.Cart()).addOneProductToCart(Search)) {
			KeywordUtil.logInfo("Cannot search the Tessco Product")
			return false
		}

		if (!(new commerceCloud.Cart()).SaveCartPopupVisible()) {
			KeywordUtil.logInfo("Cannot login & account selection the save cart popup will be displayed")
			return false
		}

		KeywordUtil.logInfo("Validated The Cart details page - Order Summary")
		return true
	}

	/**
	 * restorSavedCart( def cartName, String  Search)
	 * @param Search - user should send Cart Name
	 * @return true if Navigate to Saved Carts Quotes and Restore, otherwise false
	 */

	@Keyword
	def restorSavedCart( def cartName, String  Search) {

		KeywordUtil.logInfo("Validate the  Navigate to Saved Carts Quotes and Restore")

		String strCartName = (new commerceCloud.Cart()).getCartName(cartName)

		if (!(new commerceCloud.Cart()).navigateSavedCartsQuotes(strCartName)) {
			KeywordUtil.logInfo("Cannot navigate to Saved Carts Quotes Page")
			return false
		}

		if (!(new commerceCloud.Cart()).Restore(strCartName)) {
			KeywordUtil.logInfo("Cannot Restore Saved Carts & Quotes")
			return false
		}

		if (!(new commerceCloud.Cart()).addProductToCart(Search)) {
			KeywordUtil.logInfo("Cannot search the Tessco Product")
			return false
		}

		if (!(new commerceCloud.Cart()).navigateSavedCartsQuotes(strCartName)) {
			KeywordUtil.logInfo("Cannot navigate to Saved Carts Quotes Page")
			return false
		}

		if (!(new commerceCloud.Cart()).RemoveSaveItems(strCartName)) {
			KeywordUtil.logInfo("Cannot Remove Save Items under Save Carts & Quotes")
			return false
		}

		KeywordUtil.logInfo("Validated the Navigate to Saved Carts Quotes and Restore")
		return true
	}

	/**
	 * removeSavedCart(def cartName = null)
	 * @param Search - user should send Cart Name
	 * @return true if Navigate to Saved Carts Quotes and remove, otherwise false
	 */

	@Keyword
	def removeSavedCart(def cartName) {

		KeywordUtil.logInfo("Validate the  Navigate to Saved Carts Quotes and remove")

		String strCartName = (new commerceCloud.Cart()).getCartName(cartName)

		if (!(new commerceCloud.Cart()).navigateSavedCartsQuotes(strCartName)) {
			KeywordUtil.logInfo("Cannot navigate to Saved Carts Quotes Page")
			return false
		}

		if (!(new commerceCloud.Cart()).SortSavedCartsQuotes()) {
			KeywordUtil.logInfo("Cannot Verify Saved carts heading shows and Sorting table")
			return false
		}


		if (!(new commerceCloud.Cart()).removeSavedCartsQuotes(strCartName)) {
			KeywordUtil.logInfo("Cannot remove Saved Carts & Quotes")
			return false
		}

		KeywordUtil.logInfo("Validated the Navigate to Saved Carts Quotes and remove")
		return true
	}


	/**
	 * removeAllSavedCart(def cartName = null)
	 * @param Search - user should send Cart Name
	 * @return true if Navigate to Saved Carts Quotes and remove all, otherwise false
	 */

	@Keyword
	def removeAllSavedCart() {

		KeywordUtil.logInfo("Validate the  Navigate to Saved Carts Quotes and Remove all cart")

		if (!(new commerceCloud.Cart()).removeAllSavedCartsQuotes()) {
			KeywordUtil.logInfo("Cannot remove All Saved Carts & Quotes")
			return false
		}

		KeywordUtil.logInfo("Validated the Navigate to Saved Carts Quotes and Remove all cart")
		return true
	}

	/**
	 * ClearCart()
	 * @return true if user removed card, otherwise false
	 */

	@Keyword
	def NewCart() {

		KeywordUtil.logInfo("Validate the New Cart")

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Cart/Cart"),60)) {
			KeywordUtil.logInfo("Cannot see the Cart button")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/Cart"))) {
			KeywordUtil.logInfo("Cannot click Cart button")
			return false
		}


		if ((new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Cart/NewCart"),30)) {

			KeywordUtil.logInfo("Can see the Clear Cart")

			String strBeforeUpdate =  (new commerceCloud.utils()).sReturnText(findTestObject("Cart/NewCart"))
			println(strBeforeUpdate)
			if(!(strBeforeUpdate.trim().contains("Save Cart"))){

				if (!(new commerceCloud.utils()).click(findTestObject("Cart/NewCart"))) {
					KeywordUtil.logInfo("Cannot click New Cart")
					return false
				}

				if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Cart/CartIsEmpty"),60)) {
					KeywordUtil.logInfo("Cannot see the Your Cart Is Empty")
					return false
				}

				if (!(new commerceCloud.utils()).click(findTestObject("Cart/ContinueShopping"))) {
					KeywordUtil.logInfo("Cannot click Continue Shopping")
					return false
				}

				if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Cart/AntennasFilterProducts"),60)) {
					KeywordUtil.logInfo("Cannot see the Antennas Filter Products")
					return false
				}
			}
		}else {
			if (!(new commerceCloud.utils()).click(findTestObject("Cart/ContinueShopping"))) {
				KeywordUtil.logInfo("Cannot click Continue Shopping")
				return false
			}

			if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Cart/AntennasFilterProducts"),60)) {
				KeywordUtil.logInfo("Cannot see the Antennas Filter Products")
				return false
			}

			KeywordUtil.logInfo("Cannot see the New Cart and Save Cart is present")
		}

		KeywordUtil.logInfo("Validated the New Cart")
		return true
	}

	/**
	 * ExportProductDetailsFile()
	 * @return true if Verify the export product details is Sku, Quanity, Manufacturer, Part #, UPC, Product name, Unit price, Total price, otherwise false
	 */

	@Keyword
	def ExportProductDetailsFile() {

		String Path = ('C:\\Users\\' + System.getProperty('user.name')) + '\\Downloads\\cart.csv'
		def file = new File(Path)

		KeywordUtil.logInfo("Verify the export product details is Sku, Quanity, Manufacturer, Part #, UPC, Product name, Unit price, Total price")

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Cart/Cart"),60)) {
			KeywordUtil.logInfo("Cannot see Cart")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/Cart"))) {
			KeywordUtil.logInfo("Cannot click Cart")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Cart/ExportCSV"),60)) {
			KeywordUtil.logInfo("Cannot see the Export CSV")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/ExportCSV"))) {
			KeywordUtil.logInfo("Cannot click Export CSV")
			return false
		}


		if (!(new commerceCloud.utils().delay(10))) {
			KeywordUtil.logInfo("Cannot wait a sec")
			return false
		}

		String strRow = ((new commerceCloud.utils()).ReadCSVFile(Path, 0))

		String[] values = strRow.split(',')
		boolean blnSku = false
		boolean blnName = false
		boolean blnQuantity = false
		boolean blnManufacturer = false
		boolean blnUPC = false
		boolean blnUnit = false
		boolean blnTotal = false
		boolean blnValues = false

		for (String a : values){
			if(a.equalsIgnoreCase("[Sku")){
				blnSku = true
			}else if(a.equalsIgnoreCase("Name")){
				blnName = true
			}else if(a.equalsIgnoreCase("Quantity")){
				blnQuantity = true
			}else if(a.equalsIgnoreCase("Manufacturer Part #")){
				blnManufacturer = true
			}else if(a.equalsIgnoreCase("UPC")){
				blnUPC = true
			}else if(a.equalsIgnoreCase("Unit Price")){
				blnUnit = true
			}else if(a.equalsIgnoreCase("Total Price]")){
				blnTotal = true
			}

			if((blnSku = true) && (blnName = true) && (blnQuantity = true) && (blnManufacturer = true) && (blnUPC = true) && (blnUnit = true) && (blnTotal = true)){
				break;
			}
		}

		if (!(blnSku)) {
			KeywordUtil.logInfo("Cannot see the Sku under CSV File")
			return false
		}

		if (!(blnName)) {
			KeywordUtil.logInfo("Cannot see the Quantity under CSV File")
			return false
		}

		if (!(blnQuantity)) {
			KeywordUtil.logInfo("Cannot see the Manufacturer Part # under CSV File")
			return false
		}

		if (!(blnManufacturer)) {
			KeywordUtil.logInfo("Cannot see the UPC under CSV File")
			return false
		}

		if (!(blnUPC)) {
			KeywordUtil.logInfo("Cannot see the Name under CSV File")
			return false
		}

		if (!(blnUnit)) {
			KeywordUtil.logInfo("Cannot see the Unit Price under CSV File")
			return false
		}

		if (!(blnTotal)) {
			KeywordUtil.logInfo("Cannot see the Total Price under CSV File")
			return false
		}

		if (!(file.exists())) {
			KeywordUtil.logInfo("Cannot Download cart CSV File")
			return false
		}

		if (!(file.delete())) {
			KeywordUtil.logInfo("Cannot Delete the Download cart CSV File")
			return false
		}

		KeywordUtil.logInfo("Verified the exported product details Sku, Quanity, Manufacturer, Part #, UPC, Product name, Unit price, Total price")
		return true
	}

	/**
	 * importSKUFromManually
	 * @param
	 * @param
	 * @param ProjectName - is the name of the project to create... send null if using system created Date or specifying a prefix in global variables
	 * @return true if created, otherwise false
	 */

	@Keyword
	def importSKUFromManually(String partsName = null, String qualitySize = null) {

		KeywordUtil.logInfo("Verify the Import from SKU in Manually page displays")

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Search/Cart"),30)) {
			KeywordUtil.logInfo("Cannot see the Cart")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Search/Cart"))) {
			KeywordUtil.logInfo("Cannot click Cart")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Cart/Import"),30)) {
			KeywordUtil.logInfo("Cannot see the Import")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/Import"))) {
			KeywordUtil.logInfo("Cannot click Import")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Cart/Import from File"),30)) {
			KeywordUtil.logInfo("Cannot see the Import from File")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("Cart/SKU 1"),partsName)) {
			KeywordUtil.logInfo("Cannot enter SKU 1")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("Cart/QTY 1"),qualitySize)) {
			KeywordUtil.logInfo("Cannot enter QTY 1")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/Import Manual"))) {
			KeywordUtil.logInfo("Cannot click Import Manual")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementDidNotDisplayed(null)) {
			KeywordUtil.logInfo("Can see the Load image")
			return false
		}


		TestObject to = new TestObject("Project Name")
		String xpath = "//div[@class='col-xs-12 col-sm-8']//li[text()='" + partsName + "']"
		TestObjectProperty prop = new TestObjectProperty("xpath", ConditionType.EQUALS, xpath)
		to.addProperty(prop)

		if(!(new commerceCloud.utils()).verifyPresent(to,30)){
			KeywordUtil.logInfo("Cannot see the Upload SKUs name")
			return false
		}

		KeywordUtil.logInfo("Verified the Import from Manually page displayed")
		return true
	}



	/**
	 * importProducts(String strListName = null, String partsName = null, String qualitySize = null)
	 * @param strListName - list name
	 * @param partsName - parts number
	 * @param qualitySize - quality size of particular parts
	 * @return true if list is created and validated the presents, otherwise false
	 */

	@Keyword
	def importCSVProducts(String strListName = null, String partsName = null, String qualitySize = null) {

		String sListName = ""
		String uploadFilePath = new File('').absolutePath + '\\Template\\ImportCSV.csv'
		String uploadWrongFilePath = new File('').absolutePath + '\\Template\\ImportCSVNonFormate.xlsx'

		String Path = ('C:\\Users\\' + System.getProperty('user.name')) + '\\Downloads\\cart.csv'
		def file = new File(Path)

		KeywordUtil.logInfo("Verify the Import product from cart menu")

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Cart/Cart"),60)) {
			KeywordUtil.logInfo("Cannot see Cart")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/Cart"))) {
			KeywordUtil.logInfo("Cannot click Cart")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Cart/ImportCSV"),60)) {
			KeywordUtil.logInfo("Cannot see Import CSV")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/ImportCSV"))) {
			KeywordUtil.logInfo("Cannot click Import CSV")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("ImportCSV/DownloadSampleCSV"),60)) {
			KeywordUtil.logInfo("Cannot see the Download Sample CSV")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("ImportCSV/DownloadSampleCSV"))) {
			KeywordUtil.logInfo("Cannot click Download Sample CSV")
			return false
		}

		if (!(new commerceCloud.utils().delay(10))) {
			KeywordUtil.logInfo("Cannot wait a sec")
			return false
		}

		if (!(file.exists())) {
			KeywordUtil.logInfo("Cannot Download Sample CSV File")
			return false
		}

		if (!(file.delete())) {
			KeywordUtil.logInfo("Cannot Delete the Download Sample CSV File")
			return false
		}

		if (!(new commerceCloud.utils()).writeToCSVFile(uploadFilePath, partsName, qualitySize)) {
			KeywordUtil.logInfo("Cannot write CSV File data")
			return false
		}

		if (!(new commerceCloud.utils().delay(1))) {
			KeywordUtil.logInfo("Cannot wait a sec")
			return false
		}


		if (!(new commerceCloud.utils()).sendKeysOutWait(findTestObject("ImportCSV/csvFile"),uploadWrongFilePath)) {
			KeywordUtil.logInfo("Cannot enter csv File")
			return false
		}


		if (!(new commerceCloud.utils()).click(findTestObject("ImportCSV/uploadCsvButton"))) {
			KeywordUtil.logInfo("Cannot click upload Csv Button")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("ImportCSV/ImportCSVError"),60)) {
			KeywordUtil.logInfo("Cannot see the Import CSV File Error message")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("ImportCSV/Remove"))) {
			KeywordUtil.logInfo("Cannot click Remove Button")
			return false
		}

		if (!(new commerceCloud.utils().delay(5))) {
			KeywordUtil.logInfo("Cannot wait 5 sec")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeysOutWait(findTestObject("ImportCSV/csvFile"),uploadFilePath)) {
			KeywordUtil.logInfo("Cannot enter csv File")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("ImportCSV/uploadCsvButton"))) {
			KeywordUtil.logInfo("Cannot click upload Csv Button")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("ImportCSV/ImportCSVSuccess"),60)) {
			KeywordUtil.logInfo("Cannot see the Import CSV Success")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("ImportCSV/RemoveCSVTemplate"))) {
			KeywordUtil.logInfo("Cannot click Remove CSV Template")
			return false
		}

		TestObject toProduct = new TestObject("Upload CSV File SKU No")
		String xpathProduct = "//div[contains(text(),'" + partsName + "')]"
		TestObjectProperty propProduct = new TestObjectProperty("xpath", ConditionType.EQUALS, xpathProduct)
		toProduct.addProperty(propProduct)

		if(!(new commerceCloud.utils()).verifyPresent(toProduct,60)){
			KeywordUtil.logInfo("Cannot see the Upload CSV File SKU No")
			return false
		}

		TestObject toQty = new TestObject("Upload CSV File Qty")
		String xpathQty = "//input[@value='" + qualitySize + "' and @ type='text']"
		TestObjectProperty propQty = new TestObjectProperty("xpath", ConditionType.EQUALS, xpathQty)
		toQty.addProperty(propQty)

		if(!(new commerceCloud.utils()).verifyPresent(toQty,60)){
			KeywordUtil.logInfo("Cannot see the Upload CSV File Qty")
			return false
		}

		KeywordUtil.logInfo("Verified the Import product from cart menu")
		return true
	}

	/**
	 * quickOrder()
	 * @return true if list is created and validated the presents, otherwise false
	 */

	@Keyword
	def quickOrder() {

		KeywordUtil.logInfo("Verify the user can add skus using Quick Order")

		String strSku2 = GlobalVariable.strSku3
		String strSku3 = GlobalVariable.strSku2


		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("QuickOrder/QuickOrder"),60)) {
			KeywordUtil.logInfo("Cannot see Quick Order")
			return false
		}
 
		if (!(new commerceCloud.utils()).click(findTestObject("QuickOrder/QuickOrder"))) {
			KeywordUtil.logInfo("Cannot click Quick Order")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsEnabled(findTestObject("QuickOrder/ClearAll"),60)) {
			KeywordUtil.logInfo("Cannot see Clear All")
			return false
		}

		if (!(new commerceCloud.utils()).setText(findTestObject("QuickOrder/EnterSku1"),strSku2)) {
			KeywordUtil.logInfo("Cannot enter Enter Sku 1")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("QuickOrder/EnterSku2"))) {
			KeywordUtil.logInfo("Cannot click Enter Sku 2 text")
			return false
		}


		if (!(new commerceCloud.utils()).setText(findTestObject("QuickOrder/EnterSku2"),strSku3)) {
			KeywordUtil.logInfo("Cannot enter Enter Sku 2")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("QuickOrder/EnterSku3"))) {
			KeywordUtil.logInfo("Cannot click Enter Sku 3 text")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("QuickOrder/AddSKU"))) {
			KeywordUtil.logInfo("Cannot click Add SKU")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("QuickOrder/EnterSku4"))) {
			KeywordUtil.logInfo("Cannot click Enter Sku 4")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("QuickOrder/AddToCartBottom"),60)) {
			KeywordUtil.logInfo("Cannot see the Add To Cart Bottom")
			return false
		}

		if (!(new commerceCloud.utils()).delay(10)) {
			KeywordUtil.logInfo("Cannot wait 10 sec")
			return false
		}

		String strActOrderTotalQO = new commerceCloud.utils().sReturnText(findTestObject("QuickOrder/OrderTotal"))

		if (!(new commerceCloud.utils()).click(findTestObject("QuickOrder/AddToCartBottom"))) {
			KeywordUtil.logInfo("Cannot click Add To Cart Bottom")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsEnabled(findTestObject("Cart/CableOrderTotal"),60)) {
			KeywordUtil.logInfo("Cannot see Order Total")
			return false
		}

		if (!(new commerceCloud.utils()).delay(10)) {
			KeywordUtil.logInfo("Cannot wait 10 sec")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsEnabled(findTestObject("Cart/CableOrderTotal"),60)) {
			KeywordUtil.logInfo("Cannot see Order Total")
			return false
		}
		
		String strExpOrderTotalQO = new commerceCloud.utils().sReturnText(findTestObject("Cart/CableOrderTotal"))
		String strChangeExpOrderTotalQO = strExpOrderTotalQO.replace("*", "")
		
		if(!(strActOrderTotalQO.trim().equalsIgnoreCase(strChangeExpOrderTotalQO.trim()))){
			KeywordUtil.logInfo("Order Total is missing the Quick Order Or Cart page")
			return false
		}

		if (!(new commerceCloud.utils().scrollToElement(findTestObject("Cart/ClearCart")))) {
			KeywordUtil.logInfo("Cannot scroll the Clear Cart")
			return false
		}
		
		if (!(new commerceCloud.utils()).click(findTestObject("Cart/ClearCart"))) {
			KeywordUtil.logInfo("Cannot click Clear Cart")
			return false
		}
		
		KeywordUtil.logInfo("Verified the user can add skus using Quick Order")
		return true
	}


	/**
	 * quickOrderValidation()
	 * @return true if list is created and validated the presents, otherwise false
	 */

	@Keyword
	def quickOrderValidation() {

		KeywordUtil.logInfo("Verify the UI And Logic validation in Quick Add screen")

		String strSku1 = "12345"
		String strSku2 = GlobalVariable.strSku1
		String strSku3 = GlobalVariable.strSku2
		String strSku4 = GlobalVariable.strSku3
		String strSku5 = GlobalVariable.strProductReqAdditional
		String strSku6 = GlobalVariable.strCableSku1

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("QuickOrder/QuickOrder"),60)) {
			KeywordUtil.logInfo("Cannot see Quick Order")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Logo/TesscoHomePageLogo"),1)) {
			KeywordUtil.logInfo("Cannot see the Tessco Home Page Logo")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("QuickOrder/QuickOrder"))) {
			KeywordUtil.logInfo("Cannot click Quick Order")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("QuickOrder/ClearAll"),60)) {
			KeywordUtil.logInfo("Cannot see Clear All")
			return false
		}

		if (!(new commerceCloud.utils()).setText(findTestObject("QuickOrder/EnterSku1"),strSku1)) {
			KeywordUtil.logInfo("Cannot enter Enter Sku 1")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("QuickOrder/EnterSku2"))) {
			KeywordUtil.logInfo("Cannot click Enter Sku2")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("QuickOrder/EnterSku3"))) {
			KeywordUtil.logInfo("Cannot click Enter Sku3")
			return false
		}
		
		if (!(new commerceCloud.utils()).delay(10)) {
			KeywordUtil.logInfo("Cannot wait 10 sec")
			return false
		}
		
		if (!(new commerceCloud.utils()).verifyVisibleOfElement(findTestObject("QuickOrder/ProductNotFound"),60)) {
			KeywordUtil.logInfo("Cannot see Product Not Found")
			return false
		}


		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("QuickOrder/ClearAll"),60)) {
			KeywordUtil.logInfo("Cannot see Clear All")
			return false
		}

		if (!(new commerceCloud.utils()).setText(findTestObject("QuickOrder/EnterSku1"),strSku5)) {
			KeywordUtil.logInfo("Cannot enter Enter Sku 1")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("QuickOrder/EnterSku3"))) {
			KeywordUtil.logInfo("Cannot click Enter Sku2")
			return false
		}

		if (!(new commerceCloud.utils()).mouseOver(findTestObject("QuickOrder/ProductReqAdditionalConfiguration"))) {
			KeywordUtil.logInfo("Cannot mouse Over This product requires additional configuration. Please configure the product within the cart before placing your order")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("QuickOrder/ProductReqAdditionalConfiguration"),10)) {
			KeywordUtil.logInfo("verify Element did not Displayed this product requires additional configuration. Please configure the product within the cart before placing your order")
			return false
		}

		if (!(new commerceCloud.utils()).setText(findTestObject("QuickOrder/EnterSku1"),strSku1)) {
			KeywordUtil.logInfo("Cannot enter Enter Sku 1")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("QuickOrder/EnterSku3"))) {
			KeywordUtil.logInfo("Cannot click Enter Sku2")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementDidNotDisplayed(findTestObject("QuickOrder/ProductReqAdditionalConfiguration"),10)) {
			KeywordUtil.logInfo("verify Element is Displayed this product requires additional configuration. Please configure the product within the cart before placing your order")
			return false
		}

		if (!(new commerceCloud.utils()).setText(findTestObject("QuickOrder/EnterSku1"),strSku5)) {
			KeywordUtil.logInfo("Cannot enter Enter Sku 1")
			KeywordUtil.logInfo(strSku5)
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("QuickOrder/EnterSku2"))) {
			KeywordUtil.logInfo("Cannot click Enter Sku2")
			return false
		}

		if (!(new commerceCloud.utils()).verifyVisibleOfElement(findTestObject("QuickOrder/ProductPricePerUnit1"),60)) {
			KeywordUtil.logInfo("Cannot see Product Price Per Unit 1")
			return false
		}

		if (!(new commerceCloud.utils()).mouseOver(findTestObject("QuickOrder/ProductReqAdditionalConfiguration"))) {
			KeywordUtil.logInfo("Cannot mouse Over This product requires additional configuration. Please configure the product within the cart before placing your order")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("QuickOrder/ProductReqAdditionalConfiguration"),10)) {
			KeywordUtil.logInfo("verify Element did not Displayed this product requires additional configuration. Please configure the product within the cart before placing your order")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("QuickOrder/EnterSku2"))) {
			KeywordUtil.logInfo("Cannot click Enter Sku2")
			return false
		}

		if (!(new commerceCloud.utils()).setText(findTestObject("QuickOrder/EnterSku1"),strSku2)) {
			KeywordUtil.logInfo("Cannot enter Enter Sku 1")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("QuickOrder/EnterSku2"))) {
			KeywordUtil.logInfo("Cannot click Enter Sku2")
			return false
		}

		if (!(new commerceCloud.utils()).verifyVisibleOfElement(findTestObject("QuickOrder/ProductPricePerUnit1"),60)) {
			KeywordUtil.logInfo("Cannot see Product Price Per Unit 1")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("QuickOrder/ClearAll"))) {
			KeywordUtil.logInfo("Cannot click Clear All")
			return false
		}

		if (!(new commerceCloud.utils()).acceptAlert()) {
			KeywordUtil.logInfo("Cannot accept Alert")
			return false
		}

		if (!(new commerceCloud.utils()).verifyVisibleOfElement(findTestObject("QuickOrder/EnterSku1"),60)) {
			KeywordUtil.logInfo("Cannot see Enter Sku 1")
			return false
		}

		if (!(new commerceCloud.utils()).setText(findTestObject("QuickOrder/EnterSku1"),strSku2)) {
			KeywordUtil.logInfo("Cannot enter Enter Sku 1")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("QuickOrder/EnterSku2"))) {
			KeywordUtil.logInfo("Cannot click Enter Sku2")
			return false
		}
				
		if (!(new commerceCloud.utils()).setText(findTestObject("QuickOrder/EnterSku2"),strSku3)) {
			KeywordUtil.logInfo("Cannot enter Enter Sku 2")
			return false
		}

		if (!(new commerceCloud.utils()).setText(findTestObject("QuickOrder/EnterSku3"),strSku4)) {
			KeywordUtil.logInfo("Cannot enter Enter Sku 3")
			return false
		}

		String strBeforeFirstProduct =  (new commerceCloud.utils()).sReturnText(findTestObject("QuickOrder/ProductPrice2"))
		strBeforeFirstProduct = strBeforeFirstProduct.replaceAll('[^a-zA-Z0-9]', '')
		
		
		if (!(new commerceCloud.utils()).click(findTestObject("QuickOrder/AddSKU"))) {
			KeywordUtil.logInfo("Cannot click Add SKU")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("QuickOrder/EnterSku4"))) {
			KeywordUtil.logInfo("Cannot click Enter Sku 4")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("QuickOrder/ProductPricePerUnit3"),60)) {
			KeywordUtil.logInfo("Cannot see the Product Price Per Unit 3")
			return false
		}

		String strBeforeUpdate =  (new commerceCloud.utils()).sReturnText(findTestObject("QuickOrder/OrderTotal"))
		strBeforeUpdate = strBeforeUpdate.replaceAll('[^a-zA-Z0-9]', '')

		if (!(new commerceCloud.utils()).click(findTestObject("QuickOrder/RemoveQuickOrder3"))) {
			KeywordUtil.logInfo("Cannot click Remove Quick Order 3")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("QuickOrder/ClearAll"),60)) {
			KeywordUtil.logInfo("Cannot see the Checkout button")
			return false
		}

		if (!(new commerceCloud.utils()).verifyVisibleOfElement(findTestObject("Logo/TesscoHomePageLogo"),1)) {
			KeywordUtil.logInfo("Cannot see the Tessco Home Page Logo")
			return false
		}

		if (!(new commerceCloud.utils()).setText(findTestObject("QuickOrder/EnterSku3"),strSku1)) {
			KeywordUtil.logInfo("Cannot enter Enter Sku 1")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("QuickOrder/EnterSku2"))) {
			KeywordUtil.logInfo("Cannot click Enter Sku2")
			return false
		}

		String strAfterUpdate =  (new commerceCloud.utils()).sReturnText(findTestObject("QuickOrder/OrderTotal"))
		strAfterUpdate = strAfterUpdate.replaceAll('[^a-zA-Z0-9]', '')

		if ((strBeforeUpdate.equalsIgnoreCase(strAfterUpdate))) {
			KeywordUtil.logInfo("Cannot Verify the order total in the 'Quick Order Summary' reflects the total price of the 2 skus")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("QuickOrder/RemoveQuickOrder2"))) {
			KeywordUtil.logInfo("Cannot click Remove Quick Order 2")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("QuickOrder/ClearAll"),60)) {
			KeywordUtil.logInfo("Cannot see the Checkout button")
			return false
		}
	
		if (!(new commerceCloud.utils()).verifyVisibleOfElement(findTestObject("ProductSearch/search tessco com textbox"),60)) {
			KeywordUtil.logInfo("Cannot see the search tessco com textbox")
			return false
		}

		if (!(new commerceCloud.utils()).setText(findTestObject("ProductSearch/search tessco com textbox"), strSku3)) {
			KeywordUtil.logInfo("Cannot enter search tessco com textbox")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("ProductSearch/search tessco com button"))) {
			KeywordUtil.logInfo("Cannot click search tessco com button")
			return false
		}

		if (!(new commerceCloud.utils()).acceptAlert()) {
			KeywordUtil.logInfo("Cannot accept Alert")
			return false
		}

		if (!(new commerceCloud.utils()).verifyVisibleOfElement(findTestObject("ProductSearch/product listing price"),60)) {
			KeywordUtil.logInfo("Cannot see the product listing price")
			return false
		}

		String strAfterProduct =  (new commerceCloud.utils()).sReturnText(findTestObject("ProductSearch/product listing price"))
		strAfterProduct = strAfterProduct.replaceAll('[^a-zA-Z0-9]', '')
		println (strBeforeFirstProduct)
		println (strAfterProduct)
		
		if (!(strBeforeFirstProduct.equalsIgnoreCase(strAfterProduct))) {
			KeywordUtil.logInfo("Cannot Verify the user is taken to the PDP and the price matches the price noted in the quick order view")
			return false
		}

		if (!(new commerceCloud.utils()).verifyVisibleOfElement(findTestObject("QuickOrder/QuickOrder"),60)) {
			KeywordUtil.logInfo("Cannot see Quick Order")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("QuickOrder/QuickOrder"))) {
			KeywordUtil.logInfo("Cannot click Quick Order")
			return false
		}

		if (!(new commerceCloud.utils()).verifyVisibleOfElement(findTestObject("QuickOrder/ClearAll"),60)) {
			KeywordUtil.logInfo("Cannot see Clear All")
			return false
		}

		String strQuantityMaxLength =  (new commerceCloud.utils()).GetAttributeText(findTestObject("QuickOrder/QtyQuickOrderListInput1"),"maxlength")

		if (!(strQuantityMaxLength.equalsIgnoreCase("3"))) {
			KeywordUtil.logInfo("Cannot Verify '999' is able to be entered")
			return false
		}

		if (!(new commerceCloud.utils()).verifyVisibleOfElement(findTestObject("Logo/TesscoHomePageLogo"),1)) {
			KeywordUtil.logInfo("Cannot see the Tessco Home Page Logo")
			return false
		}

		KeywordUtil.logInfo("Verified the UI And Logic validation in Quick Add screen")
		return true
	}

	/**
	 * AddSeveralCart(def Search = null, def cartName = null)
	 * @param Search - user should send the tessco product details
	 * @return true if UI: Cart details page - Order Summary, otherwise false
	 */

	@Keyword
	def AddSeveralCart(def Search, def cartName) {

		KeywordUtil.logInfo("Validate the Cart details page - Order Summary")

		if (!(new commerceCloud.Cart()).addProductToCart(Search)) {
			KeywordUtil.logInfo("Cannot search the Tessco Product")
			return false
		}

		String strCartName = (new commerceCloud.Cart()).getCartName(cartName)
		if (!(new commerceCloud.Cart()).saveCart(strCartName)) {
			KeywordUtil.logInfo("Cannot Saved Carts & Quotes")
			return false
		}

		KeywordUtil.logInfo("Validated The Cart details page - Order Summary")
		return true
	}

	/**
	 * logout
	 * @return true if logout completed, otherwise false
	 */

	@Keyword
	def logout() {

		KeywordUtil.logInfo("Logout operation is triggered")

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Login/Signed In As"),60)) {
			KeywordUtil.logInfo("Cannot see Signed In As")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Login/Signed In As"))) {
			KeywordUtil.logInfo("Cannot click Signed In As")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Login/Log Out"),60)) {
			KeywordUtil.logInfo("Cannot see Log Out")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Login/Log Out"))) {
			KeywordUtil.logInfo("Cannot click Log Out")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Login/Log In"),60)) {
			KeywordUtil.logInfo("Cannot see Log In")
			return false
		}

		KeywordUtil.logInfo("Successfully logout the commerce cloud application")
		return true
	}

	/**
	 * RestoreSavedCartToActiveCart( def cartName, String  Search)
	 * @param Search - user should send Cart Name
	 * @return true if Navigate to Saved Carts Quotes and Restore, otherwise false
	 */

	@Keyword
	def RestoreSavedCartToActiveCart(def cartName, String  Search) {

		KeywordUtil.logInfo("Validate Review my saved carts and restore the saved cart to active cart")

		String strCartName = (new commerceCloud.Cart()).getCartName(cartName)

		if (!(new commerceCloud.Cart()).navigateSavedCartsQuotes(strCartName)) {
			KeywordUtil.logInfo("Cannot navigate to Saved Carts Quotes Page")
			return false
		}

		if (!(new commerceCloud.Cart()).RestoreAndChangeQty(strCartName)) {
			KeywordUtil.logInfo("Cannot Restore Saved Carts & Quotes")
			return false
		}
		KeywordUtil.logInfo("Validated Review my saved carts and restore the saved cart to active cart")
		return true
	}

	/**
	 * CompleteOrderingProcess(String Search)
	 * @param Search - user should send the tessco product details
	 * @return true if Complete the ordering process, otherwise false
	 */

	@Keyword
	def CompleteOrderingProcess(String strYesOrNo) {

		KeywordUtil.logInfo("Validate the Complete the ordering process")
		String YesOrNo = strYesOrNo
		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderSummary/Checkout"),60)) {
			KeywordUtil.logInfo("Cannot see the Checkout")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("OrderSummary/Checkout"))) {
			KeywordUtil.logInfo("Cannot click Checkout")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderSummary/YesKeepItSaved"),60)) {
			KeywordUtil.logInfo("Cannot see Yes Keep It Saved button")
			return false
		}

		if(strYesOrNo.equalsIgnoreCase("Yes")){
			if (!(new commerceCloud.utils()).click(findTestObject("OrderSummary/YesKeepItSaved"))) {
				KeywordUtil.logInfo("Cannot click Yes Keep It Saved button")
				return false
			}
		}else if(strYesOrNo.equalsIgnoreCase("No")){
			if (!(new commerceCloud.utils()).click(findTestObject("OrderSummary/NoDeleteIt"))) {
				KeywordUtil.logInfo("Cannot click No Delete It button")
				return false
			}
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderSummary/ContinueToPayment"),60)) {
			KeywordUtil.logInfo("Cannot see the Continue To Payment")
			return false
		}

		if (!(new commerceCloud.Checkout()).DeliveryOption()) {
			KeywordUtil.logInfo("Cannot select Delivery Option")
			return false
		}

		if (!(new commerceCloud.utils()).scrollToElement(findTestObject("OrderSummary/ContinueToPayment"))) {
			KeywordUtil.logInfo("Cannot scroll the Continue To Payment")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("OrderSummary/ContinueToPayment"))) {
			KeywordUtil.logInfo("Cannot click Continue To Payment")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderSummary/ContinueToReview"),60)) {
			KeywordUtil.logInfo("Cannot see the Continue To Review")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Payment/Payment_Type_Selection_ACCOUNT"))) {
			KeywordUtil.logInfo("Cannot click Account Payment")
			return false
		}


		if (!(new commerceCloud.utils()).sendKeys(findTestObject("OrderSummary/PurchaseOrderNumber"),"999999")) {
			KeywordUtil.logInfo("Cannot send Purchase Order Number text")
			return false
		}

		if (!(new commerceCloud.utils()).scrollToElement(findTestObject("OrderSummary/ContinueToReview"))) {
			KeywordUtil.logInfo("Cannot scroll the Continue To Review")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("OrderSummary/ContinueToReview"))) {
			KeywordUtil.logInfo("Cannot click Continue To Review")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderSummary/PlaceOrder"),60)) {
			KeywordUtil.logInfo("Cannot see the Place Order")
			return false
		}

		if (!(new commerceCloud.utils()).scrollToElement(findTestObject("OrderSummary/PlaceOrder"))) {
			KeywordUtil.logInfo("Cannot scroll the Place Order")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("OrderSummary/PlaceOrder"))) {
			KeywordUtil.logInfo("Cannot click Place Order")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderSummary/ThankYou"),60)) {
			KeywordUtil.logInfo("Cannot see the Thank You")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/OrderContinueShopping"))) {
			KeywordUtil.logInfo("Cannot click Order Continue Shopping")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("QuickOrder/QuickOrder"),60)) {
			KeywordUtil.logInfo("Cannot see the Quick Order")
			return false
		}

		KeywordUtil.logInfo("Validated the Complete the ordering process")
		return true
	}

	/**
	 * PersistSavedConvertedOrder( def cartName, String  Search)
	 * @param Search - user should send Cart Name
	 * @return true if Navigate to Saved Carts Quotes and Restore, otherwise false
	 */

	@Keyword
	def PersistSavedConvertedOrder(def cartName) {

		KeywordUtil.logInfo("Validate the permanently save a cart so the saved cart will persist even after the saved cart has been converted into an order")

		String strCartName = (new commerceCloud.Cart()).getCartName(cartName)

		if (!(new commerceCloud.Cart()).navigateSavedCartsQuotes(strCartName)) {
			KeywordUtil.logInfo("Cannot navigate to Saved Carts Quotes Page")
			return false
		}

		if (!(new commerceCloud.Cart()).RestoreOnly(strCartName)) {
			KeywordUtil.logInfo("Cannot Restore Saved Carts & Quotes")
			return false
		}

		if (!(new commerceCloud.Cart()).CompleteOrderingProcess("Yes")) {
			KeywordUtil.logInfo("Cannot Complete Ordering Process")
			return false
		}

		if (!(new commerceCloud.Cart()).navigateSavedCartsQuotes(strCartName)) {
			KeywordUtil.logInfo("Cannot navigate to Saved Carts Quotes Page")
			return false
		}

		if (!(new commerceCloud.Cart()).RestoreOnly(strCartName)) {
			KeywordUtil.logInfo("Cannot Restore Saved Carts & Quotes")
			return false
		}

		if (!(new commerceCloud.Cart()).CompleteOrderingProcess("No")) {
			KeywordUtil.logInfo("Cannot Complete Ordering Process")
			return false
		}

		if (!(new commerceCloud.Cart()).NoLongerSavedCartsQuotes(strCartName)) {
			KeywordUtil.logInfo("Cannot Remove Save Items under Save Carts & Quotes")
			return false
		}

		KeywordUtil.logInfo("Validated the permanently save a cart so the saved cart will persist even after the saved cart has been converted into an order")
		return true
	}

	/**
	 * EditSaveCart(def Search = null, def cartName = null)
	 * @param Search - user should send the tessco product details
	 * @param cartName - user should pass the cart name
	 * @return true if the ability to edit a saved cart, otherwise false
	 */

	@Keyword
	def EditSaveCart(def Search, def cartName) {

		KeywordUtil.logInfo("Validate the ability to edit a saved cart")
		String strCartName = (new commerceCloud.Cart()).getCartName(cartName)

		if (!(new commerceCloud.Cart().NewCart())) {
			KeywordUtil.markFailedAndStop('Can not click the new cart')
			return false
		}

		if (!(new commerceCloud.Cart()).addOneProductToCart(Search)) {
			KeywordUtil.logInfo("Cannot search the Tessco Product")
			return false
		}

		if (!(new commerceCloud.Cart()).navigateSavedCartsQuotes(strCartName)) {
			KeywordUtil.logInfo("Cannot navigate to Saved Carts Quotes Page")
			return false
		}

		if (!(new commerceCloud.Cart()).SaveItems(strCartName)) {
			KeywordUtil.logInfo("Cannot Verify a 'Remove items' popup modal for the active cart displays")
			return false
		}

		if (!(new commerceCloud.Cart()).navigateSavedCartsQuotes(strCartName)) {
			KeywordUtil.logInfo("Cannot navigate to Saved Carts Quotes Page")
			return false
		}

		if (!(new commerceCloud.Cart()).RestoreAndChangeQty(strCartName)) {
			KeywordUtil.logInfo("Cannot Restore Saved Carts & Quotes")
			return false
		}

		KeywordUtil.logInfo("Validated the ability to edit a saved cart")
		return true
	}

	/**
	 * SaveCartSwitchAccountAndCartNotVisible(def Search = null, def cartName = null)
	 * @param Search - user should send the tessco product details
	 * @param cartName - user should send the cart name
	 * @param strAccTypeBefore - switch account type
	 * @param strAccTypeAfter - switch account type
	 * @return true if UI: Cart details page - Order Summary, otherwise false
	 */

	@Keyword
	def SaveCartSwitchAccountAndCartNotVisible(String Search, String cartName, String strAccTypeBefore, String strAccTypeAfter) {

		KeywordUtil.logInfo("Validate the Add Product, save it the cart, Switch Account,  cart saved for the previous account is not visible")

		if (!(new commerceCloud.Cart()).addProductToCart(Search)) {
			KeywordUtil.logInfo("Cannot search the Tessco Product")
			return false
		}

		String strCartName = (new commerceCloud.Cart()).getCartName(cartName)
		if (!(new commerceCloud.Cart()).saveCart(strCartName)) {
			KeywordUtil.logInfo("Cannot Saved Carts & Quotes")
			return false
		}

		if (!(new commerceCloud.ProductSearch().switchAccount(strAccTypeBefore))) {
			KeywordUtil.markFailedAndStop('Can not Switch Account : ' + strAccTypeBefore)
			return false
		}

		String strCartNameSec = (new commerceCloud.Cart()).getCartName(cartName)
		if (!(new commerceCloud.Cart()).NoLongerSavedCartsQuotes(strCartNameSec)) {
			KeywordUtil.logInfo("Cannot navigate to Saved Carts Quotes Page")
			return false
		}

		if (!(new commerceCloud.ProductSearch().switchAccount(strAccTypeAfter))) {
			KeywordUtil.markFailedAndStop('Can not Switch Account : ' + strAccTypeAfter)
			return false
		}

		if (!(new commerceCloud.Cart().removeAllSavedCart())) {
			KeywordUtil.markFailedAndStop('Can not remove All Saved Cart and Quote')
			return false
		}

		KeywordUtil.logInfo("Validated the Add Product, save it the cart, Switch Account,  cart saved for the previous account is not visible")
		return true
	}


	/**
	 * ExistRegistration(String sUserName, String jobTitle)
	 * @return true if Existing User is created and validated the presents, otherwise false
	 */ 
	@Keyword
	def ExistRegistration(String sUserName, String jobTitle) {
		KeywordUtil.logInfo("Verify the Registration Landing Page")
		String strUserName = (new commerceCloud.Cart()).getRegistrationUserName(sUserName)
		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Login/Sign In or Register"),60)) {
			KeywordUtil.logInfo("Cannot see Sign In or Register")
			return false
		}
		if (!(new commerceCloud.utils()).click(findTestObject("Login/Sign In or Register"))) {
			KeywordUtil.logInfo("Cannot click Sign In or Register")
			return false
		}
		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Registration/RegisterLink"),60)) {
			KeywordUtil.logInfo("Cannot see Register Link")
			return false
		}
		if (!(new commerceCloud.utils()).click(findTestObject("Registration/RegisterLink"))) {
			KeywordUtil.logInfo("Cannot click Register Link")
			return false
		}
		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Registration/JoinExistingAccount"),60)) {
			KeywordUtil.logInfo("Cannot see Join Existing Account")
			return false
		}
		if (!(new commerceCloud.utils()).click(findTestObject("Registration/JoinExistingAccount"))) {
			KeywordUtil.logInfo("Cannot click Join Existing Account")
			return false
		}
		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Registration/CompanyAccount"),60)) {
			KeywordUtil.logInfo("Cannot see Company Account")
			return false
		}
		if (!(new commerceCloud.utils()).setText(findTestObject("Registration/AccountNumber"), GlobalVariable.AccountNo)) {
			KeywordUtil.logInfo("Cannot Enter Account Number")
			return false
		}
		if (!(new commerceCloud.utils()).verifyElementIsEnabled(findTestObject("Registration/Continue"),30)) {
			KeywordUtil.logInfo("Cannot Enabled Continue button")
			return false
		}
		if (!(new commerceCloud.utils()).scrollToElement(findTestObject("Registration/Continue"))) {
			KeywordUtil.logInfo("Cannot scroll Continue")
			return false
		}
		if (!(new commerceCloud.utils()).click(findTestObject("Registration/Continue"))) {
			KeywordUtil.logInfo("Cannot click Continue")
			return false
		}
		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Registration/FirstName"),60)) {
			KeywordUtil.logInfo("Cannot see First Name")
			return false
		}
		if (!(new commerceCloud.utils()).setText(findTestObject("Registration/FirstName"), "Jermaine")) {
			KeywordUtil.logInfo("Cannot Enter First Name")
			return false
		}
		if (!(new commerceCloud.utils()).setText(findTestObject("Registration/LastName"), "Wilkins")) {
			KeywordUtil.logInfo("Cannot Enter Last Name")
			return false
		}
		if (!(new commerceCloud.utils()).setText(findTestObject("Registration/Email"), "wilkinsj@tessco.com")) {
			KeywordUtil.logInfo("Cannot Enter Email")
			return false
		}
		if (!(new commerceCloud.utils()).setText(findTestObject("Registration/Phone"), "4102291000")) {
			KeywordUtil.logInfo("Cannot Enter Phone")
			return false
		}
		if (!(new commerceCloud.utils()).click(findTestObject("Registration/JobTitle"))) {
			KeywordUtil.logInfo("Cannot click Job Title")
			return false
		}
		if(jobTitle.equalsIgnoreCase("ExecutiveManagement")){
			if (!(new commerceCloud.utils()).click(findTestObject("Registration/JobTitleExecutiveManagement"))) {
				KeywordUtil.logInfo("Cannot click Job Title Executive Management")
				return false
			}
		} else if(jobTitle.equalsIgnoreCase("FieldServices")){
			if (!(new commerceCloud.utils()).click(findTestObject("Registration/JobTitleFieldServices"))) {
				KeywordUtil.logInfo("Cannot click Job Title Field Services")
				return false
			}
		} else if(jobTitle.equalsIgnoreCase("FinanceOrTreasury")){
			if (!(new commerceCloud.utils()).click(findTestObject("Registration/JobTitleFinanceOrTreasury"))) {
				KeywordUtil.logInfo("Cannot click Job Title Finance Or Treasury")
				return false
			}
		} else if(jobTitle.equalsIgnoreCase("PurchasingOrLogistics")){
			if (!(new commerceCloud.utils()).click(findTestObject("Registration/JobTitlePurchasingOrLogistics"))) {
				KeywordUtil.logInfo("Cannot click Job Title Purchasing Or Logistics")
				return false
			}
		} else if(jobTitle.equalsIgnoreCase("SalesOrMarketing")){
			if (!(new commerceCloud.utils()).click(findTestObject("Registration/JobTitleSalesOrMarketing"))) {
				KeywordUtil.logInfo("Cannot click Job Title Sales Or Marketing")
				return false
			}
		} else if(jobTitle.equalsIgnoreCase("SystemOrNetworkDesign")){
			if (!(new commerceCloud.utils()).click(findTestObject("Registration/JobTitleSystemOrNetworkDesign"))) {
				KeywordUtil.logInfo("Cannot click Job Title System Or Network Design")
				return false
			}
		}
		if (!(new commerceCloud.utils()).sendKeysOutWait(findTestObject("Registration/UserName"), strUserName)) {
			KeywordUtil.logInfo("Cannot Enter User Name")
			return false
		}
		if (!(new commerceCloud.utils()).sendKeysOutWait(findTestObject("Registration/Password"), GlobalVariable.GlobalRegistrationPassword)) {
			KeywordUtil.logInfo("Cannot Enter Password")
			return false
		}
		if (!(new commerceCloud.utils()).sendKeysOutWait(findTestObject("Registration/ConfirmPassword"), GlobalVariable.GlobalRegistrationPassword)) {
			KeywordUtil.logInfo("Cannot Enter Confirm Password")
			return false
		}
		if (!(new commerceCloud.utils()).click(findTestObject("Registration/TermsCheck"))) {
			KeywordUtil.logInfo("Cannot click Terms Check")
			return false
		}
		if (!(new commerceCloud.utils()).click(findTestObject("Registration/PromationalEmailCheck"))) {
			KeywordUtil.logInfo("Cannot click Promational Email Check")
			return false
		}
		if (!(new commerceCloud.utils()).scrollToElement(findTestObject("Registration/tcomExistingRegistrationRegisterBtn"))) {
			KeywordUtil.logInfo("Cannot scroll Registration button")
			return false
		}
		if (!(new commerceCloud.utils()).click(findTestObject("Registration/tcomExistingRegistrationRegisterBtn"))) {
			KeywordUtil.logInfo("Cannot click Existing Registration")
			return false
		}
		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Registration/ThankYouForRegistering"),60)) {
			KeywordUtil.logInfo("Cannot see Thank You For Registering")
			return false
		}
		if (!(new commerceCloud.utils()).click(findTestObject("Registration/BackToTesscoCom"))) {
			KeywordUtil.logInfo("Cannot click Back To Tessco Comn")
			return false
		}
		KeywordUtil.logInfo("Verified the Registration Landing Page")
		return true
	}


	/**
	 * Registration(String sUserName, String jobTitle)
	 * @return true if Existing User is created and validated the presents, otherwise false
	 */

	@Keyword
	def Registration(String strAccountNumber, String strFirstName, String strLastName, String strEmail, String strUserName, String jobTitle) {

		KeywordUtil.logInfo("Verify the Registration Landing Page")
		//String strUserName = (new commerceCloud.Cart()).getRegistrationUserName(sUserName)

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Login/Sign In or Register"),60)) {
			KeywordUtil.logInfo("Cannot see Sign In or Register")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Login/Sign In or Register"))) {
			KeywordUtil.logInfo("Cannot click Sign In or Register")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Registration/RegisterLink"),60)) {
			KeywordUtil.logInfo("Cannot see Register Link")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Registration/RegisterLink"))) {
			KeywordUtil.logInfo("Cannot click Register Link")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Registration/JoinExistingAccount"),60)) {
			KeywordUtil.logInfo("Cannot see Join Existing Account")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Registration/JoinExistingAccount"))) {
			KeywordUtil.logInfo("Cannot click Join Existing Account")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Registration/CompanyAccount"),60)) {
			KeywordUtil.logInfo("Cannot see Company Account")
			return false
		}

		if (!(new commerceCloud.utils()).setText(findTestObject("Registration/AccountNumber"), strAccountNumber)) {
			KeywordUtil.logInfo("Cannot Enter Account Number")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsEnabled(findTestObject("Registration/Continue"),30)) {
			KeywordUtil.logInfo("Cannot Enabled Continue button")
			return false
		}

		if (!(new commerceCloud.utils()).scrollToElement(findTestObject("Registration/Continue"))) {
			KeywordUtil.logInfo("Cannot scroll Continue")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Registration/Continue"))) {
			KeywordUtil.logInfo("Cannot click Continue")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Registration/FirstName"),60)) {
			KeywordUtil.logInfo("Cannot see First Name")
			return false
		}

		if (!(new commerceCloud.utils()).setText(findTestObject("Registration/FirstName"), strFirstName)) {
			KeywordUtil.logInfo("Cannot Enter First Name")
			return false
		}

		if (!(new commerceCloud.utils()).setText(findTestObject("Registration/LastName"), strLastName)) {
			KeywordUtil.logInfo("Cannot Enter Last Name")
			return false
		}

		if (!(new commerceCloud.utils()).setText(findTestObject("Registration/Email"), strEmail)) {
			KeywordUtil.logInfo("Cannot Enter Email")
			return false
		}

		if (!(new commerceCloud.utils()).setText(findTestObject("Registration/Phone"), "4102291000")) {
			KeywordUtil.logInfo("Cannot Enter Phone")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Registration/JobTitle"))) {
			KeywordUtil.logInfo("Cannot click Job Title")
			return false
		}

		if(jobTitle.equalsIgnoreCase("ExecutiveManagement")){
			if (!(new commerceCloud.utils()).click(findTestObject("Registration/JobTitleExecutiveManagement"))) {
				KeywordUtil.logInfo("Cannot click Job Title Executive Management")
				return false
			}
		} else if(jobTitle.equalsIgnoreCase("FieldServices")){
			if (!(new commerceCloud.utils()).click(findTestObject("Registration/JobTitleFieldServices"))) {
				KeywordUtil.logInfo("Cannot click Job Title Field Services")
				return false
			}
		} else if(jobTitle.equalsIgnoreCase("FinanceOrTreasury")){
			if (!(new commerceCloud.utils()).click(findTestObject("Registration/JobTitleFinanceOrTreasury"))) {
				KeywordUtil.logInfo("Cannot click Job Title Finance Or Treasury")
				return false
			}
		} else if(jobTitle.equalsIgnoreCase("PurchasingOrLogistics")){
			if (!(new commerceCloud.utils()).click(findTestObject("Registration/JobTitlePurchasingOrLogistics"))) {
				KeywordUtil.logInfo("Cannot click Job Title Purchasing Or Logistics")
				return false
			}
		} else if(jobTitle.equalsIgnoreCase("SalesOrMarketing")){
			if (!(new commerceCloud.utils()).click(findTestObject("Registration/JobTitleSalesOrMarketing"))) {
				KeywordUtil.logInfo("Cannot click Job Title Sales Or Marketing")
				return false
			}
		} else if(jobTitle.equalsIgnoreCase("SystemOrNetworkDesign")){
			if (!(new commerceCloud.utils()).click(findTestObject("Registration/JobTitleSystemOrNetworkDesign"))) {
				KeywordUtil.logInfo("Cannot click Job Title System Or Network Design")
				return false
			}
		}


		if (!(new commerceCloud.utils()).sendKeysOutWait(findTestObject("Registration/UserName"), strUserName)) {
			KeywordUtil.logInfo("Cannot Enter User Name")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeysOutWait(findTestObject("Registration/Password"), GlobalVariable.GlobalRegistrationPassword)) {
			KeywordUtil.logInfo("Cannot Enter Password")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeysOutWait(findTestObject("Registration/ConfirmPassword"), GlobalVariable.GlobalRegistrationPassword)) {
			KeywordUtil.logInfo("Cannot Enter Confirm Password")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Registration/TermsCheck"))) {
			KeywordUtil.logInfo("Cannot click Terms Check")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Registration/PromationalEmailCheck"))) {
			KeywordUtil.logInfo("Cannot click Promational Email Check")
			return false
		}

		if (!(new commerceCloud.utils()).scrollToElement(findTestObject("Registration/tcomExistingRegistrationRegisterBtn"))) {
			KeywordUtil.logInfo("Cannot scroll Registration button")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Registration/tcomExistingRegistrationRegisterBtn"))) {
			KeywordUtil.logInfo("Cannot click Existing Registration")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Registration/ThankYouForRegistering"),60)) {
			KeywordUtil.logInfo("Cannot see Thank You For Registering")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Registration/BackToTesscoCom"))) {
			KeywordUtil.logInfo("Cannot click Back To Tessco Comn")
			return false
		}

		KeywordUtil.logInfo("Verified the Registration Landing Page")
		return true
	}


	/**
	 * NewRegistration(String strCountry, String strState, String strStateSalesTaxExempt, String strReceiveInvoicesViaEmail, String strDesiredPaymentTerms, String strJobTitle)
	 * @return true if New User is created and validated the presents, otherwise false
	 */

	@Keyword
	def NewRegistration(String strCountry, String strState, String strStateSalesTaxExempt, String strReceiveInvoicesViaEmail, String strDesiredPaymentTerms, String strJobTitle, String strUserNameExit, String strExitjobTitle) {

		KeywordUtil.logInfo("Verify the New Registration Landing Page")

		/*if (!(new commerceCloud.Cart().Registration(strUserNameExit, strExitjobTitle))) {
		 KeywordUtil.logInfo("Verified the Existing user Registration Landing Page")
		 return false
		 }
		 if (!((new commerceCloud.Cart()).logout())) {
		 KeywordUtil.markFailedAndStop('Can not logout the commerce Cloud')
		 return false
		 }
		 */
		String strNewUserName = GlobalVariable.GlobalRegistrationUserName + "New"
		String strUserName = (new commerceCloud.Cart()).getRegistrationUserName(strNewUserName)


		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Login/Sign In or Register"),60)) {
			KeywordUtil.logInfo("Cannot see Sign In or Register")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Login/Sign In or Register"))) {
			KeywordUtil.logInfo("Cannot click Sign In or Register")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Registration/RegisterLink"),60)) {
			KeywordUtil.logInfo("Cannot see Register Link")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Registration/RegisterLink"))) {
			KeywordUtil.logInfo("Cannot click Register Link")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("NewRegistration/NewAccount"),60)) {
			KeywordUtil.logInfo("Cannot see New Account")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("NewRegistration/NewAccount"))) {
			KeywordUtil.logInfo("Cannot click New Account")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("NewRegistration/CompanyLegalEntityName"),60)) {
			KeywordUtil.logInfo("Cannot see Company Legal Entity Name")
			return false
		}

		if (!(new commerceCloud.utils()).scrollToElement(findTestObject("NewRegistration/CompanyLegalEntityName"))) {
			KeywordUtil.logInfo("Cannot scroll Company Legal Entity Name")
			return false
		}

		if (!(new commerceCloud.utils()).setText(findTestObject("NewRegistration/CompanyLegalEntityName"), "TESSCO")) {
			KeywordUtil.logInfo("Cannot Enter Company Legal Entity Name")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("NewRegistration/Country"))) {
			KeywordUtil.logInfo("Cannot click Country")
			return false
		}


		/*if (!(new commerceCloud.utils()).clearsendKeys(findTestObject("NewRegistration/Country"), strCountry)) {
		 KeywordUtil.logInfo("Cannot Enter Country Name")
		 return false
		 }*/

		TestObject toCountry = new TestObject("Country Name")
		String xpathCountry = "//li[@data-value='"+strCountry +"']/a"
		TestObjectProperty propCountry = new TestObjectProperty("xpath", ConditionType.EQUALS, xpathCountry)
		toCountry.addProperty(propCountry)


		if (!(new commerceCloud.utils()).click(toCountry)) {
			KeywordUtil.logInfo("Cannot click Country Name")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("NewRegistration/TYPEOFBUSINESSTEXT"))) {
			KeywordUtil.logInfo("Cannot click TYPE OF BUSINESS TEXT")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("NewRegistration/TYPEOFBUSINESSDROPDOWN"))) {
			KeywordUtil.logInfo("Cannot click TYPE OF BUSINESS DROP DOWN")
			return false
		}

		/*if (!(new commerceCloud.utils()).setText(findTestObject("NewRegistration/CorporateWebsite"), "https://www.tessco.com/")) {
		 KeywordUtil.logInfo("Cannot Enter Corporate Web site")
		 return false
		 }
		 */

		if (!(new commerceCloud.utils()).scrollToElement(findTestObject("NewRegistration/CompanyAddress"))) {
			KeywordUtil.logInfo("Cannot scroll Company Address")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("NewRegistration/CompanyAddress"),60)) {
			KeywordUtil.logInfo("Cannot see Company Address")
			return false
		}

		/*if (!(new commerceCloud.utils()).setText(findTestObject("NewRegistration/Company"), "TESSCO")) {
		 KeywordUtil.logInfo("Cannot Enter Company")
		 return false
		 }*/

		if (!(new commerceCloud.utils()).setText(findTestObject("NewRegistration/AddressLine1"), "11126 McCormick Rd")) {
			KeywordUtil.logInfo("Cannot Enter Address Line 1")
			return false
		}

		if (!(new commerceCloud.utils()).setText(findTestObject("NewRegistration/AddressLine2"), "Hunt Valley")) {
			KeywordUtil.logInfo("Cannot Enter Address Line 2")
			return false
		}

		if (!(new commerceCloud.utils()).setText(findTestObject("NewRegistration/City"), "MD")) {
			KeywordUtil.logInfo("Cannot Enter City")
			return false
		}

		if (!(new commerceCloud.utils()).setText(findTestObject("NewRegistration/State"),strState)) {
			KeywordUtil.logInfo("Cannot Enter State")
			return false
		}

		TestObject toState = new TestObject("State Name")
		String xpathState = "//strong[contains(text(),'" + strState + "')]"
		TestObjectProperty propState = new TestObjectProperty("xpath", ConditionType.EQUALS, xpathState)
		toState.addProperty(propState)

		if (!(new commerceCloud.utils()).click(toState)) {
			KeywordUtil.logInfo("Cannot click State Name")
			return false
		}

		if (!(new commerceCloud.utils()).setText(findTestObject("NewRegistration/PostalCode"), "21031")) {
			KeywordUtil.logInfo("Cannot Enter Postal Code")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("NewRegistration/StateSalesTaxExempt"))) {
			KeywordUtil.logInfo("Cannot click State Sales Tax Exempt")
			return false
		}

		TestObject toSales = new TestObject("STATE SALES TAX EXEMPT")
		String xpathSales = "//ul[@class='dropdown-menu salestax-select']//a[contains(text(),'" + strStateSalesTaxExempt + "')]"
		TestObjectProperty propSales = new TestObjectProperty("xpath", ConditionType.EQUALS, xpathSales)
		toSales.addProperty(propSales)

		if (!(new commerceCloud.utils()).click(toSales)) {
			KeywordUtil.logInfo("Cannot click State Sales Tax Exempt")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("NewRegistration/ReceiveInvoicesViaEmail"))) {
			KeywordUtil.logInfo("Cannot click Receive Invoices Via Email")
			return false
		}

		TestObject toReceiveInvoicesViaEmail = new TestObject("RECEIVE INVOICES VIA EMAIL")
		String xpathReceiveInvoicesViaEmail = "//ul[@class='dropdown-menu invoice-select']//a[contains(text(),'" + strReceiveInvoicesViaEmail + "')]"
		TestObjectProperty propReceiveInvoicesViaEmail = new TestObjectProperty("xpath", ConditionType.EQUALS, xpathReceiveInvoicesViaEmail)
		toReceiveInvoicesViaEmail.addProperty(propReceiveInvoicesViaEmail)

		if (!(new commerceCloud.utils()).click(toReceiveInvoicesViaEmail)) {
			KeywordUtil.logInfo("Cannot click Receive Invoices Via Email")
			return false
		}

		if (!(new commerceCloud.utils()).scrollToElement(findTestObject("NewRegistration/ReceiveInvoicesViaEmail"))) {
			KeywordUtil.logInfo("Cannot scroll Receive Invoices Via Email")
			return false
		}


		if(strReceiveInvoicesViaEmail.equalsIgnoreCase("Yes")){
			if (!(new commerceCloud.utils()).setText(findTestObject("NewRegistration/EmailAddressToReceiveInvoices"), GlobalVariable.GlobalEmailID)) {
				KeywordUtil.logInfo("Cannot Enter Email Address To Receive Invoices")
				return false
			}
		}

		if (!(new commerceCloud.utils()).click(findTestObject("NewRegistration/DesiredPaymentTerms"))) {
			KeywordUtil.logInfo("Cannot click Desired Payment Terms")
			return false
		}

		TestObject toDesiredPaymentTerms = new TestObject("DESIRED PAYMENT TERMS")
		String xpathDesiredPaymentTerms = "//a[contains(text(),'" + strDesiredPaymentTerms + "')]"
		TestObjectProperty propDesiredPaymentTerms = new TestObjectProperty("xpath", ConditionType.EQUALS, xpathDesiredPaymentTerms)
		toDesiredPaymentTerms.addProperty(propDesiredPaymentTerms)

		if (!(new commerceCloud.utils()).click(toDesiredPaymentTerms)) {
			KeywordUtil.logInfo("Cannot click Desired Payment Terms")
			return false
		}

		if(strDesiredPaymentTerms.equalsIgnoreCase("NET TERMS")){

			/*if (!(new commerceCloud.utils()).setText(findTestObject("NewRegistration/NETFederalIdNumber"), "1234567890")) {
			 KeywordUtil.logInfo("Cannot Enter NET Federal Id Number")
			 return false
			 }
			 */
			if (!(new commerceCloud.utils()).setText(findTestObject("NewRegistration/NETBusinessEstablished"), "10/10/2019")) {
				KeywordUtil.logInfo("Cannot Enter NET Business Established")
				return false
			}

			if (!(new commerceCloud.utils()).setText(findTestObject("NewRegistration/NETCreditLineRequest"), "1234567890")) {
				KeywordUtil.logInfo("Cannot Enter NET Credit Line Request")
				return false
			}

			if (!(new commerceCloud.utils()).setText(findTestObject("NewRegistration/NETParentCompany"), "Happiest Minds")) {
				KeywordUtil.logInfo("Cannot Enter NET Parent Company")
				return false
			}

			if (!(new commerceCloud.utils()).setText(findTestObject("NewRegistration/NETDunsNumber"), "1234567890")) {
				KeywordUtil.logInfo("Cannot Enter NET Duns Number")
				return false
			}

			if (!(new commerceCloud.utils()).setText(findTestObject("NewRegistration/NETAccountPayableContactFirstName"), "Jermaine")) {
				KeywordUtil.logInfo("Cannot Enter NET Account Payable Contact First Name")
				return false
			}

			if (!(new commerceCloud.utils()).setText(findTestObject("NewRegistration/NETAccountPayableContactLastName"), "Wilkins")) {
				KeywordUtil.logInfo("Cannot Enter NET Account Payable Contact Last Name")
				return false
			}

			if (!(new commerceCloud.utils()).setText(findTestObject("NewRegistration/NETAccountPayableContactCountryCode"), "+1")) {
				KeywordUtil.logInfo("Cannot Enter NET Account Payable Contact Country Code")
				return false
			}

			if (!(new commerceCloud.utils()).setText(findTestObject("NewRegistration/NETAccountPayableContactPhone"), "9986170114")) {
				KeywordUtil.logInfo("Cannot Enter NET Account Payable Contact Phone No")
				return false
			}

			if (!(new commerceCloud.utils()).setText(findTestObject("NewRegistration/NETAccountPayableContactEmail"), GlobalVariable.GlobalEmailID)) {
				KeywordUtil.logInfo("Cannot Enter NET Account Payable Contact Email")
				return false
			}
		}

		if (!(new commerceCloud.utils()).scrollToElement(findTestObject("NewRegistration/FirstName"))) {
			KeywordUtil.logInfo("Cannot scroll First Name")
			return false
		}


		if (!(new commerceCloud.utils()).setText(findTestObject("NewRegistration/FirstName"), "Jermaine")) {
			KeywordUtil.logInfo("Cannot Enter First Name")
			return false
		}

		if (!(new commerceCloud.utils()).setText(findTestObject("NewRegistration/LastName"), "Wilkins")) {
			KeywordUtil.logInfo("Cannot Enter Last Name")
			return false
		}

		if (!(new commerceCloud.utils()).setText(findTestObject("NewRegistration/Email"), strUserName)) {
			KeywordUtil.logInfo("Cannot Enter Email ID")
			return false
		}

		if (!(new commerceCloud.utils()).setText(findTestObject("NewRegistration/PhoneCountryCode"), "+1")) {
			KeywordUtil.logInfo("Cannot Enter Phone Country Code")
			return false
		}

		if (!(new commerceCloud.utils()).setText(findTestObject("NewRegistration/Phone"), "9986170114")) {
			KeywordUtil.logInfo("Cannot Enter Phone No")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("NewRegistration/JobTitle"))) {
			KeywordUtil.logInfo("Cannot click Job Title")
			return false
		}

		TestObject toJobTitle = new TestObject("JOB TITLE")
		String xpathJobTitle = "//div[contains(@class,'btn-group open')]//a[contains(text(),'" + strJobTitle + "')]"
		TestObjectProperty propJobTitle = new TestObjectProperty("xpath", ConditionType.EQUALS, xpathJobTitle)
		toJobTitle.addProperty(propJobTitle)

		if (!(new commerceCloud.utils()).click(toJobTitle)) {
			KeywordUtil.logInfo("Cannot click Job Title : " + strJobTitle)
			return false
		}

		if (!(new commerceCloud.utils()).scrollToElement(findTestObject("NewRegistration/Username"))) {
			KeywordUtil.logInfo("Cannot scroll User name")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeysOutWait(findTestObject("NewRegistration/Username"), strUserName)) {
			KeywordUtil.logInfo("Cannot Enter User name")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeysOutWait(findTestObject("NewRegistration/Password"), GlobalVariable.GlobalRegistrationPassword)) {
			KeywordUtil.logInfo("Cannot Enter Password")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeysOutWait(findTestObject("NewRegistration/PasswordConfirm"), GlobalVariable.GlobalRegistrationPassword)) {
			KeywordUtil.logInfo("Cannot Enter Password Confirm")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("NewRegistration/TermsCheck"))) {
			KeywordUtil.logInfo("Cannot click Terms Check")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("NewRegistration/PromationalEmailCheck"))) {
			KeywordUtil.logInfo("Cannot click Promational Email Check")
			return false
		}

		if (!(new commerceCloud.utils()).delay(5)) {
			KeywordUtil.logInfo("Cannot wait 5 sec")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("NewRegistration/NewRegistration"))) {
			KeywordUtil.logInfo("Cannot click New Registration")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("NewRegistration/BackToTesscoCom"),60)) {
			KeywordUtil.logInfo("Cannot see Back To Tessco Com")
			return false
		}


		if (!(new commerceCloud.utils()).click(findTestObject("NewRegistration/BackToTesscoCom"))) {
			KeywordUtil.logInfo("Cannot click Back To Tessco Com")
			return false
		}

		KeywordUtil.logInfo("Verified the New Registration Landing Page")
		return true
	}


	/**
	 * RegistrationValidation(String sUserName, String jobTitle)
	 * @return true if list is created and validated the presents, otherwise false
	 */

	@Keyword
	def RegistrationValidation(String sUserName, String jobTitle) {

		KeywordUtil.logInfo("Verify the Registration Landing Page")
		String strUserName = (new commerceCloud.Cart()).getRegistrationUserName(sUserName)

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Login/Sign In or Register"),60)) {
			KeywordUtil.logInfo("Cannot see Sign In or Register")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Login/Sign In or Register"))) {
			KeywordUtil.logInfo("Cannot click Sign In or Register")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Registration/RegisterLink"),60)) {
			KeywordUtil.logInfo("Cannot see Register Link")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Registration/RegisterLink"))) {
			KeywordUtil.logInfo("Cannot click Register Link")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Registration/JoinExistingAccount"),60)) {
			KeywordUtil.logInfo("Cannot see Join Existing Account")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Registration/NewAccountText"),2)) {
			KeywordUtil.logInfo("Cannot see We are a business-to-business company and deliver to business addresses. Tell us more about your company to register a new business account with us. text")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Registration/JoinExistingAccount"))) {
			KeywordUtil.logInfo("Cannot click Join Existing Account")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Registration/CompanyAccount"),60)) {
			KeywordUtil.logInfo("Cannot see Company Account")
			return false
		}

		if (!(new commerceCloud.utils()).setText(findTestObject("Registration/AccountNumber"), "12aa@")) {
			KeywordUtil.logInfo("Cannot Enter Invalid formate Account Number")
			return false
		}

		if (!(new commerceCloud.utils()).scrollToElement(findTestObject("Registration/Continue"))) {
			KeywordUtil.logInfo("Cannot scroll Continue")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Registration/Continue"))) {
			KeywordUtil.logInfo("Cannot click Continue")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Registration/AccountNumberValid"),60)) {
			KeywordUtil.logInfo("Cannot see Invalid formate Account Number Error Message")
			return false
		}

		/*if (!(new commerceCloud.utils()).setText(findTestObject("Registration/AccountNumber"), "12")) {
		 KeywordUtil.logInfo("Cannot Enter Invalid Account Number")
		 return false
		 }
		 if (!(new commerceCloud.utils()).scrollToElement(findTestObject("Registration/Continue"))) {
		 KeywordUtil.logInfo("Cannot scroll Continue")
		 return false
		 }
		 if (!(new commerceCloud.utils()).click(findTestObject("Registration/Continue"))) {
		 KeywordUtil.logInfo("Cannot click Continue")
		 return false
		 }
		 if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Registration/AccountNumberNotFound"),60)) {
		 KeywordUtil.logInfo("Cannot see Invalid formate Account Number Error Message")
		 return false
		 }
		 */
		if (!(new commerceCloud.utils()).setText(findTestObject("Registration/AccountNumber"), GlobalVariable.AccountNo)) {
			KeywordUtil.logInfo("Cannot Enter Account Number")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsEnabled(findTestObject("Registration/Continue"),30)) {
			KeywordUtil.logInfo("Cannot Enabled Continue button")
			return false
		}

		if (!(new commerceCloud.utils()).scrollToElement(findTestObject("Registration/Continue"))) {
			KeywordUtil.logInfo("Cannot scroll Continue")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Registration/Continue"))) {
			KeywordUtil.logInfo("Cannot click Continue")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Registration/FirstName"),60)) {
			KeywordUtil.logInfo("Cannot see First Name")
			return false
		}

		if (!(new commerceCloud.utils()).setText(findTestObject("Registration/FirstName"), "Jermaine")) {
			KeywordUtil.logInfo("Cannot Enter First Name")
			return false
		}

		if (!(new commerceCloud.utils()).setText(findTestObject("Registration/LastName"), "Wilkins")) {
			KeywordUtil.logInfo("Cannot Enter Last Name")
			return false
		}

		if (!(new commerceCloud.utils()).setText(findTestObject("Registration/Email"), "wilkinsj@tessco.com")) {
			KeywordUtil.logInfo("Cannot Enter Email")
			return false
		}

		if (!(new commerceCloud.utils()).setText(findTestObject("Registration/Phone"), "7178856212")) {
			KeywordUtil.logInfo("Cannot Enter Phone")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Registration/JobTitle"))) {
			KeywordUtil.logInfo("Cannot click Job Title")
			return false
		}

		if(jobTitle.equalsIgnoreCase("ExecutiveManagement")){
			if (!(new commerceCloud.utils()).click(findTestObject("Registration/JobTitleExecutiveManagement"))) {
				KeywordUtil.logInfo("Cannot click Job Title Executive Management")
				return false
			}
		} else if(jobTitle.equalsIgnoreCase("FieldServices")){
			if (!(new commerceCloud.utils()).click(findTestObject("Registration/JobTitleFieldServices"))) {
				KeywordUtil.logInfo("Cannot click Job Title Field Services")
				return false
			}
		} else if(jobTitle.equalsIgnoreCase("FinanceOrTreasury")){
			if (!(new commerceCloud.utils()).click(findTestObject("Registration/JobTitleFinanceOrTreasury"))) {
				KeywordUtil.logInfo("Cannot click Job Title Finance Or Treasury")
				return false
			}
		} else if(jobTitle.equalsIgnoreCase("PurchasingOrLogistics")){
			if (!(new commerceCloud.utils()).click(findTestObject("Registration/JobTitlePurchasingOrLogistics"))) {
				KeywordUtil.logInfo("Cannot click Job Title Purchasing Or Logistics")
				return false
			}
		} else if(jobTitle.equalsIgnoreCase("SalesOrMarketing")){
			if (!(new commerceCloud.utils()).click(findTestObject("Registration/JobTitleSalesOrMarketing"))) {
				KeywordUtil.logInfo("Cannot click Job Title Sales Or Marketing")
				return false
			}
		} else if(jobTitle.equalsIgnoreCase("SystemOrNetworkDesign")){
			if (!(new commerceCloud.utils()).click(findTestObject("Registration/JobTitleSystemOrNetworkDesign"))) {
				KeywordUtil.logInfo("Cannot click Job Title System Or Network Design")
				return false
			}
		}


		if (!(new commerceCloud.utils()).sendKeysOutWait(findTestObject("Registration/UserName"), GlobalVariable.GlobalRegistrationUserName)) {
			KeywordUtil.logInfo("Cannot Enter User Name")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeysOutWait(findTestObject("Registration/Password"), GlobalVariable.GlobalRegistrationPassword)) {
			KeywordUtil.logInfo("Cannot Enter Password")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeysOutWait(findTestObject("Registration/ConfirmPassword"), GlobalVariable.GlobalRegistrationPassword)) {
			KeywordUtil.logInfo("Cannot Enter Confirm Password")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Registration/TermsCheck"))) {
			KeywordUtil.logInfo("Cannot click Terms Check")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Registration/PromationalEmailCheck"))) {
			KeywordUtil.logInfo("Cannot click Promational Email Check")
			return false
		}

		/*if (!(new commerceCloud.utils()).scrollToElement(findTestObject("Registration/tcomExistingRegistrationRegisterBtn"))) {
		 KeywordUtil.logInfo("Cannot scroll Registration button")
		 return false
		 }
		 if (!(new commerceCloud.utils()).click(findTestObject("Registration/tcomExistingRegistrationRegisterBtn"))) {
		 KeywordUtil.logInfo("Cannot click Existing Registration")
		 return false
		 }
		 if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Registration/ThankYouForRegistering"),60)) {
		 KeywordUtil.logInfo("Cannot see Thank You For Registering")
		 return false
		 }*/

		KeywordUtil.logInfo("Verified the Registration Landing Page")
		return true
	}
	/**
	 * ChatNow()
	 * @return true if Storefront_ChatNow presents, otherwise false
	 */

	@Keyword
	def ChatNow() {

		KeywordUtil.logInfo("Verify the Store front Chat Now Page")
		//Sales Support,  Technical Support, Website Support
		String strUserName = (new commerceCloud.Cart()).getUserName("")
		String Path = ('C:\\Users\\' + System.getProperty('user.name')) + '\\Downloads\\transcript.txt'
		def file = new File(Path)

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("ChatNow/Cart"),60)) {
			KeywordUtil.logInfo("Cannot see Cart")
			return false
		}

		/*if (!(new commerceCloud.utils()).click(findTestObject("ChatNow/Cart"))) {
		 KeywordUtil.logInfo("Cannot click Cart")
		 return false
		 }
		 if (!(new commerceCloud.utils()).verifyPresent(findTestObject("ChatNow/ContinueShopping"),60)) {
		 KeywordUtil.logInfo("Cannot see Continue Shopping")
		 return false
		 }*/

		if ((new commerceCloud.utils()).verifyElementIsEnabled(findTestObject("ChatNow/ChatIsOnline"),10)) {
			KeywordUtil.logInfo("Can see Chat Is Online button is Enabled")

			if (!(new commerceCloud.utils()).click(findTestObject("ChatNow/ChatIsOnline"))) {
				KeywordUtil.logInfo("Cannot click Chat Is Online")
				return false
			}

			if (!(new commerceCloud.utils()).switchToWindow(1)) {
				KeywordUtil.logInfo("Cannot switch To chat Window")
				return false
			}

			if (!(new commerceCloud.utils()).verifyPresent(findTestObject("ChatNow/FirstName"),60)) {
				KeywordUtil.logInfo("Cannot see First Name")
				return false
			}

			if (!(new commerceCloud.utils()).setText(findTestObject("ChatNow/FirstName"), "Jermaine")) {
				KeywordUtil.logInfo("Cannot Enter First Name")
				return false
			}

			if (!(new commerceCloud.utils()).setText(findTestObject("ChatNow/LastName"), "Wilkins")) {
				KeywordUtil.logInfo("Cannot Enter Last Name")
				return false
			}

			if (!(new commerceCloud.utils()).setText(findTestObject("ChatNow/Email"), "WilkinsJ@tessco.com")) {
				KeywordUtil.logInfo("Cannot Enter Email")
				return false
			}

			if (!(new commerceCloud.utils()).setText(findTestObject("ChatNow/CompanyName"), "TESSCO")) {
				KeywordUtil.logInfo("Cannot Enter Company Name")
				return false
			}

			if (!(new commerceCloud.utils()).selectDropDownValue(findTestObject("ChatNow/Department"), "Technical Support")) {
				KeywordUtil.logInfo("Cannot select the Department")
				return false
			}

			if (!(new commerceCloud.utils()).click(findTestObject("ChatNow/TermsAndConditions"))) {
				KeywordUtil.logInfo("Cannot click Terms And Conditions")
				return false
			}

			if (!(new commerceCloud.utils()).scrollToElement(findTestObject("ChatNow/RequestChat"))) {
				KeywordUtil.logInfo("Cannot scroll Request Chat")
				return false
			}

			if (!(new commerceCloud.utils()).click(findTestObject("ChatNow/RequestChat"))) {
				KeywordUtil.logInfo("Cannot click Request Chat")
				return false
			}

			if ((new commerceCloud.utils()).verifyVisibleOfElement(findTestObject("ChatNow/SaveChat"),90)) {
				KeywordUtil.logInfo("Can see Save Chat")
				if (!(new commerceCloud.utils()).verifyVisibleOfElement(findTestObject("ChatNow/Send"),60)) {
					KeywordUtil.logInfo("Cannot see Send")
					return false
				}

				if (!(new commerceCloud.utils()).setText(findTestObject("ChatNow/AgentChatInput"), "Hi")) {
					KeywordUtil.logInfo("Cannot Enter Agent Chat Input")
					return false
				}

				if (!(new commerceCloud.utils()).click(findTestObject("ChatNow/Send"))) {
					KeywordUtil.logInfo("Cannot click Send")
					return false
				}

				if (!(new commerceCloud.utils()).verifyVisibleOfElement(findTestObject("ChatNow/Hi"),60)) {
					KeywordUtil.logInfo("Cannot see Hi")
					return false
				}

				if (!(new commerceCloud.utils()).click(findTestObject("ChatNow/SaveChat"))) {
					KeywordUtil.logInfo("Cannot click Save Chat")
					return false
				}

				if (!(new commerceCloud.utils()).delay(5)) {
					KeywordUtil.logInfo("Cannot wait a sec")
					return false
				}

				if (!(file.exists())) {
					KeywordUtil.logInfo("Cannot Save Chat in transcript txt File")
					return false
				}

				if (!(file.delete())) {
					KeywordUtil.logInfo("Cannot Delete Chat in transcript txt File")
					return false
				}

				if (!(new commerceCloud.utils()).click(findTestObject("ChatNow/EndChat"))) {
					KeywordUtil.logInfo("Cannot click End Chat")
					return false
				}

				//return false
			}else{
				KeywordUtil.markWarning("Verified the Your chat request has been canceled because no agents are available")

				if (!(new commerceCloud.utils()).verifyVisibleOfElement(findTestObject("ChatNow/Close"),60)) {
					KeywordUtil.logInfo("Cannot see Close")
					return false
				}

				if (!(new commerceCloud.utils()).click(findTestObject("ChatNow/Close"))) {
					KeywordUtil.logInfo("Cannot click Close")
					return false
				}
				KeywordUtil.logInfo("Can see Close")
			}

			if (!(new commerceCloud.utils()).switchToWindow(0)) {
				KeywordUtil.logInfo("Cannot switch To default Window")
				return false
			}

		}else{
			KeywordUtil.markWarning("Verified the chat is online button is disable")
			return false

		}

		KeywordUtil.logInfo("Verified the Store front Chat Now Page")
		return true
	}

	/**
	 * QuoteCreation()
	 * @return true if Quote Request Confirmation, otherwise false
	 */

	@Keyword
	def QuoteCreation() {
		KeywordUtil.logInfo("Validate the Quote Request Confirmation")

		if (!(new commerceCloud.utils()).verifyVisibleOfElement(findTestObject("ChatNow/Close"),60)) {
			KeywordUtil.logInfo("Cannot see Close")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Quote/Close"))) {
			KeywordUtil.logInfo("Cannot click Close")
			return false
		}

		if (!(new commerceCloud.utils()).verifyVisibleOfElement(findTestObject("Quote/Close"),60)) {
			KeywordUtil.logInfo("Cannot see Close")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Quote/Close"))) {
			KeywordUtil.logInfo("Cannot click Close")
			return false
		}

		if (!(new commerceCloud.utils()).setText(findTestObject("ChatNow/AgentChatInput"), "Hi")) {
			KeywordUtil.logInfo("Cannot Enter Agent Chat Input")
			return false
		}


		KeywordUtil.logInfo("Validated the Quote Request Confirmation")
	}

	/**
	 * QuoteRequestInitiation()
	 * @return true if Quote Request Confirmation, otherwise false
	 */

	@Keyword
	def QuoteRequestInitiation() {
		KeywordUtil.logInfo("Validate the Quote Request Confirmation")
		String strQuoteNoValues = ""

		if (!(new commerceCloud.utils()).verifyVisibleOfElement(findTestObject("Quote/CreateQuote"),60)) {
			KeywordUtil.logInfo("Cannot see Create Quote button")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Quote/CreateQuote"))) {
			KeywordUtil.logInfo("Cannot click Create Quote button")
			return false
		}

		if (!(new commerceCloud.utils()).verifyVisibleOfElement(findTestObject("Quote/DeliveryMethod"),60)) {
			KeywordUtil.logInfo("Cannot see Delivery Method")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Quote/DeliveryMethod"))) {
			KeywordUtil.logInfo("Cannot click Delivery Method")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Quote/5DayGround"))) {
			KeywordUtil.logInfo("Cannot click 5 Day Ground")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsEnabled(findTestObject("Quote/SubmitQuote"),30)) {
			KeywordUtil.logInfo("Cannot see Submit Quote button is Disable")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Quote/ShipAllItemsAsSingleShipment"))) {
			KeywordUtil.logInfo("Cannot click Ship All Items As Single Shipment")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Quote/LiftGate"))) {
			KeywordUtil.logInfo("Cannot click Lift Gate")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsEnabled(findTestObject("Quote/SubmitQuote"),30)) {
			KeywordUtil.logInfo("Cannot see Submit Quote button is Disable")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Quote/LiftGate"))) {
			KeywordUtil.logInfo("Cannot click Lift Gate")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsEnabled(findTestObject("Quote/SubmitQuote"),30)) {
			KeywordUtil.logInfo("Cannot see Submit Quote button is Disable")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Quote/LiftGate"))) {
			KeywordUtil.logInfo("Cannot click Lift Gate")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsEnabled(findTestObject("Quote/SubmitQuote"),30)) {
			KeywordUtil.logInfo("Cannot see Submit Quote button is Disable")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Quote/InsideDelivery"))) {
			KeywordUtil.logInfo("Cannot click Inside Delivery")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsEnabled(findTestObject("Quote/SubmitQuote"),30)) {
			KeywordUtil.logInfo("Cannot see Submit Quote button is Disable")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Quote/DeliveryMethod"))) {
			KeywordUtil.logInfo("Cannot click Delivery Method")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Quote/1DayAir"))) {
			KeywordUtil.logInfo("Cannot click 1 Day Air")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsEnabled(findTestObject("Quote/SubmitQuote"),30)) {
			KeywordUtil.logInfo("Cannot see Submit Quote button is Disable")
			return false
		}

		if (!(new commerceCloud.Cart()).addShippingAddress()) {
			KeywordUtil.logInfo("Cannot add Shipping Address")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsEnabled(findTestObject("Quote/SubmitQuote"),30)) {
			KeywordUtil.logInfo("Cannot see Submit Quote button is Disable")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Quote/SubmitQuote"))) {
			KeywordUtil.logInfo("Cannot click Submit Quote")
			return false
		}

		if (!(new commerceCloud.utils()).verifyVisibleOfElement(findTestObject("Quote/QuoteSubmitted"),30)) {
			KeywordUtil.logInfo("Cannot see Quote Submitted")
			return false
		}

		if (!(new commerceCloud.utils()).verifyVisibleOfElement(findTestObject("Quote/YourQuoteHasBeenSubmitted"),1)) {
			KeywordUtil.logInfo("Cannot see Your Quote Has Been Submitted")
			return false
		}
		/*
		 if (!(new commerceCloud.utils()).verifyVisibleOfElement(findTestObject("Quote/QuoteDetails"),1)) {
		 KeywordUtil.logInfo("Cannot see Quote Details")
		 return false
		 }
		 */
		if (!(new commerceCloud.utils()).verifyVisibleOfElement(findTestObject("Quote/DateSubmitted"),1)) {
			KeywordUtil.logInfo("Cannot see Date Submitted")
			return false
		}

		if (!(new commerceCloud.utils()).verifyVisibleOfElement(findTestObject("Quote/ValidUntil"),1)) {
			KeywordUtil.logInfo("Cannot see Valid Until")
			return false
		}

		if (!(new commerceCloud.utils()).verifyVisibleOfElement(findTestObject("Quote/Status"),1)) {
			KeywordUtil.logInfo("Cannot see Status")
			return false
		}

		if (!(new commerceCloud.utils()).verifyVisibleOfElement(findTestObject("Quote/EstimatedTax"),1)) {
			KeywordUtil.logInfo("Cannot see Estimated Tax")
			return false
		}

		if (!(new commerceCloud.utils()).verifyVisibleOfElement(findTestObject("Quote/EstimatedShipping"),1)) {
			KeywordUtil.logInfo("Cannot see Estimated Shipping")
			return false
		}

		if (!(new commerceCloud.utils()).verifyVisibleOfElement(findTestObject("Quote/PreviousEstimatedTotal"),1)) {
			KeywordUtil.logInfo("Cannot see Previous Estimated Total")
			return false
		}

		if (!(new commerceCloud.utils()).verifyVisibleOfElement(findTestObject("Quote/Shipping"),1)) {
			KeywordUtil.logInfo("Cannot see Shipping")
			return false
		}

		if (!(new commerceCloud.utils()).verifyVisibleOfElement(findTestObject("Quote/DeliveryMethodQuoteSubmit"),1)) {
			KeywordUtil.logInfo("Cannot see Delivery Method Quote Submit")
			return false
		}

		if (!(new commerceCloud.utils()).verifyVisibleOfElement(findTestObject("Quote/LiftGateQS"),1)) {
			KeywordUtil.logInfo("Cannot see Lift Gate Quote Submit")
			return false
		}

		if (!(new commerceCloud.utils()).verifyVisibleOfElement(findTestObject("Quote/InsideDeliveryQS"),1)) {
			KeywordUtil.logInfo("Cannot see Inside Delivery")
			return false
		}

		ArrayList<WebElement> strQuoteNo =  (new commerceCloud.utils()).ListOfWebElement(findTestObject("Quote/QuotesNumber"))
		String strQuoteNoList = strQuoteNo.get(1).getText()
		strQuoteNoValues = strQuoteNoList.replace("Quote","");
		println(strQuoteNoValues)
		if (strQuoteNoValues.equalsIgnoreCase("")) {
			KeywordUtil.logInfo("Cannot get Quotes Number")
			return false
		}

		KeywordUtil.logInfo("Validated the Quote Request Confirmation")
		return strQuoteNoValues.trim()
	}

	/**
	 * addShippingAddress()
	 * @return true if adding Shipping Address, otherwise false
	 */

	@Keyword
	def addShippingAddress() {

		KeywordUtil.logInfo("Validate the adding Shipping Address")
		String strCartName = (new commerceCloud.Cart()).getCartName("")

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Quote/ShippingEdit"),60)) {
			KeywordUtil.logInfo("Cannot see the Shipping Address edit button")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Quote/ShippingEdit"))) {
			KeywordUtil.logInfo("Cannot click Shipping Address edit button")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Quote/AddNewAddress"),60)) {
			KeywordUtil.logInfo("Cannot see the Add New Address")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Quote/AddNewAddress"))) {
			KeywordUtil.logInfo("Cannot click Add New Address")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("AddTemporaryAddress/Cancel"))) {
			KeywordUtil.logInfo("Cannot click Cancel")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Quote/ShippingEdit"),60)) {
			KeywordUtil.logInfo("Cannot see the Shipping Address edit button")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Quote/ShippingEdit"))) {
			KeywordUtil.logInfo("Cannot click Shipping Address edit button")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Quote/AddNewAddress"),60)) {
			KeywordUtil.logInfo("Cannot see the Add New Address")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Quote/AddNewAddress"))) {
			KeywordUtil.logInfo("Cannot click Add New Address")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("AddTemporaryAddress/AddTemporaryShippingAddressHeader"),60)) {
			KeywordUtil.logInfo("Cannot see the Add Temporary Shipping Address Header")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("AddTemporaryAddress/Name"),strCartName)) {
			KeywordUtil.logInfo("Cannot send Name")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("AddTemporaryAddress/ADDRESS"),"11126 McCormick Rd")) {
			KeywordUtil.logInfo("Cannot send ADDRESS")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("AddTemporaryAddress/ADDRESSLINE2"),"Hunt Valley")) {
			KeywordUtil.logInfo("Cannot send ADDRESS LINE 2")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("AddTemporaryAddress/CITY"),"MD")) {
			KeywordUtil.logInfo("Cannot send CITY")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("AddTemporaryAddress/STATE"))) {
			KeywordUtil.logInfo("Cannot click STATE")
			return false
		}

		if (!(new commerceCloud.utils()).delay(1)) {
			KeywordUtil.logInfo("Cannot Wait a sec")
			return false
		}


		if (!(new commerceCloud.utils()).click(findTestObject("AddTemporaryAddress/STATEName"))) {
			KeywordUtil.logInfo("Cannot click STATE Name")
			return false
		}

		if (!(new commerceCloud.utils()).delay(3)) {
			KeywordUtil.logInfo("Cannot Wait a sec")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("AddTemporaryAddress/ZIPCODE"),"21031")) {
			KeywordUtil.logInfo("Cannot send ZIPCODE")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Quote/AddNewAddressQuote"))) {
			KeywordUtil.logInfo("Cannot click Add New Address from Quote")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Quote/ShippingEdit"),60)) {
			KeywordUtil.logInfo("Cannot see the Shipping Address")
			return false
		}


		if ((new commerceCloud.utils()).verifyElementIsEnabled(findTestObject("Quote/ShippingEdit"),10)) {/*
			 if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Quote/ShippingEdit"),60)) {
			 KeywordUtil.logInfo("Cannot see the Shipping Address edit button")
			 return false
			 }
			 if (!(new commerceCloud.utils()).click(findTestObject("Quote/ShippingEdit"))) {
			 KeywordUtil.logInfo("Cannot click Shipping Address edit button")
			 return false
			 }
			 if (!(new commerceCloud.utils()).click(findTestObject("Shipping/ShippingSearchSavedAddresses"))) {
			 KeywordUtil.logInfo("Cannot click Shipping Search Saved Addresses")
			 return false
			 }
			 if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Shipping/SavedCancelSearchAddress"),60)) {
			 KeywordUtil.logInfo("Cannot see the Saved Cancel Search Address")
			 return false
			 }
			 if (!(new commerceCloud.utils()).click(findTestObject("Shipping/SavedCancelSearchAddress"))) {
			 KeywordUtil.logInfo("Cannot click Saved Cancel Search Address")
			 return false
			 }
			 if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Quote/ShippingEdit"),60)) {
			 KeywordUtil.logInfo("Cannot see the Shipping Address edit button")
			 return false
			 }
			 if (!(new commerceCloud.utils()).click(findTestObject("Quote/ShippingEdit"))) {
			 KeywordUtil.logInfo("Cannot click Shipping Address edit button")
			 return false
			 }
			 if (!(new commerceCloud.utils()).click(findTestObject("Shipping/ShippingSearchSavedAddresses"))) {
			 KeywordUtil.logInfo("Cannot click Shipping Search Saved Addresses")
			 return false
			 }
			 if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Shipping/SavedCancelSearchAddress"),60)) {
			 KeywordUtil.logInfo("Cannot see the Saved Cancel Search Address")
			 return false
			 }
			 if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Shipping/SavedAddressSearch"),60)) {
			 KeywordUtil.logInfo("Cannot see the Saved Address Search")
			 return false
			 }
			 if (!(new commerceCloud.utils()).sendKeys(findTestObject("Shipping/SavedAddressSearch"),strCartName)) {
			 KeywordUtil.logInfo("Cannot send Saved Address Search")
			 return false
			 }
			 if (!(new commerceCloud.utils()).click(findTestObject("Shipping/SavedSearchIcon"))) {
			 KeywordUtil.logInfo("Cannot click Saved Search Icon")
			 return false
			 }
			 if (!(new commerceCloud.utils()).click(findTestObject("Shipping/SavedFirstAddress"))) {
			 KeywordUtil.logInfo("Cannot click Saved First Address")
			 return false
			 }
			 if (!(new commerceCloud.utils()).click(findTestObject("Quote/UseSelectedButtonQuote"))) {
			 KeywordUtil.logInfo("Cannot click Saved Use Selected Address")
			 return false
			 }
			 if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Quote/ShippingEdit"),60)) {
			 KeywordUtil.logInfo("Cannot see the Shipping Address")
			 return false
			 }
			 KeywordUtil.logInfo("Validated the user searched Shipping Address")
			 */} else {

			/*if (!(new commerceCloud.utils()).click(findTestObject("Shipping/SavedUseSelectedAddress"))) {
			 KeywordUtil.logInfo("Cannot click Saved Use Selected Address")
			 return false
			 }*/

			KeywordUtil.logInfo("Validated the user unable to search and added first available Shipping Address")
		}

		KeywordUtil.logInfo("Validated the adding Shipping Address")
		return true
	}

	/**
	 * QuoteRequest(String Search)
	 * @param Search - user should send Cart Name
	 * @return true if complete the order process, otherwise false
	 */

	@Keyword
	def QuoteRequest(String Search) {

		KeywordUtil.logInfo("Validate the user can complete the Order")

		if (!(new commerceCloud.Checkout()).addProductToCart(Search)) {
			KeywordUtil.logInfo("Cannot search the Tessco Product")
			return false
		}
		if (!(new commerceCloud.Cart()).QuoteCreation()) {
			KeywordUtil.logInfo("Cannot search the Tessco Product")
			return false
		}

		KeywordUtil.logInfo("Validated the user can complete the Order")
		return true
	}


	/**
	 * QuoteRequestInitiation(String Search)
	 * @param Search - user should send Cart Name
	 * @return true if complete the order process, otherwise false
	 */

	@Keyword
	def QuoteRequestInitiation(String Search) {

		KeywordUtil.logInfo("Validate the Quote Request Initiation")

		if (!(new commerceCloud.Checkout()).addProductToCart(Search)) {
			KeywordUtil.logInfo("Cannot search the Tessco Product")
			return false
		}

		String strQuoteNo = (new commerceCloud.Cart()).QuoteRequestInitiation()
		println(strQuoteNo)
		if ((strQuoteNo.equalsIgnoreCase("") || strQuoteNo.equalsIgnoreCase("false"))) {
			KeywordUtil.logInfo("Cannot Quote Request Initiation")
			return false
		}

		/*
		 if (!(new commerceCloud.Cart()).navigateQuotes()) {
		 KeywordUtil.logInfo("Cannot navigate to Saved Carts Quotes Page")
		 return false
		 }*/

		KeywordUtil.logInfo("Validated the Quote Request Initiation")
		return true
	}


	/**
	 * sessionCartWithProductsAndQuoteOpen(String Search)
	 * @param Search - user should send Cart Name
	 * @return true if session cart with products And quote open status, otherwise false
	 */

	@Keyword
	def sessionCartWithProductsAndQuoteOpen(String Search) {

		KeywordUtil.logInfo("Verify quotation cart of the user is removed when user discards the quotation checkout and users has a session cart (cart with products)")

		if (!(new commerceCloud.Checkout()).addProductToCart(Search)) {
			KeywordUtil.logInfo("Cannot search the Tessco Product")
			return false
		}

		if (!(new commerceCloud.Cart()).navigateQuotes()) {
			KeywordUtil.logInfo("Cannot navigate to Saved Carts Quotes Page")
			return false
		}

		if (!(new commerceCloud.Cart()).sessionCartWithQuoteOpen()) {
			KeywordUtil.logInfo("Cannot see session Cart With Quote Open status")
			return false
		}


		KeywordUtil.logInfo("Verified quotation cart of the user is removed when user discards the quotation checkout and users has a session cart (cart with products)")
		return true
	}


	/**
	 * sessionCartWithoutProductsAndQuoteOpen() 
	 * @return true if session cart without products And quote open status, otherwise false
	 */

	@Keyword
	def sessionCartWithoutProductsAndQuoteOpen() {

		KeywordUtil.logInfo("Verify quotation cart of the user is removed when user discards the quotation checkout and users has a session cart (cart without products)")


		if (!(new commerceCloud.Cart()).navigateQuotes()) {
			KeywordUtil.logInfo("Cannot navigate to Saved Carts Quotes Page")
			return false
		}

		if (!(new commerceCloud.Cart()).sessionCartWithQuoteOpen()) {
			KeywordUtil.logInfo("Cannot navigate to Saved Carts Quotes Page")
			return false
		}


		KeywordUtil.logInfo("Verified quotation cart of the user is removed when user discards the quotation checkout and users has a session cart (cart without products)")
		return true
	}


	/**
	 * CableAddToCart() 
	 * @return true if Add Reel Lengths to cable product, otherwise false
	 */

	@Keyword
	def CableAddToCart() {

		if ((new commerceCloud.utils()).verifyPresent(findTestObject("AddToCart/Cable"),60)) {

			KeywordUtil.logInfo("Validate the confirmation modal when user adds a cable product ")
			String name = (new commerceCloud.utils()).sReturnText(findTestObject("AddToCart/Cable"))
			if(name.trim().equalsIgnoreCase("REEL LENGTH")){


				String strCUTS = (new commerceCloud.utils()).GetAttributeText(findTestObject("AddToCart/CableCUTSOne"),"value")

				if (!(strCUTS.trim().equalsIgnoreCase("1"))) {
					KeywordUtil.logInfo("Cannot get cable CUTS values : "+ strCUTS)
					return false
				}

				String strFEET = (new commerceCloud.utils()).GetAttributeText(findTestObject("AddToCart/CableFEETOne"),"value")

				if (!(strFEET.trim().equalsIgnoreCase("100"))) {
					KeywordUtil.logInfo("Cannot get cable FEET values : "+ strFEET)
					return false
				}

				if (!(new commerceCloud.utils()).setText(findTestObject("AddToCart/CableCUTSOne"), "1")) {
					KeywordUtil.logInfo("Cannot enter Cable CUT SOne textbox")
					return false
				}
				if (!(new commerceCloud.utils()).setText(findTestObject("AddToCart/CableFEETOne"), "100")) {
					KeywordUtil.logInfo("Cannot enter Cable FEET One textbox")
					return false
				}

				if (!(new commerceCloud.utils()).verifyVisibleOfElement(findTestObject("AddToCart/CableServiceRequestedText"),1)) {
					KeywordUtil.logInfo("Cannot see Cable Service Requested Text")
					return false
				}

				if (!(new commerceCloud.utils()).verifyVisibleOfElement(findTestObject("AddToCart/CableTotalOne"),1)) {
					KeywordUtil.logInfo("Cannot see Cable Total One")
					return false
				}

				if (!(new commerceCloud.utils()).click(findTestObject("AddToCart/CableAddToCart"))) {
					KeywordUtil.logInfo("Cannot click Cable Add To Cart")
					return false
				}

				if (!(new commerceCloud.utils()).verifyVisibleOfElement(findTestObject("AddToCart/GoToCart"),30)) {
					KeywordUtil.logInfo("Cannot see Go To Cart")
					return false
				}

				String strAddCartCUTS = (new commerceCloud.utils()).sReturnText(findTestObject("AddToCart/CableAddCartCUTSOne"))

				if (!(strAddCartCUTS.trim().equalsIgnoreCase("1"))) {
					KeywordUtil.logInfo("Cannot get cable CUTS values : "+ strAddCartCUTS)
					return false
				}

				String strAddCartFEET = (new commerceCloud.utils()).sReturnText(findTestObject("AddToCart/CableAddCartFeetOne"))

				if (!(strAddCartFEET.trim().equalsIgnoreCase("100"))) {
					KeywordUtil.logInfo("Cannot get cable FEET values : "+ strAddCartFEET)
					return false
				}

			}
			KeywordUtil.logInfo("Validated the confirmation modal when user adds a cable product ")

		}else{
			KeywordUtil.logInfo("Cannot see SERVICE REQUESTED Or Add To Cart")
			return false
		}
		return true
	}

	/**
	 * OnlyCableAddToCart()
	 * @return true if Add Reel Lengths to cable product, otherwise false
	 */

	@Keyword
	def OnlyCableAddToCart() {

		KeywordUtil.logInfo("Validate the confirmation modal is displayed adds cable to product")

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("AddToCart/Cable"),60)) {
			KeywordUtil.logInfo("Cannot see SERVICE REQUESTED Or Add To Cart")
			return false
		}

		if (!(new commerceCloud.utils()).setText(findTestObject("AddToCart/CableCUTSOne"), "5")) {
			KeywordUtil.logInfo("Cannot enter Cable CUT SOne textbox")
			return false
		}

		if (!(new commerceCloud.utils()).setText(findTestObject("AddToCart/CableFEETOne"), "100")) {
			KeywordUtil.logInfo("Cannot enter Cable FEET One textbox")
			return false
		}

		if (!(new commerceCloud.utils()).verifyVisibleOfElement(findTestObject("AddToCart/CableTotalOne"),1)) {
			KeywordUtil.logInfo("Cannot see Cable Total One")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("AddToCart/CableAddToCart"))) {
			KeywordUtil.logInfo("Cannot click Cable Add To Cart")
			return false
		}

		KeywordUtil.logInfo("Validated the confirmation modal is displayed adds cable to product")

		return true
	}

	/**
	 * OnlyCableAddToCart()
	 * @return true if Add Reel Lengths to cable product, otherwise false
	 */

	@Keyword
	def OnlyCableUpdateToCart() {

		KeywordUtil.logInfo("Validate the update confirmation modal is displayed in cable product")

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("AddToCart/Cable"),60)) {
			KeywordUtil.logInfo("Cannot see SERVICE REQUESTED Or Add To Cart")
			return false
		}

		if (!(new commerceCloud.utils()).setText(findTestObject("AddToCart/CableCUTSOne"), "2")) {
			KeywordUtil.logInfo("Cannot enter Cable CUT SOne textbox")
			return false
		}

		if (!(new commerceCloud.utils()).setText(findTestObject("AddToCart/CableFEETOne"), "100")) {
			KeywordUtil.logInfo("Cannot enter Cable FEET One textbox")
			return false
		}

		if (!(new commerceCloud.utils()).verifyVisibleOfElement(findTestObject("AddToCart/CableTotalOne"),1)) {
			KeywordUtil.logInfo("Cannot see Cable Total One")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/CableUpdate"))) {
			KeywordUtil.logInfo("Cannot click update button under Cable")
			return false
		}

		KeywordUtil.logInfo("Validated  the update confirmation modal is displayed in cable product")

		return true
	}

	/**
	 * backOffice(String strAccountNo, String strUserName)
	 * @return true if get if SFDC Lead ID is available in created user under back office, otherwise false
	 */

	@Keyword
	def backOffice(String strAccountNo, String strUserName) {

		KeywordUtil.logInfo("Validate SFDC Lead ID available under back office")
		//String strUserName = "automation004@tessco.com"
		String uploadFilePath = new File('').absolutePath + '\\Manual Test Case\\BackOfficeTestData.xlsx'

		if (!(new commerceCloud.utils()).openWithSiteURL("https://backoffice.cai5-tesscotec1-s1-public.model-t.cc.commerce.ondemand.com/backoffice/login.zul")) {
			KeywordUtil.logInfo('Cannot open the site')
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("backoffice/Login"),60)) {
			KeywordUtil.logInfo("Cannot see Login button")
			return false
		}

		if (!(new commerceCloud.utils()).setText(findTestObject("backoffice/UserName"), "admin")) {
			KeywordUtil.logInfo("Cannot enter User Name textbox")
			return false
		}

		if (!(new commerceCloud.utils()).setText(findTestObject("backoffice/Password"), "password")) {
			KeywordUtil.logInfo("Cannot enter Password textbox")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("backoffice/Login"))) {
			KeywordUtil.logInfo("Cannot click Login button")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("backoffice/B2B_Commerce"),60)) {
			KeywordUtil.logInfo("Cannot see B2B Commerce")
			return false
		}

		if (!(new commerceCloud.utils()).scrollToElement(findTestObject("backoffice/B2B_Commerce"))) {
			KeywordUtil.logInfo("Cannot scroll B2B Commerce")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("backoffice/B2B_Commerce"),60)) {
			KeywordUtil.logInfo("Cannot see B2B Commerce")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("backoffice/B2B_Commerce"))) {
			KeywordUtil.logInfo("Cannot see B2B Commerce")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("backoffice/B2B_Customer"),60)) {
			KeywordUtil.logInfo("Cannot see B2B Customer")
			return false
		}

		if (!(new commerceCloud.utils()).scrollToElement(findTestObject("backoffice/B2B_Customer"))) {
			KeywordUtil.logInfo("Cannot scroll B2B Customer")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("backoffice/B2B_Customer"),60)) {
			KeywordUtil.logInfo("Cannot see B2B Customer")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("backoffice/B2B_Customer"))) {
			KeywordUtil.logInfo("Cannot see B2B Customer")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("backoffice/SearchButton"),60)) {
			KeywordUtil.logInfo("Cannot see Search Button")
			return false
		}

		if (!(new commerceCloud.utils()).setText(findTestObject("backoffice/SearchTextBox"), strUserName)) {
			KeywordUtil.logInfo("Cannot enter Search textbox")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("backoffice/SearchButton"))) {
			KeywordUtil.logInfo("Cannot click Search button")
			return false
		}

		TestObject to = new TestObject("User Name")
		String xpath = "//span[text()='" + strUserName + "']"
		TestObjectProperty prop = new TestObjectProperty("xpath", ConditionType.EQUALS, xpath)
		to.addProperty(prop)

		if (!(new commerceCloud.utils()).verifyVisibleOfElement(to,60)) {
			KeywordUtil.logInfo("Cannot see User Name span text")
			return false
		}

		if (!(new commerceCloud.utils()).verifyVisibleOfElement(to,60)) {
			KeywordUtil.logInfo("Cannot see User Name span text")
			return false
		}

		if (!(new commerceCloud.utils()).click(to)) {
			KeywordUtil.logInfo("Cannot click User Name span text")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("backoffice/General"),60)) {
			KeywordUtil.logInfo("Cannot see General Button")
			return false
		}

		/*
		 if (!(new commerceCloud.utils()).click(findTestObject("backoffice/General"))) {
		 KeywordUtil.logInfo("Cannot click General button")
		 return false
		 }
		 */

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("backoffice/SFDC_Lead_ID"),60)) {
			KeywordUtil.logInfo("Cannot see SFDC Lead ID text")
			return false
		}

		if (!(new commerceCloud.utils()).scrollToElement(findTestObject("backoffice/SFDC_Lead_ID"))) {
			KeywordUtil.logInfo("Cannot scroll SFDC Lead ID text")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("backoffice/SFDC_Lead_ID"),60)) {
			KeywordUtil.logInfo("Cannot see SFDC Lead ID text")
			return false
		}

		String strSFDCid = (new commerceCloud.utils()).GetAttributeText(findTestObject("backoffice/SFDC_Lead_ID"), "value")
		println(strSFDCid)
		if (strSFDCid.equalsIgnoreCase("")) {
			KeywordUtil.markFailed('Cannot get the SFDC Lead ID text')
			return false
		}

		if (!(new commerceCloud.utils().writeExcelFile(uploadFilePath, strSFDCid, strAccountNo, strUserName))) {
			KeywordUtil.logInfo('Cannot write a file')
			return false
		}

		if (!(new commerceCloud.utils()).closeSite()) {
			KeywordUtil.logInfo('Cannot open the site')
			return false
		}

		KeywordUtil.logInfo("Validated SFDC Lead ID available under back office")

		return true
	}


	/**
	 * salesForceLogin()
	 * @return true if user can convert into lead role, otherwise false
	 */

	@Keyword
	def salesForceLogin() {

		KeywordUtil.logInfo("Validate user can login the sales force application")

		//	String strUserName = "automation003@tessco.com"
		String strLoginURL = "https://tessco--tesscotds.my.salesforce.com/"

		if (!(new commerceCloud.utils()).openWithSiteURL(strLoginURL)) {
			KeywordUtil.logInfo('Cannot open the site')
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("SalesForce/Login"),60)) {
			KeywordUtil.logInfo("Cannot see Login button")
			return false
		}

		if (!(new commerceCloud.utils()).setText(findTestObject("SalesForce/UserName"), "senthil.arumugam@happiestminds.com")) {
			KeywordUtil.logInfo("Cannot enter User Name textbox")
			return false
		}

		if (!(new commerceCloud.utils()).setText(findTestObject("SalesForce/Password"), "Bank#123")) {
			KeywordUtil.logInfo("Cannot enter Password textbox")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("SalesForce/Login"))) {
			KeywordUtil.logInfo("Cannot click Login button")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("SalesForce/LeadMore"),60)) {
			KeywordUtil.logInfo("Cannot see Lead More button")
			return false
		}

		KeywordUtil.logInfo("Validated user can login the sales force application")
		return true
	}

	/**
	 * salesForce()
	 * @return true if user can convert into lead role, otherwise false
	 */

	@Keyword
	def salesForce(String strSFDCId, String strAccountNo, String strUserName) {

		KeywordUtil.logInfo("Validate user can convert lead role under sales force")

		//	String strUserName = "automation003@tessco.com"
		String strURL = "https://tessco--tesscotds.lightning.force.com/lightning/r/Lead/"+ strSFDCId +"/view"
		String strLoginURL = "https://tessco--tesscotds.my.salesforce.com/"
		String uploadFilePath = new File('').absolutePath + '\\Manual Test Case\\LeadConvert.xlsx'

		if (!(new commerceCloud.utils()).delay(5)) {
			KeywordUtil.logInfo("Cannot wait 5 sec")
			return false
		}

		int currentWindow = new commerceCloud.utils().getWindowIndex()
		println(currentWindow)

		if(currentWindow >= 1){
			if (!(new commerceCloud.utils()).closeWindowIndex(1)) {
				KeywordUtil.logInfo("Cannot close Window by Url")
				return false
			}
		}

		if (!(new commerceCloud.utils()).delay(5)) {
			KeywordUtil.logInfo("Cannot wait 5 sec")
			return false
		}

		int currentWindownxt = new commerceCloud.utils().getWindowIndex()
		println(currentWindownxt)

		if(currentWindownxt >= 1){
			if (!(new commerceCloud.utils()).closeWindowIndex(1)) {
				KeywordUtil.logInfo("Cannot close Window by Url")
				return false
			}
		}


		if (!(new commerceCloud.utils()).navigateToUrl(strURL)) {
			KeywordUtil.logInfo('Cannot navigate To Url')
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("SalesForce/ConvertLead"),60)) {
			KeywordUtil.logInfo("Cannot see Convert Lead button")
			return false
		}
		if (!(new commerceCloud.utils()).click(findTestObject("SalesForce/ConvertLead"))) {
			KeywordUtil.logInfo("Cannot click Convert Lead button")
			return false
		}

		if (!(new commerceCloud.utils()).delay(10)) {
			KeywordUtil.logInfo("Cannot wait 10 sec")
			return false
		}

		if (!(new commerceCloud.utils()).switchToWindow(1)) {
			KeywordUtil.logInfo("Cannot switch To chat Window")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("SalesForce/Convert"),60)) {
			KeywordUtil.logInfo("Cannot see Convert button")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("SalesForce/AccountNameLookUp"))) {
			KeywordUtil.logInfo("Cannot click Account Name LookUp button")
			return false
		}

		if (!(new commerceCloud.utils()).delay(20)) {
			KeywordUtil.logInfo("Cannot wait 10 sec")
			return false
		}

		if (!(new commerceCloud.utils()).switchToWindow(2)) {
			KeywordUtil.logInfo("Cannot switch To chat Window")
			return false
		}

		if (!(new commerceCloud.utils()).switchToFrame(findTestObject("SalesForce/SearchFrame"))) {
			KeywordUtil.logInfo("Cannot switch To Frame Search Frame")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("SalesForce/GoAccount"),60)) {
			KeywordUtil.logInfo("Cannot see Go Account button")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("SalesForce/SearchAllAccount"))) {
			KeywordUtil.logInfo("Cannot click Search All Account button")
			return false
		}

		if (!(new commerceCloud.utils()).setText(findTestObject("SalesForce/SearchAccount"), strAccountNo)) {
			KeywordUtil.logInfo("Cannot enter Search Account textbox")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("SalesForce/GoAccount"))) {
			KeywordUtil.logInfo("Cannot click Go Account button")
			return false
		}

		if (!(new commerceCloud.utils()).switchToDefaultContent()) {
			KeywordUtil.logInfo("Cannot switch To default Frame")
			return false
		}

		if (!(new commerceCloud.utils()).switchToFrame(findTestObject("SalesForce/ResultsFrame"))) {
			KeywordUtil.logInfo("Cannot switch To Results Frame")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("SalesForce/AccountNameSearch"),60)) {
			KeywordUtil.logInfo("Cannot see Account Name Search button")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("SalesForce/AccountNameSearch"))) {
			KeywordUtil.logInfo("Cannot click Account Name Search button")
			return false
		}

		if (!(new commerceCloud.utils()).switchToWindow(1)) {
			KeywordUtil.logInfo("Cannot switch To chat Window")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("SalesForce/Convert"),60)) {
			KeywordUtil.logInfo("Cannot see Convert button")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeysOutWait(findTestObject("SalesForce/LeadSubject"), "Automation Conversion")) {
			KeywordUtil.logInfo("Cannot enter Subject textbox")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeysOutWait(findTestObject("SalesForce/LeadDueDate"), new commerceCloud.utils().getDueDate())) {
			KeywordUtil.logInfo("Cannot enter Due Date textbox")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeysOutWait(findTestObject("SalesForce/LeadComments"), "Automated conversion for performance testing")) {
			KeywordUtil.logInfo("Cannot enter Comments textbox")
			return false
		}

		if (!(new commerceCloud.utils()).selectDropDownValue(findTestObject("SalesForce/LeadCategory"), "Sales")) {
			KeywordUtil.logInfo("Cannot select the Category textbox")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("SalesForce/Convert"))) {
			KeywordUtil.logInfo("Cannot click Convert button")
			return false
		}

		if (!(new commerceCloud.utils()).delay(5)) {
			KeywordUtil.logInfo("Cannot wait 5 sec")
			return false
		}

		/*if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("SalesForce/Convert"),60)) {
		 KeywordUtil.logInfo("Cannot see Convert button")
		 return false
		 }*/

		if (!(new commerceCloud.utils()).delay(10)) {
			KeywordUtil.logInfo("Cannot wait 5 sec")
			return false
		}

		String strURLTitle = (new commerceCloud.utils()).getURL()
		println(strURLTitle)
		if (!(new commerceCloud.utils()).closeWindowUrl(strURLTitle)) {
			KeywordUtil.logInfo("Cannot close Window by Url")
			return false
		}

		if (!(new commerceCloud.utils()).switchToWindow(0)) {
			KeywordUtil.logInfo("Cannot switch To chat Window")
			return false
		}

		/*if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("SalesForce/ConvertLead"),60)) {
		 KeywordUtil.logInfo("Cannot see Convert Lead button")
		 return false
		 }*/

		if (!(new commerceCloud.utils().writeExcelFile(uploadFilePath, strSFDCId, strAccountNo, strUserName))) {
			KeywordUtil.logInfo('Cannot write a file')
			return false
		}
		println(strURLTitle)
		KeywordUtil.logInfo("Validated  user can convert lead role under sales force")

		return true
	}

}