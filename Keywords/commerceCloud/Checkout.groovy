package commerceCloud

import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.text.SimpleDateFormat

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.testobject.TestObjectProperty
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.driver.DriverFactory

import internal.GlobalVariable

class Checkout {


	/**
	 * getCartName(String name)
	 * @param name - is the name of the message... send null if using system created Date or specify prefix in global variables
	 * @return username
	 */
	@Keyword
	def String getCartName(String name) {

		String strCartName = ""
		if((name.equalsIgnoreCase(""))){

			def date = new Date()
			def sdf = new SimpleDateFormat("MMddyyyy") // HH:mm:ss

			strCartName = GlobalVariable.GlobalSavedCartName + ""+sdf.format(date)
			KeywordUtil.logInfo("UserName = " + strCartName)

		} else {

			def date = new Date()
			def sdf = new SimpleDateFormat("MMddyyyy") // HH:mm:ss

			strCartName = name + ""+sdf.format(date)
			KeywordUtil.logInfo("UserName = " + strCartName)

		}

		return strCartName.toLowerCase()
	}

	/**
	 * CompleteOrderingProcess()
	 * @return true if Complete the ordering process, otherwise false
	 */

	@Keyword
	def CompleteOrderingProcess() {

		KeywordUtil.logInfo("Validate the order is Completed")

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderSummary/Checkout"),60)) {
			KeywordUtil.logInfo("Cannot see the Checkout")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Logo/TesscoHomePageLogo"),1)) {
			KeywordUtil.logInfo("Cannot see the Tessco Home Page Logo")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("OrderSummary/Checkout"))) {
			KeywordUtil.logInfo("Cannot click Checkout")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderSummary/ContinueToPayment"),60)) {
			KeywordUtil.logInfo("Cannot see the Continue To Payment")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Logo/TesscoHomePageLogo"),1)) {
			KeywordUtil.logInfo("Cannot see the Tessco Home Page Logo")
			return false
		}

		if (!(new commerceCloud.Checkout()).addShippingAddress()) {
			KeywordUtil.logInfo("Cannot add Shipping Address")
			return false
		}


		if (!(new commerceCloud.Checkout()).DeliveryOption()) {
			KeywordUtil.logInfo("Cannot select Delivery Option")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("OrderSummary/ContinueToPayment"))) {
			KeywordUtil.logInfo("Cannot click Continue To Payment")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderSummary/ContinueToReview"),60)) {
			KeywordUtil.logInfo("Cannot see the Continue To Review")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Logo/TesscoHomePageLogo"),1)) {
			KeywordUtil.logInfo("Cannot see the Tessco Home Page Logo")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Payment/Payment_Type_Selection_ACCOUNT"))) {
			KeywordUtil.logInfo("Cannot click Account Payment")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("OrderSummary/PurchaseOrderNumber"),"999999")) {
			KeywordUtil.logInfo("Cannot send Purchase Order Number text")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("OrderSummary/ContinueToReview"))) {
			KeywordUtil.logInfo("Cannot click Continue To Review")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderSummary/PlaceOrder"),60)) {
			KeywordUtil.logInfo("Cannot see the Place Order")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Logo/TesscoHomePageLogo"),1)) {
			KeywordUtil.logInfo("Cannot see the Tessco Home Page Logo")
			return false
		}

		if (!(new commerceCloud.utils()).scrollToElement(findTestObject("OrderSummary/PlaceOrder"))) {
			KeywordUtil.logInfo("Cannot scroll Place Order")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("OrderSummary/PlaceOrder"))) {
			KeywordUtil.logInfo("Cannot click Place Order")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderSummary/ThankYou"),60)) {
			KeywordUtil.logInfo("Cannot see the Thank You")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Logo/TesscoHomePageLogo"),1)) {
			KeywordUtil.logInfo("Cannot see the Tessco Home Page Logo")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/OrderContinueShopping"))) {
			KeywordUtil.logInfo("Cannot click Order Continue Shopping")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("QuickOrder/QuickOrder"),60)) {
			KeywordUtil.logInfo("Cannot see the Quick Order")
			return false
		}

		KeywordUtil.logInfo("Validated the order is Completed")
		return true
	}

	/**
	 * DeliveryOption()
	 * @return true if perform delivery option, otherwise false
	 */

	@Keyword
	def DeliveryOption() {

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("OrderSummary/ContinueToPayment"),60)) {
			KeywordUtil.logInfo("Cannot see the Continue To Payment")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Payment/DeliveryDate"))) {
			KeywordUtil.logInfo("Cannot click Delivery Date")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Payment/5DayGround"))) {
			KeywordUtil.logInfo("Cannot click 5 Day Ground")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsEnabled(findTestObject("OrderSummary/ContinueToPayment"),60)) {
			KeywordUtil.logInfo("Cannot see the Continue To Payment")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Payment/ShipAllItemsAsSingleShipment"))) {
			KeywordUtil.logInfo("Cannot click Ship All Items As Single Shipment")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("OrderSummary/ContinueToPayment"),60)) {
			KeywordUtil.logInfo("Cannot see the Continue To Payment")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Payment/LiftGate"))) {
			KeywordUtil.logInfo("Cannot click Lift Gate")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("OrderSummary/ContinueToPayment"),60)) {
			KeywordUtil.logInfo("Cannot see the Continue To Payment")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Payment/InsideDelivery"))) {
			KeywordUtil.logInfo("Cannot click Lift Gate")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("OrderSummary/ContinueToPayment"),60)) {
			KeywordUtil.logInfo("Cannot see the Continue To Payment")
			return false
		}

		return true
	}
	/**
	 * CompleteCreditCartOrderingProcess()
	 * @return true if Complete the Credit Cart ordering process, otherwise false
	 */

	@Keyword
	def CompleteCreditCartOrderingProcess() {

		KeywordUtil.logInfo("Validate the order is Completed")

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("OrderSummary/Checkout"),60)) {
			KeywordUtil.logInfo("Cannot see the Checkout")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("OrderSummary/Checkout"))) {
			KeywordUtil.logInfo("Cannot click Checkout")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("OrderSummary/ContinueToPayment"),60)) {
			KeywordUtil.logInfo("Cannot see the Continue To Payment")
			return false
		}

		if (!(new commerceCloud.Checkout()).addShippingAddress()) {
			KeywordUtil.logInfo("Cannot add Shipping Address")
			return false
		}

		if (!(new commerceCloud.Checkout()).DeliveryOption()) {
			KeywordUtil.logInfo("Cannot select Delivery Option")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("OrderSummary/ContinueToPayment"))) {
			KeywordUtil.logInfo("Cannot click Continue To Payment")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("OrderSummary/ContinueToReview"),60)) {
			KeywordUtil.logInfo("Cannot see the Continue To Review")
			return false
		}

		if (!(new commerceCloud.Checkout()).AddCreditCart()) {
			KeywordUtil.logInfo("Cannot Add Credit cart details")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("OrderSummary/PurchaseOrderNumber"),"999999")) {
			KeywordUtil.logInfo("Cannot send Purchase Order Number text")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("OrderSummary/ContinueToReview"))) {
			KeywordUtil.logInfo("Cannot click Continue To Review")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("OrderSummary/PlaceOrder"),60)) {
			KeywordUtil.logInfo("Cannot see the Place Order")
			return false
		}

		if (!(new commerceCloud.utils()).scrollToElement(findTestObject("OrderSummary/PlaceOrder"))) {
			KeywordUtil.logInfo("Cannot scroll Place Order")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("OrderSummary/PlaceOrder"))) {
			KeywordUtil.logInfo("Cannot click Place Order")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("OrderSummary/ThankYou"),60)) {
			KeywordUtil.logInfo("Cannot see the Thank You")
			return false
		}

		String strOrderNo = (new commerceCloud.utils()).sReturnText(findTestObject("OrderSummary/OrderNumber"))

		if (strOrderNo.equalsIgnoreCase("")) {
			KeywordUtil.logInfo("Cannot get the Order Number")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/OrderContinueShopping"))) {
			KeywordUtil.logInfo("Cannot click Order Continue Shopping")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("QuickOrder/QuickOrder"),60)) {
			KeywordUtil.logInfo("Cannot see the Quick Order")
			return false
		}

		KeywordUtil.logInfo("Validated the order is Completed")
		return strOrderNo
	}

	/**
	 * AddCreditCart()
	 * @return true if Add Credit Cart Details, otherwise false
	 */

	@Keyword
	def AddCreditCart() {
		KeywordUtil.logInfo("Validate user can Add Credit Cart")

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Payment/Payment_Type_Selection_CARD"),60)) {
			KeywordUtil.logInfo("Cannot see the Payment Type Selection CARD")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Payment/Payment_Type_Selection_CARD"))) {
			KeywordUtil.logInfo("Cannot click Payment Type Selection CARD")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Payment/AddTemporaryCardButton"))) {
			KeywordUtil.logInfo("Cannot click Add Temporary Card")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Payment/CancelNewCard"))) {
			KeywordUtil.logInfo("Cannot click Cancel New Card")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Payment/AddTemporaryCardButton"))) {
			KeywordUtil.logInfo("Cannot click Add Temporary Card")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Payment/amex"),60)) {
			KeywordUtil.logInfo("Cannot see the amex")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Payment/visa"),1)) {
			KeywordUtil.logInfo("Cannot see the visa")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Payment/master"),1)) {
			KeywordUtil.logInfo("Cannot see the master")
			return false
		}

		/*	if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Payment/discover"),1)) {
		 KeywordUtil.logInfo("Cannot see the discover")
		 return false
		 }
		 */
		if (!(new commerceCloud.utils()).sendKeys(findTestObject("Payment/NameOnCard"),"MasterCard")) {
			KeywordUtil.logInfo("Cannot send Name on Card")
			return false
		}

		if (!(new commerceCloud.utils()).delay(15)) {
			KeywordUtil.logInfo("Cannot wait 15 sec")
			return false
		}

		if (!(new commerceCloud.utils()).switchToFrame(findTestObject("Payment/CardIFrame"))) {
			KeywordUtil.logInfo("Cannot switch To Frame")
			return false
		}

		if (!(new commerceCloud.utils()).delay(13)) {
			KeywordUtil.logInfo("Cannot wait 3 sec")
			return false
		}

		if (!(new commerceCloud.utils()).switchToFrame(findTestObject("Payment/CardIFrameTokenFrame"))) {
			KeywordUtil.logInfo("Cannot switch Token IFrame ")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeysOutWait(findTestObject("Payment/Token"),"5500000000000004")) {
			KeywordUtil.logInfo("Cannot send CARD NUMBER")
			return false
		}

		if (!(new commerceCloud.utils()).switchToDefaultContent()) {
			KeywordUtil.logInfo("Cannot switch To Default Content")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("Payment/CardExpiration"),"11/30")) {
			KeywordUtil.logInfo("Cannot send Card Expiration Date")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("Payment/CardCVV"),"123")) {
			KeywordUtil.logInfo("Cannot send Card CVV")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsEnabled(findTestObject("Payment/AddNewCard"),30)) {
			KeywordUtil.logInfo("Cannot see Enabled into Add New Card")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Payment/AddNewCard"))) {
			KeywordUtil.logInfo("Cannot click Add New Card")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Payment/ChangeTemporaryCard"),60)) {
			KeywordUtil.logInfo("Cannot see the Change Temporary Card")
			return false
		}

		KeywordUtil.logInfo("Validated the user Added Credit Cart")
		return true
	}


	/**
	 * CompleteProcessReturnOrderNumber()
	 * @return true if Complete the ordering process and return to order number, otherwise false
	 */

	@Keyword
	def CompleteProcessReturnOrderNumber() {

		KeywordUtil.logInfo("Validate the order is Completed")

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("OrderSummary/Checkout"),60)) {
			KeywordUtil.logInfo("Cannot see the Checkout")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("OrderSummary/Checkout"))) {
			KeywordUtil.logInfo("Cannot click Checkout")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("OrderSummary/ContinueToPayment"),60)) {
			KeywordUtil.logInfo("Cannot see the Continue To Payment")
			return false
		}

		if (!(new commerceCloud.Checkout()).addShippingAddress()) {
			KeywordUtil.logInfo("Cannot add Shipping Address")
			return false
		}

		if (!(new commerceCloud.Checkout()).DeliveryOption()) {
			KeywordUtil.logInfo("Cannot select Delivery Option")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("OrderSummary/ContinueToPayment"))) {
			KeywordUtil.logInfo("Cannot click Continue To Payment")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("OrderSummary/ContinueToReview"),60)) {
			KeywordUtil.logInfo("Cannot see the Continue To Review")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Payment/Payment_Type_Selection_ACCOUNT"))) {
			KeywordUtil.logInfo("Cannot click Account Payment")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("OrderSummary/PurchaseOrderNumber"),"999999")) {
			KeywordUtil.logInfo("Cannot send Purchase Order Number text")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("OrderSummary/ContinueToReview"))) {
			KeywordUtil.logInfo("Cannot click Continue To Review")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("OrderSummary/PlaceOrder"),60)) {
			KeywordUtil.logInfo("Cannot see the Place Order")
			return false
		}

		if (!(new commerceCloud.utils()).scrollToElement(findTestObject("OrderSummary/PlaceOrder"))) {
			KeywordUtil.logInfo("Cannot scroll Place Order")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("OrderSummary/PlaceOrder"))) {
			KeywordUtil.logInfo("Cannot click Place Order")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("OrderSummary/ThankYou"),60)) {
			KeywordUtil.logInfo("Cannot see the Thank You")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderConfirmation/OrderConfirmation"),1)) {
			KeywordUtil.logInfo("Cannot see the Order Confirmation")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderConfirmation/OrderConfirmationBillingAddress"),1)) {
			KeywordUtil.logInfo("Cannot see the Order Confirmation Billing Address")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderConfirmation/OrderConfirmationOrderNumber"),1)) {
			KeywordUtil.logInfo("Cannot see the Order Confirmation Order Number")
			return false
		}

		/*if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderConfirmation/OrderConfirmationOrderTotal"),1)) {
		 KeywordUtil.logInfo("Cannot see the Order Confirmation Order Total")
		 return false
		 }*/

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderConfirmation/OrderConfirmationPayment"),1)) {
			KeywordUtil.logInfo("Cannot see the Order Confirmation Payment")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderConfirmation/OrderConfirmationPlacedBy"),1)) {
			KeywordUtil.logInfo("Cannot see the Order Confirmation Placed By")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderConfirmation/OrderConfirmationPONumber"),1)) {
			KeywordUtil.logInfo("Cannot see the Order Confirmation PO Number")
			return false
		}

		/*	if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderConfirmation/OrderConfirmationShipping"),1)) {
		 KeywordUtil.logInfo("Cannot see the Order Confirmation Shipping")
		 return false
		 }*/

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderConfirmation/OrderConfirmationShippingAddress"),1)) {
			KeywordUtil.logInfo("Cannot see the Order Confirmation Shipping Address")
			return false
		}

		/*if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderConfirmation/ShipmentItemName"),1)) {
		 KeywordUtil.logInfo("Cannot see the Shipment Item Name")
		 return false
		 }*/

		/*if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderConfirmation/ShipmentManufacturerName"),1)) {
		 KeywordUtil.logInfo("Cannot see the Shipment Manufacturer Name")
		 return false
		 }*/

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderConfirmation/ShipmentsMfgPart"),1)) {
			KeywordUtil.logInfo("Cannot see the Shipments Mfg Part")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderConfirmation/ShipmentsQty"),1)) {
			KeywordUtil.logInfo("Cannot see the Shipments Qty")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderConfirmation/ShipmentsTesscoSku"),1)) {
			KeywordUtil.logInfo("Cannot see the Shipments Tessco Sku")
			return false
		}

		/*if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderConfirmation/ShipmentsUPC"),1)) {
		 KeywordUtil.logInfo("Cannot see the Shipments UPC")
		 return false
		 }
		 */
		/*	if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderConfirmation/OrderConfirmationStatus"),1)) {
		 KeywordUtil.logInfo("Cannot see the Order Confirmation Status")
		 return false
		 }
		 if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderConfirmation/OrderConfirmationSubtotal"),1)) {
		 KeywordUtil.logInfo("Cannot see the Order Confirmation Subtotal")
		 return false
		 }*/

		/*if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderConfirmation/OrderConfirmationStatus"),1)) {
		 KeywordUtil.logInfo("Cannot see the Order Confirmation Status")
		 return false
		 }
		 */
		String strOrderNo = (new commerceCloud.utils()).sReturnText(findTestObject("OrderSummary/OrderNumber"))

		if (strOrderNo.equalsIgnoreCase("")) {
			KeywordUtil.logInfo("Cannot get the Order Number")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/OrderContinueShopping"))) {
			KeywordUtil.logInfo("Cannot click Order Continue Shopping")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("QuickOrder/QuickOrder"),60)) {
			KeywordUtil.logInfo("Cannot see the Quick Order")
			return false
		}


		KeywordUtil.logInfo("Validated the order is Completed")
		return strOrderNo
	}

	/**
	 * CompleteGSAProcessReturnOrderNumber()
	 * @return true if Complete the GSA ordering process and return to order number, otherwise false
	 */

	@Keyword
	def CompleteGSAProcessReturnOrderNumber() {

		KeywordUtil.logInfo("Validate the order is Completed")

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderSummary/Checkout"),60)) {
			KeywordUtil.logInfo("Cannot see the Checkout")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("OrderSummary/Checkout"))) {
			KeywordUtil.logInfo("Cannot click Checkout")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderSummary/ContinueToPayment"),60)) {
			KeywordUtil.logInfo("Cannot see the Continue To Payment")
			return false
		}

		if (!(new commerceCloud.Checkout()).addShippingAddress()) {
			KeywordUtil.logInfo("Cannot add Shipping Address")
			return false
		}


		if (!(new commerceCloud.utils()).click(findTestObject("OrderSummary/ContinueToPayment"))) {
			KeywordUtil.logInfo("Cannot click Continue To Payment")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderSummary/ContinueToReview"),60)) {
			KeywordUtil.logInfo("Cannot see the Continue To Review")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("OrderSummary/PurchaseOrderNumber"),"999999")) {
			KeywordUtil.logInfo("Cannot send Purchase Order Number text")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("OrderSummary/ContinueToReview"))) {
			KeywordUtil.logInfo("Cannot click Continue To Review")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderSummary/PlaceOrder"),60)) {
			KeywordUtil.logInfo("Cannot see the Place Order")
			return false
		}

		if (!(new commerceCloud.utils()).scrollToElement(findTestObject("OrderSummary/PlaceOrder"))) {
			KeywordUtil.logInfo("Cannot scroll Place Order")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("OrderSummary/PlaceOrder"))) {
			KeywordUtil.logInfo("Cannot click Place Order")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderSummary/ThankYou"),60)) {
			KeywordUtil.logInfo("Cannot see the Thank You")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderConfirmation/OrderConfirmation"),1)) {
			KeywordUtil.logInfo("Cannot see the Order Confirmation")
			return false
		}

		String strOrderNo = (new commerceCloud.utils()).sReturnText(findTestObject("OrderSummary/OrderNumber"))

		if (strOrderNo.equalsIgnoreCase("")) {
			KeywordUtil.logInfo("Cannot get the Order Number")
			return false
		}

		KeywordUtil.logInfo("Validated the order is Completed")
		return strOrderNo
	}


	/**
	 * addShippingAddress()
	 * @return true if adding Shipping Address, otherwise false
	 */

	@Keyword
	def addShippingAddress() {

		KeywordUtil.logInfo("Validate the adding Shipping Address")
		String strCartName = (new commerceCloud.Cart()).getCartName("")

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("AddTemporaryAddress/NewAddress"),60)) {
			KeywordUtil.logInfo("Cannot see the Add New Address")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("AddTemporaryAddress/NewAddress"))) {
			KeywordUtil.logInfo("Cannot click New Address")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("AddTemporaryAddress/Cancel"))) {
			KeywordUtil.logInfo("Cannot click Cancel")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("AddTemporaryAddress/NewAddress"),60)) {
			KeywordUtil.logInfo("Cannot see the New Address")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("AddTemporaryAddress/NewAddress"))) {
			KeywordUtil.logInfo("Cannot click New Address")
			return false
		}


		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("AddTemporaryAddress/AddTemporaryShippingAddressHeader"),60)) {
			KeywordUtil.logInfo("Cannot see the Add Temporary Shipping Address Header")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("AddTemporaryAddress/Name"),strCartName)) {
			KeywordUtil.logInfo("Cannot send Name")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("AddTemporaryAddress/ADDRESS"),"11126 McCormick Rd")) {
			KeywordUtil.logInfo("Cannot send ADDRESS")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("AddTemporaryAddress/ADDRESSLINE2"),"Hunt Valley")) {
			KeywordUtil.logInfo("Cannot send ADDRESS LINE 2")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("AddTemporaryAddress/CITY"),"MD")) {
			KeywordUtil.logInfo("Cannot send CITY")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("AddTemporaryAddress/STATE"))) {
			KeywordUtil.logInfo("Cannot click STATE")
			return false
		}

		if (!(new commerceCloud.utils()).delay(4)) {
			KeywordUtil.logInfo("Cannot Wait a sec")
			return false
		}


		if (!(new commerceCloud.utils()).click(findTestObject("AddTemporaryAddress/STATEName"))) {
			KeywordUtil.logInfo("Cannot click STATE Name")
			return false
		}

		if (!(new commerceCloud.utils()).delay(3)) {
			KeywordUtil.logInfo("Cannot Wait a sec")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("AddTemporaryAddress/ZIPCODE"),"21031")) {
			KeywordUtil.logInfo("Cannot send ZIPCODE")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("AddTemporaryAddress/AddNewAddress"))) {
			KeywordUtil.logInfo("Cannot click Add New Address")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Shipping/ShippingAddress"),60)) {
			KeywordUtil.logInfo("Cannot see the Shipping Address")
			return false
		}

		TestObject to = new TestObject("Created Address")
		String xpath = "//strong[contains(normalize-space(),'Temporary Shipping Address')]/following::span[contains(normalize-space(),'" + strCartName + "')][1]"
		TestObjectProperty prop = new TestObjectProperty("xpath", ConditionType.EQUALS, xpath)
		to.addProperty(prop)

		WebDriver driver = DriverFactory.getWebDriver()
		int size =  driver.findElements(By.xpath(xpath)).size()
		println("size : "+ size)

		if (!(size == 1)) {
			KeywordUtil.logInfo("Cannot see the Created Address")
			return false
		}

		if ((new commerceCloud.utils()).verifyElementIsEnabled(findTestObject("Shipping/ShippingSearchSavedAddresses"),10)) {

			if (!(new commerceCloud.utils()).click(findTestObject("Shipping/ShippingSearchSavedAddresses"))) {
				KeywordUtil.logInfo("Cannot click Shipping Search Saved Addresses")
				return false
			}

			if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Shipping/SavedCancelSearchAddress"),60)) {
				KeywordUtil.logInfo("Cannot see the Saved Cancel Search Address")
				return false
			}

			if (!(new commerceCloud.utils()).click(findTestObject("Shipping/SavedCancelSearchAddress"))) {
				KeywordUtil.logInfo("Cannot click Saved Cancel Search Address")
				return false
			}

			if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Shipping/ShippingSearchSavedAddresses"),60)) {
				KeywordUtil.logInfo("Cannot see the Shipping Search Saved Addresses")
				return false
			}

			if (!(new commerceCloud.utils()).click(findTestObject("Shipping/ShippingSearchSavedAddresses"))) {
				KeywordUtil.logInfo("Cannot click Shipping Search Saved Addresses")
				return false
			}

			if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Shipping/SavedAddressSearch"),60)) {
				KeywordUtil.logInfo("Cannot see the Saved Address Search")
				return false
			}

			if (!(new commerceCloud.utils()).sendKeys(findTestObject("Shipping/SavedAddressSearch"),strCartName)) {
				KeywordUtil.logInfo("Cannot send Saved Address Search")
				return false
			}

			if (!(new commerceCloud.utils()).click(findTestObject("Shipping/SavedSearchIcon"))) {
				KeywordUtil.logInfo("Cannot click Saved Search Icon")
				return false
			}

			if (!(new commerceCloud.utils()).click(findTestObject("Shipping/SavedFirstAddress"))) {
				KeywordUtil.logInfo("Cannot click Saved First Address")
				return false
			}

			if (!(new commerceCloud.utils()).click(findTestObject("Shipping/SavedUseSelectedAddress"))) {
				KeywordUtil.logInfo("Cannot click Saved Use Selected Address")
				return false
			}

			if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Shipping/ShippingAddress"),60)) {
				KeywordUtil.logInfo("Cannot see the Shipping Address")
				return false
			}

			KeywordUtil.logInfo("Validated the user searched Shipping Address")
		} else {

			/*if (!(new commerceCloud.utils()).click(findTestObject("Shipping/SavedUseSelectedAddress"))) {
			 KeywordUtil.logInfo("Cannot click Saved Use Selected Address")
			 return false
			 }*/

			KeywordUtil.logInfo("Validated the user unable to search and added first available Shipping Address")
		}

		KeywordUtil.logInfo("Validated the adding Shipping Address")
		return true
	}




	/**
	 * addProductToCart(String Search)
	 * @param Search - user should send the tessco product details
	 * @return true if  the user able to add search product and added product to cart, otherwise false
	 */

	@Keyword
	def addProductToCart(String Search) {

		KeywordUtil.logInfo("Validate the user able to add search product and added product to cart")

		if (!(new commerceCloud.Checkout()).searchTesscoProduct(Search)) {
			KeywordUtil.logInfo("Cannot search the Tessco Product")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("ProductSearch/product listing price"),60)) {
			KeywordUtil.logInfo("Cannot see the product listing price")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Logo/TesscoHomePageLogo"),1)) {
			KeywordUtil.logInfo("Cannot see the Tessco Home Page Logo")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("ProductSearch/Product List Add to Cart"))) {
			KeywordUtil.logInfo("Cannot click search tessco com button")
			return false
		}


		if (!(new commerceCloud.Cart()).CableAddToCart()) {
			KeywordUtil.logInfo("Cannot add Cable product to Add To Cart")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("AddToCart/ContinueShopping"))) {
			KeywordUtil.logInfo("Cannot click Continue Shopping")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("ProductSearch/product listing price"),60)) {
			KeywordUtil.logInfo("Cannot see the product listing price")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Logo/TesscoHomePageLogo"),1)) {
			KeywordUtil.logInfo("Cannot see the Tessco Home Page Logo")
			return false
		}

		if (!(new commerceCloud.utils()).scrollToElement(findTestObject("ProductSearch/Product List Add to Cart Two"))) {
			KeywordUtil.logInfo("Cannot scroll the Product List Add to Cart Two")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("ProductSearch/Product List Add to Cart Two"))) {
			KeywordUtil.logInfo("Cannot click search tessco com button")
			return false
		}


		if (!(new commerceCloud.Cart()).CableAddToCart()) {
			KeywordUtil.logInfo("Cannot add Cable product to Add To Cart")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("AddToCart/RemoveAddToCart"))) {
			KeywordUtil.logInfo("Cannot click Remove Add To Cart")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("ProductSearch/product listing price"),60)) {
			KeywordUtil.logInfo("Cannot see the product listing price")
			return false
		}

		if (!(new commerceCloud.utils()).scrollToElement(findTestObject("ProductSearch/product listing price"))) {
			KeywordUtil.logInfo("Cannot scroll the product listing price")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("ProductSearch/Product List Add to Cart Three"))) {
			KeywordUtil.logInfo("Cannot click search tessco com button")
			return false
		}


		if (!(new commerceCloud.Cart()).CableAddToCart()) {
			KeywordUtil.logInfo("Cannot add Cable product to Add To Cart")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("AddToCart/GoToCart"))) {
			KeywordUtil.logInfo("Cannot click Go To Cart")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Cart/ClearCart"),60)) {
			KeywordUtil.logInfo("Cannot see the Clear Cart")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Logo/TesscoHomePageLogo"),1)) {
			KeywordUtil.logInfo("Cannot see the Tessco Home Page Logo")
			return false
		}

		KeywordUtil.logInfo("Validate the user able to add search product and added product to cart")
		return true
	}

	/**
	 * addGSAProductToCart(String Search)
	 * @param Search - user should 'Test' to search a column
	 * @return true if  the user able to add GSA search product and added product to cart, otherwise false
	 */

	@Keyword
	def addGSAProductToCart(String Search) {

		KeywordUtil.logInfo("Validate the user able to add search product and added product to cart")

		if (!(new commerceCloud.Checkout()).searchTesscoProduct(Search)) {
			KeywordUtil.logInfo("Cannot search the Tessco Product")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("ProductSearch/product listing price"),60)) {
			KeywordUtil.logInfo("Cannot see the product listing price")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("FILTERS/GSA"),60)) {
			KeywordUtil.logInfo("Cannot see the GSA text")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("FILTERS/GSA_Plus"))) {
			KeywordUtil.logInfo("Cannot click GSA Plus button")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("FILTERS/GSA_True"),60)) {
			KeywordUtil.logInfo("Cannot see the GSA True checkbox")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("FILTERS/GSA_True"))) {
			KeywordUtil.logInfo("Cannot click GSA True checkbox")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("ProductSearch/product listing price"),60)) {
			KeywordUtil.logInfo("Cannot see the product listing price")
			return false
		}

		List<WebElement> lstGSATotal = (new commerceCloud.utils()).ListOfWebElement(findTestObject("FILTERS/GSA_Product_Size"))
		int GSATotal = lstGSATotal.size()

		List<WebElement> lstNonGSATotal = (new commerceCloud.utils()).ListOfWebElement(findTestObject("FILTERS/NON_GSA_Product_Size"))
		int NonGSATotal = lstNonGSATotal.size()

		if (!(GSATotal == NonGSATotal)) {
			KeywordUtil.logInfo("Cannot Verified only results for GSA products populate the product list page")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("ProductSearch/Product List Add to Cart"))) {
			KeywordUtil.logInfo("Cannot click search tessco com button")
			return false
		}


		if (!(new commerceCloud.Cart()).CableAddToCart()) {
			KeywordUtil.logInfo("Cannot add Cable product to Add To Cart")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("AddToCart/ContinueShopping"))) {
			KeywordUtil.logInfo("Cannot click Continue Shopping")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("ProductSearch/product listing price"),60)) {
			KeywordUtil.logInfo("Cannot see the product listing price")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Logo/TesscoHomePageLogo"),1)) {
			KeywordUtil.logInfo("Cannot see the Tessco Home Page Logo")
			return false
		}

		if (!(new commerceCloud.utils()).scrollToElement(findTestObject("ProductSearch/Product List Add to Cart Two"))) {
			KeywordUtil.logInfo("Cannot scroll the Product List Add to Cart Two")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("ProductSearch/Product List Add to Cart Two"))) {
			KeywordUtil.logInfo("Cannot click search tessco com button")
			return false
		}


		if (!(new commerceCloud.Cart()).CableAddToCart()) {
			KeywordUtil.logInfo("Cannot add Cable product to Add To Cart")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("AddToCart/RemoveAddToCart"))) {
			KeywordUtil.logInfo("Cannot click Remove Add To Cart")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("ProductSearch/product listing price"),60)) {
			KeywordUtil.logInfo("Cannot see the product listing price")
			return false
		}

		if (!(new commerceCloud.utils()).scrollToElement(findTestObject("ProductSearch/product listing price"))) {
			KeywordUtil.logInfo("Cannot scroll the product listing price")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("ProductSearch/Product List Add to Cart Three"))) {
			KeywordUtil.logInfo("Cannot click search tessco com button")
			return false
		}

		if (!(new commerceCloud.Cart()).CableAddToCart()) {
			KeywordUtil.logInfo("Cannot add Cable product to Add To Cart")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("AddToCart/GoToCart"))) {
			KeywordUtil.logInfo("Cannot click Go To Cart")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Cart/ClearCart"),60)) {
			KeywordUtil.logInfo("Cannot see the Clear Cart")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Logo/TesscoHomePageLogo"),1)) {
			KeywordUtil.logInfo("Cannot see the Tessco Home Page Logo")
			return false
		}

		KeywordUtil.logInfo("Validate the user able to add search product and added product to cart")
		return true
	}


	/**
	 * addGSAProductToCart(String Search)
	 * @param Search - user should 'Test' to search a column
	 * @return true if  the user able to add GSA search product and added product to cart, otherwise false
	 */

	@Keyword
	def StandardAccount(String StandardAccountValues) {

		KeywordUtil.logInfo("Validate the Standard Account dropdown to cart")

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Cart/CheckoutTop"),60)) {
			KeywordUtil.logInfo("Cannot see the Check out Top")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/StandardAccount"))) {
			KeywordUtil.logInfo("Cannot click Standard Account")
			return false
		}


		TestObject to = new TestObject("Standard Account")
		String xpath = "//a[contains(text(),'" + StandardAccountValues + "')]"
		TestObjectProperty prop = new TestObjectProperty("xpath", ConditionType.EQUALS, xpath)
		to.addProperty(prop)

		if (!(new commerceCloud.utils()).click(to)) {
			KeywordUtil.logInfo("Cannot click values: "+StandardAccountValues)
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Cart/CheckoutTop"),60)) {
			KeywordUtil.logInfo("Cannot see the Check out Top")
			return false
		}


		KeywordUtil.logInfo("Validated the Standard Account dropdown was selected under cart")
		return true
	}

	/**
	 * searchTesscoProduct(TestObject ProductionList, TestObject ProductionSubList, def Search)
	 * @param Search - User should send production search parts details
	 * @return true if the ability to search products using product textbox, otherwise false
	 */

	@Keyword
	def searchTesscoProduct(String Search) {

		KeywordUtil.logInfo("Validate the ability to search products using product textbox")

		if(Search == ""){
			Search = "Cable"
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("ProductSearch/search tessco com textbox"),60)) {
			KeywordUtil.logInfo("Cannot see the search tessco com textbox")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Logo/TesscoHomePageLogo"),1)) {
			KeywordUtil.logInfo("Cannot see the Tessco Home Page Logo")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("ProductSearch/search tessco com textbox"), Search)) {
			KeywordUtil.logInfo("Cannot enter search tessco com textbox")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("ProductSearch/search tessco com button"))) {
			KeywordUtil.logInfo("Cannot click search tessco com button")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("ProductSearch/search result center block margin"),60)) {
			KeywordUtil.logInfo("Cannot see the search result center block margin")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Logo/TesscoHomePageLogo"),1)) {
			KeywordUtil.logInfo("Cannot see the Tessco Home Page Logo")
			return false
		}

		KeywordUtil.logInfo("Validated user successfully search products using product textbox")
		return true
	}

	/**
	 * PlaceOrder(String Search)
	 * @param Search - user should send Cart Name
	 * @return true if complete the order process, otherwise false
	 */

	@Keyword
	def PlaceOrder(String Search) {

		KeywordUtil.logInfo("Validate the user can complete the Order")


		if (!(new commerceCloud.Checkout()).addProductToCart(Search)) {
			KeywordUtil.logInfo("Cannot search the Tessco Product")
			return false
		}

		if (!(new commerceCloud.Checkout()).CompleteOrderingProcess()) {
			KeywordUtil.logInfo("Cannot the user can complete the Order")
			return false
		}

		KeywordUtil.logInfo("Validated the user can complete the Order")
		return true
	}

	/**
	 * PlaceOrderCreditCart(String Search)
	 * @param Search - user should send Cart Name
	 * @return true if complete the order process, otherwise false
	 */

	@Keyword
	def PlaceOrderCreditCart(String Search) {

		KeywordUtil.logInfo("Validate the user can complete the Order")


		if (!(new commerceCloud.Checkout()).addProductToCart(Search)) {
			KeywordUtil.logInfo("Cannot search the Tessco Product")
			return false
		}

		String strOrderNo = (new commerceCloud.Checkout()).CompleteCreditCartOrderingProcess()
		if (strOrderNo.equalsIgnoreCase("")) {
			KeywordUtil.logInfo("Cannot the user can complete and return to order number")
			return false
		}

		if (!(new commerceCloud.Checkout()).navigateOrderHistory()) {
			KeywordUtil.logInfo("Cannot navigate to Order History Page")
			return false
		}

		if (!(new commerceCloud.Checkout()).CreditCartOrderHistory(strOrderNo)) {
			KeywordUtil.logInfo("Cannot Validate the Order No and Credit Cart Name Details page")
			return false
		}

		KeywordUtil.logInfo("Validated the user can complete the Order")
		return true
	}

	/**
	 * OrderHistorySorting(String Search) 
	 * @return true if Order History is Sorted, otherwise false
	 */

	@Keyword
	def OrderHistorySorting() {
		KeywordUtil.logInfo("Validate the user can do Order History Sorting")

		if (!(new commerceCloud.Checkout()).navigateOrderHistory()) {
			KeywordUtil.logInfo("Cannot navigate to Order History Page")
			return false
		}

		if (!(new commerceCloud.Checkout()).SortOrderHistory()) {
			KeywordUtil.logInfo("Cannot Creation Date, Order, Placed By, PO, Status and Total sort the Order History Page")
			return false
		}

		KeywordUtil.logInfo("Validate the user can do Order History Sorting")
	}

	/**
	 * OrderHistory(String Search)
	 * @param Search - user should send Cart Name
	 * @return true if complete the order process, otherwise false
	 */

	@Keyword
	def OrderHistory(String Search) {

		KeywordUtil.logInfo("Validate the user can complete the Order")

		if (!(new commerceCloud.Checkout()).addProductToCart(Search)) {
			KeywordUtil.logInfo("Cannot search the Tessco Product")
			return false
		}

		String strOrderNo = (new commerceCloud.Checkout()).CompleteProcessReturnOrderNumber()
		println(strOrderNo)
		if (strOrderNo.equalsIgnoreCase("")) {
			KeywordUtil.logInfo("Cannot the user can complete and return to order number")
			return false
		}

		if (!(new commerceCloud.Checkout()).navigateOrderHistory()) {
			KeywordUtil.logInfo("Cannot navigate to Order History Page")
			return false
		}

		if (!(new commerceCloud.Checkout()).OrderHistoryDetails(strOrderNo)) {
			KeywordUtil.logInfo("Cannot Validate the Order No and Order History Details page")
			return false
		}
		KeywordUtil.logInfo("Validated the user can complete the Order")
		return true
	}

	/**
	 * GsaOrderHistory(String Search)
	 * @param Search - user should send Cart Name
	 * @return true if complete the order process, otherwise false
	 */

	@Keyword
	def GsaOrderHistory(String GSASearch,String nonGSASearch, String StandardAccountValues) {

		KeywordUtil.logInfo("Validate the user can Add Gsa product to cart")

		if (!(new commerceCloud.Checkout()).addGSAProductToCart(GSASearch)) {
			KeywordUtil.logInfo("Cannot search the GSA Tessco Product")
			return false
		}

		if (!(new commerceCloud.Checkout()).addProductToCart(nonGSASearch)) {
			KeywordUtil.logInfo("Cannot search the Tessco Product")
			return false
		}

		/* if (!(new commerceCloud.Checkout()).StandardAccount(StandardAccountValues)) {
		 KeywordUtil.logInfo("Cannot select the GSA Tessco Product")
		 return false
		 }
		 String strOrderNo = (new commerceCloud.Checkout()).CompleteGSAProcessReturnOrderNumber()
		 if (strOrderNo.equalsIgnoreCase("")) {
		 KeywordUtil.logInfo("Cannot the user can complete and return to order number")
		 return false
		 }
		 if (!(new commerceCloud.Checkout()).navigateOrderHistory()) {
		 KeywordUtil.logInfo("Cannot navigate to Order History Page")
		 return false
		 }
		 if (!(new commerceCloud.Checkout()).GSAOrderHistoryDetails(strOrderNo)) {
		 KeywordUtil.logInfo("Cannot Validate the Order No and Order History Details page")
		 return false
		 } 
		 */
		KeywordUtil.logInfo("Validated the user can Add Gsa product to cart")
		return true
	}


	/**
	 * ShippingPage(String Search)
	 * @param Search - user should send Cart Name
	 * @return true if Shipping Page validation is done, otherwise false
	 */

	@Keyword
	def ShippingPage(String Search) {

		KeywordUtil.logInfo("Validate the user can add Product To Cart")

		if (!(new commerceCloud.Checkout()).addProductToCart(Search)) {
			KeywordUtil.logInfo("Cannot search the Tessco Product")
			return false
		}

		KeywordUtil.logInfo("Validated the user can add Product To Cart")

		KeywordUtil.logInfo("Validate the Shipping, Shipment and Payment Page")

		if (!(new commerceCloud.Checkout()).ShippingPageValidation()) {
			KeywordUtil.logInfo("Cannot Validate the Shipping, Shipment and Payment Page UI")
			return false
		}

		if (!(new commerceCloud.Checkout()).OrderPageValidation()) {
			KeywordUtil.logInfo("Cannot Validate Order Page UI")
			return false
		}
		KeywordUtil.logInfo("Validated the Shipping, Shipment and Payment Page")
		return true
	}

	/**
	 * CompleteOrderingProcess()
	 * @return true if Complete the ordering process, otherwise false
	 */

	@Keyword
	def ShippingPageValidation() {

		KeywordUtil.logInfo("Validate the Validate the Shipping, Shipment and Payment Page UI")

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Cart/Cart"),60)) {
			KeywordUtil.logInfo("Cannot see the Cart")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/Cart"))) {
			KeywordUtil.logInfo("Cannot click Cart")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderSummary/Checkout"),60)) {
			KeywordUtil.logInfo("Cannot see the Checkout")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("OrderSummary/Checkout"))) {
			KeywordUtil.logInfo("Cannot click Checkout")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderSummary/ContinueToPayment"),60)) {
			KeywordUtil.logInfo("Cannot see the Continue To Payment")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Shipping/OrderSummary"),1)) {
			KeywordUtil.logInfo("Cannot see the Order Summary")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Shipping/OrderSummaryOrderTotal"),1)) {
			KeywordUtil.logInfo("Cannot see the Order Summary Order Total")
			return false
		}

		/*if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Shipping/OrderSummaryShipping"),1)) {
		 KeywordUtil.logInfo("Cannot see the Order Summary Shipping")
		 return false
		 }*/

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Shipping/OrderSummarySubtotal"),1)) {
			KeywordUtil.logInfo("Cannot see the Order Summary Subtotal")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Shipping/OrderSummaryTax"),1)) {
			KeywordUtil.logInfo("Cannot see the Order Summary Tax")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Shipping/Shipment"),1)) {
			KeywordUtil.logInfo("Cannot see the Shipment")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Shipping/ShipmentContinuePayment"),1)) {
			KeywordUtil.logInfo("Cannot see the Shipment Continue Payment")
			return false
		}

		/*if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Shipping/ShipmentDeliveryDate"),1)) {
		 KeywordUtil.logInfo("Cannot see the Shipment Delivery Date")
		 return false
		 }*/

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Shipping/ShipmentInsideDelivery"),1)) {
			KeywordUtil.logInfo("Cannot see the Shipment Inside Delivery")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Shipping/ShipmentLiftGate"),1)) {
			KeywordUtil.logInfo("Cannot see the Shipment Lift Gate")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Shipping/ShipmentShipAllItemsSingleShipment"),1)) {
			KeywordUtil.logInfo("Cannot see the Shipment Ship All Items Single Shipment")
			return false
		}

		/*if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Shipping/ShipmentTotalItem"),1)) {
		 KeywordUtil.logInfo("Cannot see the Shipment Total Item")
		 return false
		 }
		 */
		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Shipping/ShippingAddress"),1)) {
			KeywordUtil.logInfo("Cannot see the Shipping Address")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Shipping/ShippingAddTemporaryAddress"),1)) {
			KeywordUtil.logInfo("Cannot see the Shipping Add Temporary Address")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Shipping/ShippingDeliveryInstructions"),1)) {
			KeywordUtil.logInfo("Cannot see the Shipping Delivery Instructions")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Shipping/ShippingEmailNotificationList"),1)) {
			KeywordUtil.logInfo("Cannot see the Shipping Email Notification List")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Shipping/ShippingSavedAddresses"),1)) {
			KeywordUtil.logInfo("Cannot see the Shipping Saved Addresses")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Shipping/ShippingSearchSavedAddresses"),1)) {
			KeywordUtil.logInfo("Cannot see the Shipping Search Saved Addresses")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Shipping/OrderSummaryPayment"),1)) {
			KeywordUtil.logInfo("Cannot see the Payment")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Shipping/OrderSummaryReview"),1)) {
			KeywordUtil.logInfo("Cannot see the Review")
			return false
		}

		if (!(new commerceCloud.Checkout()).DeliveryOption()) {
			KeywordUtil.logInfo("Cannot select Delivery Option")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Shipping/ShipmentContinuePayment"))) {
			KeywordUtil.logInfo("Cannot click Shipment Continue Payment")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderSummary/ContinueToReview"),60)) {
			KeywordUtil.logInfo("Cannot see the Continue To Review")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Payment/PaymentTypeSelectionAccount"))) {
			KeywordUtil.logInfo("Cannot click the Payment Type Selection Account")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("OrderSummary/PurchaseOrderNumber"),"999999")) {
			KeywordUtil.logInfo("Cannot send Purchase Order Number text")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Payment/BillingAddress"),1)) {
			KeywordUtil.logInfo("Cannot see the Billing Address")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Payment/BillingAddressFirst"))) {
			KeywordUtil.logInfo("Cannot click the Billing Address First")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Payment/PaymentType"),1)) {
			KeywordUtil.logInfo("Cannot see the Payment Type")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Payment/PaymentTypeSelectionAccount"),1)) {
			KeywordUtil.logInfo("Cannot see the Payment Type Selection Account")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Payment/PurchaseOrderNumber"),1)) {
			KeywordUtil.logInfo("Cannot see the Purchase Order Number")
			return false
		}

		/*	String Values = ((new commerceCloud.utils()).GetAttributeText(findTestObject("Payment/PaymentTypeSelectionCARD"),"disabled"))
		 if (!(Values.equalsIgnoreCase("true"))) {
		 KeywordUtil.logInfo("Can not see the Disable attribute under Selection CARD")
		 return false
		 }
		 */
		if (!(new commerceCloud.utils()).click(findTestObject("OrderSummary/ContinueToReview"))) {
			KeywordUtil.logInfo("Cannot click Continue To Review")
			return false
		}


		KeywordUtil.logInfo("Validated the Shipping, Shipment and Payment Page UI")
		return true
	}


	/**
	 * OrderPageValidation()
	 * @return true if order page is validated all the object, otherwise false
	 */

	@Keyword
	def OrderPageValidation() {

		KeywordUtil.logInfo("Validate the Order Review Page, Order Review Shipments and Order Review Total Page UI")

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Review/OrderSummaryOrderTotal"),60)) {
			KeywordUtil.logInfo("Cannot see the Order Summary Order Total")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Review/OrderSummaryShipping"),1)) {
			KeywordUtil.logInfo("Cannot see the Order Summary Shipping")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Review/OrderSummarySubtotal"),1)) {
			KeywordUtil.logInfo("Cannot see the Order Summary Subtotal ")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Review/OrderSummaryTax"),1)) {
			KeywordUtil.logInfo("Cannot see the Order Summary Tax")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Review/OrderTotal"),1)) {
			KeywordUtil.logInfo("Cannot see the Order Total")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Review/ReviewMfgPart"),1)) {
			KeywordUtil.logInfo("Cannot see the Review Mfg Part")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Review/ReviewShipmentItemImage"),1)) {
			KeywordUtil.logInfo("Cannot see the Review Shipment Item Image")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Review/ReviewShipmentItemName"),1)) {
			KeywordUtil.logInfo("Cannot see the Review Shipment Item Name")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Review/ReviewShipmentItemQuantity"),1)) {
			KeywordUtil.logInfo("Cannot see the Review Shipment Item Quantity")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Review/ReviewShipmentItemTotalSale"),1)) {
			KeywordUtil.logInfo("Cannot see the Review Shipment Item Total Sale")
			return false
		}

		/*if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Review/ReviewShipmentManufacturerName"),1)) {
		 KeywordUtil.logInfo("Cannot see the Review Shipment Manufacturer Name")
		 return false
		 }
		 if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Review/ReviewShipmentStock"),1)) {
		 KeywordUtil.logInfo("Cannot see the Review Shipment Stock")
		 return false
		 }
		 */

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Review/ReviewTesscoSku"),1)) {
			KeywordUtil.logInfo("Cannot see the Review Tessco Sku")
			return false
		}

		/*if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Review/ReviewUPC"),1)) {
		 KeywordUtil.logInfo("Cannot see the Review UPC")
		 return false
		 }*/

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Review/Shipping"),1)) {
			KeywordUtil.logInfo("Cannot see the Shipping")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Review/Subtotal"),1)) {
			KeywordUtil.logInfo("Cannot see the Sub total")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Review/Tax"),1)) {
			KeywordUtil.logInfo("Cannot see the Tax")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Cart/ReturnToCart"))) {
			KeywordUtil.logInfo("Cannot click Return To Cart")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("Cart/CreateQuote"),60)) {
			KeywordUtil.logInfo("Cannot see the Create Quote")
			return false
		}

		KeywordUtil.logInfo("Validated the Order Review Page, Order Review Shipments and Order Review Total Page UI")
		return true
	}

	/**
	 * navigateOrderHistory() 
	 * @return true if navigate Order History page, otherwise false
	 */

	@Keyword
	def navigateOrderHistory() {

		KeywordUtil.logInfo("Validate the Order History Page")

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("SavedCartsQuotes/Signed In As"),60)) {
			KeywordUtil.logInfo("Cannot see the Signed In As Button")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("SavedCartsQuotes/Signed In As"))) {
			KeywordUtil.logInfo("Cannot click Signed In As Button")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderHistory/OrderHistory"),60)) {
			KeywordUtil.logInfo("Cannot see the Order History Link")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("OrderHistory/OrderHistory"))) {
			KeywordUtil.logInfo("Cannot click Order History Link")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderHistory/OrderHistoryDate"),60)) {
			KeywordUtil.logInfo("Cannot see the Order History Date Link")
			return false
		}

		KeywordUtil.logInfo("Validated the Order History Page")
		return true
	}

	/**
	 * SortOrderHistory()
	 * @return true if sorting for following columns Creation Date, Order, Placed By, PO, Status and Total, otherwise false
	 */

	@Keyword
	def SortOrderHistory() {

		KeywordUtil.logInfo("Validate the Creation Date, Order, Placed By, PO, Status and Total sorting in Order History Page")

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("OrderHistory/OrderHistoryOrder"),60)) {
			KeywordUtil.logInfo("Cannot see the Order column under Order History ")
			return false
		}

		//Date sorting

		/*if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("OrderHistory/OrderHistoryDate"),60)) {
		 KeywordUtil.logInfo("Cannot see the Date column under Order History")
		 return false
		 }
		 if (!(new commerceCloud.utils()).sorting(findTestObject("OrderHistory/OrderHistoryDateList"), 'Date', 'desc')) {
		 KeywordUtil.logInfo("Cannot sort the Date column under Order History")
		 return false
		 }
		 if (!(new commerceCloud.utils()).click(findTestObject("OrderHistory/OrderHistoryDate"))) {
		 KeywordUtil.logInfo("Cannot click Date column under Order History")
		 return false
		 }
		 if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("OrderHistory/OrderHistoryDate"),60)) {
		 KeywordUtil.logInfo("Cannot see the Date column under Order History")
		 return false
		 }
		 if (!(new commerceCloud.utils()).sorting(findTestObject("OrderHistory/OrderHistoryDateList"), 'Date', 'asc')) {
		 KeywordUtil.logInfo("Cannot sort the Date column under Order History")
		 return false
		 }
		 */
		//Order sorting
		if (!(new commerceCloud.utils()).click(findTestObject("OrderHistory/OrderHistoryOrder"))) {
			KeywordUtil.logInfo("Cannot click Order column under Order History")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsEnabled(findTestObject("OrderHistory/OrderHistoryOrder"),60)) {
			KeywordUtil.logInfo("Cannot see the Order column under Order History")
			return false
		}

		if (!(new commerceCloud.utils()).sorting(findTestObject("OrderHistory/OrderHistoryOrderList"), 'Number', 'desc')) {
			KeywordUtil.logInfo("Cannot sort the Order column under Order History")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("OrderHistory/OrderHistoryOrder"))) {
			KeywordUtil.logInfo("Cannot click Order column under Order History")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsEnabled(findTestObject("OrderHistory/OrderHistoryOrder"),60)) {
			KeywordUtil.logInfo("Cannot see the Order column under Order History")
			return false
		}


		if (!(new commerceCloud.utils()).sorting(findTestObject("OrderHistory/OrderHistoryOrderList"), 'Number', 'asc')) {
			KeywordUtil.logInfo("Cannot sort the Order column under Order History")
			return false
		}

		/*//Placed By sorting
		 if (!(new commerceCloud.utils()).click(findTestObject("OrderHistory/OrderHistoryPlacedBy"))) {
		 KeywordUtil.logInfo("Cannot click Placed By column under Order History")
		 return false
		 }
		 if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("OrderHistory/OrderHistoryPlacedBy"),60)) {
		 KeywordUtil.logInfo("Cannot see the Placed By column under Order History")
		 return false
		 }
		 if (!(new commerceCloud.utils()).sorting(findTestObject("OrderHistory/OrderHistoryPlacedByList"), 'Name', 'desc')) {
		 KeywordUtil.logInfo("Cannot sort the Placed By column under Order History")
		 return false
		 }
		 if (!(new commerceCloud.utils()).click(findTestObject("OrderHistory/OrderHistoryPlacedBy"))) {
		 KeywordUtil.logInfo("Cannot click Placed By column under Order History")
		 return false
		 }
		 if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("OrderHistory/OrderHistoryPlacedBy"),60)) {
		 KeywordUtil.logInfo("Cannot see the Placed By column under Order History")
		 return false
		 }
		 if (!(new commerceCloud.utils()).sorting(findTestObject("OrderHistory/OrderHistoryPlacedByList"), 'Name', 'asc')) {
		 KeywordUtil.logInfo("Cannot sort the Placed By column under Order History")
		 return false
		 }
		 */


		//Placed By sorting
		if (!(new commerceCloud.utils()).click(findTestObject("OrderHistory/OrderHistoryPlacedBy"))) {
			KeywordUtil.logInfo("Cannot click Placed By column under Order History")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsEnabled(findTestObject("OrderHistory/OrderHistoryPlacedBy"),60)) {
			KeywordUtil.logInfo("Cannot see the Placed By column under Order History")
			return false
		}

		if (!(new commerceCloud.utils()).sorting(findTestObject("OrderHistory/OrderHistoryPlacedByList"), 'Name', 'desc')) {
			KeywordUtil.logInfo("Cannot sort the Placed By column under Order History")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("OrderHistory/OrderHistoryPlacedBy"))) {
			KeywordUtil.logInfo("Cannot click Placed By column under Order History")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsEnabled(findTestObject("OrderHistory/OrderHistoryPlacedBy"),60)) {
			KeywordUtil.logInfo("Cannot see the Placed By column under Order History")
			return false
		}

		if (!(new commerceCloud.utils()).sorting(findTestObject("OrderHistory/OrderHistoryPlacedByList"), 'Name', 'asc')) {
			KeywordUtil.logInfo("Cannot sort the Placed By column under Order History")
			return false
		}


		//PO sorting
		if (!(new commerceCloud.utils()).click(findTestObject("OrderHistory/OrderHistoryPO"))) {
			KeywordUtil.logInfo("Cannot click PO column under Order History")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsEnabled(findTestObject("OrderHistory/OrderHistoryPO"),60)) {
			KeywordUtil.logInfo("Cannot see the PO column under Order History")
			return false
		}

		/*if (!(new commerceCloud.utils()).sorting(findTestObject("OrderHistory/OrderHistoryPOList"), 'Name', 'desc')) {
		 KeywordUtil.logInfo("Cannot sort the PO column under Order History")
		 return false
		 }*/

		if (!(new commerceCloud.utils()).click(findTestObject("OrderHistory/OrderHistoryPO"))) {
			KeywordUtil.logInfo("Cannot click PO column under Order History")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsEnabled(findTestObject("OrderHistory/OrderHistoryPO"),60)) {
			KeywordUtil.logInfo("Cannot see the PO column under Order History")
			return false
		}

		if (!(new commerceCloud.utils()).sorting(findTestObject("OrderHistory/OrderHistoryPOList"), 'Name', 'asc')) {
			KeywordUtil.logInfo("Cannot sort the PO column under Order History")
			return false
		}


		//Status sorting
		if (!(new commerceCloud.utils()).click(findTestObject("OrderHistory/OrderHistoryStatus"))) {
			KeywordUtil.logInfo("Cannot click Status column under Order History")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsEnabled(findTestObject("OrderHistory/OrderHistoryStatus"),60)) {
			KeywordUtil.logInfo("Cannot see the Status column under Order History")
			return false
		}

		if (!(new commerceCloud.utils()).sorting(findTestObject("OrderHistory/OrderHistoryStatusList"), 'Name', 'desc')) {
			KeywordUtil.logInfo("Cannot sort the Status column under Order History")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("OrderHistory/OrderHistoryStatus"))) {
			KeywordUtil.logInfo("Cannot click Status column under Order History")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsEnabled(findTestObject("OrderHistory/OrderHistoryStatus"),60)) {
			KeywordUtil.logInfo("Cannot see the Status column under Order History")
			return false
		}

		if (!(new commerceCloud.utils()).sorting(findTestObject("OrderHistory/OrderHistoryStatusList"), 'Name', 'asc')) {
			KeywordUtil.logInfo("Cannot sort the Status column under Order History")
			return false
		}


		//Total sorting
		if (!(new commerceCloud.utils()).click(findTestObject("OrderHistory/OrderHistoryTotal"))) {
			KeywordUtil.logInfo("Cannot click Total column under Order History")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsEnabled(findTestObject("OrderHistory/OrderHistoryTotal"),60)) {
			KeywordUtil.logInfo("Cannot see the Total column under Order History")
			return false
		}

		if (!(new commerceCloud.utils()).sorting(findTestObject("OrderHistory/OrderHistoryTotalList"), 'Number', 'desc')) {
			KeywordUtil.logInfo("Cannot sort the Total column under Order History")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("OrderHistory/OrderHistoryTotal"))) {
			KeywordUtil.logInfo("Cannot click Total column under Order History")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsEnabled(findTestObject("OrderHistory/OrderHistoryTotal"),60)) {
			KeywordUtil.logInfo("Cannot see the Total column under Order History")
			return false
		}

		if (!(new commerceCloud.utils()).sorting(findTestObject("OrderHistory/OrderHistoryTotalList"), 'Number', 'asc')) {
			KeywordUtil.logInfo("Cannot sort the Total column under Order History")
			return false
		}

		KeywordUtil.logInfo("Validated the Creation Date, Order, Placed By, PO, Status and Total sorting in Order History Page")
		return true
	}


	/**
	 * OrderHistoryDetails(String strOrderNo)
	 * @param strOrderNo - order no 
	 * @return true if navigate Order History Details page, otherwise false
	 */

	@Keyword
	def OrderHistoryDetails(String strOrderNo) {

		KeywordUtil.logInfo("Validate the Order History Page")


		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderHistory/OrderHistoryDate"),60)) {
			KeywordUtil.logInfo("Cannot see the Order History Date")
			return false
		}

		List<WebElement> lstTotal = (new commerceCloud.utils()).ListOfWebElement(findTestObject("OrderHistory/OrderHistoryTotalList"))
		int Total = lstTotal.size()

		if (!(Total == 15)) {
			KeywordUtil.logInfo("Cannot see the Order History Total List size")
			return false
		}

		if (!(new commerceCloud.utils()).setText(findTestObject("OrderHistory/OrderHistorySearchInput"),strOrderNo)) {
			KeywordUtil.logInfo("Cannot enter Search text under Order History")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("OrderHistory/OrderHistorySearchButton"))) {
			KeywordUtil.logInfo("Cannot click Search under Order History")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("OrderHistory/OrderHistoryDate"),60)) {
			KeywordUtil.logInfo("Cannot see the Order History Date Link")
			return false
		}

		TestObject to = new TestObject("Order No")
		String xpath = "//a[contains(text(),'" + strOrderNo + "')]"
		TestObjectProperty prop = new TestObjectProperty("xpath", ConditionType.EQUALS, xpath)
		to.addProperty(prop)

		if (!(new commerceCloud.utils()).click(to)) {
			KeywordUtil.logInfo("Cannot click Order No")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderHistory/DetailBillingAddress"),60)) {
			KeywordUtil.logInfo("Cannot see the Detail Billing Address")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderHistory/DetailDate"),1)) {
			KeywordUtil.logInfo("Cannot see the Detail Date")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderHistory/DetailOrderNumber"),1)) {
			KeywordUtil.logInfo("Cannot see the Detail Order Number")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderHistory/DetailOrderNumberHeader"),1)) {
			KeywordUtil.logInfo("Cannot see the Detail Order Number Header")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderHistory/DetailPayment"),1)) {
			KeywordUtil.logInfo("Cannot see the Detail Payment")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderHistory/DetailPlacedBy"),1)) {
			KeywordUtil.logInfo("Cannot see the Detail Placed By")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderHistory/DetailPONumber"),1)) {
			KeywordUtil.logInfo("Cannot see the Detail PO Number")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderHistory/DetailShippingAddress"),1)) {
			KeywordUtil.logInfo("Cannot see the Detail shipping Address")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderHistory/DetailStatus"),1)) {
			KeywordUtil.logInfo("Cannot see the Detail Status")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderHistory/OrderShipping"),1)) {
			KeywordUtil.logInfo("Cannot see the Order Shipping")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderHistory/OrderSubtotal"),1)) {
			KeywordUtil.logInfo("Cannot see the Order Subtotal")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderHistory/OrderTax"),1)) {
			KeywordUtil.logInfo("Cannot see the Order Tax")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderHistory/OrderTotal"),1)) {
			KeywordUtil.logInfo("Cannot see the Order Total")
			return false
		}

		if (!(new commerceCloud.utils()).scrollToElement(findTestObject("OrderHistory/PurchaseAgain"))) {
			KeywordUtil.logInfo("Cannot scroll to Purchase Again")
			return false
		}



		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderHistory/ShipmentItemName"),1)) {
			KeywordUtil.logInfo("Cannot see the Shipment Item Name")
			return false
		}
		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderHistory/ShipmentItemQuantity"),1)) {
			KeywordUtil.logInfo("Cannot see the Shipment Item Quantity")
			return false
		}

		/*if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderHistory/ShipmentManufacturerName"),1)) {
		 KeywordUtil.logInfo("Cannot see the Shipment Manufacturer Name")
		 return false
		 }*/

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderHistory/ShipmentMfgPart"),1)) {
			KeywordUtil.logInfo("Cannot see the Shipment Mfg Part")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderHistory/ShipmentTesscoSku"),1)) {
			KeywordUtil.logInfo("Cannot see the Shipment Tessco Sku")
			return false
		}
		/*
		 if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderHistory/ShipmentUPC"),1)) {
		 KeywordUtil.logInfo("Cannot see the Shipment UPC")
		 return false
		 }*/

		List<WebElement> lst = (new commerceCloud.utils()).ListOfWebElement(findTestObject("OrderHistory/QuantityOrderHistorySize"))
		int QualityOH = lst.size()

		if (!(new commerceCloud.utils()).click(findTestObject("OrderHistory/PurchaseAgain"))) {
			KeywordUtil.logInfo("Cannot click Purchase Again")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Cart/ClearCart"),60)) {
			KeywordUtil.logInfo("Cannot see the Clear Cart")
			return false
		}

		List<WebElement> lst2 = (new commerceCloud.utils()).ListOfWebElement(findTestObject("OrderHistory/QuantityCartSize"))
		int QualityCart = lst2.size()

		if (!(QualityOH == QualityCart)) {
			KeywordUtil.logInfo("Cannot see the product Quantity size")
			return false
		}


		KeywordUtil.logInfo("Validated the Order History Page")
		return true
	}

	/**
	 * CreditCartOrderHistory(String strOrderNo)
	 * @param strOrderNo - order no
	 * @return true if navigate Order History Details page, otherwise false
	 */

	@Keyword
	def CreditCartOrderHistory(String strOrderNo) {

		KeywordUtil.logInfo("Validate the Credit Cart Order History")


		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderHistory/OrderHistoryDate"),60)) {
			KeywordUtil.logInfo("Cannot see the Order History Date")
			return false
		}

		if (!(new commerceCloud.utils()).setText(findTestObject("OrderHistory/OrderHistorySearchInput"),strOrderNo)) {
			KeywordUtil.logInfo("Cannot enter Search text under Order History")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("OrderHistory/OrderHistorySearchButton"))) {
			KeywordUtil.logInfo("Cannot click Search under Order History")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("OrderHistory/OrderHistoryDate"),60)) {
			KeywordUtil.logInfo("Cannot see the Order History Date Link")
			return false
		}

		TestObject to = new TestObject("Order No")
		String xpath = "//a[contains(text(),'" + strOrderNo + "')]"
		TestObjectProperty prop = new TestObjectProperty("xpath", ConditionType.EQUALS, xpath)
		to.addProperty(prop)

		if (!(new commerceCloud.utils()).click(to)) {
			KeywordUtil.logInfo("Cannot click Order No")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderHistory/DetailBillingAddress"),60)) {
			KeywordUtil.logInfo("Cannot see the Detail Billing Address")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderHistory/CreditCartName"),1)) {
			KeywordUtil.logInfo("Cannot see the Credit Cart Name")
			return false
		}

		KeywordUtil.logInfo("Validated the Credit Cart Order History Page")
		return true
	}

	/**
	 * GSAOrderHistoryDetails(String strOrderNo)
	 * @param strOrderNo - order no
	 * @return true if navigate Order History Details page, otherwise false
	 */

	@Keyword
	def GSAOrderHistoryDetails(String strOrderNo) {

		KeywordUtil.logInfo("Validate the Order History Page")


		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderHistory/OrderHistoryDate"),60)) {
			KeywordUtil.logInfo("Cannot see the Order History Date")
			return false
		}

		List<WebElement> lstTotal = (new commerceCloud.utils()).ListOfWebElement(findTestObject("OrderHistory/OrderHistoryTotalList"))
		int Total = lstTotal.size()

		if (!(Total == 15)) {
			KeywordUtil.logInfo("Cannot see the Order History Total List size")
			return false
		}

		if (!(new commerceCloud.utils()).setText(findTestObject("OrderHistory/OrderHistorySearchInput"),strOrderNo)) {
			KeywordUtil.logInfo("Cannot enter Search text under Order History")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("OrderHistory/OrderHistorySearchButton"))) {
			KeywordUtil.logInfo("Cannot click Search under Order History")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("OrderHistory/OrderHistoryDate"),60)) {
			KeywordUtil.logInfo("Cannot see the Order History Date Link")
			return false
		}

		TestObject to = new TestObject("Order No")
		String xpath = "//a[contains(text(),'" + strOrderNo + "')]"
		TestObjectProperty prop = new TestObjectProperty("xpath", ConditionType.EQUALS, xpath)
		to.addProperty(prop)

		if (!(new commerceCloud.utils()).click(to)) {
			KeywordUtil.logInfo("Cannot click Order No")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderHistory/DetailBillingAddress"),60)) {
			KeywordUtil.logInfo("Cannot see the Detail Billing Address")
			return false
		}

		List<WebElement> lst = (new commerceCloud.utils()).ListOfWebElement(findTestObject("OrderHistory/QuantityOrderHistorySize"))
		int QualityOH = lst.size()

		if (!(new commerceCloud.utils()).click(findTestObject("OrderHistory/PurchaseAgain"))) {
			KeywordUtil.logInfo("Cannot click Purchase Again")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("Cart/ClearCart"),60)) {
			KeywordUtil.logInfo("Cannot see the Clear Cart")
			return false
		}

		List<WebElement> lst2 = (new commerceCloud.utils()).ListOfWebElement(findTestObject("OrderHistory/QuantityCartSize"))
		int QualityCart = lst2.size()

		if (!(QualityOH == QualityCart)) {
			KeywordUtil.logInfo("Cannot see the product Quantity size")
			return false
		}


		KeywordUtil.logInfo("Validated the Order History Page")
		return true
	}

	/**
	 * DeliveryOptionValidation()
	 * @return true if Complete the ordering process, otherwise false
	 */

	@Keyword
	def DeliveryOptionValidation() {

		KeywordUtil.logInfo("Validate the Delivery Option")

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderSummary/Checkout"),60)) {
			KeywordUtil.logInfo("Cannot see the Checkout")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("OrderSummary/Checkout"))) {
			KeywordUtil.logInfo("Cannot click Checkout")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderSummary/ContinueToPayment"),60)) {
			KeywordUtil.logInfo("Cannot see the Continue To Payment")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Payment/ShipAllItemsAsSingleShipment"))) {
			KeywordUtil.logInfo("Cannot click Ship All Items As Single Shipment")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("OrderSummary/ContinueToPayment"))) {
			KeywordUtil.logInfo("Cannot click Continue To Payment")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderSummary/ContinueToReview"),60)) {
			KeywordUtil.logInfo("Cannot see the Continue To Review")
			return false
		}

		List<WebElement> lst = (new commerceCloud.utils()).ListOfWebElement(findTestObject("Shipping/OrderSummaryInsideDelivery"))
		int iInsideDelivery = lst.size()
		println(iInsideDelivery)

		List<WebElement> lst1 = (new commerceCloud.utils()).ListOfWebElement(findTestObject("Shipping/OrderSummaryLiftGate"))
		int iLiftGate = lst1.size()
		println(iLiftGate)

		if (!((iInsideDelivery == 0) && (iLiftGate == 0))) {
			KeywordUtil.logInfo("Can see the Inside Delivery and Lift Gate price under Payment Page")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Shipping/ShipmentEdit"))) {
			KeywordUtil.logInfo("Cannot click Shipment Edit")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderSummary/ContinueToPayment"),60)) {
			KeywordUtil.logInfo("Cannot see the Continue To Payment")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Payment/LiftGate"))) {
			KeywordUtil.logInfo("Cannot click Lift Gate")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Payment/InsideDelivery"))) {
			KeywordUtil.logInfo("Cannot click Lift Gate")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("OrderSummary/ContinueToPayment"))) {
			KeywordUtil.logInfo("Cannot click Continue To Payment")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderSummary/ContinueToReview"),60)) {
			KeywordUtil.logInfo("Cannot see the Continue To Review")
			return false
		}

		List<WebElement> lst3 = (new commerceCloud.utils()).ListOfWebElement(findTestObject("Shipping/OrderSummaryInsideDelivery"))
		int iInsideDelivery3 = lst3.size()
		println(iInsideDelivery3)

		List<WebElement> lst4 = (new commerceCloud.utils()).ListOfWebElement(findTestObject("Shipping/OrderSummaryLiftGate"))
		int iLiftGate4 = lst4.size()
		println(iLiftGate4)

		if (!((iInsideDelivery3 == 1) && (iLiftGate4 == 1))) {
			KeywordUtil.logInfo("Can see the Inside Delivery and Lift Gate price  under Payment Page")
			return false
		}


		if (!(new commerceCloud.utils()).click(findTestObject("Payment/Payment_Type_Selection_ACCOUNT"))) {
			KeywordUtil.logInfo("Cannot click Account Payment")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("OrderSummary/PurchaseOrderNumber"),"999999")) {
			KeywordUtil.logInfo("Cannot send Purchase Order Number text")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("OrderSummary/ContinueToReview"))) {
			KeywordUtil.logInfo("Cannot click Continue To Review")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderSummary/PlaceOrder"),60)) {
			KeywordUtil.logInfo("Cannot see the Place Order")
			return false
		}

		List<WebElement> lst5 = (new commerceCloud.utils()).ListOfWebElement(findTestObject("Review/OrderSummaryInsideDelivery"))
		int iInsideDelivery5 = lst5.size()
		println(iInsideDelivery5)

		List<WebElement> lst6 = (new commerceCloud.utils()).ListOfWebElement(findTestObject("Review/OrderSummaryLiftGate"))
		int iLiftGate6 = lst6.size()
		println(iLiftGate6)

		if (!((iInsideDelivery5 == 1) && (iLiftGate6 == 1))) {
			KeywordUtil.logInfo("Can see the Inside Delivery and Lift Gate price under Review Page")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Review/ReturnToCart"))) {
			KeywordUtil.logInfo("Cannot click Return To Cart")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderSummary/Checkout"),60)) {
			KeywordUtil.logInfo("Cannot see the Checkout")
			return false
		}

		List<WebElement> lst7 = (new commerceCloud.utils()).ListOfWebElement(findTestObject("Shipping/OrderSummaryInsideDelivery"))
		int iInsideDelivery7 = lst7.size()
		println(iInsideDelivery7)

		List<WebElement> lst8 = (new commerceCloud.utils()).ListOfWebElement(findTestObject("Shipping/OrderSummaryLiftGate"))
		int iLiftGate8 = lst8.size()
		println(iLiftGate8)

		if (!(new commerceCloud.utils()).click(findTestObject("OrderSummary/Checkout"))) {
			KeywordUtil.logInfo("Cannot click Checkout")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderSummary/ContinueToPayment"),60)) {
			KeywordUtil.logInfo("Cannot see the Continue To Payment")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Payment/ShipAllItemsAsSingleShipment"))) {
			KeywordUtil.logInfo("Cannot click Ship All Items As Single Shipment")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Payment/LiftGate"))) {
			KeywordUtil.logInfo("Cannot click Lift Gate")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Payment/InsideDelivery"))) {
			KeywordUtil.logInfo("Cannot click Lift Gate")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("Review/ReturnToCart"))) {
			KeywordUtil.logInfo("Cannot click Return To Cart")
			return false
		}

		if (!(new commerceCloud.utils()).verifyPresent(findTestObject("OrderSummary/Checkout"),60)) {
			KeywordUtil.logInfo("Cannot see the Checkout")
			return false
		}

		KeywordUtil.logInfo("Validated the order is Completed")

		return true
	}


	/**
	 * BillingNameChangeUserInfoValidation()
	 * @return true if Billing Name Change User Info Field level Validation is implemented, otherwise false
	 */

	@Keyword
	def BillingNameChangeUserInfoValidation() {

		KeywordUtil.logInfo("Validate the Billing Name Change User Info Field level Validation is implemented")

		if (!(new commerceCloud.Cart()).navigateBillingNameChange()) {
			KeywordUtil.logInfo("Cannot navigate to Billing Name Change Page")
			return false
		}

		/*if (!(new commerceCloud.utils()).click(findTestObject("ChangeBillingName/ChangeBillingName"))) {
			KeywordUtil.logInfo("Cannot click Change Billing Name link")
			return false
		}*/

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ChangeBillingName/ChangeBillingNameTitle"),60)) {
			KeywordUtil.logInfo("Cannot see the Change Billing Name title")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ChangeBillingName/TesscoAccountNumber"),1)) {
			KeywordUtil.logInfo("Cannot see the Tessco Account Number textbox")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("ChangeBillingName/FirstName"),"11")) {
			KeywordUtil.logInfo("Cannot send First Name text")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("ChangeBillingName/LastName"),"11")) {
			KeywordUtil.logInfo("Cannot send Last Name text")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("ChangeBillingName/EmailAddress"),"senthil")) {
			KeywordUtil.logInfo("Cannot send Email Address text")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("ChangeBillingName/Phone"),"aa")) {
			KeywordUtil.logInfo("Cannot send Phone no text")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("ChangeBillingName/Title"),"11")) {
			KeywordUtil.logInfo("Cannot send Title text")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("ChangeBillingName/TermsCheck"))) {
			KeywordUtil.logInfo("Cannot click Terms Check checkbox")
			return false
		}


		if (!(new commerceCloud.utils()).click(findTestObject("ChangeBillingName/Submit"))) {
			KeywordUtil.logInfo("Cannot click Submit button")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ChangeBillingName/FirstNameMustBeginWithAlphabets"),30)) {
			KeywordUtil.logInfo("Cannot see the First Name Must Begin With Alphabets text")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ChangeBillingName/LastNameMustBeginWithAlphabets"),1)) {
			KeywordUtil.logInfo("Cannot see the Last Name Must Begin With Alphabets text")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ChangeBillingName/PleaseEnterValidEmail"),1)) {
			KeywordUtil.logInfo("Cannot see the Please Enter Valid Email text")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ChangeBillingName/PleaseEnterValidPhone"),1)) {
			KeywordUtil.logInfo("Cannot see the Please Enter Valid Phone text")
			return false
		}

		/*
		 if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ChangeBillingName/FirstNameMustBeginWithAlphabets"),1)) {
		 KeywordUtil.logInfo("Cannot see the First Name Must Begin With Alphabets text")
		 return false
		 }*/

		KeywordUtil.logInfo("Validated the Billing Name Change User Info Field level Validation is implemented")
		return true
	}


	/**
	 * BillingNameChangeCompanyInfoValidation()
	 * @return true if Billing Name Change Company Info Field level Validation is implemented, otherwise false
	 */

	@Keyword
	def BillingNameChangeCompanyInfoValidation() {

		KeywordUtil.logInfo("Validate the Billing Name Change form Company Info Field level Validation is implemented")

		if (!(new commerceCloud.Cart()).navigateBillingNameChange()) {
			KeywordUtil.logInfo("Cannot navigate to Billing Name Change Page")
			return false
		}

		/*if (!(new commerceCloud.utils()).click(findTestObject("ChangeBillingName/ChangeBillingName"))) {
			KeywordUtil.logInfo("Cannot click Change Billing Name link")
			return false
		}*/

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ChangeBillingName/ChangeBillingNameTitle"),60)) {
			KeywordUtil.logInfo("Cannot see the Change Billing Name title")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ChangeBillingName/TesscoAccountNumber"),1)) {
			KeywordUtil.logInfo("Cannot see the Tessco Account Number textbox")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("ChangeBillingName/FirstName"),"Jermaine")) {
			KeywordUtil.logInfo("Cannot send First Name text")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("ChangeBillingName/LastName"),"Wilkins")) {
			KeywordUtil.logInfo("Cannot send Last Name text")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("ChangeBillingName/EmailAddress"),GlobalVariable.GlobalEmailID)) {
			KeywordUtil.logInfo("Cannot send Email Address text")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("ChangeBillingName/Phone"),"444-444-4444")) {
			KeywordUtil.logInfo("Cannot send Phone no text")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("ChangeBillingName/Title"),"Mr")) {
			KeywordUtil.logInfo("Cannot send Title text")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("ChangeBillingName/TermsCheck"))) {
			KeywordUtil.logInfo("Cannot click Terms Check checkbox")
			return false
		}


		if (!(new commerceCloud.utils()).click(findTestObject("ChangeBillingName/Submit"))) {
			KeywordUtil.logInfo("Cannot click Submit button")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ChangeBillingName/CompanyLegalEntityNameError"),30)) {
			KeywordUtil.logInfo("Cannot see the Company Legal Entity Name Error textbox")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ChangeBillingName/NewCompanyLegalEntityNameError"),1)) {
			KeywordUtil.logInfo("Cannot see the New Company Legal Entity Name Error textbox")
			return false
		}

		/*if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ChangeBillingName/NewCompanyLegalEntityNameError"),1)) {
		 KeywordUtil.logInfo("Cannot see the New Company Legal Entity Name Error textbox")
		 return false
		 }*/

		KeywordUtil.logInfo("Validated the Billing Name Change Company Info Field level Validation is implemented")
		return true
	}


	/**
	 * BillingNameChange(String strReasonforNameChange)
	 * @return true if Billing Name Change Validation is implemented, otherwise false
	 */

	@Keyword
	def BillingNameChange(String strReasonforNameChange) {

		KeywordUtil.logInfo("Validate the Billing Name Change form flow")


		if (!(new commerceCloud.Cart()).navigateBillingNameChange()) {
			KeywordUtil.logInfo("Cannot navigate to Billing Name Change Page")
			return false
		}
		/*
		 if (!(new commerceCloud.utils()).click(findTestObject("ChangeBillingName/ChangeBillingName"))) {
		 KeywordUtil.logInfo("Cannot click Change Billing Name link")
		 return false
		 }
		 if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ChangeBillingName/ChangeBillingNameTitle"),60)) {
		 KeywordUtil.logInfo("Cannot see the Change Billing Name title")
		 return false
		 }*/

		if (!(new commerceCloud.utils()).click(findTestObject("Login/Signed In As"))) {
			KeywordUtil.logInfo("Cannot click Signed In As")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ChangeBillingName/UserAccountNumber"),30)) {
			KeywordUtil.logInfo("Cannot see the User Account Number")
			return false
		}

		String strAccountNoActual = (new commerceCloud.utils()).sReturnText(findTestObject("ChangeBillingName/UserAccountNumber"))

		if (!(new commerceCloud.utils()).click(findTestObject("Login/Signed In As"))) {
			KeywordUtil.logInfo("Cannot click Signed In As")
			return false
		}

		String strAccountNoExpected = (new commerceCloud.utils()).GetAttributeText(findTestObject("ChangeBillingName/TesscoAccountNumber"),"placeholder")
		if(!(strAccountNoActual.contains(strAccountNoExpected))){
			KeywordUtil.logInfo("Cannot see dropdown TESSCO ACCOUNT NUMBER and TESSCO ACCOUNT NUMBER values same")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("ChangeBillingName/CompanyLegalEntityName"),"TESSCO Technologies")) {
			KeywordUtil.logInfo("Cannot send Company Legal Entity Name text")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("ChangeBillingName/Dba"),"Dba")) {
			KeywordUtil.logInfo("Cannot send Dba text")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("ChangeBillingName/NewCompanyLegalEntityName"),"TESSCO Technologies Test")) {
			KeywordUtil.logInfo("Cannot send New Company Legal Entity Name text")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("ChangeBillingName/NewDba"),"NewDba")) {
			KeywordUtil.logInfo("Cannot send New Dba text")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("ChangeBillingName/ReasonforNameChange"))) {
			KeywordUtil.logInfo("Cannot click Reason for Name Change")
			return false
		}

		TestObject to = new TestObject("REASON FOR NAME CHANGE")
		String xpath = "//a[normalize-space()='"+ strReasonforNameChange +"']"
		TestObjectProperty prop = new TestObjectProperty("xpath", ConditionType.EQUALS, xpath)
		to.addProperty(prop)

		if (!(new commerceCloud.utils()).click(to)) {
			KeywordUtil.logInfo("Cannot click values: "+ strReasonforNameChange )
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("ChangeBillingName/FirstName"),"Jermaine")) {
			KeywordUtil.logInfo("Cannot send First Name text")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("ChangeBillingName/LastName"),"Wilkins")) {
			KeywordUtil.logInfo("Cannot send Last Name text")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("ChangeBillingName/EmailAddress"),GlobalVariable.GlobalEmailID)) {
			KeywordUtil.logInfo("Cannot send Email Address text")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("ChangeBillingName/Phone"),"444-444-4444")) {
			KeywordUtil.logInfo("Cannot send Phone no text")
			return false
		}

		if (!(new commerceCloud.utils()).sendKeys(findTestObject("ChangeBillingName/Title"),"Mr")) {
			KeywordUtil.logInfo("Cannot send Title text")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("ChangeBillingName/TermsCheck"))) {
			KeywordUtil.logInfo("Cannot click Terms Check checkbox")
			return false
		}


		if (!(new commerceCloud.utils()).click(findTestObject("ChangeBillingName/Submit"))) {
			KeywordUtil.logInfo("Cannot click Submit button")
			return false
		}

		if (!(new commerceCloud.utils()).verifyElementIsDisplayed(findTestObject("ChangeBillingName/RequestSubmitted"),60)) {
			KeywordUtil.logInfo("Cannot see the Request Submitted")
			return false
		}

		if (!(new commerceCloud.utils()).click(findTestObject("ChangeBillingName/BackTesscoCom"))) {
			KeywordUtil.logInfo("Cannot click Back Tessco Com button")
			return false
		}


		KeywordUtil.logInfo("Validated the Billing Name Change form flow")
		return true
	}

}