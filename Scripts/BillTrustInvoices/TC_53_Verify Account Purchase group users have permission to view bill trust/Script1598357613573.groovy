import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import internal.GlobalVariable as GlobalVariable

// launch browser and log in
if (!(new commerceCloud.utils().newSessionWithUserName(GlobalVariable.GlobalUserNameAccountBuyer, GlobalVariable.GlobalRolePassword))) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not launch or login the application')
}

// switch Account
if (!(new commerceCloud.ProductSearch().viewBillTrustInvoices())) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not Verify users have permission to view bill trust invoices')
}

// logout the application
if (!(new commerceCloud.user().logout())) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not logout the commerceCloud')
}