import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

// launch browser and log in
if (!(new commerceCloud.utils().newSession())) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not launch or login the application')
}

// The ability to view more products in search results page
if (!(new commerceCloud.ProductSearch().ProductTechnicalSpecs('215490'))) {
	new commerceCloud.utils().takeScreenShot()

	KeywordUtil.markFailedAndStop('Can not Type product SKU 297316 in the search field')
}


// The ability to view more products in search results page
if (!(new commerceCloud.ProductSearch().searchProductsUsingAttributes('WCS Air Filter'))) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not Type product name WCS Air Filter in the search field')
}

// The ability to view more products in search results page
if (!(new commerceCloud.ProductSearch().searchProductsUsingAttributes('COMMSCOPE 7700691 WCS Air Filter'))) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not Type product description COMMSCOPE 7700691 WCS Air Filter in the search field')
}

// The ability to view more products in search results page
if (!(new commerceCloud.ProductSearch().searchProductsUsingAttributes('203400'))) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not Type product # (sku) 203400 in the search field')
}

// The ability to view more products in search results page
if (!(new commerceCloud.ProductSearch().searchProductsUsingAttributes('7700691'))) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not Type Manufacturer Part Number - 7700691 in the search field')
}

// Type the following UPC number in the search field: 646444223348
if (!(new commerceCloud.ProductSearch().searchProductsUsingAttributes('646444223348'))) {
	new commerceCloud.utils().takeScreenShot()

	KeywordUtil.markFailedAndStop('Can not Type the following UPC number in the search field: 646444223348')
}

// The ability to view more products in search results page
if (!(new commerceCloud.ProductSearch().searchTesscoProduct('xvxvxvxvx'))) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not Type Manufacturer Part Number - xvxvxvxvx in the search field')
}

// logout the application
if (!(new commerceCloud.user().logout())) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not logout the commerceCloud')
}