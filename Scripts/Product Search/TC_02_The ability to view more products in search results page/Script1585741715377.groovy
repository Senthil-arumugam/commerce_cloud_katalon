import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

// launch browser and log in
if (!(new commerceCloud.utils().newSession())) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not launch or login the application')
}

// The ability to view more products in search results page
if (!(new commerceCloud.ProductSearch().viewMoreProductsInSearch('connector'))) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not The ability to view more products in search results page')
}

// logout the application
if (!(new commerceCloud.user().logout())) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not logout the commerceCloud')
}