import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

if (!(new commerceCloud.utils().newSession())) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not launch or login the application')
}

if (!(new commerceCloud.ProductSearch().Footer())) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Cannot see social website like Twitter or Youtube or Linked In')
}

if (!(new commerceCloud.ProductSearch().NavigationBar())) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('cannot navigation bar shows the following links Products, Solutions, Services, Industries, Resources, Suppliers, Tools, Sign In or Register ')
}

if (!(new commerceCloud.ProductSearch().LatestNews())) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Cannot see 3 News components are displayed below featured products')
}

if (!(new commerceCloud.user().logout())) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not logout the commerceCloud')
}