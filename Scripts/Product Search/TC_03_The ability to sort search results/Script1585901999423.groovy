import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

// launch browser and log in
if (!(new commerceCloud.utils().newSession())) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not launch or login the application')
}

// The ability to sort search results in Price (asc) order
if (!(new commerceCloud.ProductSearch().sortSearchResults('Test', 'Price (asc)', 'Number', 'asc'))) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not The ability to sort search results in Price (asc)')
}

// The ability to sort search results in Price (asc) order
if (!(new commerceCloud.ProductSearch().sortSearchResults('Test', 'Price (desc)', 'Number', 'desc'))) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not The ability to sort search results in Price (desc)')
}

// logout the application
if (!(new commerceCloud.user().logout())) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not logout the commerceCloud')
}