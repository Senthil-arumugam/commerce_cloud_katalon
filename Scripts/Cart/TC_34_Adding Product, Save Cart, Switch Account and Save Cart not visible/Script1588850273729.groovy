import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

import internal.GlobalVariable as GlobalVariable

// launch browser and log in
if (!(new commerceCloud.utils().newSession())) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not launch or login the application')
}

// UI: Remove saved cart confirmation modal
if (!(new commerceCloud.Cart().SaveCartSwitchAccountAndCartNotVisible(GlobalVariable.strAvailableSku2, '', GlobalVariable.SwitchAccountBefore, GlobalVariable.SwitchAccountAfter))) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not Add Product, save it the cart, Switch Account,  cart saved for the previous account is not visible')
}

// logout the application
if (!(new commerceCloud.user().logout())) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not logout the commerceCloud')
}