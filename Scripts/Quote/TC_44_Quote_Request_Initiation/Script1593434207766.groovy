import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

import internal.GlobalVariable

// launch browser and log in
if (!(new commerceCloud.utils().newSession())) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Cannot launch or login the commerce Cloud application')
}

// UI: Remove saved cart confirmation modal
if (!(new commerceCloud.Cart().QuoteRequestInitiation(GlobalVariable.strAvailableSku1))) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Cannot Quote Request Initiation')
}

// logout the application
if (!(new commerceCloud.user().logout())) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Cannot logout the commerce Cloud application')
}