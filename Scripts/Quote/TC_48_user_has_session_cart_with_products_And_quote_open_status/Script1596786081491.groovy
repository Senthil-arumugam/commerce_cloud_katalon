import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

import internal.GlobalVariable

// launch browser and log in
if (!(new commerceCloud.utils().newSession())) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Cannot launch or login the commerce Cloud application')
}

// UI: Remove saved cart confirmation modal
if (!(new commerceCloud.Cart().sessionCartWithProductsAndQuoteOpen(GlobalVariable.strAvailableSku1))) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Cannot Verify quotation cart of the user is removed when user discards the quotation checkout and users has a session cart (cart with products)')
}

// logout the application
if (!(new commerceCloud.user().logout())) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Cannot logout the commerce Cloud application')
}