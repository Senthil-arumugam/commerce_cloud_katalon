import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import internal.GlobalVariable as GlobalVariable

// launch browser and log in
if (!(new commerceCloud.utils().newSession())) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not launch or login the application')
}

if (!(new commerceCloud.ProductSearch().AddingProductToCart(GlobalVariable.strRelate, 'No'))) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not Verify there is no Related Products section')
}

if (!(new commerceCloud.ProductSearch().AddingProductToCart(GlobalVariable.strRelateNo, 'No'))) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not Verify there is no Related Products section')
}

if (!(new commerceCloud.ProductSearch().AddingProductToCart(GlobalVariable.strRelateYes, 'Yes'))) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not Verify there is Related Products section')
}

// logout the application
if (!(new commerceCloud.user().logout())) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not logout the commerceCloud')
}