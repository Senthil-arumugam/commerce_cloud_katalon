import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

// launch browser and log in
if (!(new commerceCloud.utils().newSession())) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not launch or login the application')
}

// The ability to view more products in search results page
if (!(new commerceCloud.ProductSearch().ContnueShopping())) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not confirmation modal showing the product image Manufacturer name, product description, quantity added and price')
}

// logout the application
if (!(new commerceCloud.user().logout())) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not logout the commerceCloud')
}