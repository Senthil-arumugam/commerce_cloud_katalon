import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

// launch browser
if (!(new commerceCloud.utils().openSite())) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not launch or login the application')
}

if (!(new commerceCloud.Cart().RegistrationValidation('', 'ExecutiveManagement'))) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Verify the Registration Landing Page')
}

// logout the application
if (!(new commerceCloud.utils().closeSite())) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Cannot close the application')
}