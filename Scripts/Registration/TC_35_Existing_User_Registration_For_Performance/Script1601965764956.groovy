import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import com.kms.katalon.core.testdata.ExcelData as ExcelData
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

ExcelData data = findTestData('User/Register')

for (def index : (0..data.getRowNumbers() - 1)) {
    String strAccountNumber = data.internallyGetValue('Account Number', index)

    String strFirstName = data.internallyGetValue('First Name', index)

    String strLastName = data.internallyGetValue('Last Name', index)

    String strEmail = data.internallyGetValue('Email', index)

    String strUsername = data.internallyGetValue('Username', index)

    // launch browser
    if (!(new commerceCloud.utils().openSite())) {
        new commerceCloud.utils().takeScreenShot()

        KeywordUtil.markFailed('Can not launch or login the application')
    }
    
    if (!(new commerceCloud.Cart().Registration(strAccountNumber, strFirstName, strLastName, strEmail, strUsername, 'ExecutiveManagement'))) {
        new commerceCloud.utils().takeScreenShot()

        KeywordUtil.markFailed('Verify the Registration Landing Page : ' + strEmail)
    }
    
    // logout the application
    if (!(new commerceCloud.user().logout())) {
        new commerceCloud.utils().takeScreenShot()

        KeywordUtil.markFailed('Can not logout the commerceCloud')
    }
}