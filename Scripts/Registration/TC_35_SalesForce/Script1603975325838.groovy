import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import com.kms.katalon.core.testdata.ExcelData as ExcelData
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

ExcelData data = findTestData('User/BackOffice')

//Precondition : Need to disable popup blocker
if (!(new commerceCloud.Cart().salesForceLogin())) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailed('Cannot login the sales Force application')
}

for (def index : (0..data.getRowNumbers() - 1)) {
    String strSFDCId = data.internallyGetValue('SFDCId', index)

    String strAccountNo = data.internallyGetValue('AccountNo', index)

    String strUserName = data.internallyGetValue('UserName', index)

    //Precondition : Need to disable popup blocker
    if (!(new commerceCloud.Cart().salesForce(strSFDCId, strAccountNo, strUserName))) {
        new commerceCloud.utils().takeScreenShot()

        KeywordUtil.markFailed('Cannot convert lead into sales Force')
    }
}

if (!(new commerceCloud.utils().closeSite())) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailed('Cannot close the browser')
}