import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

// launch browser
if (!(new commerceCloud.utils().openSite())) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not launch or login the application')
}

/*// logout the application
if (!(new commerceCloud.user().logout())) {
	new commerceCloud.utils().takeScreenShot()

	KeywordUtil.markFailedAndStop('Can not logout the commerceCloud')
}*/
if (!(new commerceCloud.Cart().NewRegistration('USA', 'Maryland', 'Yes', 'Yes', 'NET TERMS', 'Executive Management', '', 
    'ExecutiveManagement'))) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Verify the Registration Landing Page')
}