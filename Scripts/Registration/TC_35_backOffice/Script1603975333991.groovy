import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import com.kms.katalon.core.testdata.ExcelData as ExcelData
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

ExcelData data = findTestData('User/Register')

for (def index : (0..data.getRowNumbers() - 1)) {
    String strAccountNumber = data.internallyGetValue('Account Number', index)

    String strUsername = data.internallyGetValue('Username', index)

    //Precondition : Need to disable popup blocker
    if (!(new commerceCloud.Cart().backOffice(strAccountNumber, strUsername))) {
        new commerceCloud.utils().takeScreenShot()

        KeywordUtil.markFailed('Cannot verify the back office')
    }
}