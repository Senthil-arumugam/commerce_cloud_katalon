import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

if (!(new commerceCloud.utils().openSite())) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not launch application')
}

if (!(new commerceCloud.ProductSearch().BrandProtection('brand'))) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Cannot search Brand Protection Store front products without Login')
}

if (!(new commerceCloud.utils().closeSite())) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not close application')
}

if (!(new commerceCloud.utils().newSession())) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not launch or login the application')
}

if (!(new commerceCloud.ProductSearch().BrandProtection('brand'))) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Cannot search Brand Protection Store front products with Login')
}

if (!(new commerceCloud.user().logout())) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not logout the commerceCloud')
}