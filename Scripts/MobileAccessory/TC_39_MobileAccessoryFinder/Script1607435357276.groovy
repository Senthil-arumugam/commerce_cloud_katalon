import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

// launch browser and log in
if (!(new commerceCloud.utils().newSession())) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not launch or login the application')
}

if (!(new commerceCloud.ProductSearch().MobileAccessoryFinder('Apple', 'iPhone 7'))) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not Mobile Accessory Finder [iPhone 7] - Brand model page')
}

if (!(new commerceCloud.ProductSearch().MobileAccessoryFinder('Apple', 'Airpods Pro'))) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not Mobile Accessory Finder [Airpods Pro] - Brand model page')
}

if (!(new commerceCloud.ProductSearch().MobileAccessoryFinder('Apple', 'Macbook 15'))) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not Mobile Accessory Finder [Macbook 15] - Brand model page')
}

// logout the application
if (!(new commerceCloud.user().logout())) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not logout the commerceCloud')
}