import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

// launch browser and log in
if (!(new commerceCloud.utils().newSession())) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not launch or login the application')
}

if (!(new commerceCloud.ProductSearch().MobileAccessorySearch('Mobile Accessories', 'Airpods'))) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not search Mobile Accessories in the brand model page')
}

// logout the application
if (!(new commerceCloud.user().logout())) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not logout the commerceCloud')
}