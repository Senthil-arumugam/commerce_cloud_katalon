import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

import internal.GlobalVariable

// launch browser and log in
if (!(new commerceCloud.utils().newSession())) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not launch or login the application')
}

// UI: Remove saved cart confirmation modal
if (!(new commerceCloud.Cart().DeliveryOptionLiftGateAndInsideDelivery(GlobalVariable.strCableSku1))) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not validate the Delivery Option Lift Gate and Inside Delivery price')
}

// logout the application
if (!(new commerceCloud.user().logout())) {
    new commerceCloud.utils().takeScreenShot()

    KeywordUtil.markFailedAndStop('Can not logout the commerceCloud')
}