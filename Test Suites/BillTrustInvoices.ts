<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>BillTrustInvoices</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>6020a61a-34aa-4822-ac3e-c1d65a0c89a9</testSuiteGuid>
   <testCaseLink>
      <guid>b6954859-d548-40a9-ac08-cdc2a0f24f7b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BillTrustInvoices/TC_51_Verify Financial Contact group users have permission to view bill trust</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>542739fd-f4b9-409e-8593-35b84db9255f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BillTrustInvoices/TC_52_Verify CC Buyer group users have permission to view bill trust</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0f632214-bc99-42d7-a32f-af4ab4d12db9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BillTrustInvoices/TC_53_Verify Account Purchase group users have permission to view bill trust</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
