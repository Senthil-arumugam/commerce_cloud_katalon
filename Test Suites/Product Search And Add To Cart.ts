<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Product Search And Add To Cart</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>5ad5a514-cb7f-41d8-a540-89275bc6f35e</testSuiteGuid>
   <testCaseLink>
      <guid>1fb893e8-295a-4b79-8792-e62345ae1e00</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Registration/TC_35_Existing_Registration_Landing_Page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c4100118-1dc3-401c-aff3-8edfda0f3440</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Registration/TC_42_New_Registration_Landing_Page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ca6e8f76-dacd-4d9f-bf68-0b1c3b7ede6b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Registration/TC_37_Registration_Landing_Page_Validation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e4df8347-a533-4532-a9a3-afa905af41dd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Product Search/TC_01_The ability to search products using product attributes</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d598f2ca-f42e-465a-8633-dd6fdda3498b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Product Search/TC_02_The ability to view more products in search results page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6266bef0-8d05-413a-ab96-d445933da7bb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Product Search/TC_03_The ability to sort search results</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f50c21f5-f296-4693-bab9-53790eba734e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Product Search/TC_06_Pagination for search results</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e24d64f8-9325-4b74-9d43-6407938a6a33</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Product Search/TC_07_Breadcrumbs in Category Results</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>085400c2-a675-4255-b327-0b22a95af234</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Product Search/TC_09_Search Type Ahead Suggestion</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9642fc70-786a-4556-8a48-ddb9ba472369</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Product Search/TC_41_Homepage</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e30f1042-b3cb-4787-977b-68b79a7458d6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/MobileAccessory/TC_39_MobileAccessoryFinder</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bf2ff9c8-f36e-427a-80f9-1b4946d54d97</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/MobileAccessory/TC_40_Brand_Protection_Storefront</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>316f5b36-a001-4616-9351-f79c376898c2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/MobileAccessory/TC_43_MobileAccessorySearch</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>284d6541-f450-42fd-902f-ab91eea6ab9c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AddToCart/TC_10_Adding product cart from Product List</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>802c9087-be88-4656-b9d6-89597df05c43</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AddToCart/TC_11_Adding product cart from Product Details List</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>674a2cdb-6326-4006-a3ff-9e7edddbe38b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AddToCart/TC_12_Adding a product to the cart from search</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>523166e1-f2b0-4aa6-bc2b-ec33a45ae175</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AddToCart/TC_13_Contnue Shopping and Validate product image</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f235529b-6b4e-458d-ad53-0e0bd89e9409</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AddToCart/TC_14_Clear Cart With Validation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3a3f57e8-8f9e-425d-9ea6-27f71ca4e243</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AddToCart/TC_14_Clear Cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>06f804cb-78ec-47fa-b583-b05b067d42ee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AddToCart/TC_31_Quick Order Validation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>baf206d5-6032-47fc-a93e-4a309427b598</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AddToCart/TC_31_Quick Order</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>94869dc8-037e-48d0-a2c8-e27b52fbe49d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AddToCart/TC_33_Verify Related Products and Cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>12262b34-c179-443f-8804-8f1a298801bb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AddToCart/TC_14_Clear Cart</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
