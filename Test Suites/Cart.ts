<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Cart</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>6ef946eb-9570-4681-a45c-88645aa84e0a</testSuiteGuid>
   <testCaseLink>
      <guid>4a57447c-470b-4526-afeb-fbb56a342887</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cart/TC_28_Remove All Saved Carts</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3ab46c1d-50d0-4578-99fa-dc0d4997743f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cart/TC_20_Navigate to New Cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d58d2c4d-d39b-4f41-9853-1ffd8965aa3b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cart/TC_15_Cart details page - Order Summary</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>16a7b07b-7ead-4617-b7df-dba4ba615a28</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cart/TC_16_Cart details page - empty cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ef975213-1a1e-44c1-a6f0-3a45ef7745b1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cart/TC_17_Adding Cart From Product Search, Save Cart, Validate cart Present</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5386b752-9fbf-44fd-b25b-736fff85a916</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cart/TC_18_Navigate to Saved Carts Quotes and Restore</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>771acc0a-0d63-4ad7-bf1c-3479014dd97a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cart/TC_19_Navigate to Saved Carts Quotes and Remove</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b47a4139-2f41-4ce8-b451-d31ab251ef3b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cart/TC_20_Navigate to New Cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e2b837eb-d19e-4abf-b701-9f9eeb7addad</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cart/TC_21_Import CSV and Verify product and quantity</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>af446d38-17c0-4fd4-ab05-b1783fdf35c6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cart/TC_22_The ability to export my cart as a downloadable CSV file</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4d37785b-4225-4c18-99e8-bf1adf3b11bd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cart/TC_23_Adding Several Cart From Product Search, Save Cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2720e56b-e520-4dec-8151-adf820bd8cf2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cart/TC_20_Navigate to New Cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bc10a508-0b68-455c-aca3-7e644e6cefd2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cart/TC_28_Remove All Saved Carts</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ac97251d-fbbf-4c5f-8d5b-c7024c2d59c1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cart/TC_24_The ability to save my current cart for future use so I can create a new blank cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1febd033-2c98-4d2b-a7f3-77d44a2a1057</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cart/TC_23_Adding Several Cart From Product Search, Save Cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>44aa0344-8dfa-40ed-b5d2-524352110aec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cart/TC_25_Review my saved carts and restore the saved cart to active cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1805b43d-f91d-487a-812f-a9aa51feab61</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cart/TC_26_The ability to permanently save a cart has been converted into an order</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3118c2ee-f8a0-4050-9368-8037f4cedce5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cart/TC_23_Adding Several Cart From Product Search, Save Cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0b18cef9-ebf4-40ff-b5cd-35add2c25a84</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cart/TC_27_The ability to edit a saved cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c3948d03-dc13-4150-9bc8-da8900e129ff</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cart/TC_28_Remove All Saved Carts</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6932f4ec-bf30-4e4d-acd9-7016f1242631</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cart/TC_47_Adding Cable Product to Cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>277b4e5f-5506-4a23-8e3c-56b565c3cbf2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AddToCart/TC_14_Clear Cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a5a18e26-5198-4184-b623-6c63adf6de68</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Quote/TC_44_Quote_Request_Initiation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a60f4044-1c3b-45af-881b-4b7a43826919</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cart/TC_20_Navigate to New Cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>91f59454-bec2-4477-b81f-5a2d46201b57</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Quote/TC_48_user_has_session_cart_with_products_And_quote_open_status</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eb328156-4c46-4d8c-98cb-85d0c59e3c54</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/AddToCart/TC_14_Clear Cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ab593d3a-e5cb-4346-ac83-47bd35d968bc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Quote/TC_49_user_has_no_session_cart_without_any_products_and_quote_open_status</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a423ce32-55ab-44c8-92af-b35bbee5a9e4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BillingNameChange/TC_54_BillingNameChangeUserInfoValidation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c6371765-892c-4a59-8a80-eac27c2c7f47</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BillingNameChange/TC_56_BillingNameChangeCompanyInfoValidation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1bac7f75-4b0a-45b9-8eff-f80e49ce9a84</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/BillingNameChange/TC_55_BillingNameChange</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
