<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Cart Switch Account</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>5a9e0c6f-5585-483a-bc62-56c4ed996cdb</testSuiteGuid>
   <testCaseLink>
      <guid>9301eb7f-14fa-46f4-962a-f9b9e025dc09</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cart/TC_28_Remove All Saved Carts</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>110b0efd-74d9-4550-92e7-5230590369ab</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cart/TC_34_Adding Product, Save Cart, Switch Account and Save Cart not visible</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dcd8d862-181f-4788-868b-3fc519c3dbe0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Cart/TC_28_Remove All Saved Carts</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
