<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>New User List</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>33ce90ad-c76d-4fd4-9d9b-6e2b11458d4f</testSuiteGuid>
   <testCaseLink>
      <guid>b821723c-71cf-4ee1-bb59-cda6211cb7c5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Registration/TC_35_Existing_User_Registration_For_Performance</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>52d221a9-b75d-42c0-8e8c-22436c36253a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Registration/TC_35_backOffice</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>04005326-c6fb-4bf0-8e68-fb4d6cdb77ee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Registration/TC_35_SalesForce</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
