<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Checkout</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>5558efe7-7ec2-42b1-beda-4b247fa51a75</testSuiteGuid>
   <testCaseLink>
      <guid>02b48287-d547-48d1-8c43-e3d926759172</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Checkout/TC_29_Validate Shipping And Payment Page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a15afae1-d916-433b-a218-3dd1202ed1a6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Checkout/TC_30_Place Order</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>89809b06-3424-4a71-bd81-c22ed969686f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Checkout/TC_38_OrderHistory</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b98cd2bc-c890-4c6a-a879-68da915e622c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Checkout/TC_38_OrderHistorySorting</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6ad94ea4-a06b-43b5-8007-df3ab168adda</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Checkout/TC_36_Storefront_ChatNow</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>23537e83-8029-4160-98b4-d38f3e9895bc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Checkout/TC_45_GSAOrderHistory</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1d9258df-5857-484a-b344-1dc48302ce95</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Checkout/TC_46_Place_Order_Credit_Card</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6cc4a9a0-5bbf-49d2-84c9-e7206e7b94ec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Checkout/TC_50_Adding Cable Product and Delivery Option Validation</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
