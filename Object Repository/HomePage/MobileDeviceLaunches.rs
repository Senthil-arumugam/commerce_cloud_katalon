<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>MobileDeviceLaunches</name>
   <tag></tag>
   <elementGuidId>be7f5701-c73f-478a-a912-128c0d2f95a0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Mobile Device Launches')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[contains(text(),'Mobile Device Launches')]</value>
   </webElementXpaths>
</WebElementEntity>
