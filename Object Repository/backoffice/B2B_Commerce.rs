<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>B2B_Commerce</name>
   <tag></tag>
   <elementGuidId>c1b6cd08-fec5-4fd6-8c08-a825a8bdefa9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[contains(text(),'B2B Commerce')]/preceding::span[contains(@class,&quot;z-vfiletree-icon&quot;)][1]</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//span[contains(text(),'B2B Commerce')]/preceding::span[contains(@class,&quot;z-vfiletree-icon&quot;)][1]</value>
   </webElementXpaths>
</WebElementEntity>
