<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AddTemporaryShippingAddressHeader</name>
   <tag></tag>
   <elementGuidId>e8041e9b-c75d-4d35-97c1-0cca43d2c47d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[contains(text(),'Add Temporary Shipping Address')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//*[contains(text(),'Add Temporary Shipping Address')]</value>
   </webElementXpaths>
</WebElementEntity>
