<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ThankYouForLiveSupport</name>
   <tag></tag>
   <elementGuidId>1a319f19-f3ee-4ed3-abba-5067d463d78c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[contains(text(),'Thank you for contacting TESSCO Live Support')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//span[contains(text(),'Thank you for contacting TESSCO Live Support')]</value>
   </webElementXpaths>
</WebElementEntity>
