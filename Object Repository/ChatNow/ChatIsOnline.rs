<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ChatIsOnline</name>
   <tag></tag>
   <elementGuidId>2dc690dc-b1e3-45ee-8cbc-05cb24bf235f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//img[@id='liveagent_button_online_573a0000000TPay']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//img[@id='liveagent_button_online_573a0000000TPay']</value>
   </webElementXpaths>
</WebElementEntity>
