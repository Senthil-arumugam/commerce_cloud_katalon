<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>YouHaveEndedTheChat</name>
   <tag></tag>
   <elementGuidId>705106c2-5f24-40b8-8c9e-b576bd979174</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//em[contains(text(),'ve ended the chat')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//em[contains(text(),'ve ended the chat')]</value>
   </webElementXpaths>
</WebElementEntity>
