<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>HomepageNewsSection_1</name>
   <tag></tag>
   <elementGuidId>54ad577e-39dc-44bb-aaa2-f1d3861332f1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(@class,'newsBanner')]//img[@title='Homepage News Section 1']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[contains(@class,'newsBanner')]//img[@title='Homepage News Section 1']</value>
   </webElementXpaths>
</WebElementEntity>
