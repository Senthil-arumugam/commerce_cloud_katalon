<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>savedCartName</name>
   <tag></tag>
   <elementGuidId>5b30ab7e-81b8-406f-b9a9-da5be583882f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//p[@class='cart-saved-heading js-saved-cart-name']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//p[@class='cart-saved-heading js-saved-cart-name']</value>
   </webElementXpaths>
</WebElementEntity>
