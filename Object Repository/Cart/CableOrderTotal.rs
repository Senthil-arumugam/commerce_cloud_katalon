<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CableOrderTotal</name>
   <tag></tag>
   <elementGuidId>372d889c-a0f3-424d-b67d-02f465792d5f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(text(),'Order Total')]/following::div[@class='data-loader cartTotal']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[contains(text(),'Order Total')]/following::div[@class='data-loader cartTotal']</value>
   </webElementXpaths>
</WebElementEntity>
