<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>savedCartId</name>
   <tag></tag>
   <elementGuidId>92d7b27a-a108-4d69-a9c9-e5d7c36580e1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[@class='col-xs-12 js-saved-cart-id cart-saved-id']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//span[@class='col-xs-12 js-saved-cart-id cart-saved-id']</value>
   </webElementXpaths>
</WebElementEntity>
