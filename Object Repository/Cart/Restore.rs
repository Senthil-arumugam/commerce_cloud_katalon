<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Restore</name>
   <tag></tag>
   <elementGuidId>fa11d37d-ccb5-413a-9d8b-1bc064c99fcb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='cboxLoadedContent']//button[contains(@class,'js-save-cart-restore-btn restore-button')][contains(text(),'Restore')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='cboxLoadedContent']//button[contains(@class,'js-save-cart-restore-btn restore-button')][contains(text(),'Restore')]</value>
   </webElementXpaths>
</WebElementEntity>
