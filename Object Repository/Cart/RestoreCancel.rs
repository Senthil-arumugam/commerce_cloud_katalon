<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>RestoreCancel</name>
   <tag></tag>
   <elementGuidId>b63023ef-11f2-4fff-989a-4387806268d8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='cboxLoadedContent']//button[contains(@class,'js-cancel-restore-btn cancel-button')][contains(text(),'Cancel')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='cboxLoadedContent']//button[contains(@class,'js-cancel-restore-btn cancel-button')][contains(text(),'Cancel')]</value>
   </webElementXpaths>
</WebElementEntity>
