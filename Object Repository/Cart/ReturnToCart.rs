<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ReturnToCart</name>
   <tag></tag>
   <elementGuidId>267cc4ad-0247-40de-8997-6f6100259e47</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(.,'Secure Checkout')]/following::div[normalize-space()='Return to Cart'][2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[contains(.,'Secure Checkout')]/following::div[normalize-space()='Return to Cart'][2]</value>
   </webElementXpaths>
</WebElementEntity>
