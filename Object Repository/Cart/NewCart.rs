<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>NewCart</name>
   <tag></tag>
   <elementGuidId>2e06fac9-f741-4b8f-b3ab-822d497d441b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(@class,'save__cart--link cart__head--link')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[contains(@class,'save__cart--link cart__head--link')]</value>
   </webElementXpaths>
</WebElementEntity>
