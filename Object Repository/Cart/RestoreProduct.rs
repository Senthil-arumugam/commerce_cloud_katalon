<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>RestoreProduct</name>
   <tag></tag>
   <elementGuidId>f3a0d80b-6298-4da1-8e47-82b43c744f55</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='cboxLoadedContent']//button[contains(@class,'js-cancel-restore-btn cancel-button')][contains(text(),'Cancel')]/preceding::b[3]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='cboxLoadedContent']//button[contains(@class,'js-cancel-restore-btn cancel-button')][contains(text(),'Cancel')]/preceding::b[3]</value>
   </webElementXpaths>
</WebElementEntity>
