<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>NameList</name>
   <tag></tag>
   <elementGuidId>20650c1f-c0af-481c-aa02-e797ebba919f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a/span[text()='Name'][1]/following::a[@data-refid='recordId'][1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//input[@name = 'password' and @placeholder = 'Password' and @type = 'password']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a/span[text()='Name'][1]/following::a[@data-refid='recordId'][1]</value>
   </webElementXpaths>
</WebElementEntity>
