<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Lead</name>
   <tag></tag>
   <elementGuidId>b6098c7e-a2d2-467b-83ea-d4ae9e75a04a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a/span[1]/span[contains(text(),'Lead')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//input[@name = 'password' and @placeholder = 'Password' and @type = 'password']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a/span[1]/span[contains(text(),'Lead')]</value>
   </webElementXpaths>
</WebElementEntity>
