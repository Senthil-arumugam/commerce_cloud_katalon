<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AccountNameLookUp</name>
   <tag></tag>
   <elementGuidId>074971ca-6b12-4725-8781-914778d7ba67</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//td[contains(text(),'Account Name')]/following::a[1]/img[@alt]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//input[@name = 'password' and @placeholder = 'Password' and @type = 'password']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//td[contains(text(),'Account Name')]/following::a[1]/img[@alt]</value>
   </webElementXpaths>
</WebElementEntity>
