<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LeadDueDate</name>
   <tag></tag>
   <elementGuidId>23c55db0-ac16-446b-9c66-d77801ddb1fc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[starts-with(text(),'Due Date')]/following::input[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//input[@name = 'password' and @placeholder = 'Password' and @type = 'password']</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//*[starts-with(text(),'Due Date')]/following::input[1]</value>
   </webElementXpaths>
</WebElementEntity>
