<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LeadSubject</name>
   <tag></tag>
   <elementGuidId>7aedea97-745b-4973-bbd4-82a4b33a4aa3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[starts-with(text(),'Subject')]/following::input[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//input[@name = 'password' and @placeholder = 'Password' and @type = 'password']</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//*[starts-with(text(),'Subject')]/following::input[1]</value>
   </webElementXpaths>
</WebElementEntity>
