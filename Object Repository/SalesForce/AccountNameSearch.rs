<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AccountNameSearch</name>
   <tag></tag>
   <elementGuidId>6327af63-7ed6-409c-adbc-e31d53c33179</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Account Name')]/following::th[@scope='row']/a</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//input[@name = 'password' and @placeholder = 'Password' and @type = 'password']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[contains(text(),'Account Name')]/following::th[@scope='row']/a</value>
   </webElementXpaths>
</WebElementEntity>
