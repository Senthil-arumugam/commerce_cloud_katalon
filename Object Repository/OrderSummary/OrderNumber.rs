<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>OrderNumber</name>
   <tag></tag>
   <elementGuidId>84d3e39d-fb0a-4f48-af04-305b8eda5bf1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[contains(text(),'Thank you')]/following::b[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//span[contains(text(),'Thank you')]/following::b[1]</value>
   </webElementXpaths>
</WebElementEntity>
