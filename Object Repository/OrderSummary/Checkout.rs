<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Checkout</name>
   <tag></tag>
   <elementGuidId>81c0e3c1-2947-4bcb-bacf-1050028681c2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(@class,'row col')]//button[contains(text(),'Checkout')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[contains(@class,'row col')]//button[contains(text(),'Checkout')]</value>
   </webElementXpaths>
</WebElementEntity>
