<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ContinueToPayment</name>
   <tag></tag>
   <elementGuidId>bc37a8f8-e982-4fe8-a0e5-51d6c6e436e0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='chooseTcomShipping_continue_button']</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='chooseTcomShipping_continue_button']</value>
   </webElementXpaths>
</WebElementEntity>
