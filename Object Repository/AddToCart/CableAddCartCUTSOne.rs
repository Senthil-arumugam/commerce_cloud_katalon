<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CableAddCartCUTSOne</name>
   <tag></tag>
   <elementGuidId>93a18f98-7099-4c42-8458-1c2dfc7da11d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[normalize-space()='Number of cuts:']/following::span[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//span[normalize-space()='Number of cuts:']/following::span[1]</value>
   </webElementXpaths>
</WebElementEntity>
