<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CableAddCartSERVICEREQUESTEDOne</name>
   <tag></tag>
   <elementGuidId>e69ee997-c12a-4aa3-ae26-1761908d108d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[contains(text(),'SERVICE REQUESTED:')]/following::span[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//span[contains(text(),'SERVICE REQUESTED:')]/following::span[1]</value>
   </webElementXpaths>
</WebElementEntity>
