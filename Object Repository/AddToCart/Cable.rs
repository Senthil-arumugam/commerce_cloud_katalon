<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Cable</name>
   <tag></tag>
   <elementGuidId>03021df9-27b6-42b7-b059-ac874027ed05</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//th[normalize-space()='Reel Length'] | //span[contains(text(),'QTY')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//th[normalize-space()='Reel Length'] | //span[contains(text(),'QTY')]</value>
   </webElementXpaths>
</WebElementEntity>
