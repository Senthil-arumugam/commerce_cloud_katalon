<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CableSERVICEREQUESTEDOne</name>
   <tag></tag>
   <elementGuidId>af1ca0c8-dbea-4b75-902e-b2f98fc49d00</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//th[contains(text(),'Total')]/following::span[@class='total']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//th[contains(text(),'Total')]/following::span[@class='total']</value>
   </webElementXpaths>
</WebElementEntity>
