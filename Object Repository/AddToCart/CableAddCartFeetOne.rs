<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CableAddCartFeetOne</name>
   <tag></tag>
   <elementGuidId>ad68413b-daa9-4d73-b73d-8a39d95ade99</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//span[normalize-space()='Reel Length:']/following::span[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//span[normalize-space()='Reel Length:']/following::span[1]</value>
   </webElementXpaths>
</WebElementEntity>
