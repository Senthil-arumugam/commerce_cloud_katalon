<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>NewDba</name>
   <tag></tag>
   <elementGuidId>ca3c68f0-c254-4d5b-956f-a2aea35263d1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[normalize-space()='New dba (if applicable)']/following::input[@id='dbaNew']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//label[normalize-space()='New dba (if applicable)']/following::input[@id='dbaNew']</value>
   </webElementXpaths>
</WebElementEntity>
