<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>PleaseEnterValidPhone</name>
   <tag></tag>
   <elementGuidId>3ce2ddee-b1be-4896-9303-0110a10ffa38</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='phone']/following::span[contains(normalize-space(),'Please enter valid phone')][1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='phone']/following::span[contains(normalize-space(),'Please enter valid phone')][1]</value>
   </webElementXpaths>
</WebElementEntity>
