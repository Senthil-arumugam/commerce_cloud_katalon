<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ChangeBillingNameTitle</name>
   <tag></tag>
   <elementGuidId>934599c2-2106-4d60-9b07-2cdb46927ab1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[normalize-space()='Billing Name Change']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[normalize-space()='Billing Name Change']</value>
   </webElementXpaths>
</WebElementEntity>
