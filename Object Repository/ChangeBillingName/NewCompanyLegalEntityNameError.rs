<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>NewCompanyLegalEntityNameError</name>
   <tag></tag>
   <elementGuidId>d9b046de-bc30-43d3-857b-47de635fc3ba</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[normalize-space()='New Company Legal Entity Name']/following::div[contains(.,'Please enter a valid company legal entity name which should be less than 256 characters.')][1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//label[normalize-space()='New Company Legal Entity Name']/following::div[contains(.,'Please enter a valid company legal entity name which should be less than 256 characters.')][1]</value>
   </webElementXpaths>
</WebElementEntity>
