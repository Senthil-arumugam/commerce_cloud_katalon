<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EmailAddress</name>
   <tag></tag>
   <elementGuidId>6094323a-2d43-4bb8-b008-59dbb6d28487</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[normalize-space()='Email Address']/following::input[@id='email']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//label[normalize-space()='Email Address']/following::input[@id='email']</value>
   </webElementXpaths>
</WebElementEntity>
