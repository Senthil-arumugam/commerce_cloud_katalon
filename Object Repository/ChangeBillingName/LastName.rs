<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LastName</name>
   <tag></tag>
   <elementGuidId>a1e89918-b076-4fb0-84e3-f7ebe877a4b8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[normalize-space()='Last Name']/following::input[@id='lastName']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//label[normalize-space()='Last Name']/following::input[@id='lastName']</value>
   </webElementXpaths>
</WebElementEntity>
