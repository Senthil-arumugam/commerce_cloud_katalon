<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>NewCompanyLegalEntityName</name>
   <tag></tag>
   <elementGuidId>b8b99ad6-7150-4914-8f48-73423bed1ba1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[normalize-space()='New Company Legal Entity Name']/following::input[@id='companyNameNew']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//label[normalize-space()='New Company Legal Entity Name']/following::input[@id='companyNameNew']</value>
   </webElementXpaths>
</WebElementEntity>
