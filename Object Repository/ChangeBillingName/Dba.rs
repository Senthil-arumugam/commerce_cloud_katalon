<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Dba</name>
   <tag></tag>
   <elementGuidId>419b4230-6fb1-4112-a2cb-2c5dbb8ca821</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[normalize-space()='dba (if applicable)']/following::input[@id='companyNameOldDba']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//label[normalize-space()='dba (if applicable)']/following::input[@id='companyNameOldDba']</value>
   </webElementXpaths>
</WebElementEntity>
