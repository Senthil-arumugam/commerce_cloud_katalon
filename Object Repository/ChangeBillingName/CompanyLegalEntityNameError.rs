<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CompanyLegalEntityNameError</name>
   <tag></tag>
   <elementGuidId>3917ce24-5be3-4a9b-9e6a-900e712a954f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[normalize-space()='Company Legal Entity Name']/following::div[contains(.,'Please enter a valid company legal entity name which should be less than 256 characters.')][1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//label[normalize-space()='Company Legal Entity Name']/following::div[contains(.,'Please enter a valid company legal entity name which should be less than 256 characters.')][1]</value>
   </webElementXpaths>
</WebElementEntity>
