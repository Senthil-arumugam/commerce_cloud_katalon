<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>TesscoAccountNumber</name>
   <tag></tag>
   <elementGuidId>e867ba2a-da68-4855-9d3e-9b6c8c38e130</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[normalize-space()='Tessco Account Number']/following::input[@id='accountNumber']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//label[normalize-space()='Tessco Account Number']/following::input[@id='accountNumber']</value>
   </webElementXpaths>
</WebElementEntity>
