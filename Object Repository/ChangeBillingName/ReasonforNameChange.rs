<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ReasonforNameChange</name>
   <tag></tag>
   <elementGuidId>90cb2d73-ec18-4a7c-8ee8-179b7b8e60e3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[@class='control-label required'][normalize-space()='Reason for Name Change']/following::span[@class='fas fa-angle-down']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//label[@class='control-label required'][normalize-space()='Reason for Name Change']/following::span[@class='fas fa-angle-down']</value>
   </webElementXpaths>
</WebElementEntity>
