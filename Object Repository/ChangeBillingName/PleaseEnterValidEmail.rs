<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>PleaseEnterValidEmail</name>
   <tag></tag>
   <elementGuidId>a33bb810-cf87-4461-bd37-b2e2a9795aa4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[normalize-space()='Please enter a valid email']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//span[normalize-space()='Please enter a valid email']</value>
   </webElementXpaths>
</WebElementEntity>
