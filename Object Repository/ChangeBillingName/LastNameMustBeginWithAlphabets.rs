<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LastNameMustBeginWithAlphabets</name>
   <tag></tag>
   <elementGuidId>3e46a8e8-59f0-4b38-9356-f338299a96ac</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[normalize-space()='Last Name must begin with alphabets']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//span[normalize-space()='Last Name must begin with alphabets']</value>
   </webElementXpaths>
</WebElementEntity>
