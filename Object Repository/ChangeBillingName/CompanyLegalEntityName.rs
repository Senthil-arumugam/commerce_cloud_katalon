<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CompanyLegalEntityName</name>
   <tag></tag>
   <elementGuidId>6138eccf-78f5-4631-bb67-bce0080d3e1c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[normalize-space()='Company Legal Entity Name']/following::input[@id='companyNameOld']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//label[normalize-space()='Company Legal Entity Name']/following::input[@id='companyNameOld']</value>
   </webElementXpaths>
</WebElementEntity>
