<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BackTesscoCom</name>
   <tag></tag>
   <elementGuidId>79772980-9958-40f2-9bd6-cbc1dc1a0723</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[normalize-space()='Back to Tessco.com']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[normalize-space()='Back to Tessco.com']</value>
   </webElementXpaths>
</WebElementEntity>
