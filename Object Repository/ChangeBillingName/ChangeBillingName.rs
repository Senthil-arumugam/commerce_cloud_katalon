<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>ChangeBillingName</name>
   <tag></tag>
   <elementGuidId>c06232ee-7dc5-45ca-b6ce-5eb828c24d4a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[normalize-space()='Billing Name Change']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[normalize-space()='Billing Name Change']</value>
   </webElementXpaths>
</WebElementEntity>
