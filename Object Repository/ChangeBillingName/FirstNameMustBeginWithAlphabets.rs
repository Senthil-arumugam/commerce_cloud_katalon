<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>FirstNameMustBeginWithAlphabets</name>
   <tag></tag>
   <elementGuidId>a4b9d17a-a439-483e-8041-b99570e23b49</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//span[normalize-space()='First Name must begin with alphabets']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//span[normalize-space()='First Name must begin with alphabets']</value>
   </webElementXpaths>
</WebElementEntity>
